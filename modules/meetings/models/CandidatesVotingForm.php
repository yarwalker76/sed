<?php
namespace app\modules\meetings\models;

use Yii;
use yii\base\Model;

class CandidatesVotingForm extends Model
{
    public $question_id;
    public $participant_id;
    public $participant_sum;
    public $candidate_votings;
    public $total_sum;
    public $candidates_cnt;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['candidates_cnt', 'question_id', 'participant_id', 'total_sum', 'participant_sum'], 'required'],
            [['total_sum', 'participant_sum'], 'double'],
            [['candidates_cnt', 'question_id', 'participant_id'], 'integer'],
            ['candidate_votings', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [

        ];
    }

}
