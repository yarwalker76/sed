<?php
namespace app\modules\meetings\models;

use yii\base\Model;

class RegJournalTemplate extends Model
{
    public $current_date;
    public $reg_start_time;
    public $reg_end_time;
    public $pd_name;
    public $pd_legal_address;
    public $pd_inn;
    public $pd_ogrn;
    public $meeting_address;
    public $au_full_fio;
    public $participants_list;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->current_date = $current_date = date('d.m.Y');
    }

    public function rules()
    {
        return [
            [['current_date', 'reg_start_time', 'reg_end_time', 'pd_name', 'pd_legal_address', 'pd_inn', 'pd_ogrn',
                'meeting_address', 'au_full_fio'], 'string'],
            [['participants_list'], 'safe'],
        ];
    }
}