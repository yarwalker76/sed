<?php

namespace app\modules\meetings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\meetings\models\MeetingParticipants;
use yii\db\Query;

/**
 * MeetingParticipantsSearch represents the model behind the search form about `app\modules\meetings\models\MeetingParticipants`.
 */
class MeetingParticipantsSearch extends MeetingParticipants
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'meet_id', 'registration', 'participant_status_id'], 'integer'],
            [['name', 'representative_name', 'supporting_docs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MeetingParticipants::find()
            ->where(['meet_id' => Yii::$app->session->get('current_meet_id')]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [
                    'sum_32' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'meet_id' => $this->meet_id,
            'registration' => $this->registration,
            'participant_status_id' => $this->participant_status_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'representative_name', $this->representative_name])
            ->andFilterWhere(['like', 'supporting_docs', $this->supporting_docs]);

        return $dataProvider;
    }
}
