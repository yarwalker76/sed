<?php

namespace app\modules\meetings\models;

use Yii;

/**
 * This is the model class for table "meeting_voice_calculations".
 *
 * @property integer $id
 * @property string $name
 */
class MeetingVoiceCalculations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_voice_calculations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
