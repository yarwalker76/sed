<?php

namespace app\modules\meetings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\meetings\models\MeetingQuestions;

/**
 * MeetingParticipantsSearch represents the model behind the search form about `app\modules\meetings\models\MeetingParticipants`.
 */
class MeetingQuestionsSearch extends MeetingQuestions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'meet_id'], 'integer'],
            [['question'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MeetingQuestions::find()
            ->where(['meet_id' => Yii::$app->session->get('current_meet_id')]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [
                    'id' => SORT_ASC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'meet_id' => $this->meet_id,
        ]);

        return $dataProvider;
    }
}
