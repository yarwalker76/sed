<?php

namespace app\modules\meetings\models;

use app\models\AppHelper;
use app\modules\creditors\models\Creditor;
use Yii;
use yii\helpers\ArrayHelper;
use app\modules\creditors\models\Request;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "meeting_participants".
 *
 * @property integer $id
 * @property integer $meet_id
 * @property integer $registration
 * @property string $name
 * @property string $representative_name
 * @property string $supporting_docs
 * @property integer $participant_status_id
 * @property integer $creditor_id
 *
 * @property Meetings $meet
 */
class MeetingParticipants extends \yii\db\ActiveRecord
{
    const STATUS_K_K = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_participants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meet_id', 'name'], 'required'],
            [['meet_id', 'registration', 'participant_status_id', 'creditor_id'], 'integer'],
            [['total_sum', 'sum_31', 'sum_32'], 'double'],
            [['name', 'representative_name', 'supporting_docs'], 'string', 'max' => 512],
            [['meet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meetings::className(), 'targetAttribute' => ['meet_id' => 'id']],
            /*[['creditor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Creditor::className(), 'targetAttribute' => ['creditor_id' => 'id']],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meet_id' => 'ID Собрания',
            'registration' => 'Зарегистрирован',
            'name' => 'Наименование',
            'representative_name' => 'Представитель',
            'supporting_docs' => 'Реквизиты документа подтверждения полномочий',
            'participant_status_id' => 'Статус участника',
            'total_sum' => 'Требование',
            'sum_32' => 'Требование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeet()
    {
        return $this->hasOne(Meetings::className(), ['id' => 'meet_id']);
    }

    public function getStatus()
    {
        return $this->hasOne(MeetingParticipantStatuses::className(), ['id' => 'participant_status_id']);
    }

    public function getCreditor()
    {
        return $this->hasOne(Creditor::className(), ['id' => 'creditor_id']);
    }

    public function getName()
    {
        return AppHelper::getFIO($this->creditor_id);
    }

    public function getStatuses()
    {
        $dropdowns = MeetingParticipantStatuses::find()->asArray()->all();
        return ArrayHelper::map($dropdowns, 'id', 'name');
    }

    public function getRequestByQuestion($question_id)
    {
        $question = MeetingQuestions::findOne($question_id);

        $sql = 'SELECT p.id, c.id creditor_id, c.title, cr.total_sum 
                  FROM (SELECT id, title FROM creditor WHERE procedures_id = :proc_id) AS c
                 INNER JOIN (SELECT creditor_id, SUM(total) AS total_sum 
                               FROM creditor_request
                              WHERE is_enabled = 1 AND queue_type IN (' .
                                    ($question->mortgage_creditors_voices ? ':q31, :q32' : ':q32') .')
                              GROUP BY creditor_id) AS cr 
                    ON c.id = cr.creditor_id
                 INNER JOIN (SELECT * FROM meeting_participants WHERE meet_id = :meet_id AND registration = 1 AND id = :participant_id) AS p 
                    ON p.creditor_id = c.id';

        $params = [
            ':proc_id' => Yii::$app->session->get('current_proc_id'),
            ':q32' => Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE,
            ':meet_id' => Yii::$app->session->get('current_meet_id'),
            ':participant_id' => $this->id,
        ];
        !$question->mortgage_creditors_voices || $params[':q31'] = Request::QUEUE_TYPE_THIRD_GUARANTEE;

        return (Yii::$app->db->createCommand($sql, $params)->queryOne())['total_sum'];
    }

    public static function getParticipantsProvider()
    {
        $query = (self::className())::find()
            ->where(['meet_id' => Yii::$app->session->get('current_meet_id')])
            ->andWhere(['!=', 'creditor_id', 0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public static function getParticipantsCount()
    {
        return (self::className())::find()
            ->where(['meet_id' => Yii::$app->session->get('current_meet_id')])
            ->andWhere(['!=', 'creditor_id', 0])
            ->count();
    }

    public static function getRegisteredParticipantsCount()
    {
        return (self::className())::find()
            ->where(['meet_id' => Yii::$app->session->get('current_meet_id')])
            ->andWhere(['!=', 'creditor_id', 0])
            ->andWhere(['registration' => 1])
            ->count();
    }

    public static function getRegisteredCreditors()
    {
        $creditors = (self::className())::find()
            ->where(['meet_id' => Yii::$app->session->get('current_meet_id')])
            ->andWhere(['!=', 'creditor_id', 0])
            ->andWhere(['registration' => 1])
            ->asArray()
            ->all();

        $sql = 'SELECT c.id, SUM(cr.sum_total) request_sum FROM creditor AS c
                 INNER JOIN (SELECT creditor_id, SUM(total) sum_total 
                               FROM creditor_request 
                              WHERE is_enabled = 1 AND queue_type IN (:q31, :q32)
                              GROUP BY creditor_id 
                            ) AS cr
                         ON c.id = cr.creditor_id
                 WHERE procedures_id = :proc_id
                 GROUP BY c.id';

        $params = [
            ':proc_id' => Yii::$app->session->get('current_proc_id'),
            ':q31' => Request::QUEUE_TYPE_THIRD_GUARANTEE,
            ':q32' => Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE,
        ];

        $requests = ArrayHelper::map(Yii::$app->db->createCommand($sql, $params)->queryAll(), 'id', 'request_sum');

        foreach( $creditors as &$creditor )
            $creditor['total_request_sum'] = $requests[$creditor['creditor_id']];

        return $creditors;
    }

    public static function getRegisteredParticipants()
    {
        return (self::className())::find()
            ->where([
                'meet_id' => Yii::$app->session->get('current_meet_id'),
                'creditor_id' => 0,
                'registration' => 1
            ])
            ->asArray()
            ->all();
    }

    public static function getRegisteredRequestTotalSum()
    {
        $sql_sum = 'SELECT SUM(total_sum) 
                      FROM ' . self::tableName() .
                   ' WHERE meet_id = :meet_id
                       AND creditor_id != 0
                       AND registration = 1';

        $params = [':meet_id' => Yii::$app->session->get('current_meet_id')];

        return Yii::$app->db->createCommand($sql_sum, $params)->queryScalar();
    }

    public static function getParticipantsRequestsTotalSum()
    {
        $sql_sum = 'SELECT SUM(total_sum) 
                      FROM ' . self::tableName() .
                   ' WHERE meet_id = :meet_id';

        $params = [':meet_id' => Yii::$app->session->get('current_meet_id')];

        return Yii::$app->db->createCommand($sql_sum, $params)->queryScalar();
    }



    public static function getRequestsProvider()
    {
        $query = (self::className())::find()
                    ->where([
                            'meet_id' => Yii::$app->session->get('current_meet_id'),
                            'registration' => 1,
                        ])
                    ->andWhere(['!=', 'creditor_id', 0]);

        $dataProvider = new ActiveDataProvider(['query' => $query]);

        return $dataProvider;
    }

    public static function getRegisteredRequestsTotalSum($question)
    {
        /*$sql_sum = 'SELECT SUM(cr.total_sum)
                      FROM (SELECT id, title FROM creditor WHERE procedures_id = :proc_id) AS c
                     INNER JOIN (SELECT creditor_id, SUM(total) AS total_sum
                                   FROM creditor_request
                                  WHERE is_enabled = 1 AND queue_type IN (' .
            ($question->mortgage_creditors_voices ? ':q31, :q32' : ':q32') .')
                                  GROUP BY creditor_id) AS cr
                        ON c.id = cr.creditor_id
                     INNER JOIN (SELECT * FROM meeting_participants WHERE meet_id = :meet_id AND registration = 1) AS p
                        ON p.creditor_id = c.id';

        $params = [
            ':proc_id' => Yii::$app->session->get('current_proc_id'),
            ':q32' => Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE,
            ':meet_id' => Yii::$app->session->get('current_meet_id'),
        ];
        !$question->mortgage_creditors_voices || $params[':q31'] = Request::QUEUE_TYPE_THIRD_GUARANTEE;

        return Yii::$app->db->createCommand($sql_sum, $params)->queryScalar();*/
        $sum = 0;
        $requests = (self::className())::findAll([
            'meet_id' => Yii::$app->session->get('current_meet_id'),
            'registration' => 1
        ]);

        foreach( $requests as $request ) {
            if( $question->mortgage_creditors_voices )
                $sum += $request->sum_31 + $request->sum_32;
            else
                $sum += $request->sum_32;
        }

        return $sum;
    }
}
