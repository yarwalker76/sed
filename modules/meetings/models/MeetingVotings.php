<?php

namespace app\modules\meetings\models;

use app\models\AppHelper;
use Yii;
use app\modules\meetings\models\MeetingQuestions;

/**
 * This is the model class for table "meeting_votings".
 *
 * @property integer $id
 * @property integer $question_id
 * @property integer $participant_id
 * @property integer $voting_option_id
 *
 * @property MeetingVotingOptions $votingOption
 * @property MeetingParticipants $participant
 * @property MeetingQuestions $question
 */
class MeetingVotings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_votings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_id', 'participant_id', 'voting_option_id'], 'integer'],
            [['request_percent', 'request_sum'], 'double'],
            [['voting_option_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeetingVotingOptions::className(), 'targetAttribute' => ['voting_option_id' => 'id']],
            [['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeetingParticipants::className(), 'targetAttribute' => ['participant_id' => 'id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeetingQuestions::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_id' => 'Question ID',
            'participant_id' => 'Participant ID',
            'voting_option_id' => 'Voting Option ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotingOption()
    {
        return $this->hasOne(MeetingVotingOptions::className(), ['id' => 'voting_option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant()
    {
        return $this->hasOne(MeetingParticipants::className(), ['id' => 'participant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(MeetingQuestions::className(), ['id' => 'question_id']);
    }

    public static function saveResults($model, $provider, $all_total_sum)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $question = MeetingQuestions::findOne($model->question_id);
            if( !$question )
                throw new \Exception(AppHelper::makeErrorString($question->getErrors()));

            $question->setAttributes([
                'final_decision' => $model->final_decision,
                'discussion' => $model->discussion,
            ]);
            if( !$question->save() )
                throw new \Exception(AppHelper::makeErrorString($question->getErrors()));

            if( $question->bulletin_form_id != 4 ){
                $creditors_data = [];
                foreach($provider->getModels() as $row) {
                    if( $question->mortgage_creditors_voices )
                        $request_sum = $row->sum_31 + $row->sum_32;
                    else
                        $request_sum = $row->sum_32;

                    $creditors_data[$row->id] = ['money' => $request_sum, 'percent' => $request_sum / $all_total_sum * 100];
                }

                foreach( $model->creditor_requests as $key => $voting_option ) {
                    $mv = MeetingVotings::findOne([
                        'question_id' => $model->question_id,
                        'participant_id' => $key
                    ]);

                    $mv || $mv = new MeetingVotings();

                    $mv->setAttributes([
                        'question_id' => $model->question_id,
                        'participant_id' => $key,
                        'voting_option_id' => $voting_option,
                        'request_sum' => $creditors_data[$key]['money'],
                        'request_percent' => $creditors_data[$key]['percent'],
                    ]);

                    if( !$mv->save() )
                        throw new \Exception(AppHelper::makeErrorString($mv->getErrors()));

                    unset($mv);
                }
            }

            $transaction->commit();

            return true;

        } catch(\Exception $e) {
            $transaction->rollBack();
            \Yii::$app->session->setFlash('errors', $e->getTraceAsString());
            return false;
        }
    }

    public static function saveCandidatesVoting($model)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            foreach( $model->candidate_votings as $key => $voting_option_result ) {
                $mv = MeetingVotings::findOne([
                    'question_id' => $model->question_id,
                    'participant_id' => $model->participant_id,
                    'voting_option_id' => $key
                ]);

                $mv || $mv = new MeetingVotings();

                $mv->setAttributes([
                    'question_id' => $model->question_id,
                    'participant_id' => $model->participant_id,
                    'voting_option_id' => $key,
                    'request_sum' => $voting_option_result,
                    'request_percent' => $voting_option_result / ($model->total_sum * $model->candidates_cnt) * 100,
                ]);

                if( !$mv->save() )
                    throw new \Exception(AppHelper::makeErrorString($mv->getErrors()));

                unset($mv);
            }

            $transaction->commit();

            return true;

        } catch(\Exception $e) {
            $transaction->rollBack();
            \Yii::$app->session->setFlash('errors', $e->getTraceAsString());
            return false;
        }
    }

    public static function getVoicesSum($question_id, $participant_id)
    {
        $sql = 'SELECT SUM(request_sum) FROM meeting_votings
                 WHERE question_id = :question_id AND participant_id = :participant_id';

        $params = [
            ':question_id' => $question_id,
            ':participant_id' => $participant_id,
        ];

        return Yii::$app->db->createCommand($sql, $params)->queryScalar();
    }
}
