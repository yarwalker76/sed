<?php

namespace app\modules\meetings\models;

use app\modules\meetings\services\MeetingHelper;
use Yii;
use app\modules\meetings\models\MeetingVotingOptions;

/**
 * This is the model class for table "meeting_questions".
 *
 * @property integer $id
 * @property integer $meet_id
 * @property string $question
 * @property string $decision
 * @property string $discussion
 * @property integer $bulletin_form_id
 * @property integer $voice_calc_id
 * @property integer $mortgage_creditors_voices
 *
 * @property MeetingBulletinForms $bulletinForm
 * @property Meetings $meet
 * @property MeetingVoiceCalculations $voiceCalc
 */
class MeetingQuestions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meet_id', 'question'], 'required'],
            [['meet_id', 'bulletin_form_id', 'voice_calc_id', 'mortgage_creditors_voices'], 'integer'],
            [['question', 'decision'], 'string', 'max' => 512],
            [['discussion', 'final_decision'], 'string', 'max' => 2048],
            [['bulletin_form_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeetingBulletinForms::className(), 'targetAttribute' => ['bulletin_form_id' => 'id']],
            [['meet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Meetings::className(), 'targetAttribute' => ['meet_id' => 'id']],
            [['voice_calc_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeetingVoiceCalculations::className(), 'targetAttribute' => ['voice_calc_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meet_id' => 'ID',
            'question' => 'Вопрос',
            'discussion' => 'Обсуждение',
            'decision' => 'Формулировка решения',
            'final_decision' => 'Принятое решение',
            'bulletin_form_id' => 'Форма бюллетеня',
            'voice_calc_id' => 'Рассчет голосов',
            'mortgage_creditors_voices' => 'Учитывать голоса залоговых кредиторов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBulletinForm()
    {
        return $this->hasOne(MeetingBulletinForms::className(), ['id' => 'bulletin_form_id']);
    }

    public function getBulletinFormName()
    {
        return $this->bulletinForm->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeet()
    {
        return $this->hasOne(Meetings::className(), ['id' => 'meet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVoiceCalc()
    {
        return $this->hasOne(MeetingVoiceCalculations::className(), ['id' => 'voice_calc_id']);
    }

    public function getVotingOptions()
    {
        if( in_array($this->bulletin_form_id, [1, 3]) )
            $where = ['bull_form_id' => $this->bulletin_form_id, 'question_id' => 0];
        else
            $where = ['question_id' => $this->id];

        $options = MeetingVotingOptions::find()->where($where)->asArray()->all();
        if( $options )
            return $options;

        if( $this->bulletin_form_id == 2 || $this->bulletin_form_id == 4 ) {
            $option = new MeetingVotingOptions();
            $params = [
                'bull_form_id' => $this->bulletin_form_id,
                'question_id' => $this->id,
                'value' => 'Воздержался',
            ];
            $option->setAttributes($params);
            $option->save();
            return [$params];
        }

        return [];
    }

    public static function getQuestionsReestr()
    {
        $classname = self::className();

        return $classname::getDb()->cache(function ($db) use ($classname) {
            return $classname::find()
                ->where(['meet_id' => Yii::$app->session->get('current_meet_id')])
                ->orderBy('id')
                ->asArray()
                ->all();
        }, 30);

    }

    public static function getQuestionsQuorum()
    {
        $classname = self::className();

        $questions = $classname::find()
            ->where(['meet_id' => Yii::$app->session->get('current_meet_id')])
            ->orderBy('id')
            ->asArray()
            ->all();

        foreach( $questions as &$question ) {
            if( $question['voice_calc_id'] == 1 ) {
                // из числа присутствующих
                $question['quorum_decision'] = 'Собрание считается правомочным для принятия решения по вопросу.';
                $question['quorum'] = '-';
            } else {
                 // от общего числа
                $reg_requests_total_sum = MeetingHelper::getRegisteredRequestsTotalSum((object) $question);
                $all_requests_total_sum = MeetingHelper::getQuorumAllRequestsTotalSum((object) $question);
                $quorum = $reg_requests_total_sum / $all_requests_total_sum * 100;
                $question['quorum'] = $quorum;
                $question['quorum_decision'] = ( $quorum > 50 ) ?
                    'Собрание считается правомочным для принятия решения по вопросу.' :
                    'Собрание считается не правомочным для принятия решения по вопросу.';
            }
        }

        return $questions;
    }

}
