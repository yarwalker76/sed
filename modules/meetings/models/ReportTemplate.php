<?php
namespace app\modules\meetings\models;

use yii\base\Model;

class ReportTemplate extends Model
{
    public $current_date;
    public $proc_type;
    public $pd_name;
    public $pd_short_name;
    public $pd_full_name;
    public $pd_legal_address;
    public $meeting_number;
    public $case_number;
    public $court;
    public $meeting_address;
    public $meeting_start_date;
    public $meeting_start_time;
    public $au_short_fio;
    public $reg_date;
    public $reg_start_time;
    public $reg_end_time;
    public $au_cro;
    public $meeting_acq_start_date;
    public $meeting_acq_end_date;
    public $meeting_acq_start_time;
    public $meeting_acq_end_time;
    public $meeting_acquaintance_address;

    public $creditors_cnt;
    public $creditors_cnt_cursive;
    public $reg_creditors_cnt;
    public $reg_creditors_cnt_cursive;

    public $total_request_sum;
    public $total_request_sum_cursive;
    public $reg_request_sum;
    public $reg_request_sum_cursive;
    public $quorum_percent;
    public $participants_with_voices;
    public $participants_without_voices;
    public $participants_requests_total_sum;

    public $questions_list;
    public $voting_results;
    public $questions_quorum;


   /*

    public $pd_inn;
    public $pd_ogrn;
    public $addressee;
    public $addressee_post_address;
    public $au_full_fio;
    public $au_short_fio;
    public $au_post_address;
    public $au_cell_phone;
    public $au_email;
    public $au_komu;
    public $judge_short_fio;
    public $meeting_register_period;
    public $meeting_acquaintance_date_period;
    public $meeting_acquaintance_time_period;

    */

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->current_date = $current_date = date('d.m.Y');
    }

    public function rules()
    {
        return [
            [['current_date', 'proc_type', 'pd_name', 'pd_short_name', 'pd_full_name', 'pd_legal_address',
              'meeting_number', 'case_number', 'court',
              'meeting_address', 'meeting_start_date', 'meeting_start_time', 'au_short_fio', 'au_cro',
              'meeting_acq_start_date', 'meeting_acq_end_date', 'meeting_acq_start_time', 'meeting_acq_end_time',
              'meeting_acquaintance_address', 'creditors_cnt', 'creditors_cnt_cursive',
              'reg_creditors_cnt', 'reg_creditors_cnt_cursive', 'total_request_sum', 'total_request_sum_cursive',
              'quorum_percent', 'reg_request_sum', 'reg_request_sum_cursive', 'participants_requests_total_sum',
                ], 'string'],
            [['participants_with_voices', 'participants_without_voices', 'questions_list', 'voting_results', 'questions_quorum'], 'safe'],
        ];
    }
}