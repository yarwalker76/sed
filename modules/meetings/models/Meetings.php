<?php

namespace app\modules\meetings\models;

use Yii;
use app\modules\procedures\models\Procedures;
use app\modules\meetings\behaviors\MeetingBehavior;

/**
 * This is the model class for table "meetings".
 *
 * @property integer $id
 * @property integer $proc_id
 * @property string $start_date
 * @property string $start_time
 * @property string $reg_start_time
 * @property string $reg_end_time
 * @property string $meet_address
 * @property string $acquaintance_start_date
 * @property string $acquaintance_end_date
 * @property string $acquaintance_start_time
 * @property string $acquaintance_end_time
 * @property string $acquaintance_address
 * @property string $conclusion
 * @property Procedures $proc
 */
class Meetings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meetings';
    }

    public function behaviors()
    {
        return [
            'meetings_behavior' => MeetingBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proc_id'], 'integer'],
            ['serial_number', 'string', 'max' => 20],
            [['start_date', 'start_time', 'reg_start_time', 'reg_end_time', 'acquaintance_start_date', 'acquaintance_end_date',
                'acquaintance_start_time', 'acquaintance_end_time'], 'safe'],
            [['meet_address', 'acquaintance_address'], 'string', 'max' => 512],
            [['conclusion'], 'string', 'max' => 2048],
            [['proc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Procedures::className(), 'targetAttribute' => ['proc_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'proc_id' => 'Proc ID',
            'start_date' => 'Дата собрания',
            'start_time' => 'Начало в',
            'reg_start_time' => 'Регистрация с',
            'reg_end_time' => 'по',
            'meet_address' => 'Адрес собрания',
            'acquaintance_start_date' => 'Дата с',
            'acquaintance_end_date' => 'по',
            'acquaintance_start_time' => 'Время с',
            'acquaintance_end_time' => 'по',
            'acquaintance_address' => 'Адрес ознакомления',
            'conclusion' => 'Заключение',
            'serial_number' => 'Номер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProc()
    {
        return $this->hasOne(Procedures::className(), ['id' => 'proc_id']);
    }

    public function getQuestions()
    {
        return $this->hasMany(MeetingQuestions::className(), ['meet_id' => 'id'])
                    ->orderBy(['id' => SORT_ASC]);
    }

    public static function getMaxSerialNumber()
    {
        return self::find()->where(['proc_id' => Yii::$app->session->get('current_proc_id')])->max('convert(serial_number, unsigned)');
    }

    public function getParticipants()
    {
        return $this->hasMany(MeetingParticipants::className(), ['meet_id' => 'id'])
            ->where(['!=', 'creditor_id', 0])
            ->orderBy(['id' => SORT_ASC]);
    }

}
