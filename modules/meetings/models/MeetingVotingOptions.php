<?php

namespace app\modules\meetings\models;

use Yii;

/**
 * This is the model class for table "meeting_voting_options".
 *
 * @property integer $id
 * @property integer $bull_form_id
 * @property integer $question_id
 * @property string $value
 *
 * @property MeetingBulletinForms $bullForm
 * @property MeetingVotings[] $meetingVotings
 */
class MeetingVotingOptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_voting_options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bull_form_id', 'question_id'], 'integer'],
            [['value'], 'string', 'max' => 512],
            [['bull_form_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeetingBulletinForms::className(), 'targetAttribute' => ['bull_form_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bull_form_id' => 'Bull Form ID',
            'question_id' => 'Question ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBullForm()
    {
        return $this->hasOne(MeetingBulletinForms::className(), ['id' => 'bull_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingVotings()
    {
        return $this->hasMany(MeetingVotings::className(), ['voting_option_id' => 'id']);
    }



}
