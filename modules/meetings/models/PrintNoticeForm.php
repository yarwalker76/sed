<?php
/**
 * Created by PhpStorm.
 * User: avs
 * Date: 06.06.17
 * Time: 11:51
 */

namespace app\modules\meetings\models;

use Yii;
use yii\base\Model;

class PrintNoticeForm extends Model
{
    public $meet_id;
    public $creditors_list;
    public $submit;
    public $add_outgoing_docs;

    public function rules()
    {
        return [
            [['meet_id'], 'required'],
            ['meet_id', 'integer'],
            ['submit', 'string'],
            ['add_outgoing_docs', 'boolean'],
            ['creditors_list', 'safe'],
        ];
    }
}