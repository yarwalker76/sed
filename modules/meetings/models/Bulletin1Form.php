<?php

namespace app\modules\meetings\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Bulletin1Form extends Model
{
    public $question_id;
    public $discussion;
    public $final_decision;
    public $mortgage_creditors_voices;
    public $creditor_requests;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['question_id'], 'required'],
            [['question_id', 'mortgage_creditors_voices'], 'integer'],
            [['discussion', 'final_decision'], 'string'],
            ['creditor_requests', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'discussion' => 'Обсуждение',
            'final_decision' => 'Принятое решение',
            //'creditor_requests' => 'Варианты',
        ];
    }

}
