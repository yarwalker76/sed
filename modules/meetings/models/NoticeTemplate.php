<?php
namespace app\modules\meetings\models;

use yii\base\Model;

class NoticeTemplate extends Model
{
    public $outgoing_letter_number;
    public $current_date;
    public $pd_name;
    public $pd_short_name;
    public $pd_inn;
    public $pd_ogrn;
    public $pd_legal_address;
    public $addressee;
    public $addressee_post_address;
    public $au_full_fio;
    public $au_short_fio;
    public $au_post_address;
    public $au_cell_phone;
    public $au_email;
    public $au_komu;
    public $case_number;
    public $judge_short_fio;
    public $meeting_start;
    public $meeting_address;
    public $meeting_register_period;
    public $meeting_acquaintance_date_period;
    public $meeting_acquaintance_time_period;
    public $meeting_acquaintance_address;
    public $questions_list;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->current_date = $current_date = date('d.m.Y');
    }

    public function rules()
    {
        return [
            [['outgoing_letter_number', 'current_date', 'pd_name', 'pd_short_name', 'pd_inn', 'pd_ogrn', 'pd_legal_address',
              'addressee', 'addressee_post_address', 'au_full_fio', 'au_short_fio', 'au_post_address',
              'au_cell_phone', 'au_email', 'au_komu', 'case_number', 'judge_short_fio', 'meeting_start',
              'meeting_address', 'meeting_register_period', 'meeting_acquaintance_date_period',
              'meeting_acquaintance_time_period', 'meeting_acquaintance_address'], 'string'],
            [['questions_list'], 'safe'],
        ];
    }
}