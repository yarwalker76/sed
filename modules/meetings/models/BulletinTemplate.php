<?php
namespace app\modules\meetings\models;

use yii\base\Model;

class BulletinTemplate extends Model
{
    public $pd_name;
    public $pd_inn;
    public $pd_ogrn;
    public $pd_legal_address;
    public $question_serial_number;
    public $meeting_start_date;
    public $meeting_address;
    public $decision;
    public $participant_name;
    public $representative_name;
    public $request_sum;
    public $voting_options;

    public function rules()
    {
        return [
            [['pd_name', 'pd_inn', 'pd_ogrn', 'pd_legal_address', 'request_sum',
              'question_serial_number', 'meeting_start_date', 'meeting_address', 'decision',
              'participant_name', 'representative_name'], 'string'],
            [['voting_options'], 'safe'],
        ];
    }
}