<?php

namespace app\modules\meetings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\meetings\models\Meetings;

/**
 * MeetingsSearch represents the model behind the search form about `app\modules\meetings\models\Meetings`.
 */
class MeetingsSearch extends Meetings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'proc_id'], 'integer'],
            [['start_date', 'start_time', 'reg_start_time', 'reg_end_time',
                'meet_address', 'acquaintance_start_date', 'acquaintance_end_date',
                'acquaintance_start_time', 'acquaintance_end_time', 'acquaintance_address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Meetings::find()
                ->where(['proc_id' => Yii::$app->session->get('current_proc_id')]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [
                    'serial_number' => SORT_DESC
                ]
            ],
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'proc_id' => $this->proc_id,
            'start_date' => $this->start_date,
            'start_time' => $this->start_time,
            'reg_start_time' => $this->reg_start_time,
            'reg_end_time' => $this->reg_end_time,
            'acquaintance_start_date' => $this->acquaintance_start_date,
            'acquaintance_end_date' => $this->acquaintance_end_date,
            'acquaintance_start_time' => $this->acquaintance_start_time,
            'acquaintance_end_time' => $this->acquaintance_end_time,
        ]);

        $query->andFilterWhere(['like', 'meet_address', $this->meet_address])
            ->andFilterWhere(['like', 'acquaintance_address', $this->acquaintance_address]);

        return $dataProvider;
    }
}
