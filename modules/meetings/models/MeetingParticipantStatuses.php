<?php

namespace app\modules\meetings\models;

use Yii;

/**
 * This is the model class for table "meeting_participant_statuses".
 *
 * @property integer $id
 * @property string $name
 * @property string $short_name
 *
 * @property MeetingParticipants[] $meetingParticipants
 */
class MeetingParticipantStatuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'meeting_participant_statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100],
            [['short_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'short_name' => 'Short Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingParticipants()
    {
        return $this->hasMany(MeetingParticipants::className(), ['participant_status_id' => 'id']);
    }
}
