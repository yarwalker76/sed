<?php

namespace app\modules\meetings\models;

use Yii;

/**
 * This is the model class for table "protocol_voting_results_view".
 *
 * @property integer $meet_id
 * @property integer $question_id
 * @property string $question
 * @property string $discussion
 * @property string $final_decision
 * @property string $option_name
 * @property double $request_sum
 * @property double $request_percent
 */
class ProtocolVotingResultsView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'protocol_voting_results_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meet_id', 'question'], 'required'],
            [['meet_id', 'question_id'], 'integer'],
            [['discussion', 'final_decision'], 'string'],
            [['request_sum', 'request_percent'], 'number'],
            [['question', 'option_name'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'meet_id' => 'Meet ID',
            'question_id' => 'Question ID',
            'question' => 'Question',
            'discussion' => 'Discussion',
            'final_decision' => 'Final Decision',
            'option_name' => 'Option Name',
            'request_sum' => 'Request Sum',
            'request_percent' => 'Request Percent',
        ];
    }

    public static function getMeetingResults()
    {
        $data = (self::className())::find()
            ->where(['meet_id' => Yii::$app->session->get('current_meet_id')])
            ->asArray()
            ->all();

        $current_question_id = -1;
        $result = [];
        $i = 0;
        foreach( $data as $row ) {
            if( $current_question_id != $row['question_id'] ) {
                $result[] = [
                    'question' => $row['question'],
                    'discussion' => $row['discussion'],
                    'final_decision' => $row['final_decision'],
                    'options' => [
                        [
                            'name' => $row['option_name'],
                            'request_sum' => round($row['request_sum'], 2),
                            'request_percent' => round($row['request_percent'], 2)
                        ]
                    ]
                ];
                $current_question_id = $row['question_id'];
                $i = count($result) - 1;
            } else {
                $result[$i]['options'][] = [
                    'name' => $row['option_name'],
                    'request_sum' => round($row['request_sum'], 2),
                    'request_percent' => round($row['request_percent'], 2)
                ];
            }
        }

        return $result;
    }
}
