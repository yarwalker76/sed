<?php
namespace app\modules\meetings\controllers;

use Yii;
use app\modules\meetings\models\MeetingParticipants;
use app\modules\meetings\models\MeetingParticipantsSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use app\models\AppHelper;
use app\modules\meetings\services\MeetingHelper;
use app\modules\meetings\controllers\BaseController;
use app\modules\creditors\models\Request;

/**
 * CheckinController implements the CRUD actions for MeetingParticipants model.
 */
class CheckinController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'test'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer', 'bookkeeping'],
                    ],
                    [
                        'actions' => ['create', 'add-participant', 'update', 'delete', 'update-participants'],
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                    [
                        'actions' => ['create', 'add-participant', 'update', 'update-participants'],
                        'allow' => true,
                        'roles' => ['backOffice'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MeetingParticipants models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeetingParticipantsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actions_template' => $this->_question_actions_template,
            'has_voting_results' => MeetingHelper::hasVotingResults(),
        ]);
    }

    /**
     * Displays a single MeetingParticipants model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MeetingParticipants model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MeetingParticipants();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionAddParticipant()
    {
        $model = new MeetingParticipants();
        $model->meet_id = Yii::$app->session->get('current_meet_id');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model->save()) {
            echo json_encode(['msg' => 'ok']);
        } else {
            return $this->renderPartial('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MeetingParticipants model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $id = $id ? $id : Yii::$app->request->post('MeetingParticipants')['id'];
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && $model->save()) {
            echo json_encode(['msg' => 'ok']);
        } else {
            return $this->renderPartial('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MeetingParticipants model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if( $id ) {
            $question = $this->findModel($id);

            if( $question->delete() )
                $result = ['msg' => 'ok'];
            else
                $result = ['msg' => AppHelper::makeErrorString($question->getErrors())];

            echo json_encode($result);
        }
    }

    public function actionUpdateParticipants() {
        if( Yii::$app->request->isAjax ){
            if( !MeetingHelper::hasVotingResults() ){
                Yii::$app->response->format = Response::FORMAT_JSON;

                MeetingParticipants::deleteAll(['AND',
                    ['meet_id' => Yii::$app->session->get('current_meet_id')],
                    ['!=', 'creditor_id', 0]
                ]);

                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $sql = 'SELECT c.id, c.title, cr.queue_type, cr.total_sum FROM
                                (SELECT id, title FROM creditor WHERE procedures_id = :proc_id) AS c
                             INNER JOIN (SELECT creditor_id, queue_type, SUM(total) AS total_sum 
                                           FROM creditor_request
                                          WHERE is_enabled = 1 AND queue_type in (:q31, :q32)
                                          GROUP BY creditor_id, queue_type) AS cr 
                                ON c.id = cr.creditor_id';

                    $participants = \Yii::$app->db->createCommand($sql)
                        ->bindValue(':proc_id', Yii::$app->session->get('current_proc_id'))
                        ->bindValue(':q31', Request::QUEUE_TYPE_THIRD_GUARANTEE)
                        ->bindValue(':q32', Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE)
                        ->queryAll();

                    foreach( $participants as $participant ) {
                        $model = MeetingParticipants::findOne([
                            'meet_id' => Yii::$app->session->get('current_meet_id'),
                            'creditor_id' => $participant['id'],
                        ]);

                        if( $model ) {
                            $model->setAttribute('total_sum', $model->total_sum + doubleval($participant['total_sum']));
                        } else {
                            $model = new MeetingParticipants();
                            $model->setAttributes([
                                'meet_id' => Yii::$app->session->get('current_meet_id'),
                                'registration' => 0,
                                'name' => $participant['title'],
                                'representative_name' => null,
                                'supporting_docs' => null,
                                'participant_status_id' => MeetingParticipants::STATUS_K_K,
                                'total_sum' => doubleval($participant['total_sum']),
                                'creditor_id' => $participant['id'],
                            ]);
                        }

                        if( $participant['queue_type'] == 3 )
                            $model->setAttributes(['sum_31' => doubleval($participant['total_sum'])]);
                        else
                            $model->setAttributes(['sum_32' => doubleval($participant['total_sum'])]);

                        if( !$model->save() )
                            throw new \Exception(AppHelper::makeErrorString($model->getErrors()));

                        unset($model);
                    }

                    $transaction->commit();
                    return ['msg' => 'ok'];

                } catch(\Exception $e) {
                    $transaction->rollBack();
                    return ['msg' => $e->getTraceAsString()];
                }
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


    }

    /**
     * Finds the MeetingParticipants model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MeetingParticipants the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MeetingParticipants::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
