<?php

namespace app\modules\meetings\controllers;

use app\models\AppHelper;
use Yii;
use app\modules\meetings\models\MeetingsSearch;
use app\modules\meetings\controllers\BaseController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\TblValues;
use app\models\TblOrganizations;
use app\models\TblParams;
use yii\helpers\ArrayHelper;


/**
 * Default controller for the `meetings` module
 */
class DefaultController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'get-participants', 'test'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer', 'bookkeeping'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $searchModel = new MeetingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actions_template' => $this->_meeting_actions_template,
        ]);
    }

    public function actionGetParticipants()
    {
        $make_name = function($item) {
            $item['value'] = $item['label'] = AppHelper::getFIO($item['organizations_id']);

            return $item;
        };

        if( Yii::$app->request->isAjax ){
            $title = \Yii::$app->request->post('query');
            $types = [1, 4, 5, 6, 7];
            $query1 =
                (new \yii\db\Query())->
                select(['v.value', 'v.value as label', 'v.organizations_id as id'])->
                from(TblValues::tableName() . ' as v')->
                leftJoin( TblOrganizations::tableName() . ' as o', 'o.id = v.organizations_id')->
                leftJoin( TblParams::tableName() . ' as p', 'p.id = v.params_id')->
                where(['p.title' => "Название"])->
                andFilterWhere(['like', 'v.value', $title])->
                andFilterWhere(['o.organizations_types_id' => $types])->
                orderBy('v.value ASC')->
                all();
            $query2 =
                (new \yii\db\Query())->
                select(['v.value', 'v.value as label', 'v.organizations_id'])->
                from(TblValues::tableName() . ' as v')->
                leftJoin( TblOrganizations::tableName() . ' as o', 'o.id = v.organizations_id')->
                leftJoin( TblParams::tableName() . ' as p', 'p.id = v.params_id')->
                where(['p.title' => "Фамилия"])->
                andFilterWhere(['like', 'v.value', $title])->
                andFilterWhere(['o.organizations_types_id' => $types])->
                orderBy('v.value ASC')->
                all();
            if (count($query2) > 0) {
                $query2 = array_map($make_name, $query2);
                $query1 = array_merge ($query1, $query2);
            }


            echo json_encode($query1);
        }
    }

    public function actionTest()
    {
        phpinfo();
    }

}
