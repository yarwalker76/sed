<?php
namespace app\modules\meetings\controllers;

use app\models\AppHelper;
use Yii;
use app\modules\meetings\controllers\BaseController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\meetings\models\MeetingBulletinForms;
use app\modules\meetings\models\MeetingVoiceCalculations;
use app\modules\meetings\models\MeetingQuestions;
use yii\helpers\ArrayHelper;
use app\modules\meetings\models\MeetingQuestionsSearch;

class QuestionsController extends BaseController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer', 'bookkeeping'],
                    ],
                    [
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                    [
                        'actions' => ['create', 'update'],
                        'allow' => true,
                        'roles' => ['backOffice'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($meet_id, $actions_template = null)
    {
        $searchModel = new MeetingQuestionsSearch();
        $questionsDataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('partials/_question_reestr',
                [
                    'questionsProvider' => $questionsDataProvider,
                    'question_actions_template' => is_null($actions_template) ? $this->_question_actions_template : $this->$actions_template,
                ]);
    }

    /**
     * Добавляет вопрос в собрание
     */
    public function actionCreate()
    {
        $model = new MeetingQuestions();

        if( $model->load(Yii::$app->request->post()) && $model->save() ) {
            return json_encode(['msg' => 'ok']);
        }

        $model->meet_id = Yii::$app->session->get('current_meet_id');

        $bulletin_forms = MeetingBulletinForms::find()->asArray()->all();
        $voice_calcs = MeetingVoiceCalculations::find()->asArray()->all();

        return $this->renderAjax('partials/_question_form', [
            'model' => $model,
            'bulletin_forms' => ArrayHelper::map($bulletin_forms, 'id', 'name'),
            'voice_calcs' => ArrayHelper::map($voice_calcs, 'id', 'name'),
            'disabled' => $this->_disabled,
            'change_question_type' => false,
        ]);
    }

    /**
     * Удаляет вопрос из собрания
     * @param null $id
     * @return string
     */
    public function actionDelete($id = null)
    {
        if( $id ) {
            $question = MeetingQuestions::findOne($id);
            $meet_id = $question->meet_id;

            if( $question->delete() )
                $result = [
                    'msg' => 'ok',
                    'count' => MeetingQuestions::find()->where(['meet_id' => $meet_id])->count()
                ];
            else
                $result = [
                    'msg' => AppHelper::makeErrorString($question->getErrors()),
                    'count' => MeetingQuestions::find()->where(['meet_id' => $meet_id])->count()
                ];

            echo json_encode($result);
        }
    }

    /**
     * Изменяет вопрос в собрании
     * @param null $id
     * @return string
     */
    public function actionUpdate($id = null)
    {
        $id = $id ? $id : Yii::$app->request->post('MeetingQuestions')['id'];
        $model = MeetingQuestions::findOne($id);

        if( $model->load(Yii::$app->request->post()) && $model->save() ) {
            return json_encode(['msg' => 'ok']);
        } elseif ( $model->hasErrors() ) {
            return json_encode(['msg' => AppHelper::makeErrorString($model->errors)]);
        }

        $bulletin_forms = MeetingBulletinForms::find()->asArray()->all();
        $voice_calcs = MeetingVoiceCalculations::find()->asArray()->all();

        return $this->renderAjax('partials/_question_form', [
            'model' => $model,
            'bulletin_forms' => ArrayHelper::map($bulletin_forms, 'id', 'name'),
            'voice_calcs' => ArrayHelper::map($voice_calcs, 'id', 'name'),
            'disabled' => $this->_disabled,
            'change_question_type' => $this->_disabled || \app\modules\meetings\services\MeetingHelper::hasVotingResults()
        ]);
    }
}