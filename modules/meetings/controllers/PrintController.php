<?php

namespace app\modules\meetings\controllers;

use app\models\AppHelper;
use Yii;
use app\modules\meetings\controllers\BaseController;
use yii\web\NotFoundHttpException;
use app\modules\meetings\models\MeetingParticipants;
use app\modules\meetings\models\PrintNoticeForm;


class PrintController extends BaseController
{
    public function actionIndex()
    {
        $model = new PrintNoticeForm();
        $model->meet_id = Yii::$app->session->get('current_meet_id');

        if( $model->load(Yii::$app->request->post()) ) {
            switch( $model->submit ){
                case 'notices':
                    $this->_print_service->makeNoticeTemplates($model);
                    break;

                case 'envelopes':
                    $this->_print_service->makeEnvelopeTemplates($model);
                    break;

                case 'report':
                    $this->_print_service->makeReportTemplate($model);
                    break;
            }

        } else {
            return $this->render('index', [
                'dataProvider' => MeetingParticipants::getParticipantsProvider(),
                'model' => $model,
            ]);
        }
    }

    public function actionPrepareNotices($id = null)
    {
        //$model =
        if( $id ) {
            $this->_print_service->makeNoticeTemplates();
        } else {
            throw new NotFoundHttpException("Такая страница не найдена.");
        }
    }

    public function actionPrintBulletin($id = null)
    {
        if( $id ) {
            $this->_print_service->makeBulletinTemplates($id);
        } else {
            throw new NotFoundHttpException("Такая страница не найдена.");
        }
    }

    public function actionPrintReport($id = null)
    {
        if( $id ) {
            $this->_print_service->makeReportTemplate($id);
        } else {
            throw new NotFoundHttpException("Такая страница не найдена.");
        }
    }

    public function actionRegJournal()
    {
        $this->_print_service->makeRegJournalTemplate();
    }
}
