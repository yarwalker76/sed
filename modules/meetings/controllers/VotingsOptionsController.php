<?php

namespace app\modules\meetings\controllers;

use app\modules\meetings\models\MeetingQuestions;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use app\models\AppHelper;
use app\modules\meetings\models\MeetingVotingOptions;
use yii\web\NotFoundHttpException;
use app\modules\meetings\services\MeetingHelper;

class VotingsOptionsController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer', 'bookkeeping'],
                    ],
                     [
                         'actions' => ['create', 'update', 'delete'],
                         'allow' => true,
                         'roles' => ['manager'],
                     ],
                     [
                         'actions' => ['create', 'update'],
                         'allow' => true,
                         'roles' => ['backOffice'],
                     ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if( Yii::$app->request->isAjax ) {
            Yii::$app->response->format = Response::FORMAT_HTML;
            $dataProvider = new ActiveDataProvider([
                'query' => MeetingHelper::getGroupedVotingResults(Yii::$app->request->get('question_id'))
                /*MeetingVotingOptions::find()
                            ->where(['question_id' => Yii::$app->request->get('question_id')])*/
            ]);
            $question = MeetingQuestions::findOne(Yii::$app->request->get('question_id'));
            return $this->renderAjax('@app/modules/meetings/views/votings/partials/_variants_list',
                [
                    'dataProvider' => $dataProvider,
                    'bulletin_form_id' => $question->bulletin_form_id,
                ]);
        } else {
            throw new NotFoundHttpException("Страница не найдена.");
        }
    }

    public function actionCreate()
    {
        if( Yii::$app->request->isAjax ) {
            $model = new MeetingVotingOptions();
            if( $model->load(Yii::$app->request->post()) && $model->save() ) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['msg' => 'ok', 'variant_id' => $model->id, 'variant_value' => $model->value];

            } else {
                return ['msg' => AppHelper::makeErrorString($model->getErrors())];
            }
        } else {
            throw new NotFoundHttpException("Страница не найдена.");
        }
    }

    public function actionUpdate()
    {
        if( Yii::$app->request->isAjax && Yii::$app->request->post('id')) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $option = MeetingVotingOptions::findOne(Yii::$app->request->post('id'));
            if(!$option)
                return ['msg' => 'Такой вариант не найден'];

            $option->value = Yii::$app->request->post('value');
            if( $option->save() )
                return [
                    'msg' => 'ok',
                    'question_id' => $option->question_id,
                ];
            else
                return ['msg' => AppHelper::makeErrorString($option->getErrors())];


        } else {
            throw new NotFoundHttpException("Страница не найдена.");
        }
    }

    public function actionDelete()
    {
        if( Yii::$app->request->isAjax && Yii::$app->request->post('id')) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $option = MeetingVotingOptions::findOne(Yii::$app->request->post('id'));
            if(!$option)
                return ['msg' => 'Такой вариант не найден'];

            $question_id = $option->question_id;
            if( $option->delete() )
                return [
                    'msg' => 'ok',
                    'question_id' => $question_id,
                ];
            else
                return ['msg' => AppHelper::makeErrorString($option->getErrors())];

        } else {
            throw new NotFoundHttpException("Страница не найдена.");
        }
    }



}
