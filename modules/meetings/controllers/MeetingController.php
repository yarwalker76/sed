<?php

namespace app\modules\meetings\controllers;

use app\models\AppHelper;
use Yii;
use app\modules\meetings\models\Meetings;
use app\modules\meetings\models\MeetingsSearch;
use app\modules\meetings\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use app\modules\meetings\models\MeetingQuestions;
use app\modules\meetings\models\MeetingParticipantsSearch;

/**
 * MeetingsController implements the CRUD actions for Meetings model.
 */
class MeetingController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'checkin', 'prepare'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer', 'bookkeeping'],
                    ],
                    [
                        'actions' => ['create', 'update', 'clone', 'delete', 'update-conclusion'],
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                    [
                        'actions' => ['create', 'update', 'clone', 'update-conclusion'],
                        'allow' => true,
                        'roles' => ['backOffice'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Meetings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeetingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Meetings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Выводит представление для создания нового собрания и сопуствующих аттрибутов
     * @return string
     */
    public function actionCreate()
    {
        $meeting = new Meetings();
        $meeting->setAttributes([
            'serial_number' => Meetings::getMaxSerialNumber() + 1,
            'proc_id' => Yii::$app->session->get('current_proc_id'),
            'start_date' => date_format($this->_meet_start_date, 'Y-m-d'),
            'acquaintance_start_date' => date_format($this->_acq_start_date, 'Y-m-d'),
            'acquaintance_end_date' => date_format($this->_acq_end_date, 'Y-m-d'),
            'start_time' => '12:00',
            'acquaintance_start_time' => '11:00',
            'acquaintance_end_time' => '16:00',
        ]);

        Yii::$app->session->set('current_meet_id', $meeting->id);
        $questionsDataProvider = null;

        if( Yii::$app->request->isAjax ) {
            if( $meeting->load(Yii::$app->request->post()) && $meeting->save() )
                Yii::$app->session->set('current_meet_id', $meeting->id);
                    $questionsDataProvider = new ActiveDataProvider([
                        'query' => $meeting->getQuestions(),
                        'pagination' => false,
                    ]);

            return $this->renderAjax('prepare', [
                    'meeting' => $meeting,
                    'questionsProvider' => ($questionsDataProvider ? $questionsDataProvider : null ),
                    'question_actions_template' => $this->_question_actions_template,
                    'disabled' => $this->_disabled,
                ]);
        }

        return $this->render('prepare',[
            'meeting' => $meeting,
            'questionsProvider' => ($questionsDataProvider ? $questionsDataProvider : null ),
            'question_actions_template' => $this->_question_actions_template,
            'disabled' => $this->_disabled,
        ]);
    }

    /**
     * Выводит представление для иизменения собрания и сопуствующих аттрибутов
     * @return string
     */
    public function actionUpdate($id)
    {
        $meeting = Meetings::findOne($id);
        \Yii::$app->session->set('current_meet_id', $id);
        $questionsDataProvider = new ActiveDataProvider([
            'query' => $meeting->getQuestions(),
            'pagination' => false,
        ]);

        if( Yii::$app->request->isAjax ) {
            if( $meeting->load(Yii::$app->request->post()) && $meeting->save() )
                return $this->renderAjax('prepare', [
                    'meeting' => $meeting,
                    'questionsProvider' => ($questionsDataProvider ? $questionsDataProvider : null ),
                    'question_actions_template' => $this->_question_actions_template,
                    'disabled' => $this->_disabled,
                ]);
        }

        return $this->render('prepare',[
            'meeting' => $meeting,
            'questionsProvider' => ($questionsDataProvider ? $questionsDataProvider : null ),
            'question_actions_template' => $this->_question_actions_template,
            'disabled' => $this->_disabled,
        ]);
    }

    /**
     * Удаляет собрание кредиторов
     * @param null $id
     * @return string
     */
    public function actionDelete($id)
    {
        Meetings::findOne($id)->delete();

        $dataProvider = new ActiveDataProvider([
            'query' => Meetings::find()
                ->where(['proc_id' => Yii::$app->session->get('current_proc_id')])
                ->orderBy(['start_date' => SORT_DESC]),
            'pagination' => false,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'start_date' => [
                    'asc' => ['start_date' => SORT_ASC],
                    'desc' => ['start_date' => SORT_DESC],
                    //'label' => $searchModel->getAttributeLabel('start_date')
                ],
            ]
        ]);

        return $this->renderAjax('/default/index', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actions_template' => $this->_meeting_actions_template,
        ]);
    }

    /**
     * Finds the Meetings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meetings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Meetings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Клонирует собрание
     * @param null $id
     * @return string
     */
    public function actionClone($id = null)
    {
        $meeting = Meetings::findOne($id);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            // клонируем собрание
            $new_meeting = new Meetings();
            $new_meeting->setAttributes([
                'serial_number' => (string)(Meetings::getMaxSerialNumber() + 1),
                'proc_id' => $meeting->proc_id,
                'start_date' => $meeting->start_date,
                'start_time' => $meeting->start_time,
                'reg_start_time' => $meeting->reg_start_time,
                'reg_end_time' => $meeting->reg_end_time,
                'meet_address' => $meeting->meet_address,
                'acquaintance_start_date' => $meeting->acquaintance_start_date,
                'acquaintance_end_date' => $meeting->acquaintance_end_date,
                'acquaintance_start_time' => $meeting->acquaintance_start_time,
                'acquaintance_end_time' => $meeting->acquaintance_end_time,
                'acquaintance_address' => $meeting->acquaintance_address,
            ]);

            if( !$new_meeting->save() )
                throw new \Exception(AppHelper::makeErrorString($new_meeting->getErrors()));

            // клонируем вопросы собрания
            $questions = MeetingQuestions::findAll(['meet_id' => $id]);
            foreach( $questions as $question ) {
                $new_question = new MeetingQuestions();
                $new_question->setAttributes([
                    'meet_id' => $new_meeting->id,
                    'question' => $question->question,
                    'decision' => $question->decision,
                    'bulletin_form_id' => $question->bulletin_form_id,
                    'voice_calc_id' => $question->voice_calc_id,
                    'mortgage_creditors_voice' => $question->mortgage_creditors_voices,
                ]);

                if( !$new_question->save() )
                    throw new \Exception(AppHelper::makeErrorString($new_question->getErrors()));

                unset($new_question);
            }

            $transaction->commit();

        } catch(\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('errors', $e->getTraceAsString());

            $dataProvider = new ActiveDataProvider([
                'query' => Meetings::find()
                    ->where(['proc_id' => Yii::$app->session->get('current_proc_id')])
                    ->orderBy(['start_date' => SORT_DESC]),
                'pagination' => false,
            ]);

            $dataProvider->setSort([
                'attributes' => [
                    'start_date' => [
                        'asc' => ['start_date' => SORT_ASC],
                        'desc' => ['start_date' => SORT_DESC],
                        //'label' => $searchModel->getAttributeLabel('start_date')
                    ],
                ]
            ]);

            return $this->renderAjax('/default/index', [
                //'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'actions_template' => $this->_meeting_actions_template,
            ]);
        }

        return $this->redirect(['update', 'id' => $new_meeting->id]);
    }

    public function actionCheckin($id)
    {
        $searchModel = new MeetingParticipantsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderPartial(
            '@app/modules/' . $this->module->uniqueId . '/views/checkin/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actions_template' => $this->_question_actions_template,
        ]);
    }

    public function actionPrepare()
    {
        if( \Yii::$app->session->get('current_meet_id') ) {
            $meeting = Meetings::findOne(\Yii::$app->session->get('current_meet_id'));
        } else {
            $meeting = new Meetings();

            $meeting->setAttributes([
                'proc_id' => Yii::$app->session->get('current_proc_id'),
                'start_date' => date_format($this->_meet_start_date, 'Y-m-d'),
                'acquaintance_start_date' => date_format($this->_acq_start_date, 'Y-m-d'),
                'acquaintance_end_date' => date_format($this->_acq_end_date, 'Y-m-d'),
                'start_time' => '12:00',
                'acquaintance_start_time' => '11:00',
                'acquaintance_end_time' => '16:00',
            ]);
        }

        $questionsDataProvider = new ActiveDataProvider([
                                    'query' => $meeting->getQuestions(),
                                    'pagination' => false,
                                ]);

        if( Yii::$app->request->isAjax ) {
            if( $meeting->load(Yii::$app->request->post()) && $meeting->save() )
                return $this->renderAjax('prepare', [
                    'meeting' => $meeting,
                    'questionsProvider' => ($questionsDataProvider ? $questionsDataProvider : null ),
                    'question_actions_template' => $this->_question_actions_template,
                    'disabled' => $this->_disabled,
                ]);
        }

        return $this->render('prepare', [
            'meeting' => $meeting,
            'questionsProvider' => ($questionsDataProvider ? $questionsDataProvider : null ),
            'question_actions_template' => $this->_question_actions_template,
            'disabled' => $this->_disabled,
            ]);
    }

    public function actionUpdateConclusion()
    {
        if( Yii::$app->request->isAjax ) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

            $model = Meetings::findOne(\Yii::$app->session->get('current_meet_id'));
            if( $model->load(Yii::$app->request->post()) && $model->save() ) {
                return '<p>Заключение по собранию успешно сохранено</p>';
            } else {
                return '<p>' . AppHelper::makeErrorString($model->getErrors()) . '</p>';
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
