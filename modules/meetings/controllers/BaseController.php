<?php
namespace app\modules\meetings\controllers;

use Yii;
use yii\web\Controller;
use app\modules\meetings\services\PrintService;

class BaseController extends Controller
{
    protected $_meeting_actions_template;
    protected $_question_actions_template;
    protected $_voting_actions_template;
    protected $_meet_start_date;
    protected $_acq_start_date;
    protected $_acq_end_date;
    protected $_disabled;

    protected $_print_service;

    public function __construct($id, $module, PrintService $print_service, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_print_service = $print_service;

        $this->_meeting_actions_template = '{update}';
        $this->_question_actions_template = '{update}';
        $this->_voting_actions_template = '{update}';
        if( Yii::$app->user->can('manager') ) {
            $this->_meeting_actions_template = '{update} {delete} {clone}';
            $this->_question_actions_template = '{update} {delete}';
            $this->_voting_actions_template = '{update} {bulletin} {print}';
        }

        if( Yii::$app->user->can('backOffice') ) {
            $this->_meeting_actions_template = '{update} {clone}';
            $this->_question_actions_template = '{update}';
            $this->_voting_actions_template = '{update} {bulletin} {print}';
        }

        $this->_meet_start_date = date_create(date('Y-m-d'));
        date_add($this->_meet_start_date, date_interval_create_from_date_string('14 days'));

        $this->_acq_start_date = date_create(date('Y-m-d'));
        date_add($this->_acq_start_date, date_interval_create_from_date_string('9 days'));

        $this->_acq_end_date = date_create(date('Y-m-d'));
        date_add($this->_acq_end_date, date_interval_create_from_date_string('13 days'));

        $this->_disabled = !( Yii::$app->user->can('manager') || Yii::$app->user->can('manager') );

    }
}