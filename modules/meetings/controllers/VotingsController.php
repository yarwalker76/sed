<?php

namespace app\modules\meetings\controllers;

use app\modules\meetings\models\CandidatesVotingForm;

use app\modules\meetings\models\MeetingParticipants;
use app\modules\meetings\models\MeetingVotingOptions;
use Yii;
use app\models\AppHelper;
use app\modules\meetings\models\MeetingQuestions;
use app\modules\meetings\controllers\BaseController;
use app\modules\meetings\models\MeetingQuestionsSearch;
use app\modules\meetings\models\Meetings;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use app\modules\meetings\services\MeetingHelper;
use app\modules\meetings\models\MeetingVotings;
use yii\web\Response;


class VotingsController extends BaseController
{
    const MODEL_NAMESPACE = 'app\modules\meetings\models\\';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'print', 'bulletin', 'candidates-voting', 'print-bulletin'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer', 'bookkeeping'],
                    ],
                   /* [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['manager'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['backOffice'],
                    ],*/
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $meeting = Meetings::findOne(Yii::$app->session->get('current_meet_id'));

        $searchModel = new MeetingQuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'voting_actions_template' => $this->_voting_actions_template,
            'meeting' => $meeting,
        ]);
    }

    public function actionBulletin($id = null)
    {
        if( !is_null($id) && Yii::$app->request->isAjax ) {
            $question = MeetingQuestions::findOne($id);

            if( $question ){
                $bulletin_form = self::MODEL_NAMESPACE . 'Bulletin' . $question->bulletin_form_id . 'Form';
                $model = new $bulletin_form();

                // выбираем кредиторов для голосования по вопросу
                $requests_provider = MeetingParticipants::getRequestsProvider(); //MeetingHelper::getRequestsProvider($question);
                $requests_total_sum = MeetingParticipants::getRegisteredRequestsTotalSum($question); //MeetingHelper::getRegisteredRequestsTotalSum($question);

                if( $model->load(Yii::$app->request->post()) && $model->validate() ) {
                    Yii::$app->response->format = Response::FORMAT_HTML;

                    if( MeetingVotings::saveResults($model, $requests_provider, $requests_total_sum) )
                        return 'ok';
                    else
                        return '<p>' . AppHelper::makeErrorString(Yii::$app->session->getFlash('errors')) . '</p>';

                } else {
                    // создаем форму голосвания
                    $model->setAttributes([
                        'question_id' => $question->id,
                        'discussion' => $question->discussion,
                        'final_decision' => $question->final_decision,
                        'mortgage_creditors_voices' => $question->mortgage_creditors_voices,
                    ]);

                    $votings = MeetingVotings::find()
                                    ->where(['question_id' => $question->id, ])
                                    ->asArray()
                                    ->all();
                    $model->creditor_requests = ArrayHelper::map($votings, 'participant_id', 'voting_option_id');

                    $data = [
                        'question' => $question->question,
                        'bulletin_name' => $question->bulletinFormName,
                        'voting_options' => $question->votingOptions,
                        'total_sum' => $requests_total_sum,
                        'bulletin_form_id' => $question->bulletin_form_id,
                    ];

                    $view_params = [
                        'model' => $model,
                        'requests_provider' => $requests_provider,
                    ];

                    if( $question->bulletin_form_id == 2 || $question->bulletin_form_id == 4 ) {
                        $variant_model = new MeetingVotingOptions();
                        $variantsProvider = new ActiveDataProvider([
                            'query' => MeetingHelper::getGroupedVotingResults($question->id)
                        ]);
                        $variant_model->setAttributes([
                            'bull_form_id' => $question->bulletin_form_id,
                            'question_id' => $question->id,
                        ]);
                        $view_params['variant_model'] = $variant_model;
                        $view_params['variantsProvider'] = $variantsProvider;
                    }

                    if( $question->bulletin_form_id == 4 ) {
                        $data['candidates_cnt_list'] = MeetingVotingOptions::find()
                            ->where(['question_id' => 0])
                            ->andWhere(['bull_form_id' => 3])
                            ->andWhere(['!=', 'value', 'Воздержался'])
                            ->all();
                    }

                    $view_params['data'] = $data;

                    return $this->renderAjax('bulletin' . $question->bulletin_form_id, $view_params);
                }
            } else {
                return '<p class="errors">' . AppHelper::makeErrorString($question->getErroros()) . '</p>';
            }
        } else {
            throw new NotFoundHttpException("Такой бюллетень голосования не найден.");
        }
    }

    public function actionCandidatesVoting()
    {
        if( Yii::$app->request->isAjax && Yii::$app->request->post() ) {

            $model = new CandidatesVotingForm();
            $model->setAttributes([
                'question_id' => Yii::$app->request->post('question_id'),
                'participant_id' => Yii::$app->request->post('participant_id'),
                'participant_sum' => Yii::$app->request->post('participant_sum'),
                'total_sum' => Yii::$app->request->post('total_sum'),
                'candidates_cnt' => Yii::$app->request->post('multiplier')
            ]);

            $query = MeetingVotingOptions::find()->where([
                'question_id' => Yii::$app->request->post('question_id'),
            ])->orderBy(['id' => SORT_ASC]);

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
               /* 'pagination' => [
                    'pageSize' => 5,
                ],
                'totalCount' => $query->count() - 1*/
            ]);

            $votings = MeetingVotings::find()
                        ->where([
                                'question_id' => Yii::$app->request->post('question_id'),
                                'participant_id' => Yii::$app->request->post('participant_id'),
                            ])
                        ->asArray()
                        ->all();
            $model->candidate_votings = ArrayHelper::map($votings, 'voting_option_id', 'request_sum');

            if( $model->load(Yii::$app->request->post()) && $model->validate() ) {
                Yii::$app->response->format = Response::FORMAT_HTML;

                if( MeetingVotings::saveCandidatesVoting($model) )
                    return 'ok';
                else
                    return '<p>' . AppHelper::makeErrorString(Yii::$app->session->getFlash('errors')) . '</p>';
            }

            $voice_sum = Yii::$app->request->post('multiplier') * Yii::$app->request->post('participant_sum');

            return $this->renderAjax('candidates_voting_form', [
                'model' => $model,
                'candidate_votings_provider' => $dataProvider,
                'voice_sum' => $voice_sum,
            ]);
        } else {
            throw new NotFoundHttpException("Такой бюллетень голосования не найден.");
        }
    }

    /*public function actionPrintBulletin($id = null)
    {
        $this->_print_service->makeBulletinTemplates($id);
    }*/

}
