<?php

namespace app\modules\meetings;

/**
 * meetings module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\meetings\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {

        parent::init();

        \Yii::configure($this, require(__DIR__ . '/config/config.php'));

        $this->setAliases(['@meetings-assets' => __DIR__ . '/assets']);
    }
}
