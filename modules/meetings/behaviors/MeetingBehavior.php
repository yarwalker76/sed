<?php
namespace app\modules\meetings\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use app\modules\meetings\models\MeetingParticipants;
use app\modules\creditors\models\Request;
use app\models\AppHelper;

class MeetingBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
        ];
    }

    public function beforeValidate($event)
    {
        if ($this->owner->start_date != null) {
            $new_date_format = date('Y-m-d', strtotime($this->owner->start_date));
            $this->owner->start_date = $new_date_format;
        }

        if ($this->owner->acquaintance_start_date != null) {
            $new_date_format = date('Y-m-d', strtotime($this->owner->acquaintance_start_date));
            $this->owner->acquaintance_start_date = $new_date_format;
        }

        if ($this->owner->acquaintance_end_date != null) {
            $new_date_format = date('Y-m-d', strtotime($this->owner->acquaintance_end_date));
            $this->owner->acquaintance_end_date = $new_date_format;
        }
    }

    public function afterInsert($event)
    {
        // сохраним ID собрания после его создания для табов
        \Yii::$app->session->set('current_meet_id', $this->owner->id);

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $sql = 'SELECT c.id, c.title, cr.queue_type, cr.total_sum FROM
                        (SELECT id, title FROM creditor WHERE procedures_id = :proc_id) AS c
                     INNER JOIN (SELECT creditor_id, queue_type, SUM(total) AS total_sum 
                                   FROM creditor_request
                                  WHERE is_enabled = 1 AND queue_type in (:q31, :q32)
                                  GROUP BY creditor_id, queue_type) AS cr 
                        ON c.id = cr.creditor_id';

            $participants = \Yii::$app->db->createCommand($sql)
                ->bindValue(':proc_id', $this->owner->proc_id)
                ->bindValue(':q31', Request::QUEUE_TYPE_THIRD_GUARANTEE)
                ->bindValue(':q32', Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE)
                ->queryAll();

            foreach( $participants as $participant ) {
                $model = MeetingParticipants::findOne([
                    'meet_id' => $this->owner->id,
                    'creditor_id' => $participant['id'],
                ]);

                if( $model ) {
                    $model->setAttribute('total_sum', $model->total_sum + doubleval($participant['total_sum']));
                } else {
                    $model = new MeetingParticipants();
                    $model->setAttributes([
                        'meet_id' => $this->owner->id,
                        'registration' => 0,
                        'name' => $participant['title'],
                        'representative_name' => null,
                        'supporting_docs' => null,
                        'participant_status_id' => MeetingParticipants::STATUS_K_K,
                        'total_sum' => doubleval($participant['total_sum']),
                        'creditor_id' => $participant['id'],
                    ]);
                }

                if( $participant['queue_type'] == 3 )
                    $model->setAttributes(['sum_31' => doubleval($participant['total_sum'])]);
                else
                    $model->setAttributes(['sum_32' => doubleval($participant['total_sum'])]);

                if( !$model->save() )
                    throw new \Exception(AppHelper::makeErrorString($model->getErrors()));

                unset($model);
            }

            $transaction->commit();

        } catch(\Exception $e) {
            $transaction->rollBack();
            \Yii::$app->session->setFlash('errors', $e->getTraceAsString());
        }
    }
}