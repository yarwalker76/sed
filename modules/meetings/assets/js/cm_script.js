(function($){
    "use strict";

    var question_modal_header = 'Вопрос собрания',
        participant_modal_header = 'Участник собрания',
        voting_modal_header = 'Бюллетень голосования',
        print_bulletin_modal_header = 'Печать бюллетеня';


    $('body').on('changeTime.timepicker', '#meetings-start_time', function(e){
        var $new_time_min, $t_hours, $t_mins;

        if( (e.time.hours === 0 && e.time.minutes >= 30) || e.time.hours > 0) {
            $new_time_min = e.time.hours * 60 + e.time.minutes - 30;
            $t_hours = Math.trunc($new_time_min / 60);
            $t_mins = $new_time_min % 60;
            $('#meetings-reg_start_time').val( ($t_hours < 10 ? '0' + $t_hours.toString() : $t_hours.toString()) + ':' +
                ($t_mins < 10 ? '0' + $t_mins.toString() : $t_mins.toString()));
            $('#meetings-reg_end_time').val(e.time.value);
        }
    });

    $('body').on('change', '#meetings-start_date', function(){
        var res = $(this).val().split('.'),
            date = new Date(res[2], parseInt(res[1]) - 1, res[0]),
            day, month, year;

        date.setDate(date.getDate() - 1);

        day = ( date.getDate() < 10 ? '0' + date.getDate() : date.getDate() );
        month = ( date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1) );
        year = date.getFullYear();

        $('#meetings-acquaintance_end_date').val(day + '.' +  month + '.' + year);

        date.setDate(date.getDate() - 4);

        day = ( date.getDate() < 10 ? '0' + date.getDate() : date.getDate() );
        month = ( date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1) );
        year = date.getFullYear();

        $('#meetings-acquaintance_start_date').val(day + '.' +  month + '.' + year);
    });

    $('body').on('click', '#create-question, #pjax-questions-reestr .update_item,' +
        '#pjax-voting-questions-reestr .update_item', function(ev) {
        var $html_str = '';

        ev.preventDefault();
        $('#meeting-modal #modalHeader h3').text(question_modal_header);

        $.get($(this).attr('href'), {'meet_id': $(this).data('meet_id')}, function(data){
            $html_str = data;
            $('#meeting-modal .modal-body').html($html_str);
            $('#meeting-modal').modal('show');
        }, "html")
        .fail(function(error){
            $html_str = error.responseText;
            $('#meeting-modal .modal-body').html($html_str);
            $('#meeting-modal').modal('show');
        });
    });

    $('body').on('submit', 'form#question-form', function(ev){
        var $this = $(this);

        ev.preventDefault();
        $('#meeting-modal #modalHeader h3').text(question_modal_header);

        $.post($this.attr('action'), $this.serialize(), function(data){
            if( data.msg === 'ok' ) {
                $('#meeting-modal').modal('hide');

                if( $('#pjax-questions-reestr').length ){
                    $('#print-notifications').removeClass('disabled');

                    $.pjax.reload({
                        container: '#pjax-questions-reestr',
                        replace: false,
                        url: '/meetings/questions/index?meet_id=' +
                        $this.find('input#meetingquestions-meet_id').val(),
                    });
                }

                if( $('#pjax-voting-questions-reestr').length ) {
                    $.pjax.reload({
                        container: '#pjax-voting-questions-reestr',
                        replace: false,
                        url: '/meetings/votings',
                    });
                }

            } else {
                $('#meeting-modal .modal-body').html(data.msg);
            }
        }, 'json')
        .fail(function(error){
            //$('#meeting-modal').modal('hide');
            $('#meeting-modal .modal-body').html(error.responseText);
            //$('#meeting-modal').modal('show');
        });
    });

    $('body').on('click', '#pjax-questions-reestr .del_item', function(ev) {
        var $this = $(this);

        ev.preventDefault();
        $('#meeting-modal #modalHeader h3').text(question_modal_header);

        $.post($this.attr('href'), {}, function(data){
            if( data.msg === 'ok' ) {
                $.pjax.reload({
                    container: '#pjax-questions-reestr',
                    replace: false,
                    url: '/meetings/questions/index?meet_id=' +
                    $this.data('meet_id'),
                });

                if( data.count === '0' ) {
                    $('#print-notifications').addClass('disabled');
                }
            } else {
                $('#meeting-modal .modal-body').html(data.msg);
            }
        }, 'json')
            .fail(function(error){
                //$('#meeting-modal').modal('hide');
                $('#meeting-modal .modal-body').html(error.responseText);
                $('#meeting-modal').modal('show');
            });

        $.pjax.reload({
            container:"#pjax-questions-reestr",
            replace: false,
            url: '/meetings/questions/index?meet_id=' +
                $this.find('input#meetingquestions-meet_id').val(),
        });  //Reload GridView
    });

    $('body').on('click', '#add-participant, #pjax-participants-reestr .update_item', function(ev) {
        var $html_str = '';

        ev.preventDefault();
        $('#meeting-modal #modalHeader h3').text(participant_modal_header);

        $.get($(this).attr('href'), {'meet_id': $(this).data('meeting-id')}, function(data){
            $html_str = data;
            $('#meeting-modal .modal-body').html($html_str);
            $('#meeting-modal').modal('show');

            // init autocomplete
            $('#meetingparticipants-name').autocomplete({
                source: function(request, response){
                    var name = $('#meetingparticipants-name').val();
                    $.post('/meetings/default/get-participants', {query: name}, function(data){
                        response(data);
                    }, 'json');
                },
                minLength: 3,
                select: function(event, ui){
                }
            });
        }, "html")
        .fail(function(error){
            $html_str = error.responseText;
            $('#meeting-modal .modal-body').html($html_str);
            $('#meeting-modal').modal('show');
        });
    });

    $('body').on('click', '#update-participants', function(ev){
        var $this = $(this);

        ev.preventDefault();

        $.post($this.attr('href'), function(data){
            if( data.msg === 'ok' ) {
                $.pjax.reload({
                    container: '#pjax-participants-reestr',
                    replace: false,
                    url: '/meetings/checkin/index?meet_id=' +
                    $this.find('input#meetingparticipants-meet_id').val(),
                });
            } else {
                $('#meeting-modal .modal-body').html(data.msg);
                $('#meeting-modal').modal('show');
            }
        }, 'json')
        .fail(function(error){
            //$('#meeting-modal').modal('hide');
            $('#meeting-modal .modal-body').html(error.responseText);
            $('#meeting-modal').modal('show');
        });
    });

    $('body').on('click', '#pjax-participants-reestr .del_item', function(ev) {
        var $this = $(this);

        ev.preventDefault();
        $('#meeting-modal #modalHeader h3').text(question_modal_header);

        $.post($this.attr('href'), {}, function(data){
            if( data.msg === 'ok' ) {
                $.pjax.reload({
                    container: '#pjax-participants-reestr',
                    replace: false,
                    url: '/meetings/checkin/index?meet_id=' +
                    $this.data('meet_id'),
                });
            } else {
                $('#meeting-modal .modal-body').html(data.msg);
            }
        }, 'json')
            .fail(function(error){
                //$('#meeting-modal').modal('hide');
                $('#meeting-modal .modal-body').html(error.responseText);
                $('#meeting-modal').modal('show');
            });

        $.pjax.reload({
            container:"#pjax-participants-reestr",
            replace: false,
            url: '/meetings/questions/index?meet_id=' +
            $this.find('input#meetingquestions-meet_id').val(),
        });  //Reload GridView
    });

    $('body').on('submit', 'form#participant-form', function(ev){
        var $this = $(this);

        ev.preventDefault();
        $('#meeting-modal #modalHeader h3').text(participant_modal_header);

        $.post($this.attr('action'), $this.serialize(), function(data){
            if( data.msg === 'ok' ) {
                $('#meeting-modal').modal('hide');
                $.pjax.reload({
                    container: '#pjax-participants-reestr',
                    replace: false,
                    url: '/meetings/checkin/index?meet_id=' +
                    $this.find('input#meetingparticipants-meet_id').val(),
                });
            } else {
                $('#meeting-modal .modal-body').html(data.msg);
            }
        }, 'json')
            .fail(function(error){
                //$('#meeting-modal').modal('hide');
                $('#meeting-modal .modal-body').html(error.responseText);
                //$('#meeting-modal').modal('show');
            });
    });

    $('body').on('click', '#pjax-participants-reestr th a', function(ev){
        var $this = $(this);
        ev.preventDefault();

        $.pjax.reload({
            container: '#pjax-participants-reestr',
            replace: false,
            url: $this.attr('href').replace('/meeting/', '/')
        });
    });

    $('body').on('input', '#meetingparticipants-supporting_docs', function(){
        var chkbox = $('#meetingparticipants-registration'),
            representative_name = $('#meetingparticipants-representative_name');

        if( $(this).val().length && representative_name.val().length ) {
            chkbox.attr('disabled', false);
        } else {
            chkbox.attr('disabled', true);
            chkbox.attr('checked', false);
        }

    });

    $('body').on('input', '#meetingparticipants-representative_name', function() {
        var chkbox = $('#meetingparticipants-registration'),
            docs = $('#meetingparticipants-supporting_docs');

        if ($(this).val().length && docs.val().length) {
            chkbox.attr('disabled', false);
        } else {
            chkbox.attr('disabled', true);
            chkbox.attr('checked', false);
        }
    });

    $('body').on('keypress', '#prepare-form input, #bulletin4-candidates-voting-form input', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            e.stopPropagation();
            $(this).blur();
        }
    });

    $('body').on('click', '#pjax-voting-questions-reestr .bulletin_item', function(ev){
        var $html_str = '';

        ev.preventDefault();
        $('#meeting-modal #modalHeader h3').text(voting_modal_header + ' №' + $(this).data('type_id'));

        $.get($(this).attr('href'), {}, function(data){
            $html_str = data;
            $('#meeting-modal .modal-body').html($html_str);
            $('#meeting-modal').modal('show');
        }, "html")
            .fail(function(error){
                $html_str = error.responseText;
                $('#meeting-modal .modal-body').html($html_str);
                $('#meeting-modal').modal('show');
            });
    });

    $('body').on('submit', '#bulletin1-form, #bulletin2-form, #bulletin3-form, #bulletin4-form', function(ev){
        var $this = $(this);

        ev.preventDefault();

        $.post($this.attr('action'), $this.serialize(), function(data){
            if( data === 'ok' ) {
                $('#meeting-modal').modal('hide');
            } else {
                $('#meeting-modal .modal-body').html(data);
            }
        }, 'html')
        .fail(function(error){
            //$('#meeting-modal').modal('hide');
            $('#meeting-modal .modal-body').html(error.responseText);
            //$('#meeting-modal').modal('show');
        });
    });

    $('body').on('submit', '#meeting-conclusion-form', function(ev){
        var $this = $(this);

        ev.preventDefault();

        $.post($this.attr('action'), $this.serialize(), function(data){
            $('#meeting-modal .modal-body').html(data);
            $('#meeting-modal').modal('show');
        }, 'html')
        .fail(function(error){
            $('#meeting-modal .modal-body').html(error.responseText);
            $('#meeting-modal').modal('show');
        });
    });

    // подсчет итогов голосования при смене вариантов Бюллетень №1
    $('body').on('change', 'form#bulletin1-form .bulletin1-radiolist :radio', function(ev){
        var results = {},
            result_str = '';

        $('form#bulletin1-form .bulletin1-radiolist:first-of-type :radio').each(function(){
            results[$(this).parent('label').text()] = {'percent': 0, 'money': 0};
        });

        $('form#bulletin1-form .bulletin1-radiolist :radio:checked').each(function(){
            results[$(this).parent('label').text()].percent += parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.'));
            results[$(this).parent('label').text()].money += parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''));
        });

        $.each(results, function(k, v){
            result_str += k + ' - ' + v.percent + '% (' + v.money + ' руб.)<br>';
        });

        $('#bulletin1-voting-result').html(result_str);
    });

    // подсчет итогов голосования при смене вариантов Бюллетень №3
    $('body').on('change', 'form#bulletin3-form .bulletin3-dropdown', function(ev){
        var results = {},
            result_str = '';

        $('form#bulletin3-form .bulletin3-dropdown').each(function(){
            var selected = $(this).find('option:selected')[0].label;

            if(results.hasOwnProperty(selected)) {
                results[selected].percent += parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.'));
                results[selected].money += parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''));
            } else {
                results[selected] = { 'percent': parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.')),
                    'money': parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''))};
            }
        });

        $.each(results, function(k, v){
            result_str += k + ' - ' + v.percent + '% (' + v.money + ' руб.)\n';
        });

        $('#bulletin3-voting-result').val(result_str);
    });

    // добавляем вариант голосования в бюллетене №2
    $('body').on('submit', '#bulletin-variant-form', function(ev){
        var $this = $(this);

        ev.preventDefault();

        if( $('#meetingvotingoptions-value').val().length ) {
            if( ! $('#bulletin4-wrapper').length ||
                ( $('#bulletin4-wrapper').length && $('#variants-list > div').length < 16 ) ) {
                $.post($this.attr('action'), $this.serialize(), function(data){
                    if( data.msg === 'ok' ) {
                        $.pjax.reload({
                            container: '#pjax-variants-reestr',
                            replace: false,
                            url: '/meetings/votings-options/index?question_id=' + $this.find('#meetingvotingoptions-question_id').val()
                        });

                        if( $('.bulletin2-dropdown').length ) {
                            $('.bulletin2-dropdown').each(function(){
                                $(this).append($("<option></option>")
                                    .attr("value", data.variant_id)
                                    .text(data.variant_value));
                            });
                        }

                        if( $('.bulletin4-dropdown').length ) {
                            $('.bulletin4-dropdown').each(function(){
                                $(this).append($("<option></option>")
                                    .attr("value", data.variant_id)
                                    .text(data.variant_value));
                            });
                        }

                        $this.find('#meetingvotingoptions-value').val('');
                    } else {
                        $('#meetingvotingoptions-value')
                            .next('.help-block')
                            .text(data.msg);
                    }
                }, 'json')
                    .fail(function(error){
                        //$('#meeting-modal').modal('hide');
                        $('#meeting-modal .modal-body').html(error.responseText);
                        //$('#meeting-modal').modal('show');
                    });
            } else {
                $('#bulletin-variant-form')
                    .append('<div class="alert alert-danger" role="alert">' +
                        '       <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                        '           <span aria-hidden="true">&times;</span>' +
                        '       </button>' +
                        '       Вы не можете добавить кандидатов больше выбранного числа.</div>');
            }
        }
    });

    $('body').on('click', '.delete_variant',  function(ev){
        var $this = $(this),
            id = $this.attr('href').substring($this.attr('href').indexOf('=')+1);

        ev.preventDefault();

        $.post($this.attr('href'), {'id': id}, function(data){
            if( data.msg === 'ok' ) {
                $.pjax.reload({
                    container: '#pjax-variants-reestr',
                    replace: false,
                    url: '/meetings/votings-options/index?question_id=' + data.question_id
                });

                $('.bulletin2-dropdown').each(function(){
                    $(this).find('option[value=' + id + ']').remove();
                });

            } else {
                $('#meetingvotingoptions-value')
                    .next('.help-block')
                    .text(data.msg);
            }
        }, 'json')
        .fail(function(error){
            //$('#meeting-modal').modal('hide');
            $('#meeting-modal .modal-body').html(error.responseText);
            //$('#meeting-modal').modal('show');
        });

    });

    $('body').on('click', '.update_variant', function(ev){
        var $this = $(this),
            parent = $this.closest('.variants-item'),
            variant_name = parent.children('strong'),
            id = $this.attr('href').substring($this.attr('href').indexOf('=')+1);

        ev.preventDefault();

        if( !$('.edit-variant').length ){
            ev.preventDefault();
            variant_name.hide();

            $('<input type="text" data-id="' + id + '" class="form-control edit-variant" value="' + variant_name.text() + '" />').appendTo(parent);
            $('.edit-variant').focus();
        }

    });

    $('body').on('blur', '.edit-variant', function(){
        var $this = $(this),
            parent = $this.closest('.variants-item'),
            variant_name = parent.children('strong');

        $this.remove();
        variant_name.show();
    });

    $('body').on('keypress', '.edit-variant', function (e) {
        if (e.which === 13) {
            var $this = $(this),
                id = $this.data('id'),
                new_value = $this.val();

            e.preventDefault();
            e.stopPropagation();

            $.post('/meetings/votings-options/update', {'id': id, 'value': new_value}, function(data){
                if( data.msg === 'ok' ) {
                    $.pjax.reload({
                        container: '#pjax-variants-reestr',
                        replace: false,
                        url: '/meetings/votings-options/index?question_id=' + data.question_id
                    });

                    $('.bulletin2-dropdown').each(function(){
                        $(this).find('option[value=' + id + ']').text(new_value);
                    });

                } else {
                    $('#meetingvotingoptions-value')
                        .next('.help-block')
                        .text(data.msg);
                }
            }, 'json')
            .fail(function(error){
                //$('#meeting-modal').modal('hide');
                $('#meeting-modal .modal-body').html(error.responseText);
                //$('#meeting-modal').modal('show');
            });
        }
    });

    // подсчет итогов голосования при смене вариантов Бюллетень №2
    $('body').on('change', 'form#bulletin2-form .bulletin2-dropdown', function(ev){
        var results = {},
            result_str = '';

        $('form#bulletin2-form .bulletin2-dropdown').each(function(){
            var selected = $(this).find('option:selected')[0].label;

            if(results.hasOwnProperty(selected)) {
                results[selected].percent += parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.'));
                results[selected].money += parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''));
            } else {
                results[selected] = { 'percent': parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.')),
                    'money': parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''))};
            }
        });

        $.each(results, function(k, v){
            result_str += k + ' - ' + v.percent + '% (' + v.money + ' руб.)\n';
        });

        $('#bulletin2-voting-result').val(result_str);
    });

    // подсчет итогов голосования при смене вариантов Бюллетень №4
    $('body').on('change', 'form#bulletin4-form .bulletin4-dropdown', function(ev){
        var results = {},
            result_str = '';

        $('form#bulletin4-form .bulletin4-dropdown').each(function(){
            var selected = $(this).find('option:selected')[0].label;

            if(results.hasOwnProperty(selected)) {
                results[selected].percent += parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.'));
                results[selected].money += parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''));
            } else {
                results[selected] = { 'percent': parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.')),
                    'money': parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''))};
            }
        });

        $.each(results, function(k, v){
            result_str += k + ' - ' + v.percent + '% (' + v.money + ' руб.)\n';
        });

        $('#bulletin4-voting-result').val(result_str);
    });

    $('body').on('click', 'button.candidates-choice', function(ev){
        var $this = $(this),
            parent_tr = $this.closest('tr');

        ev.preventDefault();

        if( parent_tr.next('tr').find('.candidates-voting-form').length ) {
            $('.candidates-voting-form')
                .slideUp(function(){
                    $(this).closest('tr').remove();
                });
        } else {
            $('.candidates-voting-form')
                .slideUp(function(){
                    $(this).closest('tr').remove();
                });

            $.post($this.data('url'), {
                'participant_id': $this.data('participant_id'),
                'participant_sum': parseFloat($this.closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, '')),
                'question_id': $('#bulletin4form-question_id').val(),
                'multiplier': parseInt($('.bulletin4-candidates-cnt-dropdown option:selected').text()),
                'total_sum': parseFloat($('#total_sum').text().replace(/[,]+/g, '.').replace(/\s/g, '')),
            }, function(data){
                parent_tr.after('<tr><td colspan="4" style="padding: 0"><div class="candidates-voting-form"></div></td></tr>');

                $('.candidates-voting-form')
                    .html(data)
                    .slideDown();
            }, 'html')
            .fail(function(error){
                //$('#meeting-modal').modal('hide');
                // $('#meeting-modal .modal-body').html(error.responseText);
                //$('#meeting-modal').modal('show');
            });
        }
    });

    $('body').on('keyup', '.candidate-voice', function(e) {
        var $this = $(this),
            form = $this.closest('form'),
            creditor_voice_sum = parseInt($('#creditor_voice_sum').text());

        $('.candidate-voice').each(function(){
            creditor_voice_sum -= $(this).val() ? parseInt($(this).val()) : 0;
        });

        if( creditor_voice_sum === 0 ) {
            $('#save-candidates-voting').prop('disabled', false);
        } else {
            $('#save-candidates-voting').prop('disabled', true);
        }

        if( creditor_voice_sum < 0 ) {
            $this.closest('.form-group').addClass('has-error');
            //$('#save-candidates-voting').prop('disabled', true);
            if( !$('.alert-danger').length ) {
                $('.candidates-voting-form .well').after('<div class="alert alert-danger" role="alert">' +
                    '       <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '           <span aria-hidden="true">&times;</span>' +
                    '       </button>' +
                    '       Вы превысили общую сумму голосов кредитора.</div>');
            }

        } else {
            form.find('.form-group.has-error').removeClass('has-error');
            //$('#save-candidates-voting').prop('disabled', true);
            $('.alert-danger').fadeIn(350, function(){ $(this).remove(); });
        }
    });

    /*$('body').on('input', '.candidate-voice', function(ev){
        var creditor_voice_sum = parseInt($('#creditor_voice_sum'));
        this.value = this.value.replace(/[^\d\.\-]/g,'');
    });*/

    $('body').on('keydown', '.candidate-voice', function(e){
        if( (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) ){
            return;
        }
    });


    $('body').on('click', '#save-candidates-voting', function(ev){
        var $this = $(this),
            form = $this.closest('form');

        ev.preventDefault();

        $.post(form.attr('action'), form.serialize(), function(data){
            if( data === 'ok' ) {
                $('.candidates-voting-form')
                    .slideUp(function(){
                        $(this).closest('tr').remove();
                    });

                $.pjax.reload({
                    container: '#pjax-variants-reestr',
                    replace: false,
                    url: '/meetings/votings-options/index?question_id=' + form.find('#candidatesvotingform-question_id').val()
                });
            } else {
                $('.candidates-voting-form .well').after('<div class="alert alert-danger" role="alert">' +
                    '       <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '           <span aria-hidden="true">&times;</span>' +
                    '       </button>' + data + '</div>');
            }
        }, 'html')
        .fail(function(error){
            $('.candidates-voting-form .well').after('<div class="alert alert-danger" role="alert">' +
                '       <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '           <span aria-hidden="true">&times;</span>' +
                '       </button>' + error.responseText + '</div>');
        });
    });

    $('#creditors-list input:checkbox').on('change', function(){
        if( $('#creditors-list input:checkbox:checked').length ) {
            $('.prepare_notices, .prepare_envelopes, .prepare_post_notices, .add-outgoing-docs :checkbox').attr('disabled', false);
        } else {
            $('.prepare_notices, .prepare_envelopes, .prepare_post_notices, .add-outgoing-docs :checkbox').attr('disabled', true);
        }
    });

    if( $('#print-section #creditors-list input:checkbox:checked').length ) {
        $('.prepare_notices, .prepare_envelopes, .prepare_post_notices, .add-outgoing-docs :checkbox').attr('disabled', false);
    }

})(jQuery);