<?php
namespace app\modules\meetings\services;

use app\modules\creditors\models\Creditor;
use app\modules\meetings\models\ProtocolVotingResultsView;
use app\modules\meetings\models\ReportTemplate;
use app\modules\printdocs\models\DocTemplates;
use app\modules\printdocs\models\forms\LetterForm;
use Yii;
use \yii\base\Object;
use app\modules\meetings\models\MeetingQuestions;
use PhpOffice\PhpWord\TemplateProcessor;
use app\modules\procedures\models\Procedures;
use app\models\TblOrganizationsMethods;
use app\models\AppHelper;
use DocxMerge\DocxMerge;
use app\modules\meetings\models\BulletinTemplate;
use app\modules\documents\models\OutgoingDocs;
use app\modules\meetings\models\MeetingParticipants;
use app\modules\meetings\models\Meetings;
use app\modules\meetings\models\MeetingVotings;
use app\modules\meetings\models\NoticeTemplate;
use app\models\TblValues;
use app\modules\meetings\models\RegJournalTemplate;

use app\components\petrovich\Petrovich;
use app\components\petrovich\traits\TraitPetrovich;
use yii\helpers\ArrayHelper;


class PrintService extends Object
{
    use TraitPetrovich;

    const ENVELOPE_GROUP_ID = 6;

    public $template_path;
    private $_months = ["нулября", "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];
    private $_question_serial = [['первый', 'первому'], ['второй', 'второму'], ['третий', 'третьему'], ['четвертый', 'четвертому'],
        ['пятый', 'пятому'], ['шестой', 'шестому'], ['седьмой', 'седьмому'], ['восьмой', 'восьмому'], ['девятый', 'девятому'], ['десятый', 'десятому']];

    public function __construct(array $config = [])
    {
        $this->template_path = Yii::$app->params['uploadPath'] . 'templates/meetings/';

        parent::__construct($config);
    }

    public function getQuestionSerialNumber($id)
    {
        $sql = 'SELECT q.rownum FROM
                    (SELECT @rownum := @rownum + 1 AS rownum, id 
                       FROM meeting_questions,
                            (SELECT @rownum := 0) r 
                      WHERE meet_id = :meet_id ORDER BY id) AS q
                 WHERE q.id = :question_id';

        $params = [
            ':question_id' => $id,
            ':meet_id' => Yii::$app->session->get('current_meet_id'),
        ];

        return Yii::$app->db->createCommand($sql, $params)->queryScalar();
    }

    public function makeEnvelopeTemplates($form_model)
    {
        $templates_list = [];
        $procedure = AppHelper::getCurrentProcedure();

        foreach( $form_model->creditors_list as $participant_id => $value ) {
            if( $value ) {
                $printTemplateParams = $this->initEnvelopeTemplateParams($procedure, $participant_id);

                $tmp_path = $this->makeEnvelopeTemplate($printTemplateParams, $participant_id);
                !$tmp_path || $templates_list[] = $tmp_path;

            }
        }

        if( !empty($templates_list) )
            $this->mergeAndSave($templates_list, 'Конверты.docx');
    }

    public function makeBulletinTemplates($question_id)
    {
        $question = MeetingQuestions::findOne($question_id);
        $meeting = $question->meet;

        $printTemplateParams = $this->initBulletinTemplateParams($question, $meeting);

        $templates_list = [];
        foreach( $meeting->participants as $p ) {
            $this->addBulletinParticipantParams($printTemplateParams, $p, $question);
            $tmp_path = $this->makeBulletinTemplate($printTemplateParams, $question->bulletin_form_id, $p->id, $question->id);
            if( $tmp_path )
                $templates_list[] = $tmp_path;
        }

        if( !empty($templates_list) )
            $this->mergeAndSave($templates_list, 'Бюллетень по вопросу №' . $question->bulletin_form_id . '.docx');
    }

    public function makeNoticeTemplates($form_model)
    {
        $templates_list = [];
        $procedure = AppHelper::getCurrentProcedure();
        //$meeting = Meetings::findOne($form)

        //if( $procedure->type_id == 3 ) {
        $printTemplateParams = $this->initNoticeTemplateParams($procedure);

        foreach( $form_model->creditors_list as $participant_id => $value ) {
            if( $value ) {
                $this->addNoticeParticipantParams($printTemplateParams, $participant_id);

                !$form_model->add_outgoing_docs || $printTemplateParams->outgoing_letter_number = $this->getOutgoingNextNumber();

                $tmp_path = $this->makeNoticeTemplate($printTemplateParams, ($procedure->type)->type_abbr, $participant_id);
                if( $tmp_path ) {
                    $templates_list[] = $tmp_path;

                    if( $form_model->add_outgoing_docs )
                        # создаем исходящее сообщение
                        $this->createOutgoingDoc($printTemplateParams->addressee, $printTemplateParams->addressee_post_address);
                }

            }
        }
        //}

        if( !empty($templates_list) )
            $this->mergeAndSave($templates_list, 'Уведомление.docx');
    }

    public function makeRegJournalTemplate()
    {
        $printTemplateParams = $this->initRegJournalTemplateParams();

        try {
            $tpl = DocTemplates::find()
                ->where(['group_id' => 8, 'template_type' => 'Журнал'])
                ->one();

            $templateProcessor = new TemplateProcessor(Yii::getAlias('@webroot') . '/' . $tpl->path);
            $tpl_mapping = $this->getRegJournalTemplateMapping();

            foreach($tpl_mapping as $key => $alias)
                if( !is_array($printTemplateParams->{$key}) ){
                    $templateProcessor->setValue($alias, $printTemplateParams->{$key});
                } else {
                    if( $key == 'participants_list' ) {
                        $templateProcessor->cloneRow('Участник№пп', count($printTemplateParams->{$key}));
                        $index = 0;
                        foreach( $printTemplateParams->{$key} as $k => $participant ):
                            $templateProcessor->setValue('Участник№пп#' . ($index + 1), ($index + 1));
                            $templateProcessor->setValue('Участник#' . ($index + 1), $participant['name']);
                            $templateProcessor->setValue('УчастникСтатусСокр#' . ($index + 1), $participant['short_name']);
                            $templateProcessor->setValue('УчастникАдрес#' . ($index + 1), $participant['address']);
                            $templateProcessor->setValue('ПредставительФИО#' . ($index + 1), $participant['representative_name']);
                            $templateProcessor->setValue('ПредставительПодтверждДок#' . ($index + 1), $participant['supporting_docs']);
                            $templateProcessor->setValue('СумВсехТребований#' . ($index + 1), $participant['request_total_sum']);
                            $templateProcessor->setValue('СумГолосов#' . ($index + 1), $participant['total_sum']);

                            $index++;
                        endforeach;
                    }
                }

            $path = $this->template_path . 'Журнал_Регистрации.docx';
            !file_exists($path) || unlink($path);
            $templateProcessor->saveAs($path);

            $this->_file_force_download($path);
        }
        catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
            // do nothing
            return false;
        }
    }

    private function initRegJournalTemplateParams()
    {
        $procedure = AppHelper::getCurrentProcedure();
        $org = TblOrganizationsMethods::getOrganization( $procedure->organization_id );
        $org_params = $this->makeAssociativeArray(TblOrganizationsMethods::getParams( $procedure->organization_id ));


        # Собрание
        $meeting = Meetings::findOne(Yii::$app->session->get('current_meet_id'));

        $model = new RegJournalTemplate([
            'reg_start_time' => $this->getCursiveTime(substr($meeting->reg_start_time, 0, 5)),
            'reg_end_time' => $this->getCursiveTime(substr($meeting->reg_end_time, 0, 5)),
            'pd_name' => $org_params['Название'],
            'pd_legal_address' => $this->buildAddress([
                @$org['address_0_postcode'],
                @$org['address_0_region'],
                @$org['address_0_locality'],
                $org['address_0_address']
            ]),
            'pd_inn' => $org_params['ИНН'],
            'pd_ogrn' => $org_params['ОГРН'],
            'meeting_address' => $meeting->meet_address,
            'au_full_fio' => AppHelper::getFIO( $procedure->manager_id ),
        ]);



        $model->participants_list = MeetingHelper::getRegJournalParticipants();

        return $model;
    }

    public function makeReportTemplate($meeting_id)
    {
        $meeting = Meetings::findOne($meeting_id);
        $procedure = AppHelper::getCurrentProcedure();

        $printTemplateParams = $this->initReportTemplateParams($procedure, $meeting);

        try {
            $tpl = DocTemplates::find()
                ->where(['group_id' => 8, 'template_type' => 'Протокол'])
                ->andWhere(['like', 'proc_type', ($procedure->type)->type_abbr])
                ->one();

            $templateProcessor = new TemplateProcessor(Yii::getAlias('@webroot') . '/' . $tpl->path);
            $tpl_mapping = $this->getReportTemplateMapping();

            foreach($tpl_mapping as $key => $alias) {
                if( !is_array($printTemplateParams->{$key}) ){
                    $templateProcessor->setValue($alias, $printTemplateParams->{$key});
                } else {
                    // участники с правом голоса
                    if( $key == 'participants_with_voices' ) {
                        $templateProcessor->cloneRow('УчастникПГ№пп', count($printTemplateParams->{$key}));
                        $index = $reg_sum = $total_sum = 0;
                        foreach($printTemplateParams->{$key} as $alias => $value):
                            $templateProcessor->setValue('УчастникПГ№пп#' . ($index + 1), ($index + 1));
                            $templateProcessor->setValue('УчастникПГ#' . ($index + 1), $value['participant']);
                            $templateProcessor->setValue('УчастникЗарегТребования#' . ($index + 1), $value['reg_sum']);
                            $templateProcessor->setValue('УчастникОбщиеТребования#' . ($index + 1), $value['total_sum']);

                            $index++;
                            $reg_sum += $value['reg_sum'];
                            $total_sum += $value['total_sum'];
                        endforeach;

                        $templateProcessor->setValue('ИтогЗарегТребования', $reg_sum);
                        $templateProcessor->setValue('ИтогОбщиеТребования', $total_sum);
                    }

                    // участники без права голоса
                    if( $key == 'participants_without_voices' ) {
                        if( count($printTemplateParams->{$key}) ) {
                            $templateProcessor->cloneRow('УчастникБПГ№пп', count($printTemplateParams->{$key}));
                            $index = 0;
                            foreach($printTemplateParams->{$key} as $alias => $value):
                                $templateProcessor->setValue('УчастникБПГ№пп#' . ($index + 1), ($index + 1));
                                $templateProcessor->setValue('УчастникБПГ#' . ($index + 1), $value);

                                $index++;
                            endforeach;
                        } else {
                            $templateProcessor->setValue('УчастникБПГ№пп', 1);
                            $templateProcessor->setValue('УчастникБПГ', 'Отсутствуют');
                        }
                    }

                    // реестр вопросов
                    if( $key == 'questions_list' ) {
                        $templateProcessor->cloneRow('Вопрос№пп', count($printTemplateParams->{$key}));
                        $index = 0;
                        foreach($printTemplateParams->{$key} as $alias => $value):
                            $templateProcessor->setValue('Вопрос№пп#' . ($index + 1), ($index + 1));
                            $templateProcessor->setValue('Вопрос#' . ($index + 1), $value);

                            $index++;
                        endforeach;
                    }

                    // реестр вопрос-решение
                    if( $key == 'voting_results' ) {
                        $templateProcessor->cloneRow('ВопросПорядНомер', count($printTemplateParams->questions_list));
                        $index = 0;

                        foreach($printTemplateParams->{$key} as $k => $row) {
                            $options_list_str = $voting_results_str = '';
                            $templateProcessor->setValue('ВопросПорядНомер#' . ($index + 1), mb_strtoupper($this->_question_serial[$index][0]));
                            $templateProcessor->setValue('ВопросГолосования#' . ($index + 1), $row['question']);
                            $templateProcessor->setValue('ВопросОбсуждение#' . ($index + 1), $row['discussion']);
                            $templateProcessor->setValue('ВопросПорядНомерДП#' . ($index + 1), $this->_question_serial[$index][1]);
                            $templateProcessor->setValue('ПринятоеРешение#' . ($index + 1), $row['final_decision']);

                            foreach( $row['options'] as $i =>$option ) {
                                $options_list_str .= ($i + 1) . '. ' . $option['name'] . ".<w:br/>";
                                $voting_results_str .= '"' . $option['name'] .'" - ' . $option['request_sum'] .
                                    ' (' . $this->num2str($option['request_sum'], 'voices') . ') голосов, ' .
                                    'что составило ' . $option['request_percent'] . '% от числа голосов ' .
                                    "конкурсных кредиторов, присутствующих на собрании.<w:br/><w:br/>";
                            }

                            $templateProcessor->setValue('ВариантыГолосования#' . ($index + 1), $options_list_str);
                            $templateProcessor->setValue('ИтогиГолосования#' . ($index + 1), $voting_results_str);

                            $index++;
                        }

                        /*$index = 0;
                        $current_question_id = -1;
                        $str = '';
                        $options = [];
                        $options_list_str = '';
                        $voting_results_str = '';
                        $final_decision = '';
                        foreach($printTemplateParams->{$key} as $k => $row):
                            if( $current_question_id != $row['question_id'] ) {
                                if( $index ) {
                                    foreach( $options as $i => $option ) {
                                        $options_list_str .= ($i + 1) . '. ' . $option['name'] . ".\n";
                                        $voting_results_str .= '"' . $option['name'] .'" - ' . $option['request_sum'] .
                                                               ' (' . $this->num2str($option['request_sum'], false) . ') голосов, ' .
                                                               'что составило ' .$option['request_percent'] . '% от числа голосов ' .
                                                                "конкурсных кредиторов, присутствующих на собрании.\n\n";
                                    }

                                    $str .= $options_list_str . "\n" .
                                            "   Лицам, участвующим в собрании с правом голоса, розданы бюллетени для голосования.\n" .
                                            "   Произведено голосование по " . $this->_question_serial[$index - 1][1] . " вопросу.\n" .
                                            "   Председателем собрания кредиторов " . $printTemplateParams->pd_name . " собраны бюллетени, " .
                                            "произведен подсчет голосов, оглашены следующие итоги голосования по данному вопросу повестки дня:\n\n" .
                                            $voting_results_str . "\n" .
                                            "ПРИНЯТОЕ РЕШЕНИЕ:\n" .
                                            $final_decision . "\n\n";

                                    $templateProcessor->setValue('ВопросРешение#' . $index, $str);
                                    $str = '';
                                    $options = [];
                                    $options_list_str = '';
                                    $voting_results_str = '';
                                    $final_decision = '';

                                }

                                // формируем шаблон резульата голосования по вопросу
                                $current_question_id = $row['question_id'];
                                $str = mb_strtoupper($this->_question_serial[$index][0]) . ' ВОПРОС: ' . $row['question'] . "\n\n";


                                $str .= $row['discussion'] . "\n";
                                $str .= "На голосование поставлен вопрос:\n" . $row['question'] . "\n";
                                $str .= "Варианты решений, включенные в бюллетени для голосования по " . $this->_question_serial[$index][1] . " вопросу повестки дня:\n";
                                $final_decision = $row['final_decision'];
                                $options[] = [
                                                'name' => $row['option_name'],
                                                'request_sum' => round($row['request_sum'], 2),
                                                'request_percent' => round($row['request_percent'], 2),
                                             ];
                                $index++;
                            } else {
                                $options[] = [
                                    'name' => $row['option_name'],
                                    'request_sum' => round($row['request_sum'], 2),
                                    'request_percent' => round($row['request_percent'], 2),
                                ];
                            }
                        endforeach;

                        // последний вопрос
                        $templateProcessor->setValue('ВопросРешение#' . $index, $str);*/
                    }

                    // реестр вопрос-кворум-вывод
                    if( $key == 'questions_quorum' ) {
                        $templateProcessor->cloneRow('Кворум№пп', count($printTemplateParams->{$key}));
                        $index = 0;
                        foreach( $printTemplateParams->{$key} as $k => $row ):
                            $templateProcessor->setValue('Кворум№пп#' . ($index + 1), ($index + 1));
                            $templateProcessor->setValue('КворумВопрос#' . ($index + 1), $row['question']);
                            $templateProcessor->setValue('КворумЗначение#' . ($index + 1), $row['quorum'] . '%');
                            $templateProcessor->setValue('КворумВывод#' . ($index + 1), $row['quorum_decision']);

                            $index++;
                        endforeach;
                    }
                }
            }


            $path = $this->template_path . 'Протокол.docx';
            !file_exists($path) || unlink($path);
            $templateProcessor->saveAs($path);

            $this->_file_force_download($path);
        }
        catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
            // do nothing
            return false;
        }
    }

    private function initReportTemplateParams($procedure, $meeting)
    {
        $org = TblOrganizationsMethods::getOrganization( $procedure->organization_id );
        $org_params = $this->makeAssociativeArray(TblOrganizationsMethods::getParams( $procedure->organization_id ));
        $court_params = $this->makeAssociativeArray(TblOrganizationsMethods::getParams( $procedure->court_id ));

        # АУ
        $arr = TblOrganizationsMethods::getOrganization( $procedure->manager_id );
        $au_full_fio = AppHelper::getFIO( $procedure->manager_id );
        list($lastname, $firstname, $patronymic) = explode(' ', $au_full_fio);
        $au_short_fio = $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.';

        // АУСРО
        $au_cro = '';
        if( $cro_id = TblValues::find()->where(['params_id' => 117, 'organizations_id' => $procedure->manager_id])->one()->value ):
            if( is_object(TblValues::find()->where(['params_id' => 106, 'organizations_id' => $cro_id])->one()) ):
                $au_cro = TblValues::find()->where(['params_id' => 106, 'organizations_id' => $cro_id])->one()->value;
            else:
                $au_cro = '';
            endif;
        endif;

        // Кредиторы
        $creditors_cnt = MeetingParticipants::getParticipantsCount();
        $registered_creditors_count = MeetingParticipants::getRegisteredParticipantsCount();

        // Суммы требований
        $total_request_sum = MeetingHelper::getAllRequestsTotalSum();
        $reg_request_sum = MeetingParticipants::getRegisteredRequestTotalSum();
        $quorum_percent = round($reg_request_sum / $total_request_sum * 100, 2);

        $model = new ReportTemplate([
            'current_date' => date('d.m.Y'),
            'meeting_number' => $meeting->serial_number,
            'proc_type' => ($procedure->type)->type_abbr,
            'pd_name' => $org_params['Название'],
            'pd_short_name' => $org_params['Сокр. наименование'],
            'pd_full_name' => $org_params['Полное наименование'],
            'pd_legal_address' => $this->buildAddress([
                @$org['address_0_postcode'],
                @$org['address_0_region'],
                @$org['address_0_locality'],
                $org['address_0_address']
            ]),
            'case_number' => $procedure->case_number,
            'court' => $court_params['Название'],
            'meeting_address' => $meeting->meet_address,
            'meeting_start_date' => date('d.m.Y', strtotime($meeting->start_date)),
            'meeting_start_time' => substr($meeting->start_time, 0, 5),
            'au_short_fio' => $au_short_fio,
            'reg_date' => $this->getCursiveDate(date('d.m.Y', strtotime($meeting->start_date))),
            'reg_start_time' => $this->getCursiveTime(substr($meeting->reg_start_time, 0, 5)),
            'reg_end_time' => $this->getCursiveTime(substr($meeting->reg_end_time, 0, 5)),
            'au_cro' => $au_cro,
            'meeting_acq_start_date' => $this->getCursiveDate(date('d.m.Y', strtotime($meeting->acquaintance_start_date))),
            'meeting_acq_end_date' => $this->getCursiveDate(date('d.m.Y', strtotime($meeting->acquaintance_end_date))),
            'meeting_acq_start_time' => $this->getCursiveTime(substr($meeting->acquaintance_start_time, 0, 5)),
            'meeting_acq_end_time' => $this->getCursiveTime(substr($meeting->acquaintance_end_time, 0, 5)),
            'meeting_acquaintance_address' => $meeting->acquaintance_address,

            'creditors_cnt' => $creditors_cnt,
            'creditors_cnt_cursive' => $this->num2str($creditors_cnt, 'digits'),
            'reg_creditors_cnt' => $registered_creditors_count,
            'reg_creditors_cnt_cursive' => $this->num2str($registered_creditors_count, 'digits'),

            'total_request_sum' => $total_request_sum,
            'total_request_sum_cursive' => $this->num2str($total_request_sum),

            'reg_request_sum' => $reg_request_sum,
            'reg_request_sum_cursive' => $this->num2str($reg_request_sum),
            'quorum_percent' => $quorum_percent,
            'participants_requests_total_sum' => MeetingParticipants::getParticipantsRequestsTotalSum(),
        ]);

        $this->addListableProtocolParams($model);

        return $model;
    }

    private function initEnvelopeTemplateParams($procedure, $participant_id)
    {
        $participant = MeetingParticipants::findOne($participant_id);
        $organization = TblOrganizationsMethods::getOrganization($procedure->manager_id);
        $creditor = Creditor::findOne($participant->creditor_id);
        $creditor_org = TblOrganizationsMethods::getOrganization($creditor->organization_id);

        $model = new LetterForm([
            'senderTitle' => AppHelper::getFIO($procedure->manager_id),
            'senderAddress' => $this->buildAddress([
                @$organization['address_2_address'],
                @$organization['address_2_region'],
                @$organization['address_2_locality'],
            ]),
            'senderZip' => $organization['address_2_postcode'],

            'receiverTitle' => $participant->name,
            'receiverZip' => $creditor_org['address_2_postcode'],
            'receiverAddress' => $this->buildAddress([
                @$creditor_org['address_2_address'],
                @$creditor_org['address_2_region'],
                @$creditor_org['address_2_locality'],
            ]),
        ]);

        return $model;
    }

    private function buildAddress($attributes)
    {
        $res = '';

        foreach ($attributes as $key => $value) {
            $comma = ', ';

            if ($key === count($attributes) - 1) {
                $comma = '';
            }

            if ($value) {
                $res .= $value . $comma;
            }
        }

        return $res;
    }

    private function initNoticeTemplateParams($procedure)
    {
        $org = TblOrganizationsMethods::getOrganization( $procedure->organization_id );
        $params = $this->makeAssociativeArray(TblOrganizationsMethods::getParams( $procedure->organization_id ));

        $params['pd_legal_address'] = ( $org['address_0_postcode'] ? $org['address_0_postcode'] . ', ' : '' ) .
            ( $org['address_0_region'] ? $org['address_0_region'] . ', ' : '' ) .
            ( $org['address_0_locality'] ? $org['address_0_locality'] . ', ' : '' ) .
            $org['address_0_address'];

        # АУ
        $arr = TblOrganizationsMethods::getOrganization( $procedure->manager_id );
        $au_full_fio = AppHelper::getFIO( $procedure->manager_id );
        list($lastname, $firstname, $patronymic) = explode(' ', $au_full_fio);
        $au_short_fio = $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.';
        $au_komu = $this->getDativeShortFIO($procedure->manager_id);

        # Судья
        list($lastname, $firstname, $patronymic) = explode(' ', AppHelper::getFIO( $procedure->judge_id ));
        $judge_short_fio = $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.';

        # Собрание
        $meeting = Meetings::findOne(Yii::$app->session->get('current_meet_id'));

        $model = new NoticeTemplate([
            'pd_name' => $params['Название'],
            'pd_short_name' => $params['Сокр. наименование'],
            'pd_inn' => $params['ИНН'],
            'pd_ogrn' => $params['ОГРН'],
            'pd_legal_address' => $params['pd_legal_address'],
            'au_full_fio' => $au_full_fio,
            'au_short_fio' => $au_short_fio,
            'au_post_address' => ( $arr['address_2_postcode'] ? $arr['address_2_postcode'] . ', ' : '' ) .
                                 ( $arr['address_2_region'] ? $arr['address_2_region'] . ', ' : '' ) .
                                 ( $arr['address_2_locality'] ? $arr['address_2_locality'] . ', ' : '' ) .
                                 $arr['address_2_address'],
            'au_cell_phone' => $arr['mobile'],
            'au_email' => $arr['email'],
            'au_komu' => $au_komu,
            'case_number' => $procedure->case_number,
            'judge_short_fio' => $judge_short_fio,
            'meeting_start' => date('d.m.Y', strtotime($meeting->start_date)) . 'г. в ' .
                substr($meeting->start_time, 0, 5),
            'meeting_address' => $meeting->meet_address,
            'meeting_register_period' => date('d.m.Y', strtotime($meeting->start_date)) . ' года с ' .
                substr($meeting->reg_start_time, 0, 5) . ' до ' .
                substr($meeting->reg_end_time, 0, 5),
            'meeting_acquaintance_date_period' => date('d.m.Y', strtotime($meeting->acquaintance_start_date)) .
                ' года по ' . date('d.m.Y', strtotime($meeting->acquaintance_end_date)) .' года ',
            'meeting_acquaintance_time_period' => substr($meeting->acquaintance_start_time, 0, 5) . ' до ' .
                substr($meeting->acquaintance_end_time, 0, 5),
            'meeting_acquaintance_address' => $meeting->acquaintance_address,
            'questions_list' => $meeting->questions
        ]);

        return $model;
    }

    private function initBulletinTemplateParams($question, $meeting)
    {
        $procedure = Procedures::findOne(AppHelper::getLastSeenProcedureID());
        $org = TblOrganizationsMethods::getOrganization( $procedure->organization_id );
        $params = $this->makeAssociativeArray(TblOrganizationsMethods::getParams( $procedure->organization_id ));

        $params['pd_legal_address'] = ( $org['address_0_postcode'] ? $org['address_0_postcode'] . ', ' : '' ) .
            ( $org['address_0_region'] ? $org['address_0_region'] . ', ' : '' ) .
            ( $org['address_0_locality'] ? $org['address_0_locality'] . ', ' : '' ) .
            $org['address_0_address'];

        $model = new BulletinTemplate([
            'pd_name' => $params['Название'],
            'pd_inn' => $params['ИНН'],
            'pd_ogrn' => $params['ОГРН'],
            'pd_legal_address' => $params['pd_legal_address'],
            'question_serial_number' => $this->getQuestionSerialNumber($question->id),
            'meeting_start_date' => date('d.m.Y', strtotime($meeting->start_date)),
            'meeting_address' => $meeting->meet_address,
            'decision' => $question->decision
        ]);

        return $model;
    }

    private function addBulletinParticipantParams($modelParams, $participant, $question)
    {
        $modelParams->participant_name = $participant->name;
        $modelParams->representative_name = $participant->representative_name;
        $modelParams->request_sum = $question->bulletin_form_id != 4 ? $participant->getRequestByQuestion($question->id) :
                                    MeetingVotings::getVoicesSum($question->id, $participant->id);

        $modelParams->voting_options = $this->makeAssociativeArray($this->prepareVotingResults($question, $participant->id));
    }

    private function addNoticeParticipantParams($modelParams, $participant_id)
    {
        $creditor = (MeetingParticipants::findOne($participant_id))->creditor;
        $creditor_org = TblOrganizationsMethods::getOrganization( $creditor->organization_id );
        
        $modelParams->addressee = $creditor->title;
        $modelParams->addressee_post_address = ( $creditor_org['address_2_postcode'] ? $creditor_org['address_2_postcode'] . ', ' : '' ) .
            ( $creditor_org['address_2_region'] ? $creditor_org['address_2_region'] . ', ' : '' ) .
            ( $creditor_org['address_2_locality'] ? $creditor_org['address_2_locality'] . ', ' : '' ) .
            $creditor_org['address_2_address'];
    }

    private function getBulletinTemplateMapping()
    {
        return [
            'pd_name' => 'ПДНазвание',
            'question_serial_number' => 'ПорядковыйНомерВопроса',
            'meeting_start_date' => 'ДатаСобрания',
            'meeting_address' => 'АдресСобрания',
            'pd_inn' => 'ИННПредприятия',
            'pd_ogrn' => 'ОГРНПредприятия',
            'pd_legal_address' => 'ПДЮрАдрес',
            'decision' => 'ФормулировкаРешения',
            'participant_name' => 'ФИОУчастника',
            'representative_name' => 'ФИОПредставителя',
            'request_sum' => 'СуммаТребований',
            'voting_options' => ''
        ];
    }

    private function getNoticeTemplateMapping()
    {
        return [
            'outgoing_letter_number' => '№Письма',
            'current_date' => 'ТекДата',
            'pd_name' => 'ПДНазвание',
            'pd_short_name' => 'ПДНазваниеСокр',
            'pd_inn' => 'ИННПредприятия',
            'pd_ogrn' => 'ОГРНПредприятия',
            'pd_legal_address' => 'ПДЮрАдрес',
            'addressee' => 'Адресат',
            'addressee_post_address' => 'АдресАдресатаДляКорреспонденции',
            'au_full_fio' => 'ФИОАУПолное',
            'au_short_fio' => 'АУФамилияИнициалы',
            'au_post_address' => 'АУАдрКор',
            'au_cell_phone' => 'АУМобТелефон',
            'au_email' => 'АУПочта',
            'au_komu' => 'АУКому',
            'case_number' => 'Проц№Дела',
            'judge_short_fio' => 'ПроцСудья',
            'meeting_start' => 'СобраниеДатаВремя',
            'meeting_address' => 'СобраниеАдрес',
            'meeting_register_period' => 'СобраниеПериодРегистрации',
            'meeting_acquaintance_date_period' => 'СобраниеДатаОзнакомления',
            'meeting_acquaintance_time_period' => 'СобраниеВремяОзнакомления',
            'meeting_acquaintance_address' => 'СобраниеАдресОзнакомления',
            'questions_list' => ''
        ];
    }

    private function getRegJournalTemplateMapping()
    {
        return [
            'current_date' => 'ТекДата',
            'pd_name' => 'ПДНазвание',
            'pd_inn' => 'ИННПредприятия',
            'pd_ogrn' => 'ОГРНПредприятия',
            'pd_legal_address' => 'ПДЮрАдрес',
            'au_full_fio' => 'ФИОАУПолное',
            'meeting_address' => 'СобраниеАдрес',
            'reg_start_time' => 'СобраниеРегВремяНачПрописью',
            'reg_end_time' => 'СобраниеРегВремяКонецПрописью',
            'participants_list' => ''
        ];
    }

    private function getReportTemplateMapping()
    {
        return [
            //'outgoing_letter_number' => '№Письма',
            'current_date' => 'ТекДата',
            'proc_type' => 'ПроцТип',
            'pd_name' => 'ПДНазвание',
            'pd_short_name' => 'ПДНазваниеСокр',
            'pd_full_name' => 'ПДНазваниеПолное',
            'pd_legal_address' => 'ПДЮрАдрес',
            'meeting_number' => 'СобраниеНомер',
            'case_number' => 'Проц№Дела',
            'court' => 'ПроцСуд',
            'meeting_address' => 'СобраниеАдрес',
            'meeting_start_date' => 'СобраниеДата',
            'meeting_start_time' => 'СобраниеВремя',
            'au_short_fio' => 'АУФамилияИнициалы',
            'reg_date' => 'СобраниеДатаРегПрописью',
            'reg_start_time' => 'СобраниеРегВремяНачПрописью',
            'reg_end_time' => 'СобраниеРегВремяКонецПрописью',
            'au_cro' => 'АУСРО',

            'meeting_acq_start_date' => 'ДатаОзнакомленияНачало',
            'meeting_acq_end_date' => 'ДатаОзнакомленияКонец',
            'meeting_acq_start_time' => 'ВремяОзнакомленияНачало',
            'meeting_acq_end_time' => 'ВремяОзнакомленияКонец',
            'meeting_acquaintance_address' => 'СобраниеАдресОзнакомления',

            'creditors_cnt' => 'КредиторыКолво',
            'creditors_cnt_cursive' => 'КредиторыКолвоПрописью',
            'reg_creditors_cnt' => 'КредиторыКолвоЗарег',
            'reg_creditors_cnt_cursive' => 'КредиторыКолвоЗарегПрописью',

            'total_request_sum' => 'СуммаТребований',
            'total_request_sum_cursive' => 'СуммаТребованийПрописью',
            'reg_request_sum' => 'СуммаЗарегистрТребований',
            'reg_request_sum_cursive' => 'СуммаЗарегистрТребованийПрописью',
            'quorum_percent' => 'ПроцентКворума',
            'participants_requests_total_sum' => 'СуммаТребованийРеестра',

            'participants_with_voices' => 'УчастникПГ',
            'participants_without_voices' => 'УчастникБПГ',

            'questions_list' => 'Вопрос',
            'voting_results' => 'ВопросРешение',
            'questions_quorum' => 'ВопросКворум'
        ];
    }

    private function getEnvelopeTemplateMapping()
    {
        return [
            'senderTitle' => 'ФИОАУПолное',
            'senderAddress' => 'АУАдрКор',
            'senderZip' => 'АУИндекс',
            'receiverTitle' => 'АдрКому',
            'receiverAddress' => 'АдрКуда',
            'receiverZip' => 'КомуИндекс',
        ];
    }

    private function makeBulletinTemplate($model, $bulletin_form_id, $participant_id, $question_id)
    {
        try {
            //$tpl = MeetingTemplates::findOne(['name' => 'bulletin' . $bulletin_form_id]);
            $tpl = DocTemplates::findOne(['group_id' => 8, 'template_type' => 'Бюллетень', 'question_type' => $bulletin_form_id]);

            $templateProcessor = new TemplateProcessor(Yii::getAlias('@webroot') . '/' . $tpl->path);
            $tpl_mapping = $this->getBulletinTemplateMapping();

            foreach($tpl_mapping as $key => $alias) {
                if( !is_array($model->{$key}) ) {
                    $templateProcessor->setValue($alias, $model->{$key});
                } else {
                    if( $bulletin_form_id == 1 ) {
                        foreach( $model->{$key} as $alias => $value )
                            $templateProcessor->setValue($alias, $value ? 'V' : '-');
                    } else {

                            $templateProcessor->cloneRow('ВариантГолосования', count($model->{$key}));
                            $index = 0;
                            foreach($model->{$key} as $alias => $value):
                                $templateProcessor->setValue('№пп#' . ($index + 1), ($index + 1));
                                $templateProcessor->setValue('ВариантГолосования#' . ($index + 1), $alias);
                                $templateProcessor->setValue(
                                    'РезультатГолосования#' . ($index + 1),
                                    ($bulletin_form_id == 4 ? $value : ($value ? 'V' : '-'))
                                );
                                $index++;
                            endforeach;
                    }

                }
            }

            $path = $this->template_path . 'Бюллетень' . $bulletin_form_id . '-' .
                $question_id . '-' . $participant_id .  '.docx';
            !file_exists($path) || unlink($path);
            $templateProcessor->saveAs($path);
            return $path;
        }
        catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
            // do nothing
            return false;
        }
    }

    private function makeNoticeTemplate($model, $proc_type, $participant_id)
    {
        try {
            $tpl = DocTemplates::find()->where(['group_id' => 8, 'template_type' => 'Уведомление'])->andWhere(['like', 'proc_type', $proc_type])->one();

            $templateProcessor = new TemplateProcessor(Yii::getAlias('@webroot') . '/' . $tpl->path);
            $tpl_mapping = $this->getNoticeTemplateMapping();

            foreach($tpl_mapping as $key => $alias)
                if( $key != 'questions_list' )
                    $templateProcessor->setValue($alias, $model->{$key});
                else {
                    $templateProcessor->cloneRow('Вопрос', count($model->{$key}));

                    foreach($model->{$key} as $index => $question) {
                        $templateProcessor->setValue('Вопрос№пп#' . ($index + 1), ($index + 1));
                        $templateProcessor->setValue('Вопрос#' . ($index + 1), $question->question);
                    }
                }

            $path = $this->template_path . $proc_type . '_Уведомление_СК_' . $participant_id . '.docx';
            !file_exists($path) || unlink($path);
            $templateProcessor->saveAs($path);

            return $path;
        }
        catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
            // do nothing
            return false;
        }
    }

    private function makeEnvelopeTemplate($model, $participant_id)
    {
        try {
            $tpl = DocTemplates::findOne(['group_id' => self::ENVELOPE_GROUP_ID]);

            $templateProcessor = new TemplateProcessor(Yii::getAlias('@webroot') . '/' . $tpl->path);
            $tpl_mapping = $this->getEnvelopeTemplateMapping();

            foreach($tpl_mapping as $key => $alias)
                $templateProcessor->setValue($alias, $model->{$key});

            $path = $this->template_path . 'Конверт_' . $participant_id . '.docx';
            !file_exists($path) || unlink($path);
            $templateProcessor->saveAs($path);

            return $path;
        }
        catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
            // do nothing
            return false;
        }
    }

    private function makeAssociativeArray($arr)
    {
        $new_arr = [];
        foreach( $arr as $item ) {
            $new_arr[$item['label']] = $item['value'];
        }

        return $new_arr;
    }

    private function prepareVotingResults($question, $participant_id)
    {
        if( $question->bulletin_form_id != 4 )
            // вариант без кандидатов
            $sql = 'SELECT o.value AS label, IF(ISNULL(v.id), 0, 1) AS value 
                      FROM (SELECT id, value FROM meeting_voting_options
                             WHERE bull_form_id = :bull_form_id
                               AND question_id = :options_question_id
                               ORDER BY id) AS o
                      LEFT JOIN (SELECT id, voting_option_id 
                                   FROM meeting_votings 
                                  WHERE participant_id = :participant_id
                                    AND question_id = :question_id) AS v 
                        ON o.id = v.voting_option_id';
        else
            // вариант с кандидатами
            $sql = 'SELECT o.value AS label, v.request_sum AS value 
                      FROM (SELECT id, value FROM meeting_voting_options
                             WHERE bull_form_id = :bull_form_id
                               AND question_id = :options_question_id
                               ORDER BY id) AS o
                      LEFT JOIN (SELECT id, voting_option_id, request_sum 
                                   FROM meeting_votings 
                                  WHERE participant_id = :participant_id
                                    AND question_id = :question_id) AS v 
                        ON o.id = v.voting_option_id';

        $params = [
            ':options_question_id' => (in_array($question->bulletin_form_id, [1, 3]) ? 0 : $question->id),
            ':question_id' => $question->id,
            ':bull_form_id' => $question->bulletin_form_id,
            ':participant_id' => $participant_id
        ];

        $results = Yii::$app->db->createCommand($sql, $params)->queryAll();
        return $results;
    }

    private function _file_force_download($file) {
        if (file_exists($file)) {
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
                ob_end_clean();
            }
            $arr = explode('/', $file);
            $filename = str_replace(' ', '_', array_pop($arr));
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            //header('Content-Type: application/octet-stream');
            header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment; filename=' . $filename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            readfile($file);
            unlink($file);
            exit();
        }
    }

    private function getDativeShortFIO($id)
    {
        $lastname = $firstname = $patronymic = '';

        $arr = explode(' ', AppHelper::getFIO($id));

        if( !empty($arr[2]) ):
            $this->gender = null;
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_DATIVE);
            $firstname = $this->firstname(Petrovich::CASE_DATIVE);
            $patronymic = $this->middlename(Petrovich::CASE_DATIVE);
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );

            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_DATIVE);
            $firstname = $this->firstname(Petrovich::CASE_DATIVE);
            //$patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
        endif;

        return $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . ( $patronymic ? mb_substr($patronymic, 0, 1) . '.' : '' );
    }

    private function mergeAndSave($templates_list, $result_file = 'result.docx')
    {
        $path = $this->template_path . $result_file;
        $dm = new DocxMerge();
        $dm->merge( $templates_list, $path );

        foreach($templates_list as $t)
            unlink($t);

        if(file_exists($path))
            $this->_file_force_download($path);
        else
            return false;
    }

    private function createOutgoingDoc($addressee, $addressee_post_address)
    {
        $model = new OutgoingDocs();
        $current_date = date('d.m.Y');

        $model->setAttributes([
            'proc_id' => Yii::$app->session->get('current_proc_id'),
            'serial_number' => (string)($model->getMaxSerialNumber() + 1),
            'request_type_id' => 4, // Уведомление для исходящих
            'name' => 'Уведомление о собрании кредиторов',
            'contractor' => $addressee,
            'send_date' => $current_date,
            'reg_date' => $current_date,
            'delivery_address' => $addressee_post_address,
            'notes' => null,
            'files' => null
        ]);

        return $model->save();
    }

    private function getOutgoingNextNumber()
    {
        return (string)(OutgoingDocs::find()->where(['proc_id' => Yii::$app->session->get('current_proc_id')])
                                            ->max('convert(serial_number, unsigned)') + 1);
    }

    private function addListableProtocolParams($model)
    {
        $this->getRegisteredCreditors($model);
        $this->getRegisteredParticipants($model);
        $this->getQuestionsReestr($model);
        $this->getVotingResults($model);
        $this->getQuestionsQuorum($model);
    }

    private function getRegisteredCreditors($model)
    {
        $model->participants_with_voices = ArrayHelper::map(
            MeetingParticipants::getRegisteredCreditors(),
                'id', function($item){
                return [
                    'participant' => $item['name'] . ' в лице ' . $item['representative_name'],
                    'reg_sum' => $item['total_sum'],
                    'total_sum' => $item['total_request_sum'],
                ];
            } );
    }

    private function getRegisteredParticipants($model)
    {
        $model->participants_without_voices = ArrayHelper::map(
            MeetingParticipants::getRegisteredParticipants(),
            'id', function($item){ return $item['name'] . ' в лице ' . $item['representative_name']; } );
    }

    private function getQuestionsReestr($model)
    {
        $model->questions_list = ArrayHelper::map( MeetingQuestions::getQuestionsReestr(), 'id', 'question' );
    }

    private function getQuestionsQuorum($model)
    {
        $model->questions_quorum = MeetingQuestions::getQuestionsQuorum();
    }

    private function getVotingResults($model)
    {
        $model->voting_results = ProtocolVotingResultsView::getMeetingResults();
    }

    private function getCursiveDate($date)
    {
        list($day, $month, $year) = explode('.', $date);
        return $day . ' ' . $this->_months[(int)$month] . ' ' . $year . ' г.';
    }

    private function getCursiveTime($str_time)
    {
        list($hours, $minutes) = explode(':', $str_time);
        return $hours . ' ч. ' . $minutes . ' мин.';
    }

    /**
     * Склоняем словоформу
     */
    private function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ( $n > 10 && $n < 20 ) return $f5;
        $n = $n % 10;
        if ( $n > 1 && $n < 5 ) return $f2;
        if ( $n == 1 ) return $f1;
        return $f5;
    }

    /**
     * Возвращает сумму прописью
     * @uses morph(...)
     */
    private function num2str($num, $format = 'money') {
        $nul = 'ноль';
        $ten = [
            ['','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'],
            ['','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'],
        ];
        $a20 = ['десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать'];
        $tens = array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred = ['','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот'];
        $unit = [ // Units
            ['копейка' ,'копейки' ,'копеек',    1],
            ['рубль'   ,'рубля'   ,'рублей'    ,0],
            ['тысяча'  ,'тысячи'  ,'тысяч'     ,1],
            ['миллион' ,'миллиона','миллионов' ,0],
            ['миллиард','милиарда','миллиардов',0],
        ];
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if( intval($rub) > 0 ) {
            foreach( str_split($rub,3) as $uk => $v ) { // by 3 symbols
                if( !intval($v) ) continue;
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if( $i2 > 1 ) $out[] = $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if( $uk > 1 ) $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;

        if( $format == 'money' ) {
            if( intval($kop) > 0 ) {
                list($ten_kop, $unit_kop) = array_map('intval', str_split($kop));
                if( $ten_kop == 0 ) $kop_str = $ten[1][$unit_kop];
                if( $ten_kop == 1 ) $kop_str = $a20[$unit_kop];
                if( $ten_kop > 1 ) $kop_str = $tens[$ten_kop] . ' ' . $ten[1][$unit_kop];
            } else
                $kop_str = $nul;

            $out[] = $this->morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
            $out[] = $kop_str . ' ' . $this->morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        }

        if( $format == 'voices' ) {
            !intval($kop) || $out[] = intval($kop) . '/100';
        }

        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }
}