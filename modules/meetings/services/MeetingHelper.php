<?php
namespace app\modules\meetings\services;

use app\modules\meetings\models\MeetingQuestions;
use app\modules\meetings\models\Meetings;
use app\modules\meetings\models\MeetingVotingOptions;
use app\modules\meetings\models\MeetingVotings;
use Yii;
use app\modules\creditors\models\Request;
use yii\data\SqlDataProvider;
use yii\db\Query;

class MeetingHelper
{
    public static function getRequestsProvider($question)
    {
        $sql = 'SELECT p.id, c.id creditor_id, c.title, cr.total_sum 
                  FROM (SELECT id, title FROM creditor WHERE procedures_id = :proc_id) AS c
                 INNER JOIN (SELECT creditor_id, SUM(total) AS total_sum 
                               FROM creditor_request
                              WHERE is_enabled = 1 AND queue_type IN (' .
                                    ($question->mortgage_creditors_voices ? ':q31, :q32' : ':q32') .')
                              GROUP BY creditor_id) AS cr 
                    ON c.id = cr.creditor_id
                 INNER JOIN (SELECT * FROM meeting_participants WHERE meet_id = :meet_id AND registration = 1) AS p 
                    ON p.creditor_id = c.id';

        $params = [
            ':proc_id' => Yii::$app->session->get('current_proc_id'),
            ':q32' => Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE,
            ':meet_id' => Yii::$app->session->get('current_meet_id'),
        ];
        !$question->mortgage_creditors_voices || $params[':q31'] = Request::QUEUE_TYPE_THIRD_GUARANTEE;

        return new SqlDataProvider([
                'sql' => $sql,
                'params' => $params,
                'pagination' => false,
            ]);
    }

    public static function getAllRequestsTotalSum()
    {
        $sql_sum = 'SELECT SUM(total) 
                      FROM creditor_request AS cr
                     WHERE creditor_id IN (
                                SELECT c.id FROM creditor c
                                 INNER JOIN meeting_participants p ON p.creditor_id = c.id 
                                 WHERE procedures_id = :proc_id 
                                   AND meet_id = :meet_id 
                                   AND creditor_id != 0
                           ) 
                       and cr.is_enabled = 1 AND cr.queue_type IN (:q31, :q32)';

        $params = [
            ':proc_id' => Yii::$app->session->get('current_proc_id'),
            ':q31' => Request::QUEUE_TYPE_THIRD_GUARANTEE,
            ':q32' => Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE,
            ':meet_id' => Yii::$app->session->get('current_meet_id'),
        ];

        return Yii::$app->db->createCommand($sql_sum, $params)->queryScalar();
    }

    public static function getRegisteredRequestsTotalSum($question)
    {
        $sql_sum = 'SELECT SUM(cr.total_sum) 
                      FROM (SELECT id, title FROM creditor WHERE procedures_id = :proc_id) AS c
                     INNER JOIN (SELECT creditor_id, SUM(total) AS total_sum 
                                   FROM creditor_request
                                  WHERE is_enabled = 1 AND queue_type IN (' .
                                        ($question->mortgage_creditors_voices ? ':q31, :q32' : ':q32') .')
                                  GROUP BY creditor_id) AS cr 
                        ON c.id = cr.creditor_id
                     INNER JOIN (SELECT * FROM meeting_participants WHERE meet_id = :meet_id AND registration = 1) AS p 
                        ON p.creditor_id = c.id';

        $params = [
            ':proc_id' => Yii::$app->session->get('current_proc_id'),
            ':q32' => Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE,
            ':meet_id' => Yii::$app->session->get('current_meet_id'),
        ];
        !$question->mortgage_creditors_voices || $params[':q31'] = Request::QUEUE_TYPE_THIRD_GUARANTEE;

        return Yii::$app->db->createCommand($sql_sum, $params)->queryScalar();
    }

    public static function getQuorumAllRequestsTotalSum($question)
    {
        $sql_sum = 'SELECT SUM(cr.total_sum) 
                      FROM (SELECT id, title FROM creditor WHERE procedures_id = :proc_id) AS c
                     INNER JOIN (SELECT creditor_id, SUM(total) AS total_sum 
                                   FROM creditor_request
                                  WHERE is_enabled = 1 AND queue_type IN (' .
            ($question->mortgage_creditors_voices ? ':q31, :q32' : ':q32') .')
                                  GROUP BY creditor_id) AS cr 
                        ON c.id = cr.creditor_id
                     INNER JOIN (SELECT * FROM meeting_participants WHERE meet_id = :meet_id) AS p 
                        ON p.creditor_id = c.id';

        $params = [
            ':proc_id' => Yii::$app->session->get('current_proc_id'),
            ':q32' => Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE,
            ':meet_id' => Yii::$app->session->get('current_meet_id'),
        ];
        !$question->mortgage_creditors_voices || $params[':q31'] = Request::QUEUE_TYPE_THIRD_GUARANTEE;

        return Yii::$app->db->createCommand($sql_sum, $params)->queryScalar();
    }

    public static function hasVotingResults()
    {
        $query = new Query();
        return $query->select( ['count(v.id)'] )
                     ->from( MeetingVotings::tableName() . ' as v')
                     ->innerJoin( MeetingQuestions::tableName() . ' as q', 'q.id = v.question_id' )
                     ->innerJoin( Meetings::tableName() . ' as m', 'm.id = q.meet_id' )
                     ->where( ['m.id' => Yii::$app->session->get('current_meet_id')] )
                     ->createCommand()
                     ->queryScalar();
    }

    public static function getGroupedVotingResults($id)
    {
        $query = new Query();
        return $query->select(['vo.id, vo.value, sum(request_sum) request_sum'])
              ->from(MeetingVotingOptions::tableName() . ' as vo')
              ->leftJoin(MeetingVotings::tableName() . ' as v', 'vo.id = v.voting_option_id')
              ->where(['vo.question_id' => $id])
              ->groupBy(['vo.id', 'vo.value']);
              //->createCommand();
              //->queryAll();
    }

    public static function getRegJournalParticipants()
    {
        $sql = 'SELECT p.name, s.short_name, make_address_string(o.addresses_id_3) address, p.representative_name, 
                       p.supporting_docs, p.total_sum, cr.request_total_sum
                  FROM meeting_participants AS p
                  LEFT JOIN creditor AS c ON p.creditor_id = c.id
                  LEFT JOIN (SELECT creditor_id, SUM(total) AS request_total_sum 
                               FROM creditor_request
                              WHERE is_enabled = 1 AND queue_type IN (3,4,5,6)
                              GROUP BY creditor_id) AS cr 
                    ON c.id = cr.creditor_id
                  LEFT JOIN tbl_organizations AS o ON c.organization_id = o.id 
                 INNER JOIN meeting_participant_statuses AS s ON p.participant_status_id = s.id
                 WHERE p.meet_id = :meet_id';

        $params = [':meet_id' => Yii::$app->session->get('current_meet_id')];
        return Yii::$app->db->createCommand($sql, $params)->queryAll();
    }
}