<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="question-form">

    <?php $form = ActiveForm::begin(['action' => ['/meetings/questions/' . $this->context->action->id], 'id' => 'question-form']); ?>

    <?php
        echo $form->field($model, 'question')->textarea([
                'rows' => 4,
                'style' => 'resize: none',
                'disabled' => $disabled,
        ]);
    ?>

    <?php
        echo $form->field($model, 'decision')->textarea([
                'rows' => 4,
                'style' => 'resize: none',
                'disabled' => $disabled,
        ]);
    ?>

    <?= $form->field($model, 'bulletin_form_id')->dropDownList($bulletin_forms, [
            'disabled' => $change_question_type
            ]); ?>

    <?= $form->field($model, 'voice_calc_id')->dropDownList($voice_calcs, ['disabled' => $disabled,]); ?>

    <?= $form->field($model, 'mortgage_creditors_voices')
        ->checkbox([
            //'label' => 'Неактивный чекбокс',
//            'labelOptions' => [
//                'style' => 'padding-left:20px;'
//            ],
            'disabled' => $disabled,
        ]); ?>

    <div class="form-group">
        <?php
        if( !$disabled )
            echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
        ?>
    </div>

    <?= $form->field($model, 'meet_id')
        ->label(false)
        ->hiddenInput(); ?>
    <?= $form->field($model, 'id')
        ->label(false)
        ->hiddenInput(); ?>

    <?php ActiveForm::end(); ?>

</div>
