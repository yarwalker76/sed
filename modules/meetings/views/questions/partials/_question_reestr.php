<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>
<?php Pjax::begin(['id' => 'pjax-questions-reestr']); ?>
    <?= GridView::widget([
    'dataProvider' => $questionsProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => [ 'width' => '30' ]
        ],
        'question',
        [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions' => [ 'width' => '80' ],
            'template' => $question_actions_template,
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                        '/meetings/questions/update?id=' . $model->id,
                        [
                            'title' => 'Редактировать',
                            'class' => 'update_item',
                            'data-meet_id' => $model->meet_id,
                        ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                        '/meetings/questions/delete?id=' . $model->id,
                        [
                            'title' => 'Удалить',
                            'class' => 'del_item',
                            'data-meet_id' => $model->meet_id,
                           /* 'data-method' => 'post',
                            'data-pjax' => 1,*/
                        ]);
                },
                'bulletin' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-stats"></span>',
                        '/meetings/votings/bulletin?id=' . $model->id,
                        [
                            'title' => 'Бюллетень',
                            //'data-method' => 'post',
                            'class' => 'bulletin_item',
                            'data-pjax' => 1
                        ]);
                },
                'print' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-print"></span>',
                        '/meetings/voting/print?id=' . $model->id,
                        [
                            'title' => 'Печать бюллетеня',
                            'data-method' => 'post',
                            'class' => 'print_item',
                            //'data-pjax' => 1
                        ]);
                }
            ],
        ],
    ],
]); ?>
<?php Pjax::end(); ?>
