<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

// add module assets
use app\modules\meetings\assets\MeetingsAsset;
MeetingsAsset::register($this);

$this->title = ( $this->context->action->id == 'create' ? 'Создание' : 'Изменение' ). ' собрания';
$this->params['breadcrumbs'][] = ['label' => 'Собрания кредиторов', 'url' => ['/meetings']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h3><?= $this->title; ?></h3>
<div class="creditors_meeting">
    <?= $this->render('@app/modules/meetings/views/meeting/partials/_meeting_tabs'); ?>

    <div class="meeting-participants-index">
        <p>
            <?= Html::a('Добавить участника', ['checkin/add-participant'], [
                    'class' => 'btn btn-success',
                    'id' => 'add-participant'
                ]) ?>
            <?php
            if( !$has_voting_results )
                echo Html::a('Обновить данные', ['checkin/update-participants'], [
                    'class' => 'btn btn-success',
                    'id' => 'update-participants'
                ]);
            ?>
            <?= Html::a('Печать журнала участников', ['/meetings/print/reg-journal'], [
                    'class' => 'btn btn-primary',
                    'id' => 'print-reg-journal',
                    'data-method' => 'post',
                ]) ?>
        </p>
        <?php Pjax::begin([
                'id' => 'pjax-participants-reestr',
                'enablePushState' => false,
                'clientOptions' => ['method' => 'POST']
            ]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'registration',
                        'headerOptions' => [ 'width' => '80' ],
                        'content' => function($data){
                            return ( $data['registration'] ?
                                '<span class="glyphicon glyphicon-check" aria-hidden="true"></span>' :
                                '<span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>' );
                        },
                    ],
                    'name',
                    'representative_name',
                    'sum_32',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => [ 'width' => '60' ],
                        'template' => $actions_template,
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    '/meetings/checkin/update?id=' . $model->id,
                                    [
                                        'title' => 'Редактировать',
                                        //'data-method' => 'post',
                                        'class' => 'update_item',
                                        'data-pjax' => 1
                                    ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                    '/meetings/checkin/delete?id=' . $model->id,
                                    [
                                        'title' => 'Удалить',
                                        'data-method' => 'post',
                                        'class' => 'del_item',
                                        'data-pjax' => 1
                                    ]);
                            }
                        ],
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

