<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\meetings\models\MeetingParticipantsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-participants-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'meet_id') ?>

    <?= $form->field($model, 'registration') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'representative_name') ?>

    <?php // echo $form->field($model, 'supporting_docs') ?>

    <?php // echo $form->field($model, 'participant_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
