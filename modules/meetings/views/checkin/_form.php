<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

// add module assets
use app\modules\meetings\assets\MeetingsAsset;
MeetingsAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\meetings\models\MeetingParticipants */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-participants-form">

    <?php $form = ActiveForm::begin(['action' => ['/meetings/checkin/' . $this->context->action->id], 'id' => 'participant-form']); ?>

    <?= $form->field($model, 'registration')->checkbox(['disabled' => !(strlen($model->representative_name) && strlen($model->supporting_docs))]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'representative_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'supporting_docs')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'participant_status_id')->dropDownList($model->statuses) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'meet_id')
        ->label(false)
        ->hiddenInput(); ?>
    <?= $form->field($model, 'id')
        ->label(false)
        ->hiddenInput(); ?>

    <?php ActiveForm::end(); ?>

</div>
