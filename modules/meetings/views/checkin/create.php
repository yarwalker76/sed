<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\meetings\models\MeetingParticipants */

$this->title = 'Create Meeting Participants';
$this->params['breadcrumbs'][] = ['label' => 'Meeting Participants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-participants-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
