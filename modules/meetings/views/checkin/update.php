<?php

use yii\helpers\Html;

?>
<div class="meeting-participants-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
