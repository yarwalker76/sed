<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\widgets\Pjax;
use yii\bootstrap\Alert;

// add module assets
use app\modules\meetings\assets\MeetingsAsset;
MeetingsAsset::register($this);

$this->title = ( $this->context->action->id == 'create' ? 'Создание' : 'Изменение' ). ' собрания';
$this->params['breadcrumbs'][] = ['label' => 'Собрания кредиторов', 'url' => ['/meetings']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h3><?= $this->title; ?></h3>
<div class="creditors_meeting">
    <?= $this->render('partials/_meeting_tabs'); ?>
    <?php
    Pjax::begin(['id' => 'pjax-meeting-form']);
    ?>
        <p id="prepare-tab" class="container-fluid">
            <?php
            if( Yii::$app->session->hasFlash('errors') ) {
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-danger',
                    ],
                    'body' => Yii::$app->session->getFlash('errors'),
                ]);
            }
            ?>

            <?php

            $form = ActiveForm::begin([
                'id' => 'prepare-form',
                'options' => [
                    'class' => 'form-horizontal row',
                    'data' => ['pjax' => true],
                ],
            ]);
            ?>

            <?= $form->field($meeting, 'start_date', [
                'options' => ['class' => 'col-lg-3']])
                ->widget(DatePicker::classname(), [
                    'disabled' => $disabled,
                    'options' => [
                        'value' => date('d.m.Y', strtotime($meeting->start_date))
                    ],
                    'type' => 3,
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ]
                ]); ?>

            <?= $form->field($meeting, 'start_time', ['options' => ['class' => 'col-lg-2']])
                ->widget(TimePicker::classname(),[
                        'disabled' => $disabled,
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 10,
                        ],
                    ]
                );?>

            <?= $form->field($meeting, 'serial_number', ['options' => ['class' => 'col-lg-1']])
                ->textInput();?>

        <div class="clearfix"></div>

    <?= $form->field($meeting, 'reg_start_time', ['options' => ['class' => 'col-lg-2']])
        ->widget(TimePicker::classname(),[
                'disabled' => $disabled,
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 10,
                ],
            ]
        );?>

    <?= $form->field($meeting, 'reg_end_time', ['options' => ['class' => 'col-lg-2']])
        ->widget(TimePicker::classname(),[
                'disabled' => $disabled,
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 10,
                ],

            ]
        );?>

        <div class="clearfix"></div>

    <?= $form->field($meeting, 'meet_address', ['options' => ['class' => 'col-lg-10']])
        ->textInput(['disabled' => $disabled,])?>

        <div class="clearfix"></div>

        <div class="col-lg-12 acquaintance-block"><strong>Ознакомление с материалами</strong></div>

    <?= $form->field($meeting, 'acquaintance_start_date', [
        'options' => ['class' => 'col-lg-3']])
        ->widget(DatePicker::classname(), [
            'disabled' => $disabled,
            'options' => [
                'value' => date('d.m.Y', strtotime($meeting->acquaintance_start_date))
            ],
            'type' => 3,
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy'
            ]
        ]); ?>

    <?= $form->field($meeting, 'acquaintance_end_date', [
        'options' => ['class' => 'col-lg-3']])
        ->widget(DatePicker::classname(), [
            'disabled' => $disabled,
            'options' => [
                'value' => date('d.m.Y', strtotime($meeting->acquaintance_end_date))
            ],
            'type' => 3,
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy'
            ]
        ]); ?>

        <div class="clearfix"></div>

    <?= $form->field($meeting, 'acquaintance_start_time', ['options' => ['class' => 'col-lg-2']])
        ->widget(TimePicker::classname(),[
                'disabled' => $disabled,
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 10,
                ],
            ]

        );?>

    <?= $form->field($meeting, 'acquaintance_end_time', ['options' => ['class' => 'col-lg-2']])
        ->widget(TimePicker::classname(),[
                'disabled' => $disabled,
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 10,
                ],
            ]

        );?>
        <div class="clearfix"></div>

    <?= $form->field($meeting, 'acquaintance_address', ['options' => ['class' => 'col-lg-10']])
        ->textInput(['disabled' => $disabled,])?>
        <div class="clearfix"></div>

        <div class="btn-panel">
            <?php if( !$disabled )
                echo Html::submitButton($meeting->isNewRecord ? 'Создать' : 'Изменить', [
                    'class' => $meeting->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                ]);
            ?>

            <?php
            echo Html::a('Печать уведомлений', ['/meetings/print?id=' . $meeting->id],
                [
                    'class' => 'btn btn-success ' .
                        ($questionsProvider && $questionsProvider->count ? '' : 'disabled'),
                    'id' => 'print-notifications',
                    'data-pjax' => 0,
                ]);
            ?>
        </div>

    <?= $form->field($meeting, 'proc_id')
        ->label(false)
        ->hiddenInput(['class' => '']); ?>
    <?= $form->field($meeting, 'id')
        ->label(false)
        ->hiddenInput(); ?>

    <?php ActiveForm::end(); ?>

    <?php if( !$meeting->isNewRecord ) { ?>
        <div id="questions_block">
            <h4>Повестка дня</h4>
            <p><?= Html::a('Добавить вопрос', '/meetings/questions/create', ['id' => 'create-question', 'class' => 'btn btn-success', 'data-meeting-id' => $meeting->id]) ?></p>
            <?= $this->render('/questions/partials/_question_reestr', [
                'questionsProvider' => $questionsProvider,
                'question_actions_template' => $question_actions_template,
            ]); ?>
        </div>
    <?php } ?>


    <?php
    Pjax::end();
    ?>
</div>


