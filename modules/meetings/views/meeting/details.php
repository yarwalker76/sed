<?php
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\bootstrap\Modal;

// add module assets
use app\modules\meetings\assets\MeetingsAsset;
MeetingsAsset::register($this);

$this->title = ( $this->context->action->id == 'create' ? 'Создание' : 'Изменение' ). ' собрания';
$this->params['breadcrumbs'][] = ['label' => 'Собрания кредиторов', 'url' => ['/meetings']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditors_meeting-create">
    <h3><?= $this->title; ?></h3>



    <?php

    ?>

    <?php /*echo Tabs::widget([
        'id' => 'meeting-tabs',
        'items' => [
            [
                'label' => 'Подготовка',
                'content' => $prepare_tab,
                'active' => true,
                //'url' => 'prepare',
                'headerOptions' => [
                    'data-target' => 'tab1',
                    'data-meet-id' => $meeting->id
                ],
                'options' => [
                    'id' => 'tab1'
                ],
            ],
            [
                'label' => 'Регистрация',
                'content' => $checkin_tab,
                'active' => false,
                //'url' => 'checkin',
                'headerOptions' => [
                    'data-target' => 'tab2',
                    'data-meet-id' => $meeting->id
                ],
                'options' => [
                    'id' => 'tab2'
                ],
            ],
            [
                'label' => 'Проведение',
                'content' => '3',
                'active' => false,
                //'url' => '',
                'headerOptions' => [
                    'data-target' => 'tab3',
                    'data-meet-id' => $meeting->id
                ],
                'options' => [
                    'id' => 'tab3'
                ],
            ],
            [
                'label' => 'Протокол',
                'content' => '4',
                'active' => false,
                //'url' => '',
                'headerOptions' => [
                    'data-target' => 'tab4',
                    'data-meet-id' => $meeting->id
                ],
                'options' => [
                    'id' => 'tab4'
                ],
            ],

        ],
        'options' => ['tag' => 'div'],
        'itemOptions' => ['tag' => 'div'],
        'headerOptions' => ['class' => 'my-class'],
        'clientOptions' => ['collapsible' => false],
    ]);*/
    ?>
</div>

