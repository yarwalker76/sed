<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\creditors_meeting\models\Meetings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meetings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'proc_id')->textInput() ?>

    <?= $form->field($model, 'start_date')->textInput() ?>

    <?= $form->field($model, 'start_time')->textInput() ?>

    <?= $form->field($model, 'reg_start_time')->textInput() ?>

    <?= $form->field($model, 'meet_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'acquaintance_start_date')->textInput() ?>

    <?= $form->field($model, 'acquaintance_start_time')->textInput() ?>

    <?= $form->field($model, 'acquaintance_address')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
