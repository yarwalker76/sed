<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\creditors_meeting\models\MeetingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meetings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'proc_id') ?>

    <?= $form->field($model, 'start_date') ?>

    <?= $form->field($model, 'start_time') ?>

    <?= $form->field($model, 'reg_start_time') ?>

    <?php // echo $form->field($model, 'meet_address') ?>

    <?php // echo $form->field($model, 'acquaintance_date') ?>

    <?php // echo $form->field($model, 'acquaintance_time') ?>

    <?php // echo $form->field($model, 'acquaintance_address') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
