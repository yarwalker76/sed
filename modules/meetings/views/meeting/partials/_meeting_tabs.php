<ul class="nav nav-tabs" id="meeting-tabs">
    <li role="presentation" class="<?= strpos(Yii::$app->request->url, 'create') !== false ||
        strpos(Yii::$app->request->url, 'update') !== false ||
        strpos(Yii::$app->request->url, 'prepare') !== false ? 'active' : ''; ?>">
        <a href="/meetings/meeting/prepare">Подготовка</a>
    </li>
    <li role="presentation" class="<?= strpos(Yii::$app->request->url, '/meetings/checkin') !== false ? 'active' : ''; ?>">
        <a href="/meetings/checkin">Регистрация</a>
    </li>
    <li role="presentation" class="<?= strpos(Yii::$app->request->url, '/meetings/votings') !== false ? 'active' : ''; ?>">
        <a href="/meetings/votings">Проведение</a>
    </li>
</ul>
