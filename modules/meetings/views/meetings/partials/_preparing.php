<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

// add module assets
use app\modules\meetings\assets\CreditorsMeetingAsset;
CreditorsMeetingAsset::register($this);

$acq_date = date_create(date('Y-m-d'));
date_add($acq_date, date_interval_create_from_date_string('-5 days'));

Pjax::begin(['id' => 'pjax-meeting-form']);
?>
<p id="prepare-tab" class="container-fluid">
    <?php

    $form = ActiveForm::begin([
        'id' => 'prepare-form',
        'options' => ['class' => 'form-horizontal row', 'data' => ['pjax' => true]],
    ]);
    ?>

    <?= $form->field($meeting, 'start_date', [
                     'options' => ['class' => 'col-lg-3']])
             ->widget(DatePicker::classname(), [
                'options' => [
                    'value' => ( $meeting->start_date ? date('d.m.Y', strtotime($meeting->start_date)) : date('d.m.Y') )],
                    'type' => 3,
                    'removeButton' => false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ]
                ]); ?>

    <?= $form->field($meeting, 'start_time', ['options' => ['class' => 'col-lg-2']])
             ->widget(TimePicker::classname(),[
                     'pluginOptions' => [
                         'showSeconds' => false,
                         'showMeridian' => false,
                         'minuteStep' => 5,
                     ],
                 ]
             );?>

    <div class="clearfix"></div>

    <?= $form->field($meeting, 'reg_start_time', ['options' => ['class' => 'col-lg-2']])
             ->widget(TimePicker::classname(),[
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 5,
                    ],
                ]
        );?>

    <?= $form->field($meeting, 'reg_end_time', ['options' => ['class' => 'col-lg-2']])
        ->widget(TimePicker::classname(),[
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 5,
                    'size' => 'md'
                ],

            ]
        );?>

    <div class="clearfix"></div>

    <?= $form->field($meeting, 'meet_address', ['options' => ['class' => 'col-lg-10']]) ?>

    <div class="clearfix"></div>

    <div class="col-lg-12 acquaintance-block"><strong>Ознакомление с материалами</strong></div>

    <?= $form->field($meeting, 'acquaintance_start_date', [
        'options' => ['class' => 'col-lg-3']])
        ->widget(DatePicker::classname(), [
            'options' => [
                'value' => ( $meeting->acquaintance_start_date ?
                    date('d.m.Y', strtotime($meeting->acquaintance_start_date)) :
                    date_format($acq_date,'d.m.Y') )
            ],
            'type' => 3,
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy'
            ]
        ]); ?>

    <?= $form->field($meeting, 'acquaintance_end_date', [
        'options' => ['class' => 'col-lg-3']])
        ->widget(DatePicker::classname(), [
            'options' => [
                'value' => ( $meeting->acquaintance_end_date ?
                    date('d.m.Y', strtotime($meeting->acquaintance_end_date)) :
                    date_format($acq_date,'d.m.Y') )
            ],
            'type' => 3,
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd.mm.yyyy'
            ]
        ]); ?>

    <div class="clearfix"></div>

    <?= $form->field($meeting, 'acquaintance_start_time', ['options' => ['class' => 'col-lg-2']])
        ->widget(TimePicker::classname(),[
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 5,
                ],
            ]

        );?>

    <?= $form->field($meeting, 'acquaintance_end_time', ['options' => ['class' => 'col-lg-2']])
        ->widget(TimePicker::classname(),[
                'pluginOptions' => [
                    'showSeconds' => false,
                    'showMeridian' => false,
                    'minuteStep' => 5,
                ],
            ]

        );?>
    <div class="clearfix"></div>

    <?= $form->field($meeting, 'acquaintance_address', ['options' => ['class' => 'col-lg-10']]) ?>
    <div class="clearfix"></div>



    <div class="btn-panel">
        <?= Html::submitButton($meeting->isNewRecord ? 'Создать' : 'Изменить', [
                'class' => $meeting->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
            ]);
        ?>
        <?php
        if( $questionsProvider && $questionsProvider->count )
            echo Html::a('Печать уведомлений', ['/meetings/print'], ['class' => 'btn btn-success']);
        ?>
    </div>

    <?= $form->field($meeting, 'proc_id')
             ->label(false)
             ->hiddenInput(['class' => '']); ?>
    <?= $form->field($meeting, 'id')
             ->label(false)
             ->hiddenInput(); ?>

    <?php
    ActiveForm::end();

    ?>

    <?php if( !$meeting->isNewRecord ) { ?>
        <div id="questions_block">
            <h4>Повестка дня</h4>
            <p><?= Html::a('Добавить вопрос', '/meetings/add-question', ['id' => 'create-question', 'class' => 'btn btn-success', 'data-meeting-id' => $meeting->id]) ?></p>
            <?= $this->render('_question_reestr', ['questionsProvider' => $questionsProvider]); ?>
        </div>
    <?php } ?>
</div>

<?php
Pjax::end();

Modal::begin([
        'header' => '<h3>Вопрос собрания</h3>',
        'headerOptions' => ['id' => 'modalHeader'],
        'id' => 'meeting-question-modal',
        'size' => 'modal-lg',
        //keeps from closing modal with esc key or by clicking out of the modal.
        // user must click cancel or X to close
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);
Modal::end();
?>
