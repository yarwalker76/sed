<?php
use yii\bootstrap\Tabs;
use yii\helpers\Html;

$this->title = ( $this->context->action->id == 'create' ? 'Создание' : 'Изменение' ). ' собрания';
$this->params['breadcrumbs'][] = ['label' => 'Собрания кредиторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditors_meeting-create">
    <h3><?= $this->title; ?></h3>
    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Подготовка',
                'content' => $this->render('partials/_preparing', [
                        'meeting' => $meeting,
                        'questionsProvider' => ($questionsProvider ? $questionsProvider : null ),
                ]),
                'active' => true,
            ],
            [
                'label' => 'Регистрация',
                'content' => '',
                'active' => false,
            ],
            [
                'label' => 'Проведение',
                'content' => '',
                'active' => false,
            ],
            [
                'label' => 'Протокол',
                'content' => '',
                'active' => false,
            ],

        ],
        'options' => ['tag' => 'div'],
        'itemOptions' => ['tag' => 'div'],
        'headerOptions' => ['class' => 'my-class'],
        'clientOptions' => ['collapsible' => false],
    ]);
    ?>
</div>