<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php
if( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') ) {
    $form = ActiveForm::begin(['action' => '/meetings/votings-options/create', 'id' => 'bulletin-variant-form',
        'options' => ['class' => 'form-inline', ['data-pjax' => true]]]); ?>
    <?= $form->field($variant_model, 'value')
        ->label('')
        ->textInput(['placeholder' => 'Добавьте новый вариант']);
    ?>
    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']); ?>

    <?= $form->field($variant_model, 'question_id')
        ->label(false)
        ->hiddenInput(); ?>
    <?= $form->field($variant_model, 'bull_form_id')
        ->label(false)
        ->hiddenInput(); ?>
    <?php
    ActiveForm::end();
}
?>

<h4>Варианты</h4>
<?= $this->render('_variants_list', ['dataProvider' => $dataProvider, 'bulletin_form_id' => $bulletin_form_id]); ?>


