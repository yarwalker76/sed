<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;
?>

<?php Pjax::begin(['id' => 'pjax-variants-reestr']); ?>
    <?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_variants_list_item',
    'layout' => '{items}',
    'options' => [
        'tag' => 'div',
        'id' => 'variants-list',
    ],
    'viewParams' => ['bulletin_form_id' => $bulletin_form_id]
]); ?>
<?php Pjax::end(); ?>