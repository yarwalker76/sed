<?php
use yii\helpers\Html;
?>

<div class="variants-item">
    <strong><?= Html::encode($model['value']) ?></strong>

    <div class="pull-right">
        <?php
        if( $bulletin_form_id == 4 ) {
            echo '<span class="voices-result" id="voices-result' . $model['id'] .'" >' . (is_null($model['request_sum']) ? 0 : $model['request_sum']) . '</span>';
        }
        ?>

        <?php
        if( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') )
            echo Html::a(
                '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',
                '/meetings/votings-options/update?id=' . $model['id'],
                ['class' => 'update_variant']);
        ?>

        <?php
        if( Yii::$app->user->can('manager') )
            echo Html::a(
                '<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>',
                '/meetings/votings-options/delete?id=' . $model['id'],
                ['class' => 'delete_variant', 'data-pjax' => 1]);
        ?>
    </div>
</div>