<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

$formatter = \Yii::$app->formatter;
?>
<div id="bulletin4-wrapper">
    <div class="well">Вопрос: <?= $data['question'] ?></div>

    <div class="form-group">
        <label class="control-label" for="bulletin4-candidate_cnt">Количество кандидатов</label>
        <?= Html::dropDownList('bulletin4-candidate_cnt', null,
            \yii\helpers\ArrayHelper::map($data['candidates_cnt_list'], 'id', 'value'),
            ['class' => 'form-control bulletin4-candidates-cnt-dropdown']
        );?>
    </div>

    <?= $this->render('partials/_variants_view', [
            'variant_model' => $variant_model,
            'dataProvider' => $variantsProvider,
            'bulletin_form_id' => $data['bulletin_form_id'],
        ]);
    ?>

    <?php Pjax::begin(['id' => 'pjax-bulletin4-form']); ?>
        <?php $form = ActiveForm::begin(['id' => 'bulletin4-form']); ?>
        <?= $form->field($model, 'discussion')->textarea([
            'rows' => 4,
            'style' => 'resize: none',
            //'disabled' => $disabled,
        ]) ?>

        <?= GridView::widget([
            'dataProvider' => $requests_provider,
            'layout'=>"{items}",
            'columns' => [
                [
                    'attribute' => 'name', //'title',
                    'header' => 'Кредитор',
                    'footer' => '<strong>Итого</strong>',
                ],
                [
                    'attribute' => 'total_sum',
                    'value' => function($obj, $key, $index, $widget) use ($formatter, $model) {
                        return $formatter->asDecimal(
                            $model->mortgage_creditors_voices ?
                                $obj['sum_31'] + $obj['sum_32'] : $obj['sum_32'], 2);
                    },
                    'header' => 'Сумма<br/>требований',
                    'footer' => '<span id="total_sum">' . $formatter->asDecimal($data['total_sum'], 2) . '</span>',
                ],
                [
                    'header' => '% Голосования',
                    'value' => function ($obj, $key, $index, $widget) use ($data, $formatter, $model) {
                        $request_sum = $model->mortgage_creditors_voices ? $obj['sum_31'] + $obj['sum_32'] : $obj['sum_32'];
                        return $formatter->asDecimal($request_sum / $data['total_sum'] * 100, 2);
                    },
                    'footer' => '100.00',
                ],
                [
                    'header' => 'Варианты',
                    'headerOptions' => ['width' => 160],
                    'content' => function ($obj, $key, $index, $widget)  {
                        return Html::button('Заполнить', [
                                'data-url' => '/meetings/votings/candidates-voting',
                                'class' => 'btn btn-link candidates-choice',
                                'data-participant_id' => $obj['id'],
                            ]);
                    },
                ]
            ],
            'showFooter' => true,
        ]); ?>

        <div class="form-group">
            <label class="control-label" for="bulletin4form-final_decision">Результаты голосования</label>
            <textarea id="bulletin4-voting-result" class="form-control" rows="4" style="resize: none"></textarea>
        </div>

        <?= $form->field($model, 'final_decision')->textarea([
            'rows' => 4,
            'style' => 'resize: none',
            //'disabled' => $disabled,
        ]) ?>

        <div class="form-group">
            <?php
            if( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') )
                echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
            ?>
        </div>
        <?= $form->field($model, 'question_id')
            ->label(false)
            ->hiddenInput(); ?>
        <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>

<?php

$this->registerJs(
    "$('document').ready(function(){
     var results = {},
         result_str = '';

    $('form#bulletin4-form .bulletin4-dropdown').each(function(){
        var selected = $(this).find('option:selected')[0].label;

        if(results.hasOwnProperty(selected)) {
            results[selected].percent += parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.'));
            results[selected].money += parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''));
        } else {
            results[selected] = { 'percent': parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.')),
            'money': parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''))};
        }
    });
    
    $.each(results, function(k, v){
        result_str += k + ' - ' + v.percent + '% (' + v.money + ' руб.)\\n';
    });

    $('#bulletin4-voting-result').val(result_str);
    });"
);

