<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>

<div class="well"><strong>Общая сумма</strong> <span id="creditor_voice_sum"><?= $voice_sum; ?></span></div>

<?php $form = ActiveForm::begin(['id' => 'bulletin4-candidates-voting-form']) ?>
    <?= GridView::widget([
        'dataProvider' => $candidate_votings_provider,
        'layout' => "{items}",
        'id' => 'candidate-voices-list',
        'columns' => [
            [
                'attribute' => 'value',
                'header' => 'Кандидат',
            ],
            [
                'header' => 'Голоса',
                'headerOptions' => ['width' => 100],
                'content' => function ($obj, $key, $index, $widget) use ($form, $model) {
                    return $form->field($model, 'candidate_votings[' . $obj['id'] . ']')
                                ->label(false)
                                ->textInput([
                                    'class' => 'form-control candidate-voice',
                                    'data-id' => $obj['id'],
                                    'value' => isset($model->candidate_votings[$obj['id']]) ?
                                                $model->candidate_votings[$obj['id']] : 0
                                ]);
                },
            ]
        ],

    ]); ?>

    <div class="form-group">
        <?php
        if( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') )
            echo Html::submitButton('Сохранить', [
                    'class' => 'btn btn-primary',
                    'id' => 'save-candidates-voting',
                    'disabled' => true,
            ]);
        ?>
    </div>
    <?= $form->field($model, 'question_id')
        ->label(false)
        ->hiddenInput(); ?>
    <?= $form->field($model, 'participant_id')
        ->label(false)
        ->hiddenInput(); ?>
    <?= $form->field($model, 'participant_sum')
        ->label(false)
        ->hiddenInput(); ?>
    <?= $form->field($model, 'total_sum')
        ->label(false)
        ->hiddenInput(); ?>
    <?= $form->field($model, 'candidates_cnt')
        ->label(false)
        ->hiddenInput(); ?>

<?php ActiveForm::end(); ?>

