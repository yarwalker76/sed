<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

// add module assets
use app\modules\meetings\assets\MeetingsAsset;
MeetingsAsset::register($this);

$formatter = \Yii::$app->formatter;
?>
<div id="bulletin1-wrapper">
    <div class="well">Вопрос: <?= $data['question'] ?></div>
    <?php $form = ActiveForm::begin(['id' => 'bulletin1-form']); ?>
    <?= $form->field($model, 'discussion')->textarea([
        'rows' => 4,
        'style' => 'resize: none',
        //'disabled' => $disabled,
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $requests_provider,
        'layout'=>"{items}",
        'columns' => [
            [
                'attribute' => 'name', //'title',
                'header' => 'Кредитор',
                'footer' => '<strong>Итого</strong>',
            ],
            [
                'attribute' => 'total_sum',
                'value' => function($obj, $key, $index, $widget) use ($formatter, $model) {
                    return $formatter->asDecimal(
                            $model->mortgage_creditors_voices ?
                                $obj['sum_31'] + $obj['sum_32'] : $obj['sum_32'], 2);
                    },
                'header' => 'Сумма<br/>требований',
                'footer' => $formatter->asDecimal($data['total_sum'], 2),
            ],
            [
                'header' => '% Голосования',
                'value' => function ($obj, $key, $index, $widget) use ($data, $formatter, $model) {
                    $request_sum = $model->mortgage_creditors_voices ? $obj['sum_31'] + $obj['sum_32'] : $obj['sum_32'];
                    return $formatter->asDecimal($request_sum / $data['total_sum'] * 100, 2);
                },
                'footer' => '100.00',
            ],
            [
                'header' => 'Варианты',
                'headerOptions' => ['width' => 230],
                'content' => function ($obj, $key, $index, $widget) use ($form, $model, $data) {
                    return $form->field($model, 'creditor_requests[' . $obj['id'] . ']')
                                ->label(false)
                                ->radioList(\yii\helpers\ArrayHelper::map($data['voting_options'], 'id', 'value'),
                                    ['class' => 'bulletin1-radiolist']);
                },
                'footer' => '<span id="bulletin1-voting-result"></span>',
            ]
        ],
        'showFooter' => true,
    ]); ?>

    <?= $form->field($model, 'final_decision')->textarea([
        'rows' => 4,
        'style' => 'resize: none',
        //'disabled' => $disabled,
    ]) ?>

    <div class="form-group">
        <?php
        if( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') )
            echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        ?>
    </div>
    <?= $form->field($model, 'question_id')
        ->label(false)
        ->hiddenInput(); ?>
    <?php ActiveForm::end(); ?>
</div>

<?php
$this->registerJs(
    "$('document').ready(function(){
        var results = {},
            result_str = '';

        $('form#bulletin1-form .bulletin1-radiolist:first-of-type :radio').each(function(){
            results[$(this).parent('label').text()] = {'percent': 0, 'money': 0};
        });

        $('form#bulletin1-form .bulletin1-radiolist :radio:checked').each(function(){
           results[$(this).parent('label').text()].percent += parseFloat($(this).closest('td').prev('td').text().replace(/[,]+/g, '.'));
           results[$(this).parent('label').text()].money += parseFloat($(this).closest('td').prev('td').prev('td').text().replace(/[,]+/g, '.').replace(/\s/g, ''));
        });

        $.each(results, function(k, v){
            result_str += k + ' - ' + v.percent + '% (' + v.money + ' руб.)<br>';
        });

        $('#bulletin1-voting-result').html(result_str);
    });"
);
?>

