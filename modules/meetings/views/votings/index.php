<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

// add module assets
use app\modules\meetings\assets\MeetingsAsset;
MeetingsAsset::register($this);

$this->title = ( $this->context->action->id == 'create' ? 'Создание' : 'Изменение' ). ' собрания';
$this->params['breadcrumbs'][] = ['label' => 'Собрания кредиторов', 'url' => ['/meetings']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h3><?= $this->title; ?></h3>
<div class="creditors_meeting">
    <?= $this->render('@app/modules/meetings/views/meeting/partials/_meeting_tabs'); ?>

    <?php
    if( $meeting ) {
        ?>

        <?php Pjax::begin(['id' => 'pjax-voting-questions-reestr']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['width' => '30'],
                ],
                [
                    'attribute' => 'question',
                    'content' => function ($data) {
                        return Html::a($data->question, '/meetings/votings/bulletin?id=' . $data->id ,
                            ['class' => 'bulletin_item', 'data-type_id' => $data->bulletin_form_id]);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '80'],
                    'template' => $voting_actions_template,
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                '/meetings/questions/update?id=' . $model->id,
                                [
                                    'title' => 'Редактировать',
                                    //'data-method' => 'post',
                                    'class' => 'update_item',
                                    'data-pjax' => 1,
                                    //'data-meet_id' => $model->meet_id,
                                ]);
                        },
                        'bulletin' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-stats"></span>',
                                '/meetings/votings/bulletin?id=' . $model->id,
                                [
                                    'title' => 'Голосование',
                                    //'data-method' => 'post',
                                    'class' => 'bulletin_item',
                                    //'data-meet_id' => $model->meet_id,
                                    'data-pjax' => 1,
                                    'data-type_id' => $model->bulletin_form_id
                                ]);
                        },
                        'print' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-print"></span>',
                                '/meetings/print/print-bulletin?id=' . $model->id,
                                [
                                    'title' => 'Печать бюллетеня',
                                    'data-method' => 'post',
                                    'class' => 'print_item',
                                    //'data-type_id' => $model->bulletin_form_id,
                                    //'data-pjax' => 1
                                ]);
                        }
                    ],
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>

        <?php $form = ActiveForm::begin([
            'action' => ['/meetings/meeting/update-conclusion'],
            'id' => 'meeting-conclusion-form',
            //'options' => ['data' => ['pjax' => true]]
        ]); ?>
        <?php
        echo $form->field($meeting, 'conclusion')->textarea([
            'rows' => 4,
            'style' => 'resize: none',
            //'disabled' => $disabled,
        ]);
        ?>
        <div class="form-group float-left">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
        </div>

        <?= $form->field($meeting, 'id')
            ->label(false)
            ->hiddenInput(); ?>
        <?php ActiveForm::end(); ?>

        <?= Html::a('Печать протокола', '/meetings/print/print-report?id=' . $meeting->id, [
            'class' => 'btn btn-primary',
            'data-method' => 'post'
        ]); ?>
    <?php
    } else echo '<h3>Данных не найдено</h3>';
    ?>

</div>


