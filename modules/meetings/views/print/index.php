<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;

// add module assets
use app\modules\meetings\assets\MeetingsAsset;
MeetingsAsset::register($this);

$this->title = 'Печать уведомлений';
$this->params['breadcrumbs'][] = ['label' => 'Собрания кредиторов', 'url' => ['/meetings']];
$this->params['breadcrumbs'][] = ['label' => 'Собрание', 'url' => ['/meetings/meeting/update?id=' . Yii::$app->request->get('id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="print-section">
    <h3><?= $this->title; ?></h3>
    <?php $form = ActiveForm::begin([
                        //'action' => ['/meetings/print/prepare-notices'],
                        'id' => 'print-form', 'method' => 'post'
                    ]) ?>
        <div class="col-lg-6">
            <?php echo $this->render('partials/_creditors_list', [
                        'dataProvider' => $dataProvider,
                        'form' => $form,
                        'model' => $model
            ]); ?>
        </div>

        <div class="clearfix"></div>

        <div class="col-lg-6">
            <div class="form-group grey-border">
                <?= Html::submitButton('<span class="glyphicon glyphicon-print"></span> Формировать письма',
                    [
                        'name' => 'PrintNoticeForm[submit]',
                        'value' => 'notices',
                        'data-method' => 'post',
                        'class' => 'prepare_notices btn btn-primary',
                        'disabled' => true,
                    ]);
                ?>
                <?= $form->field($model, 'add_outgoing_docs', ['options' => ['class' => 'add-outgoing-docs']])
                         ->checkbox(['label' => 'Создать исходящие сообщения', 'disabled' => true]); ?>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="form-group">
                <?= Html::submitButton('<span class="glyphicon glyphicon-print"></span> Формировать конверты',
                    [
                        'name' => 'PrintNoticeForm[submit]',
                        'value' => 'envelopes',
                        'data-method' => 'post',
                        'class' => 'prepare_envelopes btn btn-primary',
                        'disabled' => true,
                    ]);
                ?>

            </div>
        </div>

        <div class="col-lg-12">
            <div class="form-group">
                <?= Html::submitButton('<span class="glyphicon glyphicon-print"></span> Формировать почтовые уведомления',
                    [
                        'name' => 'PrintNoticeForm[submit]',
                        'value' => 'report',
                        'data-method' => 'post',
                        'class' => 'prepare_post_notices btn btn-primary',
                        'disabled' => true,
                    ]);
                ?>

            </div>
        </div>
        <?= $form->field($model, 'meet_id')->label(false)->hiddenInput(); ?>
        <?= $form->field($model, 'submit')->label(false)->hiddenInput(); ?>
    <?php ActiveForm::end(); ?>

</div>