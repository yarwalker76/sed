<?php
use yii\grid\GridView;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'id' => 'creditors-list',
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'name',
            'label' => 'Кредитор',
        ],
        [
            'label' => 'Отметка',
            'headerOptions' => ['width' => 70],
            'content' => function($data) use ($form, $model) {
                return $form->field($model, 'creditors_list[' . $data['id'] . ']')
                            ->checkbox(['class' => 'creditor-checkbox', 'label' => false]);

            },
        ]
    ],
]); ?>
