<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="question-form">

    <?php $form = ActiveForm::begin(['action' => ['/creditors-meeting/' . $this->context->action->id], 'id' => 'question-form']); ?>

    <?= $form->field($model, 'question')->textarea(['rows' => 4, 'style' => 'resize: none']) ?>

    <?= $form->field($model, 'decision')->textarea(['rows' => 4, 'style' => 'resize: none']) ?>

    <?= $form->field($model, 'bulletin_form_id')->dropDownList($bulletin_forms); ?>

    <?= $form->field($model, 'voice_calc_id')->dropDownList($voice_calcs); ?>

    <?= $form->field($model, 'mortgage_creditors_voices')
        ->checkbox([
            //'label' => 'Неактивный чекбокс',
//            'labelOptions' => [
//                'style' => 'padding-left:20px;'
//            ],
            'disabled' => false
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?= $form->field($model, 'meet_id')
        ->label(false)
        ->hiddenInput(); ?>
    <?= $form->field($model, 'id')
        ->label(false)
        ->hiddenInput(); ?>

    <?php ActiveForm::end(); ?>

</div>
