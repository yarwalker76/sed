<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>

<?= GridView::widget([
    'dataProvider' => $questionsProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => [ 'width' => '30' ]
        ],
        'question',
        [
            'class' => 'yii\grid\ActionColumn',
            'headerOptions' => [ 'width' => '60' ],
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                        '/creditors-meeting/update-question?id=' . $model->id,
                        [
                            'title' => 'Редактировать',
                            'class' => 'update_item',
                        ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                        '/creditors-meeting/delete-question?id=' . $model->id,
                        [
                            'title' => 'Удалить',
                            'class' => 'del_item',
                            'data-method' => 'post',
                            'data-pjax' => 1
                        ]);
                }
            ],
        ],
    ],
]); ?>
