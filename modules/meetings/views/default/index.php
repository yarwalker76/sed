<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Alert;

// add module assets
use app\modules\meetings\assets\MeetingsAsset;
MeetingsAsset::register($this);

$this->title = 'Собрания кредиторов';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="creditors_meeting-index">
    <h3><?= Html::encode($this->title) ?></h3>

    <?php
        if( Yii::$app->session->hasFlash('errors') ) {
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => Yii::$app->session->getFlash('errors'),
            ]);
        }
    ?>

    <?php if( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') ) { ?>
        <p><?= Html::a('Создать собрание', ['meeting/create'], ['class' => 'btn btn-success']) ?></p>
    <?php } ?>

    <?php Pjax::begin(['id' => 'meetings-reestr', 'enablePushState' => false, 'clientOptions' => ['method' => 'POST']]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                /*[
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => [ 'width' => '30' ]
                ],*/
                [
                    'attribute' => 'serial_number',
                    'headerOptions' => [ 'width' => '30' ],
                    'label' => '№',
                ],
                [
                    'attribute' => 'start_date',
                    'format' => ['date', 'php:d.m.Y'],
                    'content' => function($data){
                        return Html::a(date('d.m.Y', strtotime($data->start_date)), '/meetings/meeting/update?id=' . $data->id);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => [ 'width' => '80' ],
                    'template' => $actions_template,
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                '/meetings/meeting/update?id=' . $model->id,
                                [
                                    'title' => 'Редактировать',
                                    //'data-method' => 'post',
                                    'class' => 'update_item',
                                    'data-pjax' => 0
                                ]);
                        },
                        'clone' => function($url, $model){
                             return Html::a('<span class="glyphicon glyphicon-duplicate"></span>',
                                 '/meetings/meeting/clone?id=' . $model->id,
                                 [
                                    'title' => 'Клонировать',
                                    'id' => 'clone-meeting',
                                    'class' => 'clone_item',
                                    'data-method' => 'post',
                                    'data-pjax' => 1
                                ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                '/meetings/meeting/delete?id=' . $model->id,
                                [
                                    'title' => 'Удалить',
                                    'data-method' => 'post',
                                    'class' => 'del_item',
                                    'data-pjax' => 1
                                ]);
                        }
                    ],
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?>
</div>

