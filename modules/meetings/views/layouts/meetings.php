<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Nav;
use app\assets\AppAsset;
use app\models\AppHelper;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Modal;

yii\bootstrap\BootstrapAsset::register($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php
if (!Yii::$app->user->isGuest) {
    $menuItems[] = [
        'label' => Yii::t('menu', 'Logout'),
        'url' => '#',
        'template' => Html::beginForm(array('/logout')) . Html::submitButton('{label}', ['class' => 'btn-link btn-logout']) . Html::endForm(),
    ];
    $menuItems[] = [
        'label' => Yii::t('menu', 'Tasks'),
        'url' => ['/#'],
        'template' => '<a href="{url}" class="order-btn link-white">' . Yii::t('menu', 'Tasks') . '</a><span class="number-order">99</span>',
    ];
}
?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-4 logo-box">
                <a href="#" class="logo">Logo</a>
                <p>Система Автоматизации Деятельности Арбитражного Управляющего</p>
            </div>
            <?php if (!Yii::$app->user->isGuest) : ?>

                <div class="col-md-5 main-logo">
                    <?php
                    if( !Yii::$app->user->can('administrator') ):

                        $procedure = AppHelper::getLastSeenProcedure();

                        if( $procedure['name'] != '' && $procedure['type'] != '' ):
                            ?>
                            <a href="#" class="court-icon"></a>

                            <div class="dropdown" id="proc-list-wrapper">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle" id="proc-list"><h1><?= $procedure['name'] ?> <b class="caret"></b></h1></a>
                                <?php
                                echo Dropdown::widget([
                                    'items' => AppHelper::getUserProceduresList(),
                                ]);
                                ?>
                            </div>

                            <span class="status-icon"><?= $procedure['type'] ?></span>
                            <a href="/procedures/view" class="proc-icon"></a>
                            <?php
                        endif;
                        ?>
                        <?php
                        if( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') ):
                            echo Html::a( '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>', Url::to('/procedures/create'), ['class' => 'proc-add'] );
                        endif;

                    endif;
                    ?>
                </div>

                <div class="col-md-3">
                    <div class="main-name">
                        <span><?= Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getNameFIO(); ?></span><br>
                        <span><?= count(Yii::$app->user->identity->assignedRules) > 0 ? Yii::$app->user->identity->assignedRules[0]->description : '' ?></span>
                    </div>
                    <?=
                    Menu::widget([
                        'options' => ['class' => 'user-link'],
                        'items' => $menuItems,
                    ]);
                    ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</header>
<?php $menuItems = '' ?>
<main>
    <div class="container">
        <div class="row">
            <?php if (!Yii::$app->user->isGuest) : ?>
            <div class="col-md-2">
                <nav class="main-nav">
                    <div class="menu">
                        <h2>Управляющий</h2>
                        <?php
                        echo Nav::widget([
                            'items' => [
                                [
                                    'label' => Yii::t('menu', 'Home'),
                                    'url' => ['/site/index'],
                                    'active' => \Yii::$app->controller->id == 'site'
                                ],
                                [
                                    'label' => Yii::t('menu', 'Profile'),
                                    'url' => ['/profile'],
                                    'active' => \Yii::$app->controller->id == 'user'
                                ],
                                [
                                    'label' => Yii::t('menu', 'Users'),
                                    'url' => ['/users'],
                                    'visible' => Yii::$app->user->can('administrator'),
                                    'active' => \Yii::$app->controller->id == 'admin'
                                ],
                                [
                                    'label' => Yii::t('menu', 'RBAC'),
                                    'url' => ['/rbac'],
                                    'visible' => Yii::$app->user->can('administrator'),
                                    'active' => \Yii::$app->controller->id == 'rbac'
                                ],
                                [

                                    'label' => Yii::t('menu', 'References'),
                                    'url' => ['/references'],
                                    'active' => \Yii::$app->controller->id == 'references'
                                    //'visible' => !Yii::$app->user->can('administrator')
                                ],
                                [
                                    'label' => Yii::t('menu', 'Documents'),
                                    'url' => ['/documents'],
                                    'visible' => !Yii::$app->user->can('administrator'),
                                    'items' => [
                                        ['label' => Yii::t('documents', 'INCOMING'), 'url' => '/documents/incoming', 'active' => \Yii::$app->controller->id == 'incoming-docs'],
                                        ['label' => Yii::t('documents', 'OUTGOING'), 'url' => '/documents/outgoing', 'active' => \Yii::$app->controller->id == 'outgoing-docs'],
                                        ['label' => Yii::t('documents', 'INTERNAL'), 'url' => '/documents/internal', 'active' => \Yii::$app->controller->id == 'internal-docs'],
                                        ['label' => Yii::t('documents', 'OTHER'), 'url' => '/documents/other', 'active' => \Yii::$app->controller->id == 'other-docs'],
                                    ],
                                    'options' => ['class' => ( in_array(\Yii::$app->controller->id, ['outgoing-docs', 'incoming-docs', 'internal-docs', 'other-docs']) ? 'visible' : '')],
                                ],
                                [
                                    'label' => Yii::t('menu', 'Events'),
                                    'url' => ['/events'],
                                    'visible' => Yii::$app->user->can('administrator'),
                                    'active' => \Yii::$app->controller->id == 'default'
                                ],
                                [
                                    'label' => Yii::t('menu', 'LegalService'),
                                    'url' => ['/legal-service'],
                                    'visible' => ( Yii::$app->user->can('manager') ||
                                            Yii::$app->user->can('backOffice') ||
                                            Yii::$app->user->can('lawyer') ) && !Yii::$app->user->can('administrator'),
                                    'active' => \Yii::$app->controller->id == 'legal-service'
                                ],
                                [
                                    'label' => Yii::t('menu', 'BackOffice'),
                                    'url' => ['/back-office'],
                                    'visible' => ( Yii::$app->user->can('manager') ||
                                            Yii::$app->user->can('backOffice') ||
                                            Yii::$app->user->can('lawyer') ) && !Yii::$app->user->can('administrator'),
                                    'active' => \Yii::$app->controller->id == 'back-office'
                                ],
                                [
                                    'label' => Yii::t('menu', 'Creditor List'),
                                    'url' => ['/creditors'],
                                    'visible' => ( Yii::$app->user->can('manager') ||
                                            Yii::$app->user->can('backOffice') ||
                                            Yii::$app->user->can('lawyer') ) && !Yii::$app->user->can('administrator'),
                                    'items' => [
                                        ['label' => Yii::t('menu', 'Creditors'), 'url' => '/creditors/default/index', 'active' => \Yii::$app->controller->id == 'default'],
                                        ['label' => Yii::t('menu', 'Requests'), 'url' => '/creditors/request', 'active' => \Yii::$app->controller->id == 'request'],
                                    ],
                                    'options' => ['class' => ( \Yii::$app->controller->module->id == 'creditors') ? 'visible' : ''],
                                ],

                                [
                                    'label' => Yii::t('menu', 'CreditorsMeeting'),
                                    'url' => ['/meetings'],
                                    'visible' => ( Yii::$app->user->can('manager') ||
                                            Yii::$app->user->can('backOffice') ||
                                            Yii::$app->user->can('lawyer') ||
                                            Yii::$app->user->can('bookkeeping')) && !Yii::$app->user->can('administrator'),
                                    'active' => \Yii::$app->controller->module->id == 'meetings'
                                ],

                                [
                                    'label' => Yii::t('menu', 'PrintDocs'),
                                    'url' => ['/printdocs'],
                                    'visible' => ( Yii::$app->user->can('manager') ||
                                            Yii::$app->user->can('backOffice') ||
                                            Yii::$app->user->can('lawyer') ||
                                            Yii::$app->user->can('officeManager')) && !Yii::$app->user->can('administrator'),
                                    'active' => \Yii::$app->controller->module->id == 'printdocs'
                                ],
                            ],
                            'options' => ['class' => 'nav-vertical'],
                            //'activateParents' => true
                        ]);
                        ?>
                    </div>
                    <div class = "news">
                        <h2>Новости</h2>
                        <!--ul class = "news-list">
                            <li>
                                <h3>Заголовок новости</h3>
                                <time>20.03.2015</time>
                                <p>Парсонс аналитически вычленяет подсистемы социальной структуры, культуры, личности. Ориентации </p>
                                <a href = "#">Подробнее</a>
                            </li>
                            <li>
                                <h3>Заголовок новости</h3>
                                <time>20.03.2015</time>
                                <p>Парсонс аналитически вычленяет подсистемы социальной структуры, культуры, личности. Ориентации </p>
                                <a href = "#">Подробнее</a>
                            </li>
                            <li>
                                <h3>Заголовок новости</h3>
                                <time>20.03.2015</time>
                                <p>Парсонс аналитически вычленяет подсистемы социальной структуры, культуры, личности. Ориентации </p>
                                <a href = "#">Подробнее</a>
                            </li>
                        </ul-->
                    </div>
                </nav>
            </div>
            <div class="col-md-10">
                <div class="panel-danger" id="create-form-status-panel">
                    <div class="panel-heading" id="create-form-status" style="display: none;"></div>
                </div>
                <br>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>

                <?= $content ?>

            </div>
        </div>
        <?php else: ?>
            <?= $content ?>
        <?php endif; ?>
    </div>
    </div>
</main>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <a href="#" class="small-logo">logo</a>
            </div>
            <div class="col-md-8">
                <p>Copyright 2017, All right reserved</p>
            </div>
            <div class="col-md-2">
                <a href="#" class="social-btn vk-icon"></a>
                <a href="#" class="social-btn f-icon"></a>
            </div>
        </div>
    </div>
</footer>

<?php
Modal::begin([
        'header' => '<h3>Вопрос собрания</h3>',
        'headerOptions' => ['id' => 'modalHeader'],
        'id' => 'meeting-modal',
        'size' => 'modal-lg',
        //keeps from closing modal with esc key or by clicking out of the modal.
        // user must click cancel or X to close
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);
Modal::end();
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
