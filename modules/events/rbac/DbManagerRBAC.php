<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\events\rbac;

use Yii;
use yii\rbac\Item;
use app\modules\events\models\Events;

/**
 * DbManager represents an authorization manager that stores authorization information in database.
 *
 * The database connection is specified by [[db]]. The database schema could be initialized by applying migration:
 *
 * ```
 * yii migrate --migrationPath=@yii/rbac/migrations/
 * ```
 *
 * If you don't want to use migration and need SQL instead, files for all databases are in migrations directory.
 *
 * You may change the names of the tables used to store the authorization and rule data by setting [[itemTable]],
 * [[itemChildTable]], [[assignmentTable]] and [[ruleTable]].
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since 2.0
 */
class DbManagerRBAC extends \yii\rbac\DbManager {

    private function addEvent($role, $userId, $assign = true) {
        
       // var_dump($role);
        
        $regEvent = new Events();

        $regEvent->eventName = ($assign ? "assign" : "revoke") . ($role->type == Item::TYPE_ROLE ? "Role" : "Permission");

        $regEvent->modelClassName = $role::className();
        $regEvent->userId = !\Yii::$app->user->isGuest ? (int) \Yii::$app->user->identity->id : -1;
        $user = Yii::$app->user->identity->findIdentity($userId);
        $user = $user !== null ? $user->email : $userId;

        $changedAttributes["name"] = [
            'oldValue' => $assign ? "" : $role->name,
            'newValue' => $assign ? $role->name : "",
        ];
        $changedAttributes["description"] = [
            'oldValue' => $assign ? "" : $role->description,
            'newValue' => $assign ? $role->description : "",
        ];
        $changedAttributes["user"] = [
        'oldValue' => $assign ? "" : $user,
        'newValue' => $assign ? $user : "",
        ];

        $regEvent->changedAttributes = json_encode($changedAttributes);
        $regEvent->save();
    }

    protected function addItem($item) {
        parent::addItem($item);
    }

    public function addChild($parent, $child) {
        return parent::addChild($parent, $child);
    }

    public function removeChild($parent, $child) {
        return parent::removeChild($parent, $child);
    }

    public function assign($role, $userId) {
        $this->addEvent($role, $userId);
        return parent::assign($role, $userId);
    }

    public function revoke($role, $userId) {
        $this->addEvent($role, $userId, false);
        return parent::revoke($role, $userId);
    }

}
