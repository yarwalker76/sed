<?php

return [
    'Events' => 'Events',
    'EVENT_NAME' => 'Event name',
    'MODEL_CLASS_NAME' => 'Class model',
    'USER_ID' => 'User',
    'DATE_TIME' => 'Date event',
    'EVENT_AFTER_UPDATE' => 'Update',
    'EVENT_AFTER_INSERT' => 'Insert',
    'EVENT_BEFORE_DELETE' => 'Delete',
    
    'ASSIGN_ROLE' => 'Assign role',
    'ASSIGN_PERMISSION' => 'Assign permission',
    'REVOKE_ROLE' => 'Revoke role',
    'REVOKE_PERMISSION' => 'Revoke permission',
    
    'CHANGED_ATTRIBUTES' => 'Changed attributes',
    'ATTRIBUTE' => 'Attribute',
    'VALUE' => 'Value',
    'OLD_VALUE' => 'Old value',
    'NEW_VALUE' => 'New value',
];
