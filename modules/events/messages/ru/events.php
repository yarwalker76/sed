<?php

return [
    'Events' => 'События',
    
    'EVENT_NAME' => 'Название события',
    'MODEL_CLASS_NAME' => 'Класс модели',
    'USER_ID' => 'Пользователь',
    'DATE_TIME' => 'Дата события',
    
    'EVENT_AFTER_UPDATE' => 'Изменение',
    'EVENT_AFTER_INSERT' => 'Добавление',
    'EVENT_BEFORE_DELETE' => 'Удаление',
    
    'ASSIGN_ROLE' => 'Добавление роли',
    'ASSIGN_PERMISSION' => 'Добавление разрешения',
    'REVOKE_ROLE' => 'Удаление роли',
    'REVOKE_PERMISSION' => 'Удаление разрешения',
    
    
    'CHANGED_ATTRIBUTES' => 'Измененные поля',
    'ATTRIBUTE' => 'Название поля',
    'VALUE' => 'Значение',
    'OLD_VALUE' => 'Старое значение',
    'NEW_VALUE' => 'Новое значение',
    
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
];
