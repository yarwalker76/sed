<?php

namespace app\modules\events\behaviors;

use Yii;
use yii\db\Expression;
use yii\db\BaseActiveRecord;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\base\Event;
use yii\base\ModelEvent;
use app\modules\events\models\Events;

use app\modules\events\Module;
//use yii\db\AfterSaveEvent;

class EventBehavior extends Behavior {

    public $attributes = null;
    public $params = 1;
    public $notInsert = [];

    public function __construct($config = array()) {
        parent::__construct($config);
       // Event::off(BaseActiveRecord::className(), BaseActiveRecord::EVENT_BEFORE_DELETE);
    }

    public function events() {

        Event::on(BaseActiveRecord::className(), BaseActiveRecord::EVENT_AFTER_INSERT, function ($event) {
            $this->afterSave($event);
        });
        Event::on(BaseActiveRecord::className(), BaseActiveRecord::EVENT_AFTER_UPDATE, function ($event) {
            $this->afterSave($event);
        });

        Event::on(BaseActiveRecord::className(), BaseActiveRecord::EVENT_BEFORE_DELETE, function ($event) {
            $this->afterDelete($event);
        });
        return[
            BaseActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'afterSave', // 'afterSave',
            //ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
           // BaseActiveRecord::EVENT_BEFORE_DELETE => 'afterDelete',
        ];
    }

    public function afterSave($event) {
        $changedAttributes = [];
        $sender = $event->sender;

        if (($sender !== null) && ($sender::className() !== Events::className())) {
            $regEvent = new Events();
            $regEvent->eventName = $event->name;
            $regEvent->modelClassName = ($sender !== null) ? $sender::className() : "";
            $regEvent->userId = !\Yii::$app->user->isGuest ? (int) \Yii::$app->user->identity->id : -1;
            if ($sender !== null) {
                $pk = $sender->tableSchema->primaryKey[0];
                $regEvent->modelPKName = $pk;
                $regEvent->modelPKValue = (int) $sender->$pk;

                foreach ($event->changedAttributes as $key => $value) {
                    if ($event->changedAttributes[$key] . '' !== $event->sender->$key . '') {
                        $this->addAttributes($changedAttributes, $key, $event->changedAttributes[$key], $event->sender->$key);
                    }
                }
                $regEvent->changedAttributes = json_encode($changedAttributes);
            }
            if (count($changedAttributes) > 0) {
                $regEvent->save();
            }
        }
    }

    public function afterDelete($event) {
        $changedAttributes = [];
        $sender = $event->sender;
        if (($sender !== null) && ($sender::className() !== Events::className())) {
            $regEvent = new Events();
            $regEvent->eventName = $event->name;
            $regEvent->modelClassName = ($sender !== null) ? $sender::className() : "";
            $regEvent->userId = !\Yii::$app->user->isGuest ? \Yii::$app->user->identity->id : -1;

            if ($sender !== null) {
                $pk = $sender->tableSchema->primaryKey[0];
                $regEvent->modelPKName = $pk;
                $regEvent->modelPKValue = (int) $sender->$pk;

                foreach ($sender as $key => $value) {
                    $this->addAttributes($changedAttributes, $key, $value, "");
                }
                $regEvent->changedAttributes = json_encode($changedAttributes);
            }
            
            if (count($changedAttributes) > 0) {
                $regEvent->save();
            }
        }
    }

    function checkSender($sender = null, $params) {
        $result = false;
        if ($sender !== null) {
            foreach ($params as $param) {
                
            }
        }

        return $result;
    }

    function addAttributes(&$changedAttributes, $key, $oldValue, $newValue) {
       // var_dump(Module::getInstance());
                
        if ($this->attributes !== null) {
            if (!in_array($key, $this->attributes)) {
                return;
            }
        }
        $changedAttributes[$key] = [
            'oldValue' => $oldValue, // $event->changedAttributes[$key],
            'newValue' => $newValue, // $event->sender->$key,
        ];
    }

}
