<?php

namespace app\modules\events\models;

use Yii;

/**
 * This is the model class for table "event_titles".
 *
 * @property integer $id
 * @property string $event
 * @property string $title
 */
class EventTitles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_titles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event', 'title'], 'required'],
            [['event', 'title'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event' => Yii::t('app', 'Event'),
            'title' => Yii::t('app', 'Title'),
        ];
    }
}
