<?php

namespace app\modules\events\models;

use budyaga\users\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\events\models\Events;
use app\modules\events\models\EventsTitles;

/**
 * EventsSearch represents the model behind the search form about `app\models\Events`.
 */
class EventsSearch extends Events
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'modelPKValue', 'userId'], 'integer'],
            [['eventName', 'modelClassName', 'modelPKName', 'changedAttributes', 'date_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Events::find(); //->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],
        ]);

        $query->joinWith('eventTitles');
        $query->joinWith('eventModels');

        $dataProvider->setSort([
            'attributes' => [
                'eventName' => [
                    'asc' => [ 'event_titles.title' => SORT_ASC ],
                    'desc' => [ 'event_titles.title' => SORT_DESC ],
                ],
                'modelClassName' => [
                    'asc' => [ 'event_models.model_title' => SORT_ASC ],
                    'desc' => [ 'event_models.model_title' => SORT_DESC ],
                ],
                'userId',
                'date_time'
            ],
            'defaultOrder' => ['date_time' => SORT_DESC]
        ]);

        $this->load($params);
/*
        if (!$this->validate(['scenario' => Events::SCENARIO_USER_SEARCH])) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
*/
        // grid filtering conditions
        $query->andFilterWhere([
           // 'id' => $this->id,
            //'eventName' => $this->eventName,
            //'modelPKValue' => $this->modelPKValue,
            //'userId' => $this->userId,
            //'date_time' => $this->date_time,
        ]);

        $query
            ->andFilterWhere(['eventName' => $this->getEventKeys($this->eventName)])
            ->andFilterWhere(['modelClassName' => $this->getModelKeys($this->modelClassName)])
            ->andFilterWhere(['userId' => $this->getUsers($this->userId)])
            ->andFilterWhere(['like', 'date_time', $this->date_time]);

            //->andFilterWhere(['like', 'modelPKName', $this->modelPKName])
            //->andFilterWhere(['like', 'changedAttributes', $this->changedAttributes]);

        return $dataProvider;
    }

    public function getEventKeys($str) {
        $result = [];

        if( $str ):
            $event_titles = EventTitles::find()->all();

            foreach( $event_titles as $item ):
                if(strpos(mb_strtoupper($item->title), mb_strtoupper($str)) !== FALSE ):
                    $result[] = $item->event;
                endif;
            endforeach;
        endif;

        return $result;
    }

    public function getModelKeys($str) {
        $result = [];

        if( $str ):
            $model_titles = EventModels::find()->all();
            foreach( $model_titles as $item ):
                if(strpos(mb_strtoupper($item->model_title), mb_strtoupper($str)) !== FALSE ):
                    $result[] = $item->event_model;
                endif;
            endforeach;
        endif;

        return $result;
    }

    public function getUsers($str) {
        $result = [];

        if( $str ):
            $users = User::find()->where(['like', 'email', $str])->all();
            foreach( $users as $user ):
                if(strpos(mb_strtoupper($user->email), mb_strtoupper($str)) !== FALSE ):
                    $result[] = $user->id;
                endif;
            endforeach;
        endif;

        return $result;
    }

}
