<?php

namespace app\modules\events\models;

use Yii;

/**
 * This is the model class for table "event_models".
 *
 * @property integer $id
 * @property string $event_model
 * @property string $model_title
 */
class EventModels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_models';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_model', 'model_title'], 'required'],
            [['event_model', 'model_title'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_model' => Yii::t('app', 'Event Model'),
            'model_title' => Yii::t('app', 'Model Title'),
        ];
    }
}
