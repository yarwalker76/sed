<?php

namespace app\modules\events\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $eventName
 * @property string $modelClassName
 * @property string $modelPKName
 * @property integer $modelPKValue
 * @property string $changedAttributes
 * @property integer $userId
 * @property string $date_time
 */
class Events extends \yii\db\ActiveRecord {

    const SCENARIO_USER_SEARCH = 'user_search';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'events';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_USER_SEARCH] = ['modelPKValue', 'date_time', 'modelClassName', 'modelPKName', 'eventName' ];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['modelPKValue', 'userId'], 'integer'],
            [['changedAttributes'], 'string'],
            [['date_time'], 'safe'],
            [['eventName'], 'string', 'max' => 20],
            [['modelClassName'], 'string', 'max' => 50],
            [['modelPKName'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('events', 'ID'),
            'eventName' => Yii::t('events', 'EVENT_NAME'),
            'modelClassName' => Yii::t('events', 'MODEL_CLASS_NAME'),
            'modelPKName' => Yii::t('events', 'MODEL_PKNAME'),
            'modelPKValue' => Yii::t('events', 'MODEL_PKVALUE'),
            'changedAttributes' => Yii::t('events', 'CHANGED_ATTRIBUTES'),
            'userId' => Yii::t('events', 'USER_ID'),
            'date_time' => Yii::t('events', 'DATE_TIME'),
        ];
    }

    public function getEventTitles()
    {
        return $this->hasOne(EventTitles::className(), ['event' => 'eventName']);
    }

    public function getEventTitle()
    {
        $event_title = $this->eventTitles;

        return $event_title ? $event_title->title : '';
    }

    public function getEventModels()
    {
        return $this->hasOne(EventModels::className(), ['event_model' => 'modelClassName']);
    }

    public function getEventModelTitle()
    {
        $event_model = $this->eventModels;

        return $event_model ? $event_model->model_title : '';
    }

    public function getEventName($name) {
        switch ($name) {
            case \yii\db\BaseActiveRecord::EVENT_AFTER_INSERT:
                return Yii::t('events', 'EVENT_AFTER_INSERT');
            case \yii\db\BaseActiveRecord::EVENT_AFTER_UPDATE:
                return Yii::t('events', 'EVENT_AFTER_UPDATE');
            case \yii\db\BaseActiveRecord::EVENT_BEFORE_DELETE:
                return Yii::t('events', 'EVENT_BEFORE_DELETE');
            case "assignRole":
                return Yii::t('events', 'ASSIGN_ROLE');
            case "assignPermission":
                return Yii::t('events', 'ASSIGN_PERMISSION');
            case "revokeRole":
                return Yii::t('events', 'REVOKE_ROLE');
            case "revokePermission":
                return Yii::t('events', 'REVOKE_PERMISSION');
            case 'afterDelete':
                return Yii::t('events', 'EVENT_BEFORE_DELETE');
                break;
            default :
                return $name;
        }
    }

    public function getModelAttributeLabel($name) {
        $model = new $this->modelClassName(); //Создаем класс из текстового поля
        return $model !== null && method_exists($model, 'getAttributeLabel') && $model->getAttributeLabel($name) ? $model->getAttributeLabel($name) : $name;
        /*
          if ($model !== null) {
          return method_exists($model, 'getAttributeLabel') && $model->getAttributeLabel($name) !== null ? $model->getAttributeLabel($name) : $name;
          } else{
          return $name;
          } */
    }

    public function addAttribute($key, $oldValue, $newValue) {
        if ($this->attributes !== null) {
            if (!in_array($key, $this->attributes)) {
                return;
            }
        }
        $changedAttributes[$key] = [
            'oldValue' => $oldValue, // $event->changedAttributes[$key],
            'newValue' => $newValue, // $event->sender->$key,
        ];
    }

}
