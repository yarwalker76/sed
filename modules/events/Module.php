<?php

namespace app\modules\events;

use Yii;
use yii\base\Event;
use yii\db\ActiveRecord;

/**
 * events module definition class
 */
class Module extends \yii\base\Module {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\events\controllers';
    public $ModelClassNames = [];
    public $ModelSettings = [];

    /**
     * @inheritdoc
     */
    
    
    public function init() {
        parent::init();
      
        self::registerTranslations();
    }

    public static function registerTranslations() {
        if (!isset(Yii::$app->i18n->translations['events']) && !isset(Yii::$app->i18n->translations['events/*'])) {
            Yii::$app->i18n->translations['events'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/modules/events/messages',
                'sourceLanguage' => Yii::$app->language,
                'forceTranslation' => true,
                'fileMap' => [
                    'events' => 'events.php'
                ]
            ];
        }
    }
        
    public function getModelClassName($class){
        
        return array_key_exists($class, $this->ModelSettings) && isset($this->ModelSettings[$class]["modelName"]) ? $this->ModelSettings[$class]["modelName"] : $class;
        /*
        if(array_key_exists($class, $this->ModelSettings)){
            return isset($this->ModelSettings[$class]["modelName"]) ? $this->ModelSettings[$class]["modelName"] : $class;
        }else{
            return $class;
        }*/
    }
    
    /*
    public function getModelClassName($class){
        
        return array_key_exists($class, $this->ModelSettings) &&  isset($this->ModelSettings[$class]["modelName"]) ? $this->ModelSettings[$class]["modelName"] : $class;
        /*
        if(array_key_exists($class, $this->ModelSettings)){
            return isset($this->ModelSettings[$class]["modelName"]) ? $this->ModelSettings[$class]["modelName"] : $class;
        }else{
            return $class;
        }
    }
    */
}
