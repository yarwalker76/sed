<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use budyaga\users\models\User;
use app\models\TblOrganizationsTypes;
use app\models\TblOrganizationsMethods;

/* @var $this yii\web\View */
/* @var $model app\models\Events */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('events', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=
        Html::a(Yii::t('events', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('events', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p> 

    <?php if (!\Yii::$app->user->isGuest): ?>
        <table id="w0" class="table table-striped table-bordered detail-view">
            <tbody> 
                <tr>
                    <th><?= \Yii::t('events', $model->getAttributeLabel('eventName')) ?></th>
                    <td><?= $model->getEventName($model->eventName) ?></td>
                </tr>
                <tr>
                    <th><?= \Yii::t('events', $model->getAttributeLabel('modelClassName')) ?></th>
                    <td><?= $this->context->module->getModelClassName($model->modelClassName) ?></td>
                </tr>
                <tr>
                    <th><?= \Yii::t('events', $model->getAttributeLabel('changedAttributes')) ?></th>
                    <td>
                        <table id="w0" class="table table-striped table-bordered detail-view">
                            <?php $changedAttributes = json_decode($model->changedAttributes); ?>

                            <?php 
                                $update = false;
                                foreach ($changedAttributes as $item => $value){
                                    $update = (($value->oldValue !== NULL) && ($value->newValue !== NULL));
                                    if($update)break;
                            }?>
                            <?php if ($update): ?>
                                <tbody>
                                    <tr>
                                        <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                                        <th><?= \Yii::t('events', 'OLD_VALUE') ?></th>
                                        <th><?= \Yii::t('events', 'NEW_VALUE') ?></th>
                                    </tr>
                                    <?php foreach ($changedAttributes as $item => $value): ?>
                                        <tr>
                                            <?php  //var_dump($item); ?>
                                            <?php if($item =='password_hash'){ ?>
                                            <td><?= \Yii::t('users', 'PASSWORD')?> </td>
                                            <td colspan="2" style="text-align:center">Данное поле запрещено к отображению</td>
                                            <?php }elseif($item == 'updated_at'){ ?>
                                            <td><?= $model->getModelAttributeLabel($item) ?></td>
                                            <td><?= date('Y-m-d H:i:s', $value->oldValue) ?></td>
                                            <td><?= date('Y-m-d H:i:s', $value->newValue) ?></td>
                                            <?php }else{ ?>
                                            <td><?= $model->getModelAttributeLabel($item) ?></td>
                                            <td>
                                            <?php 
                                                
                                                if($item == 'status' && isset(User::getStatusArray()[$value->oldValue])){
                                                    echo User::getStatusArray()[$value->oldValue];
                                                }elseif($item == 'organizations_types_id' && ($res = TblOrganizationsTypes::findOne($val)) !== null){
                                                    echo $res->type;
                                                }else{
                                                    echo $value->oldValue;
                                                }
                                            ?>
                                            </td>
                                            <td>
                                            <?php
                                                if($item == 'status'&& isset(User::getStatusArray()[$value->oldValue])){
                                                    echo User::getStatusArray()[$value->newValue];
                                                }elseif($item == 'organizations_types_id' && ($res = TblOrganizationsTypes::findOne($val)) !== null){
                                                    echo $res->type;
                                                }else{
                                                    echo $value->newValue;
                                                }
                                            ?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            <?php else: ?>
                                <tbody>
                                    <tr>
                                        <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                                        <th><?= \Yii::t('events', 'VALUE') ?></th>
                                    </tr>
                                    <?php foreach ($changedAttributes as $item => $value): ?>
                       
                                        <tr><?php // var_dump($item); ?>
                                            <td><?= $model->getModelAttributeLabel($item) ?></td>
                                            <td>
                                            
                                            <?php 
                                            
                                                $val = $value->oldValue !== NULL ? $value->oldValue : $value->newValue;
                                                if($item == 'organizations_types_id' && ($res = TblOrganizationsTypes::findOne($val)) !== null){
                                                    echo $res->type;
                                                }elseif($item == 'proc_id1'){
                                                    $params = TblOrganizationsMethods::getParams($val);
                                                    print_r($params);
                                                }elseif($item == 'status'&& isset(User::getStatusArray()[$value->oldValue])){
                                                    echo User::getStatusArray()[$val];
                                                }else{
                                                    echo $val;
                                                }
                                            ?>
                                            
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            <?php endif; ?>
                            <?php /*
                              <?php if ($changedAttributes->name->newValue): ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'OLD_VALUE') ?></th>
                              <th><?= \Yii::t('events', 'NEW_VALUE') ?></th>
                              </tr>
                              <?php foreach ($changedAttributes as $item => $value): ?>
                              <tr>
                              <td><?= $model->getModelAttributeLabel($item) ?></td>
                              <td><?= $value->oldValue ?></td>
                              <td><?= $value->newValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php else: ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'VALUE') ?></th>
                              </tr>
                              <?php foreach (json_decode($model->changedAttributes) as $item => $value): ?>
                              <tr>
                              <td><?= $model->getModelAttributeLabel($item) ?></td>
                              <td><?= $value->oldValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php endif; ?>

                              <?php elseif ($changedAttributes->name->newValue): ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'VALUE') ?></th>
                              </tr>
                              <?php foreach (json_decode($model->changedAttributes) as $item => $value): ?>
                              <tr>
                              <td><?= $model->getModelAttributeLabel($item) ?></td>
                              <td><?= $value->newValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php endif; ?>



                              <?php if ($changedAttributes->name->oldValue): ?>

                              <?php if ($changedAttributes->name->newValue): ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'OLD_VALUE') ?></th>
                              <th><?= \Yii::t('events', 'NEW_VALUE') ?></th>
                              </tr>
                              <?php foreach ($changedAttributes as $item => $value): ?>
                              <tr>
                              <td><?= $model->getModelAttributeLabel($item) ?></td>
                              <td><?= $value->oldValue ?></td>
                              <td><?= $value->newValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php else: ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'VALUE') ?></th>
                              </tr>
                              <?php foreach (json_decode($model->changedAttributes) as $item => $value): ?>
                              <tr>
                              <td><?= $model->getModelAttributeLabel($item) ?></td>
                              <td><?= $value->oldValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php endif; ?>

                              <?php elseif ($changedAttributes->name->newValue): ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'VALUE') ?></th>
                              </tr>
                              <?php foreach (json_decode($model->changedAttributes) as $item => $value): ?>
                              <tr>
                              <td><?= $model->getModelAttributeLabel($item) ?></td>
                              <td><?= $value->newValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php endif; ?>
                              <?php /*
                              <?php if ($model->eventName == "afterInsert"): ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'VALUE') ?></th>
                              </tr>
                              <?php foreach (json_decode($model->changedAttributes) as $item => $value): ?>
                              <tr>
                              <td><?= $model->getModelAttributeLabel($item) ?></td>
                              <td><?= $value->newValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php elseif ($model->eventName == "afterUpdate"): ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'OLD_VALUE') ?></th>
                              <th><?= \Yii::t('events', 'NEW_VALUE') ?></th>
                              </tr>
                              <?php foreach (json_decode($model->changedAttributes) as $item => $value): ?>
                              <tr>
                              <td><?= $model->getModelAttributeLabel($item) ?></td>
                              <td><?= $value->oldValue ?></td>
                              <td><?= $value->newValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php elseif ($model->eventName == "afterDelete"): ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'VALUE') ?></th>
                              </tr>
                              <?php foreach (json_decode($model->changedAttributes) as $item => $value): ?>
                              <tr>
                              <td><?= $model->getModelAttributeLabel($item) ?></td>
                              <td><?= $value->oldValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php elseif ($model->eventName == "assignPermission"): ?>
                              <tbody>
                              <tr>
                              <th><?= \Yii::t('events', 'ATTRIBUTE') ?></th>
                              <th><?= \Yii::t('events', 'VALUE') ?></th>
                              </tr>
                              <?php foreach (json_decode($model->changedAttributes) as $item => $value): ?>
                              <tr>
                              <td><?= $item ?></td>
                              <td><?= $value->newValue ?></td>
                              </tr>
                              <?php endforeach; ?>
                              </tbody>
                              <?php endif; ?>

                             */ ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th><?= \Yii::t('events', $model->getAttributeLabel('userId')) ?></th>
                    <td><?= Yii::$app->user->identity->findIdentity($model->userId) !== null ? Yii::$app->user->identity->findIdentity($model->userId)->email : "" ?></td>
                </tr>
                <tr>
                    <th><?= \Yii::t('events', $model->getAttributeLabel('date_time')) ?></th>
                    <td><?= $model->date_time ?></td>
                </tr>
            </tbody>
        </table>
    <?php endif; ?>
    <?php /*
      <?=
      DetailView::widget([
      'model' => $model,
      'attributes' => [
      'id',
      'eventName',
      'modelClassName',
      'changedAttributes:ntext',
      'userId',
      'date_time',
      ],
      ]) */
    ?>

</div>
