<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('events', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="events-index-index shadow-box">
    <div class = "row">
        <div class = "col-xs-12 tables">
            <h2 class="title"><?= Html::encode($this->title) ?></h2>
            <div class="col-xs-12">
                <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
                <br>
                <?php

                Pjax::begin();

                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'id',
                        //'eventName',
                        [
                            'attribute' => 'eventName',
                            'content' => function($data) {
                                return $data->getEventTitle(); //$data->getEventName($data->eventName);
                            },
                        ],
                        //'modelClassName',
                        [
                            'attribute' => 'modelClassName',
                            'content' => function($data) {
                                return $data->getEventModelTitle(); //$this->context->module->getModelClassName($data->modelClassName);
                            }
                        ],
                        [
                            'attribute' => 'userId',
                            'content' => function($data) {
                                return isset($data->userId) && (Yii::$app->user->identity->findIdentity($data->userId) !== null) ? Html::a(Yii::$app->user->identity->findIdentity($data->userId)->email, ['/admin/view', 'id' => $data->userId]) : ""; // $data->getEventName($data->eventName);
                            }
                                ],
                                'date_time',
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {delete}',
                                ],
                            ],
                            'tableOptions' => [
                                'class' => 'sTables table'
                            ],
                                ]
                        );
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php /*
          <div class="events-index">

          <h1><?= Html::encode($this->title) ?></h1>
          <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
          <?php /*
          <p>
          <?= Html::a(Yii::t('events', 'Create Events'), ['create'], ['class' => 'btn btn-success']) ?>
          </p>
          ?>
          <?=
          GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          // 'id',
          [
          'attribute' => 'eventName',
          'content' => function($data) {
          return $data->getEventName($data->eventName);
          }
          ],
          //'modelClassName',
          [
          'attribute' => 'modelClassName',
          'content' => function($data) {
          return $this->context->module->getModelClassName($data->modelClassName);
          }
          ],
          [
          'attribute' => 'userId',
          'content' => function($data) {
          return Html::a(Yii::$app->user->identity->findIdentity($data->userId)->email, ['/admin/view', 'id' => $data->userId]); // $data->getEventName($data->eventName);
          }
          ],
          'date_time',
          [
          'class' => 'yii\grid\ActionColumn',
          'template' => '{view} {delete}',
          ],
          ],
          ]);
          ?>
          </div>
         */ ?>