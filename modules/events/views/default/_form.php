<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'eventName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelClassName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelPKName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelPKValue')->textInput() ?>

    <?= $form->field($model, 'changedAttributes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'date_time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('events', 'Create') : Yii::t('events', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
