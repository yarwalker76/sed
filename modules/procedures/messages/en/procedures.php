<?php

return [
    'DOCUMENTS' => 'Делопроизводство',
    'INCOMING' => 'Входящие',
    'OUTGOING' => 'Исходящие',
    'INTERNAL' => 'Внутренние',
    'OTHER' => 'Другие',
    'CREATE_RECORD' => 'Создать запись',
    'CLONE_RECORD' => 'Клонировать запись',
    'DELETE_RECORD' => 'Удалить запись',
    'PRINT_ENVELOPS' => 'Печать конвертов',
    'PRINT_REGISTER' => 'Печать реестра',
    'ID' => 'ID',
    'PROC_ID' => 'Идентификатор процедуры',
    'SERIAL_NUMBER' => '№',
    'REQUEST_TYPE_ID' => 'Тип запроса',
    'RESPONSE_TYPE_ID' => 'Тип ответа',
    'NAME' => 'Название',
    'RECEIVE_CONTRACTOR' => 'Адресат',
    'SEND_CONTRACTOR' => 'Отправитель',
    'SEND_DATE' => 'Дата отправки',
    'RECEIVE_DATE' => 'Дата получения',
    'DATE' => 'Дата',
    'DELIVERY_ADDRESS' => 'Адрес доставки',
    'NOTES' => 'Заметки',
    'FILES' => 'Файлы',
    'CREATE_OUTGOING_RECORD' => 'Создание записи исходящего реестра',
    'CHOOSE_ONE' => 'Выберите одно значение',
    'CHOOSE' => 'Выбрать',
    'CONTRACTORS' => 'Контрагенты',

    'Organizations' => 'Организации',
    'Create organization' => 'Создать организацию',
    'Creatig organization' => 'Создание организации',
    'Title'=> 'Название',
    'Card of organization'=> 'Карточка организации',
    
    'Params' => 'Список параметров',
    'Dynamic params' => 'Динамические параметры',
    'Custom params' => 'Специальные параметры',

    'Main params' => 'Общие параметры',
    'Type of organization' => 'Тип организации',
    'Title of organization' => 'Название организации',
    'INN' => 'ИНН',
    
    'Contact information' => 'Контактная информация',
    'Contacts' => 'Контакты',
    'Phone' => 'Телефон',
    'Mobile' => 'Мобильный',
    'Fax' => 'Факс',
    'E-mail' => 'Эл. почта',
    
    'Full address' => 'Полный адрес',
    'Postcode' => 'Почтовый индекс',
    'Region' => 'Субъект РФ',
    'Locality' => 'Населенный пункт',
    'Address' => 'Адрес',
        
    //'' => '',

    'Create' => 'Создать',
    'Update' => 'Изменить',
    'Delete' => 'Удалить',
    
    'Saved!' => 'Сохранено!',
    'WARNING!!!' => 'Сохранено без параметров!',
    'ERROR.' => 'Ошибка сохранения данных.',
    
    'Are you sure you want to delete this item?' => 'Удалить?',
    
];