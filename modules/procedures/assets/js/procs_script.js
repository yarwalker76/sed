(function($){
	
        /*$('.nav-vertical a.dropdown-toggle').on('click', function(ev){
            ev.preventDefault();
            $(this).next('ul.dropdown-menu').slideToggle(350);
        }); */

        $(document).on('submit', '#contractor-search-form', function (e) {
            e.preventDefault();
            
            $.post( $(this).attr('action'), $(this).serialize(), function(data){
                
                if( data.error !== undefined ) {
                    $('#contractors-list-form').html('<p>' + data.error + '</p>');
                  //  $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                  //  $('#modalMessage').modal('show');
                } else {
                    $('#contractors-list-form').html(data.contractors);
                } 
                
            }, "json")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                //$('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                //$('#modalMessage').modal('show');
            }); 

        });

        $('#manager-search-form').on('submit', function(ev){
            ev.preventDefault();

            $.post( $(this).attr('action'), $(this).serialize(), function(data){
                console.info('managers', data);

                if( data.error !== undefined ) {
                    $('#managers-list-form').html('<p>' + data.error + '</p>');
                  //  $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                  //  $('#modalMessage').modal('show');
                } else {
                    $('#managers-list-form').html(data.managers);
                } 
                
            }, "json")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                //$('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                //$('#modalMessage').modal('show');
            });   
            
        });

        // определяем тип для последующей обработки формы
        $(document).on('click','.choose-contractor-btn, .choose-applicant-btn', function(){
            var type = $(this).attr('data-type');
            console.info('click btn type', type);
            
            $('#contractors-list-form').data('type', type);    
            console.info('form type', $('#contractors-list-form').data('type') );
            
            // запрос на выбор типов справочников в форме поиска 
            $.post( '/procedures/default/get-search-form', { 'type': type }, function(data){
                $('#modalContractors .modal-body #contractor-search-form').remove();
                $('#modalContractors .modal-body').prepend(data);
                
            }, "html")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }); 
        });
        
        $(document).on('submit','#contractors-list-form', function(ev){
            ev.preventDefault();

            var type = $(this).data('type');
            console.info('submit form type', $(this).data('type'));
            switch (type) {
                case 'choose_pd':
                    $('#procedures-name').val( $('#contractors-list :radio:checked').val() );
                    $('#procedures-inn').val( $('#contractors-list :radio:checked').data('inn') );
                    $('#procedures-organization_id').val( $('#contractors-list :radio:checked').data('id') );
                    $('#procedures-applicant_id').val( $('#contractors-list :radio:checked').data('id') );
                    break;
                case 'choose_court':
                    $('#procedures-court').val( $('#contractors-list :radio:checked').val() );
                    $('#procedures-court_id').val( $('#contractors-list :radio:checked').data('id') );
                    break;
                case 'choose_judge':
                    $('#procedures-judge').val( $('#contractors-list :radio:checked').val() );
                    $('#procedures-judge_id').val( $('#contractors-list :radio:checked').data('id') );
                    break;  
                case 'choose_applicant':
                    $('#procedures-applicant').val( $('#contractors-list :radio:checked').val() );
                    $('#procedures-applicant_id').val( $('#contractors-list :radio:checked').data('id') );
                    break; 
                case 'choose_manager':
                    $('#procedures-manager').val( $('#contractors-list :radio:checked').val() );
                    $('#procedures-manager_id').val( $('#contractors-list :radio:checked').data('id') );
                    break;    
                case 'choose_newspaper':
                    $('#proceduresadditionalproperties-name').val( $('#contractors-list :radio:checked').val() );
                    $('#proceduresadditionalproperties-np_id').val( $('#contractors-list :radio:checked').data('id') );
                    break;    
            }

            // на закрытие модального окна очищаем форму
            $('#modalContractors').modal('hide');
            $('#contractors-list-form').html('');
        });

        $('#managers-list-form').on('submit', function(ev){
            ev.preventDefault();

            $('#procedures-manager').val( $('#managers-list :radio:checked').val() );
            $('#procedures-manager_id').val( $('#managers-list :radio:checked').data('id') );
        
            // на закрытие модального окна очищаем форму
            $('#modalManagers').modal('hide');
            $('#managers-list-form').html('');
        });

        // на закрытие модального окна очищаем форму
        $('#modalContractors').on('hide.bs.modal', function(){
            // очистим форму
            $('#contractors-list-form').html('');
            $('#modalContractors .modal-body .contractors-form').remove();
            console.log('clear modal form');
            
        });

        // на закрытие модального окна очищаем форму
        $('#modalManagers').on('hide.bs.modal', function(){
            // очистим форму
            $('#managers-list-form').html('');
        });

        $(document).on('click', '.del-file', function(ev){
            ev.preventDefault();

            $.post( '/documents/default/del-file', { 'id': $(this).data('id') }, function(data){
                
                if( data.error !== undefined ) {
                    //$('#contractors-list').html('<p>' + data.error + '</p>');
                    $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                    $('#modalMessage').modal('show');
                } else {
                    $('#modalMessage .modal-body').html('<p>' + data.result + '</p>');
                    $('#modalMessage').modal('show');

                    // обновим список файлов в записи
                    $.post('/documents/default/get-files-list', { 'id': $('#' + $('#doc-type').val() + 'docs-id').val(), 'doc_type': $('#doc-type').val() }, function(data){
                        $('ul.doc-files li').remove();
                        $('ul.doc-files').html(data.files);    
                        console.info('data', data);
                    }, "json")
                    .fail(function(error){
                        //$('#modal').modal('hide');
                        console.info('func error', error);
                        $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                        $('#modalMessage').modal('show');
                    });
                } 
                
            }, "json")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }); 
        });


        $(document).on('click', '#delete-doc-records', function(ev){
            ev.preventDefault();

            $.post('/documents/default/get-delete-confirm', {}, function(data){
                $('#modalMessage .modal-body').html(data);
                $('#modalMessage').modal('show');
            }, "html")
            .fail(function(error){
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            });

            
        });
        
        $(document).on('click', 'button#cancel, #proc-confirm-cancel', function(){
            $('#modalMessage').modal('hide');
            $('#modalExportRegister').modal('hide');
        });

        $(document).on('click', 'button#delete-selected-records', function(){

            var keys = $('#' + $('#delete-doc-records').data('doc-type') + '-docs-grid').yiiGridView('getSelectedRows');
            
            $.post( '/documents/default/delete-doc-records', { 'ids': keys, 'doc_type': $('#delete-doc-records').data('doc-type') }, function(data){
                if( data.error !== undefined ) {
                    //$('#contractors-list').html('<p>' + data.error + '</p>');
                    $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                    $('#modalMessage').modal('show');
                } else {
                    $('#modalMessage .modal-body').html('<p>' + data.result + '</p>');
                    $('#modalMessage').modal('show');
                } 

                // обновим GridView с записями
                $.pjax.reload({container: '#' + $('#delete-doc-records').data('doc-type') + '-docs-pjax'});
                
            }, "json")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }); 
        });

        // дизейблим кнопку клонирования если выделено больше одной записи
        $(document).on('click','#outgoing-docs-grid tbody :checkbox, #incoming-docs-grid tbody :checkbox, #internal-docs-grid tbody :checkbox, #other-docs-grid tbody :checkbox', function(){ 
            if( $('#outgoing-docs-grid tbody :checkbox:checked').length ) {
                $('.outgoing-docs-index #export-register').removeClass('disabled');
            } else {
                $('.outgoing-docs-index #export-register').addClass('disabled');
            }

            if( $('#outgoing-docs-grid tbody :checkbox:checked').length != 1) {
                $('.outgoing-docs-index #clone-doc-record').addClass('disabled');
            } else {
                $('.outgoing-docs-index #clone-doc-record').removeClass('disabled');
                var keys = $('#outgoing-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);
                var url = $('.outgoing-docs-index #clone-doc-record').attr('href'); console.info('url out', url + ' - ' + url.replace(/id=\d+/, 'id=' + keys[0]));
                $('.outgoing-docs-index #clone-doc-record').attr('href', url.replace(/id=\d+/, 'id=' + keys[0]));
            }

            if( $('#incoming-docs-grid tbody :checkbox:checked').length != 1) {
                $('.incoming-docs-index #clone-doc-record').addClass('disabled');
            } else {
                $('.incoming-docs-index #clone-doc-record').removeClass('disabled');
                var keys = $('#incoming-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);
                var url = $('.incoming-docs-index #clone-doc-record').attr('href'); console.info('url in', url + ' - ' + url.replace(/id=\d+/, 'id=' + keys[0]));
                $('.incoming-docs-index #clone-doc-record').attr('href', url.replace(/id=\d+/, 'id=' + keys[0]));
            }

            if( $('#internal-docs-grid tbody :checkbox:checked').length != 1) {
                $('.internal-docs-index #clone-doc-record').addClass('disabled');
            } else {
                $('.internal-docs-index #clone-doc-record').removeClass('disabled');
                var keys = $('#internal-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);
                var url = $('.internal-docs-index #clone-doc-record').attr('href'); console.info('url out', url + ' - ' + url.replace(/id=\d+/, 'id=' + keys[0]));
                $('.internal-docs-index #clone-doc-record').attr('href', url.replace(/id=\d+/, 'id=' + keys[0]));
            }

            if( $('#other-docs-grid tbody :checkbox:checked').length != 1) {
                $('.other-docs-index #clone-doc-record').addClass('disabled');
            } else {
                $('.other-docs-index #clone-doc-record').removeClass('disabled');
                var keys = $('#other-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);
                var url = $('.other-docs-index #clone-doc-record').attr('href'); console.info('url out', url + ' - ' + url.replace(/id=\d+/, 'id=' + keys[0]));
                $('.other-docs-index #clone-doc-record').attr('href', url.replace(/id=\d+/, 'id=' + keys[0]));
            }
        });

        $(document).on('click','#outgoing-docs-grid thead :checkbox, #incoming-docs-grid thead :checkbox, #internal-docs-grid thead :checkbox, #other-docs-grid thead :checkbox, #delete-doc-records', function(){
            $('#clone-doc-record').addClass('disabled');
            

            setTimeout(function(){ 
                if( $('#outgoing-docs-grid tbody :checkbox:checked').length ) {
                    $('.outgoing-docs-index #export-register').removeClass('disabled');
                } else {
                    $('.outgoing-docs-index #export-register').addClass('disabled');
                }  
            }, 100);
            
        });

        // обнуляем поле ID исходящего документа в форме входящего документа, если поле поиска пустое
        $('#fake-response_on_name').on('change', function(){
            if( $('#fake-response_on_name').val() == ''){
                $('#incomingdocs-response_on_id').val(0);
            }
        });

        $('#export-register').on('click', function(ev){
            ev.preventDefault();

            var keys = $('#outgoing-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);

            $.post('/documents/outgoing-docs/get-export-register-confirm', { 'ids': keys }, function(data){
                $('#modalExportRegister .modal-body').html(data);
                $('#modalExportRegister').modal('show');
            }, "html")
            .fail(function(error){
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }); 
        });

        $(document).on('click', '#print-register-btn', function(ev){
            var keys = $('#outgoing-docs-grid').yiiGridView('getSelectedRows');

            $.post( '/documents/outgoing-docs/export-register', { 'ids': keys }, function(data){
                console.info('data', data);

                if( data.error !== undefined ) {
                    //$('#contractors-list').html('<p>' + data.error + '</p>');
                    //$('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                    //$('#modalMessage').modal('show');
                } else {
                    //$('#modalMessage .modal-body').html('<p>' + data.result + '</p>');
                    //$('#modalMessage').modal('show');
                } 

                // обновим GridView с записями
                //$.pjax.reload({container: '#' + $('#delete-doc-records').data('doc-type') + '-docs-pjax'});
                
            }, "json")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }); 
        });

        $('#procedures-create_date').on('change', function(){
            $('#procedures-manager_approval_date').val( $(this).val() );
            $('#procedures-judgment_date').val( $(this).val() );
        });
        
        $(document).on('click','#applicant-fields :radio', function(){
            if( $('#other-debtor').is(':checked') ){
                $('.choose-applicant-btn').prop('disabled', false);
                $('#procedures-applicant_id').val('');
            } else {
                $('.choose-applicant-btn').prop('disabled', true);
                $('#procedures-applicant').val('');
                $('#procedures-applicant_id').val( $('#debtor').val() );
            }
        });
        
        $(document).on('click','.choose-applicant-btn', function(){
            $('#contractors-list-form').data( 'type', $(this).data('type') );
        });
        
        $('#procedures-approval_basis :radio').on('click', function(){
            console.info('click');

            if( $('#procedures-approval_basis :radio[value="Определение"]').is(':checked') ) { console.info('определение');
                $('#procedures-manager_approval_date')
                    .prop('disabled', false)
                    .val('');
            } else { console.info('не определение');
                $('#procedures-manager_approval_date')
                    .prop('disabled', true)
                    .val( $('#procedures-create_date').val() );
            }
        });
        
        $(document).on('click','#collapse-prolongation-list .well .glyphicon.glyphicon-plus', function(){
            var $this = $(this),
                $groups = $this.parent().find('.form-group'),
                $group_cnt = $groups.length,
                $group = $groups.last().clone(),
                str = '';
            
            if( $group_cnt < 6 ){
                
                $group.find(':text').each(function(idx){
                    var order = $(this).data('order') + 1; 

                    // Обнулим значение поля
                    $(this).val('');
                    
                    switch (idx) {
                       case 0:
                          $(this).attr('name', 'ProceduresProlongations[' + order + '][date_with]');
                          $(this).attr('data-order', order);
                          $(this).attr('data-datepicker-source', 'w' + (order * 3) + '-kvdate');
                          $(this).attr('id', 'w' + (order * 3));
                          $(this).closest('div').attr('id', 'w' + (order * 3) + '-kvdate');
                          str += "kvInitPlugin(jQuery('#w" + (order * 3) + "-kvdate').kvSelector(), function(){ \
                                    if (jQuery('#w" + (order * 3) + "').data('kvDatepicker')) { jQuery('#w" + (order * 3) + "').kvDatepicker('destroy'); } \
                                    jQuery('#w" + (order * 3) + "-kvdate').kvDatepicker(kvDatepicker_1643d6f1); \
                                  }); \
                                  initDPAddon('w" + (order * 3) + "'); ";
                          break;
                       case 1:
                          $(this).attr('name', 'ProceduresProlongations[' + order + '][date_till]');
                          $(this).attr('data-order', order);
                          $(this).attr('data-datepicker-source', 'w' + (order * 3 + 1) + '-kvdate');
                          $(this).attr('id', 'w' + (order * 3 + 1));
                          $(this).closest('div').attr('id', 'w' + (order * 3 + 1) + '-kvdate');
                          str += "kvInitPlugin(jQuery('#w" + (order * 3 + 1) + "-kvdate').kvSelector(), function(){ \
                                    if (jQuery('#w" + (order * 3 + 1) + "').data('kvDatepicker')) { jQuery('#w" + (order * 3 + 1) + "').kvDatepicker('destroy'); } \
                                    jQuery('#w" + (order * 3 + 1) + "-kvdate').kvDatepicker(kvDatepicker_1643d6f1); \
                                  }); \
                                  initDPAddon('w" + (order * 3 + 1) + "'); ";
                          break;
                       case 2:
                          $(this).attr('name', 'ProceduresProlongations[' + order + '][date_dssz]');
                          $(this).attr('data-order', order);
                          $(this).attr('data-datepicker-source', 'w' + (order * 3 + 2) + '-kvdate');
                          $(this).attr('id', 'w' + (order * 3 + 2));
                          $(this).closest('div').attr('id', 'w' + (order * 3 + 2) + '-kvdate');
                          str += "kvInitPlugin(jQuery('#w" + (order * 3 + 2) + "-kvdate').kvSelector(), function(){ \
                                    if (jQuery('#w" + (order * 3 + 2) + "').data('kvDatepicker')) { jQuery('#w" + (order * 3 + 2) + "').kvDatepicker('destroy'); } \
                                    jQuery('#w" + (order * 3 + 2) + "-kvdate').kvDatepicker(kvDatepicker_1643d6f1); \
                                  }); \
                                  initDPAddon('w" + (order * 3 + 2) + "'); ";
                          break;
                    } 
                }); 
                
                $this.parent('.well').append($group);
                
                $('body').append("<script> \
                jQuery(document).ready(function () { " + str + " });</script>");
            }
        });
        
        $(document).on('click','#proc-common-prop-reset, #proc-additional-prop-reset', function(ev){
            ev.preventDefault();
            
            // перечитаем форму для сброса изменений
            $.pjax.reload({container: '#' + $(this).data('property') + '-properties'});
        });
        
        $(document).on('submit', '#common-properties-form, #additional-properties-form', function(ev){
            ev.preventDefault();
        });
        
        // выводит диалог о подтверждении сохранения данных формы
        $(document).on('click', '#proc-common-prop-submit, #proc-additional-prop-submit', function(ev){
            var $this= $(this);
            
            ev.preventDefault();
            
            $.post( '/procedures/default/save-dialog', {}, function(data){
                //console.info('data', data);

                //if( data.error !== undefined ) {
                    //$('#contractors-list').html('<p>' + data.error + '</p>');
                  //  $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                  //  $('#modalMessage').modal('show');
                //} else {
                    $('#modalMessage .modal-body').html(data);
                    $('#modalMessage').modal('show');
                    
                    console.log($this.data('property'));
                    
                    $('#proc-confirm-submit').data('property', $this.data('property'));
                //} 

                // обновим GridView с записями
                //$.pjax.reload({container: '#' + $('#delete-doc-records').data('doc-type') + '-docs-pjax'});
                
            }, "html")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }); 
        });
        
        $(document).on('click', '#proc-confirm-submit', function(ev){
            var $this = $(this),
                $form = $('#' + $this.data('property') + '-properties-form'), 
                $url = $form.attr('action'),
                $property = $this.data('property');
            
            ev.preventDefault();
            
            $('#modalMessage').modal('hide');
            
            setTimeout(function(){
                $.post( $url, $form.serialize(), function(data){
                    if( data.error !== undefined ) {
                        $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                        $('#modalMessage').modal('show');
                    } else {
                        $('#modalMessage .modal-body').html(data.msg);
                        $('#modalMessage').modal('show');

                        console.info('submit post', $property);
                        // обновим форму
                        $.pjax.reload({container: '#' + $property + '-properties'});
                    } 
                }, "json")
                .fail(function(error){
                    //$('#modal').modal('hide');
                    console.info('func error', error);
                    $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                    $('#modalMessage').modal('show');
                }); 
            }, 400, $property);
            
        });
        
        
        
        
})(jQuery);