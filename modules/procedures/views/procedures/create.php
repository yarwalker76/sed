<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\procedures\models\Procedures */

$this->title = Yii::t('procedures', 'Create Procedures');
$this->params['breadcrumbs'][] = ['label' => Yii::t('procedures', 'Procedures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procedures-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
