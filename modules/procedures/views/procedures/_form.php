<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\procedures\models\Procedures */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="procedures-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'organization_id')->textInput() ?>

    <?= $form->field($model, 'type_id')->textInput() ?>

    <?= $form->field($model, 'case_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'court_id')->textInput() ?>

    <?= $form->field($model, 'judge_id')->textInput() ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'results_session_date')->textInput() ?>

    <?= $form->field($model, 'close_date')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'in_work' => 'In work', 'archive' => 'Archive', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'manager_id')->textInput() ?>

    <?= $form->field($model, 'introduction_basis')->dropDownList([ 'Определение' => 'Определение', 'Решение' => 'Решение', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'approval_basis')->dropDownList([ 'Вышеупомянутое' => 'Вышеупомянутое', 'Определение' => 'Определение', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'judgment_date')->textInput() ?>

    <?= $form->field($model, 'recept_application_date')->textInput() ?>

    <?= $form->field($model, 'applicant_id')->textInput() ?>

    <?= $form->field($model, 'manager_approval_date')->textInput() ?>

    <?= $form->field($model, 'last_seen')->textInput() ?>

    <div class="form-group">
        
        <?= Html::submitButton($model->isNewRecord ? Yii::t('procedures', 'Create') : Yii::t('procedures', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
