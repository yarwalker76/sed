<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\procedures\models\Procedures */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('procedures', 'Procedures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procedures-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('procedures', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('procedures', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('procedures', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'organization_id',
            'type_id',
            'case_number',
            'court_id',
            'judge_id',
            'create_date',
            'results_session_date',
            'close_date',
            'status',
            'manager_id',
            'introduction_basis',
            'approval_basis',
            'judgment_date',
            'recept_application_date',
            'applicant_id',
            'manager_approval_date',
            'last_seen',
        ],
    ]) ?>

</div>
