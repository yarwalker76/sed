<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\procedures\models\Procedures */

$this->title = Yii::t('procedures', 'Update {modelClass}: ', [
    'modelClass' => 'Procedures',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('procedures', 'Procedures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('procedures', 'Update');
?>
<div class="procedures-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
