<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\procedures\models\ProceduresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('procedures', 'Procedures');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procedures-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('procedures', 'Create Procedures'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'organization_id',
            'type_id',
            'case_number',
            'court_id',
            // 'judge_id',
            // 'create_date',
            // 'results_session_date',
            // 'close_date',
            // 'status',
            // 'manager_id',
            // 'introduction_basis',
            // 'approval_basis',
            // 'judgment_date',
            // 'recept_application_date',
            // 'applicant_id',
            // 'manager_approval_date',
            // 'last_seen',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
