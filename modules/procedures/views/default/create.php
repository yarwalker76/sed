<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;

// add module assets
use app\modules\procedures\assets\ProceduresAsset;
ProceduresAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\procedures\models\Procedures */

$this->title = Yii::t('procedures', 'CREATE_PROCEDURE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('procedures', 'Procedures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procedures-create">
    <h3><?= Html::encode($this->title) ?></h3>
    
    <?php
    $errors = $model->getErrors();
    $str = '';

    if( !empty($errors) ):
            foreach ($errors as $value) {
                    foreach( $value as $v ):
                            $str .= $v . '<br/>';
                    endforeach;
            }

            echo Alert::widget([
        'options' => [
            'class' => 'alert-danger'
        ],
        'body' => '<b>Ошибка!</b> ' . $str
    ]);
    endif;	
    ?>

    <?= $this->render('_form', [
        'model' => $model,
        'search_model' => $search_model,
        'manager_search_model' => $manager_search_model,
        'contractor_types' => $contractor_types,
    ]) ?>

</div>
