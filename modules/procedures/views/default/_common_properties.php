<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

// add module assets
use app\modules\procedures\assets\ProceduresAsset;
ProceduresAsset::register($this);
?>

<?php Pjax::begin(['id' => 'common-properties']); ?> 

    <?php $form = ActiveForm::begin(['id' => 'common-properties-form', 'action' => '/procedures/update-common-properties']); ?>
    
    <?php echo $form->field($model, 'name', [ 'options' => [ 'class' => 'col-lg-8 field-procedures' ] ])
            ->textInput(['readonly' => 'readonly']) ?>
    
    <?= $form->field($model, 'type_id', ['options' => ['class' => 'col-lg-5 form-group field-procedures']])
            ->dropDownList($model->procedureTypeList, ['prompt' => Yii::t('procedures', 'CHOOSE_ONE')]); ?>
    
    <?php echo $form->field($model, 'manager', [ 'options' => [ 'class' => 'col-lg-8 field-procedures' ] ])->textInput(['readonly' => 'readonly']) ?>

    <div class="form-group col-lg-4">
        <?php echo Html::button( Yii::t('procedures', 'CHOOSE'), [
                'class' => 'btn btn-primary choose-contractor-btn', 
                'data-toggle' => 'modal', 
                'data-target' => '#modalContractors', //'#modalManagers', 
                'data-type' => 'choose_manager'
            ] ); ?>
    </div>
    <input type="hidden" id="procedures-manager_id" class="form-control" name="Procedures[manager_id]" value="<?= $model->manager_id ?>">
    <div class="clearfix"></div>  
    
    <?= $form->field($model, 'recept_application_date', ['options' => ['class' => 'col-lg-3 form-group field-procedures']])
                    ->widget(DatePicker::classname(), [
                        'options' => ['value' => ( $model->recept_application_date ? Yii::$app->formatter->asDate( $model->recept_application_date, 'php:d.m.Y') : '')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]); ?>
    <div class="clearfix"></div>    
    
    <div class="form-group">
        <div class="col-lg-1 field-procedures"><label><?= Yii::t('procedures', 'APPLICANT') ?></label></div>
        <div class="col-lg-10">
            <fieldset id="applicant-fields">
                <div class="radio">
                    <label>
                        <input type="radio" name="applicant" id="debtor" value="<?= $model->applicant_id ?>" 
                            <?= ( $model->organization_id == $model->applicant_id ?  'checked' : '') ?> >
                        <?= Yii::t('procedures', 'DEBTOR'); ?>
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="applicant" id="other-debtor" value="" 
                            <?= ( $model->organization_id != $model->applicant_id ?  'checked' : '') ?> >
                        <?= Yii::t('procedures', 'OTHER'); ?>
                    </label>
                    <?php echo $form->field($model, 'applicant', [ 'options' => [ 'class' => 'col-lg-7 field-procedures' ] ])
                            ->label('')
                            ->textInput(['readonly' => 'readonly']) ?>

                    <div class="form-group col-lg-2">
                        <?php echo Html::button( Yii::t('procedures', 'CHOOSE'), [
                                    'class' => 'btn btn-primary choose-applicant-btn', 
                                    'data-toggle' => 'modal', 
                                    'data-target' => '#modalContractors', 
                                    'data-type' => 'choose_applicant',
                                    'disabled' => ( $model->organization_id != $model->applicant_id ? '' : 'disabled'),
                            ] ); ?>
                    </div>
                </div>
            </fieldset>
            <input type="hidden" id="procedures-applicant_id" class="form-control" name="Procedures[applicant_id]" value="<?= $model->applicant_id ?>">
        </div>
    </div>
    
    <?= $form->field($model, 'create_date', ['options' => ['class' => 'col-lg-4 form-group field-procedures']])
                    ->label( Yii::t( 'procedures', 'INPUT_DATE' ) )
                    /*->textInput(['readonly' => 'readonly', 'value' => Yii::$app->formatter->asDate( $model->create_date, 'php:d.m.Y') ]);*/
                    ->widget(DatePicker::classname(), [
                        'options' => ['value' => ( $model->create_date ? Yii::$app->formatter->asDate( $model->create_date, 'php:d.m.Y') : '')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]);
    ?>
    
    <?= $form->field($model, 'close_date', ['options' => ['class' => 'col-lg-4 col-lg-offset-1 form-group field-procedures']])
                    ->widget(DatePicker::classname(), [
                        'options' => ['value' => ( $model->close_date ? Yii::$app->formatter->asDate( $model->close_date, 'php:d.m.Y') : '')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]); ?>
    <div class="clearfix"></div>
    
    <?= $form->field($model, 'manager_approval_date', ['options' => ['class' => 'col-lg-4 form-group field-procedures']])
                    ->widget(DatePicker::classname(), [
                        'options' => [
                            'disabled' => 'disabled',
                            'value' => ( $model->manager_approval_date ? Yii::$app->formatter->asDate( $model->manager_approval_date, 'php:d.m.Y') : ''),
                            ],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]);        
                    //->textInput(['readonly' => 'readonly', 'value' => Yii::$app->formatter->asDate( $model->manager_approval_date, 'php:d.m.Y') ]); ?>
    
    <?= $form->field($model, 'results_session_date', ['options' => ['class' => 'col-lg-4 col-lg-offset-1 form-group field-procedures']])
                    ->label( Yii::t( 'procedures', 'NEXT_JUDGMENT_DATE' ) )
                    ->widget(DatePicker::classname(), [
                        'options' => ['value' => ( $model->results_session_date ? Yii::$app->formatter->asDate( $model->results_session_date, 'php:d.m.Y') : '')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]); ?>
    <div class="clearfix"></div>
    
    <div class="form-group">
        <a role="button" class="btn btn-info btn-mini btn-collapse" data-toggle="collapse" href="#collapse-prolongation-list" aria-expanded="false" aria-controls="collapse-prolongation-list">
            <?= Yii::t('procedures', 'PROCEDURE_PROLONGATION'); ?>
        </a>
    </div>  
    <div class="form-group col-lg-9 field-procedures">
        <div class="collapse" id="collapse-prolongation-list">
            <div class="well">
                <span class="glyphicon glyphicon-plus" id="add-prolongation-item" aria-hidden="true"></span>
                
                <?php if( count($prolongations_model) ): 
                        foreach( $prolongations_model as $key => &$p ): ?>
                            <div class="form-group">
                                <div class="col-lg-4">
                                    <label class="control-label"><?= Yii::t('procedures', 'PROC_PROLONGED_WITH') ?></label>
                                    <?= DatePicker::widget([
                                            'name' => 'ProceduresProlongations[' . $key .'][date_with]',
                                            'value' => ( $p->date_with ? Yii::$app->formatter->asDate( $p->date_with, 'php:d.m.Y') : ''),
                                            'options' => [ 'data-order' => $key, ],
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                                'autoclose' => true,
                                                'format' => 'dd.mm.yyyy',
                                            ]
                                        ]); ?>
                                </div>

                                <div class="col-lg-4">
                                    <label class="control-label"><?= Yii::t('procedures', 'PROC_PROLONGED_TILL') ?></label>
                                    <?= DatePicker::widget([
                                            'name' => 'ProceduresProlongations[' . $key . '][date_till]',
                                            'value' => ( $p->date_till ? Yii::$app->formatter->asDate( $p->date_till, 'php:d.m.Y') : ''),
                                            'options' => [ 'data-order' => $key, ],
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'format' => 'dd.mm.yyyy'
                                            ]
                                        ]); ?>
                                </div>

                                <div class="col-lg-4">
                                    <label class="control-label"><?= Yii::t('procedures', 'DSSZ') ?></label>
                                    <?= DatePicker::widget([
                                            'name' => 'ProceduresProlongations[' . $key . '][date_dssz]',
                                            'value' => ( $p->date_dssz ? Yii::$app->formatter->asDate( $p->date_dssz, 'php:d.m.Y') : ''),
                                            'options' => [ 'data-order' => $key, ],
                                            'removeButton' => false,
                                            'pluginOptions' => [
                                                'autoclose'=>true,
                                                'format' => 'dd.mm.yyyy'
                                            ]
                                        ]); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                <?php   endforeach;
                      else: 
                ?>
                
                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label"><?= Yii::t('procedures', 'PROC_PROLONGED_WITH') ?></label>
                        <?= DatePicker::widget([
                                'name' => 'ProceduresProlongations[0][date_with]',
                                'options' => ['data-order' => 0],
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd.mm.yyyy',
                                ]
                            ]); ?>
                    </div>
                    
                    <div class="col-lg-4">
                        <label class="control-label"><?= Yii::t('procedures', 'PROC_PROLONGED_TILL') ?></label>
                        <?= DatePicker::widget([
                                'name' => 'ProceduresProlongations[0][date_till]',
                                'options' => ['data-order' => 0],
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd.mm.yyyy'
                                ]
                            ]); ?>
                    </div>
                    
                    <div class="col-lg-4">
                        <label class="control-label"><?= Yii::t('procedures', 'DSSZ') ?></label>
                        <?= DatePicker::widget([
                                'name' => 'ProceduresProlongations[0][date_dssz]',
                                'options' => ['data-order' => 0],
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose'=>true,
                                    'format' => 'dd.mm.yyyy'
                                ]
                            ]); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="field-procedures">
        <label class="control-label"><?= Yii::t('procedures', 'COURT_LABEL') ?></label>
    </div>
    
    <?php echo $form->field($model, 'court', [ 'options' => [ 'class' => 'col-lg-8 field-procedures' ] ])->textInput(['readonly' => 'readonly']) ?>

    <div class="form-group col-lg-4">
        <?php echo Html::button( Yii::t('procedures', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-toggle' => 'modal', 'data-target' => '#modalContractors', 'data-type' => 'choose_court'] ); ?>
    </div>
    <input type="hidden" id="procedures-court_id" class="form-control" name="Procedures[court_id]"  value="<?= $model->court_id; ?>">
    <div class="clearfix"></div>

    <?php echo $form->field($model, 'judge', [ 'options' => [ 'class' => 'col-lg-8 field-procedures' ] ])->textInput(['readonly' => 'readonly']) ?>

    <div class="form-group col-lg-4">
        <?php echo Html::button( Yii::t('procedures', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-toggle' => 'modal', 'data-target' => '#modalContractors', 'data-type' => 'choose_judge'] ); ?>
    </div>
    <input type="hidden" id="procedures-judge_id" class="form-control" name="Procedures[judge_id]" value="<?= $model->judge_id; ?>">
    <div class="clearfix"></div>

    <?php $introduction_basis = $model->introduction_basis; ?>
    <?= $form->field($model, 'introduction_basis', ['options' => ['class' => 'col-lg-4 field-procedures form-group']])
        ->radioList($model->getEnumList('introduction_basis'),[
            'unselect' => null,
            'item' => function ($index, $label, $name, $checked, $value) use ($introduction_basis) {
                return '<label class="radio-inline">' . 
                    Html::radio($name, ($label == $introduction_basis), ['value' => $label]) . $label . '</label>';
            },
        ]); ?>
    
    <?php $approval_basis = $model->approval_basis; ?>
    <?= $form->field($model, 'approval_basis', ['options' => ['class' => 'col-lg-4 col-lg-offset-1 field-procedures form-group']])
        ->radioList($model->getEnumList('approval_basis'),[
            'unselect' => null,
            'item' => function ($index, $label, $name, $checked, $value) use ($approval_basis) {
                return '<label class="radio-inline">' .
                    Html::radio($name, ($label == $approval_basis), ['value' => $label]) . $label . '</label>';
            },
        ]); ?>
    <div class="clearfix"></div>
    
    <div class="field-procedures">
        <label class="control-label"><?= Yii::t('procedures', 'JUDGMENT') ?></label>
    </div>
    
    <?= $form->field($model, 'case_number', ['options' => ['class' => 'col-lg-2 form-group field-procedures']])->textInput([ 'readonly' => 'readonly' ]); //textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'judgment_date', ['options' => ['class' => 'col-lg-3 col-lg-offset-1 form-group field-procedures']])
                    ->widget(DatePicker::classname(), [
                        'options' => ['value' => ( $model->judgment_date ? Yii::$app->formatter->asDate( $model->judgment_date, 'php:d.m.Y') : '')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]); ?>
    
    <div class="clearfix"></div>
    
    <div class="form-group col-lg-9">
        <div class="pull-left">
            <?php
            if( Yii::$app->user->can('backOffice') || Yii::$app->user->can('manager') ):
                echo Html::a(Yii::t('procedures', 'DELETE'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('procedures', 'Are you sure you want to delete this item?'),
                        'method' => 'post'
                    ],
                    'data-pjax'=>1
                ]);
            endif;
            ?>
        </div>
        <div class=" pull-right">
            <?= Html::submitButton(Yii::t('procedures', 'SAVE'), ['class' => 'btn btn-primary', 'id' => 'proc-common-prop-submit', 'data-property' => 'common']) ?>
            <?= Html::resetButton(Yii::t('procedures', 'RESET'), ['class' => 'btn btn-default', 'id' => 'proc-common-prop-reset', 'data-property' => 'common']) ?>
        </div>
    </div>
                
    <?php ActiveForm::end(); ?>
    
<?php Pjax::end(); ?>


