<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use yii\bootstrap\Modal;

// add module assets
use app\modules\procedures\assets\ProceduresAsset;
ProceduresAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\procedures\models\Procedures */

$this->title = Yii::t('procedures', 'PROCEDURE_PROPERTIES'); // . ' ' . $model->getName();
$this->params['breadcrumbs'][] = ['label' => Yii::t('procedures', 'Procedures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procedures-view">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= Tabs::widget([
    'items' => [
        [
            'label' => Yii::t('procedures', 'COMMON'),
            'content' => $this->render('_common_properties', [ 
                'model' => $model, 
                'search_model' => $search_model,
                'manager_search_model' => $manager_search_model,
                'contractor_types' => $contractor_types,
                'prolongations_model' => $prolongations_model,
                ]),
            'active' => true
        ],
        [
            'label' => Yii::t('procedures', 'ADDITIONAL'),
            'content' => $this->render('_additional_properties', [ 'additional_properties_model' => $additional_properties_model ]),
        ],
        ]
        ]);
    ?>
</div>

<?php
Modal::begin([
    'header' => '<h3>' . Yii::t('procedures', 'CONTRACTORS') . '</h3>',
    'headerOptions' => ['id' => 'modalContractorsHeader'],
    'id' => 'modalContractors',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

echo $this->render('/default/contractor-search-form', ['model' => $search_model, 'contractor_types' => $contractor_types]);
?>

<form id="contractors-list-form" data-type=""></form>

<?php
Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h3>' . Yii::t('procedures', 'MANAGERS') . '</h3>',
    'headerOptions' => ['id' => 'modalManagersHeader'],
    'id' => 'modalManagers',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

echo $this->render('/default/manager-search-form', ['model' => $manager_search_model, 'contractor_types' => $contractor_types]);
?>

<form id="managers-list-form" data-type=""></form>

<?php
Modal::end();
?>

<?php Modal::begin([
    'header' => '<h2>Message</h2>',
    'headerOptions' => ['id' => 'modalMessageHeader'],
    'id' => 'modalMessage',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    /*'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ], */
    ]
);

Modal::end();
?>
