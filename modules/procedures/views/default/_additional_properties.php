<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use yii\widgets\Pjax;

// add module assets
use app\modules\procedures\assets\ProceduresAsset;
ProceduresAsset::register($this);
?>

<h4><?= Yii::t('procedures', 'ADDITIONAL_TITLE') ?></h4>

<?php Pjax::begin(['id' => 'additional-properties']); ?> 

    <?php $additional_form = ActiveForm::begin(['id' => 'additional-properties-form', 'action' => '/procedures/update-additional-properties']); ?>

    <input type="hidden" id="proceduresadditionalproperties-id" class="form-control" name="ProceduresAdditionalProperties[id]" value="<?= $additional_properties_model->id ?>">

    <?php echo $additional_form->field($additional_properties_model, 'name', [ 'options' => [ 'class' => 'col-lg-8 field-procedures' ] ])->textInput(['readonly' => 'readonly']) ?>

    <div class="form-group col-lg-4">
        <div class="form-group col-lg-4">
        <?php echo Html::button( Yii::t('procedures', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-toggle' => 'modal', 'data-target' => '#modalContractors', 'data-type' => 'choose_newspaper'] ); ?>
    </div>
    </div>
    <input type="hidden" id="proceduresadditionalproperties-np_id" class="form-control" name="ProceduresAdditionalProperties[np_id]" value="<?= $additional_properties_model->np_id ?>">
    <div class="clearfix"></div> 
    
    <?= $additional_form->field($additional_properties_model, 'np_date', ['options' => ['class' => 'col-lg-2 form-group field-procedures-first']])
                    ->widget(DatePicker::classname(), [
                        'options' => ['value' => ( $additional_properties_model->np_date ? Yii::$app->formatter->asDate( $additional_properties_model->np_date, 'php:d.m.Y') : '')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]); ?>
    
    <?= $additional_form->field($additional_properties_model, 'np_number', ['options' => ['class' => 'col-lg-2 form-group']])->textInput(); ?>
    
    <?= $additional_form->field($additional_properties_model, 'np_page_number', ['options' => ['class' => 'col-lg-2 form-group']])->textInput(); ?>
    
    <?= $additional_form->field($additional_properties_model, 'np_post_number', ['options' => ['class' => 'col-lg-2 form-group']])->textInput(); ?>
    
    <div class="clearfix"></div>
    
    <?= $additional_form->field($additional_properties_model, 'efrsb_date', ['options' => ['class' => 'col-lg-2 form-group field-procedures-first']])
                    ->widget(DatePicker::classname(), [
                        'options' => ['value' => ( $additional_properties_model->efrsb_date ? Yii::$app->formatter->asDate( $additional_properties_model->efrsb_date, 'php:d.m.Y') : '')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]); ?>
    
    <?= $additional_form->field($additional_properties_model, 'efrsb_post_number', ['options' => ['class' => 'col-lg-2 form-group']])->textInput(); ?>
    
    <?= $additional_form->field($additional_properties_model, 'reg_credit_close_date', ['options' => ['class' => 'col-lg-2 form-group']])
                    ->widget(DatePicker::classname(), [
                        'options' => ['value' => ( $additional_properties_model->reg_credit_close_date ? Yii::$app->formatter->asDate( $additional_properties_model->reg_credit_close_date, 'php:d.m.Y') : '')],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]); ?>
    <div class="clearfix"></div>
    
    <?= $additional_form->field($additional_properties_model, 'property_inventory', ['options' => ['class' => 'col-lg-8 form-group']])->textInput(); ?>
    
    <?= $additional_form->field($additional_properties_model, 'property_assessment', ['options' => ['class' => 'col-lg-8 form-group']])->textInput(); ?>
    <div class="clearfix"></div>
    
    <h5><?= Yii::t('procedures', 'ADDITIONAL_ENSURANCE_AGREEMENT') ?></h5>
    
    <?= $additional_form->field($additional_properties_model, 'insurance_company_name', ['options' => ['class' => 'col-lg-8 form-group']])->textInput(); ?>
    
    <?= $additional_form->field($additional_properties_model, 'insurance_register_data', ['options' => ['class' => 'col-lg-8 form-group']])->textInput(); ?>
    
    <?= $additional_form->field($additional_properties_model, 'proc_introduction_background', ['options' => ['class' => 'col-lg-8 form-group']])->textarea(['rows' => '6']); ?>
    
    <?= $additional_form->field($additional_properties_model, 'proc_introduction_reason', ['options' => ['class' => 'col-lg-8 form-group']])->textarea(['rows' => '6']); ?>
    <div class="clearfix"></div>
    
    <div class="form-group col-lg-8">

        <div class=" pull-right">
            <?= Html::submitButton(Yii::t('procedures', 'SAVE'), ['class' => 'btn btn-primary', 'id' => 'proc-additional-prop-submit', 'data-property' => 'additional']) ?>
            <?= Html::resetButton(Yii::t('procedures', 'RESET'), ['class' => 'btn btn-default', 'id' => 'proc-additional-prop-reset', 'data-property' => 'additional']) ?>
        </div>
    </div>
    
    

    <?php ActiveForm::end(); ?>
    
<?php Pjax::end(); ?>

