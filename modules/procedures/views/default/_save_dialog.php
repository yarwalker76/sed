<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="save-dialog">
    <h4><?= Yii::t('procedures', 'SAVE_QUESTION') ?></h4>
    <h5><?= Yii::t('procedures', 'USER') . Yii::$app->user->identity->getNameFIO(); ?></h5>
    <div class="form-group">
        <div class=" pull-right">
            <?= Html::submitButton(Yii::t('procedures', 'SAVE'), ['class' => 'btn btn-primary', 'id' => 'proc-confirm-submit', 'data-property' => '']) ?>
            <?= Html::resetButton(Yii::t('procedures', 'CANCEL'), ['class' => 'btn btn-default', 'id' => 'proc-confirm-cancel']) ?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
