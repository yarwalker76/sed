<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\procedures\models\ProceduresSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="procedures-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'organization_id') ?>

    <?= $form->field($model, 'type_id') ?>

    <?= $form->field($model, 'case_number') ?>

    <?= $form->field($model, 'court_id') ?>

    <?php // echo $form->field($model, 'judge_id') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'results_session_date') ?>

    <?php // echo $form->field($model, 'close_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'manager_id') ?>

    <?php // echo $form->field($model, 'introduction_basis') ?>

    <?php // echo $form->field($model, 'approval_basis') ?>

    <?php // echo $form->field($model, 'judgment_date') ?>

    <?php // echo $form->field($model, 'recept_application_date') ?>

    <?php // echo $form->field($model, 'applicant_id') ?>

    <?php // echo $form->field($model, 'manager_approval_date') ?>

    <?php // echo $form->field($model, 'last_seen') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('procedures', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('procedures', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
