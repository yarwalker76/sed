<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use yii\widgets\MaskedInput ;

/* @var $this yii\web\View */
/* @var $model app\modules\procedures\models\Procedures */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="procedures-form">

    <?php $form = ActiveForm::begin(['id' => 'create-procedure']); ?>

    <?php //echo $form->field($model, 'organization_id')->label('')->hiddenInput() ?>
    <input type="hidden" id="procedures-organization_id" class="form-control" name="Procedures[organization_id]" value="">

    <?php echo $form->field($model, 'name', [ 'options' => [ 'class' => 'col-lg-8 field-procedures' ] ])->textInput(['readonly' => 'readonly']) ?>

    <!--div class="form-group col-lg-8 field-procedures">
        <label class="control-label" for="procedures-name"><?= Yii::t('procedures', 'NAME'); ?></label>
        <input type="text" id="procedures-name" class="form-control" readonly="readonly" required="required">
    </div-->

    <div class="form-group col-lg-4">
        <?php echo Html::button( Yii::t('procedures', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-toggle' => 'modal', 'data-target' => '#modalContractors', 'data-type' => 'choose_pd'] ); ?>
    </div>

    <div class="form-group col-lg-5 field-procedures">
        <label class="control-label" for="procedures-inn"><?= Yii::t('procedures', 'INN'); ?></label>
        <input type="text" id="procedures-inn" class="form-control" readonly="readonly">
    </div>    
    <div class="clearfix"></div>

    <?= $form->field($model, 'type_id', ['options' => ['class' => 'col-lg-5 form-group field-procedures']])->dropDownList($model->procedureTypeList, ['prompt' => Yii::t('procedures', 'CHOOSE_ONE')]); ?>

    <?= $form->field($model, 'case_number', ['options' => ['class' => 'col-lg-2 col-lg-offset-1 form-group field-procedures']])
            ->textInput(['maxlength' => 14]); //widget(MaskedInput::className(), [ 'mask' => 'a99-99999/9999', ]); //textInput(['maxlength' => true]) ?>
    <div class="clearfix"></div>


    <?php echo $form->field($model, 'court', [ 'options' => [ 'class' => 'col-lg-8 field-procedures' ] ])->textInput(['readonly' => 'readonly']) ?>

    <div class="form-group col-lg-4">
        <?php echo Html::button( Yii::t('procedures', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-toggle' => 'modal', 'data-target' => '#modalContractors', 'data-type' => 'choose_court'] ); ?>
    </div>
    <input type="hidden" id="procedures-court_id" class="form-control" name="Procedures[court_id]"  value="">
    <div class="clearfix"></div>

    <?php echo $form->field($model, 'judge', [ 'options' => [ 'class' => 'col-lg-8 field-procedures' ] ])->textInput(['readonly' => 'readonly']) ?>

    <div class="form-group col-lg-4">
        <?php echo Html::button( Yii::t('procedures', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-toggle' => 'modal', 'data-target' => '#modalContractors', 'data-type' => 'choose_judge'] ); ?>
    </div>
    <input type="hidden" id="procedures-judge_id" class="form-control" name="Procedures[judge_id]" value="">
    <div class="clearfix"></div>
    

    <?= $form->field($model, 'create_date', ['options' => ['class' => 'col-lg-3 form-group ']])->widget(DatePicker::classname(), [
        'options' => ['value' => date('d.m.Y')],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]); ?>
    <?= $form->field($model, 'results_session_date', ['options' => ['class' => 'col-lg-3 form-group']])->widget(DatePicker::classname(), [
        'options' => [],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]); ?>
    <?= $form->field($model, 'close_date', ['options' => ['class' => 'col-lg-3 form-group']])->widget(DatePicker::classname(), [
        'options' => [],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]); ?>
    <div class="clearfix"></div>
    
    <?php echo $form->field($model, 'manager', [ 'options' => [ 'class' => 'col-lg-8 field-procedures' ] ])->textInput(['readonly' => 'readonly']) ?>

    <div class="form-group col-lg-4">
        <?php echo Html::button( Yii::t('procedures', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-toggle' => 'modal', 'data-target' => '#modalContractors', 'data-type' => 'choose_manager'] ); ?>
    </div>
    <input type="hidden" id="procedures-manager_id" class="form-control" name="Procedures[manager_id]">
    <div class="clearfix"></div>
    
    <input type="hidden" id="procedures-applicant_id" class="form-control" name="Procedures[applicant_id]">
    

    
    <?php //echo $form->field($model, 'introduction_basis')->dropDownList([ 'Определение' => 'Определение', 'Решение' => 'Решение', ], ['prompt' => '']) ?>

    <?php //echo $form->field($model, 'approval_basis')->dropDownList([ 'Вышеупомянутое' => 'Вышеупомянутое', 'Определение' => 'Определение', ], ['prompt' => '']) ?>

    <?php //echo $form->field($model, 'judgment_date')->textInput() ?>

    <?php //echo $form->field($model, 'recept_application_date')->textInput() ?>

    <?php //echo  $form->field($model, 'applicant_id')->textInput() ?>

    <?php //echo $form->field($model, 'manager_approval_date')->textInput() ?>

    <input type="hidden" id="procedures-manager_approval_date" class="form-control" name="Procedures[manager_approval_date]" value="<?= date('d.m.Y') ?>">
    <input type="hidden" id="procedures-judgment_date" class="form-control" name="Procedures[judgment_date]" value="<?= date('d.m.Y') ?>">

    <?php //echo $form->field($model, 'last_seen')->textInput() ?>

    <?php //echo $form->field($model, 'id')->label('')->hiddenInput() ?>
    <input type="hidden" id="procedures-id" class="form-control" name="Procedures[id]">

    <div class="form-group">
        
        <?= Html::submitButton($model->isNewRecord ? Yii::t('procedures', 'CREATE') : Yii::t('procedures', 'UPDATE'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
Modal::begin([
    'header' => '<h3>' . Yii::t('procedures', 'CONTRACTORS') . '</h3>',
    'headerOptions' => ['id' => 'modalContractorsHeader'],
    'id' => 'modalContractors',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

//echo $this->render('/default/contractor-search-form', ['model' => $search_model, 'contractor_types' => $contractor_types]);
?>

<form id="contractors-list-form" data-type=""></form>

<?php
Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h3>' . Yii::t('procedures', 'MANAGERS') . '</h3>',
    'headerOptions' => ['id' => 'modalManagersHeader'],
    'id' => 'modalManagers',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

echo $this->render('/default/manager-search-form', ['model' => $manager_search_model, 'contractor_types' => $contractor_types]);
?>

<form id="managers-list-form" data-type=""></form>

<?php
Modal::end();
?>
