<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="managers-form">
	<?php $form = ActiveForm::begin(['id' => 'manager-search-form', 'action' => '/procedures/default/search-manager']); ?>
		<?= $form->field($model, 'search', ['options' => ['class' => 'col-lg-8 form-group field-procedures']])->label(Yii::t('procedures', 'SEARCH'))->textInput() ?>
		<div class="form-group col-lg-4">
	        <?= Html::submitButton(Yii::t('procedures', 'SEARCH'), ['class' => 'btn btn-primary', 'id' => 'search-managers-btn']) ?>
	    </div>
	    <div class="clearfix"></div>
		
	<?php ActiveForm::end(); ?>
</div>