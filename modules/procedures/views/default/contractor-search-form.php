<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="contractors-form">
	<?php $form = ActiveForm::begin(['id' => 'contractor-search-form', 'action' => '/procedures/default/search-contractor']); ?>
		<?= $form->field($model, 'search', ['options' => ['class' => 'col-lg-8 form-group field-procedures']])->label(Yii::t('procedures', 'SEARCH'))->textInput() ?>
		<div class="form-group col-lg-4">
	        <?= Html::submitButton(Yii::t('procedures', 'SEARCH'), ['class' => 'btn btn-primary', 'id' => 'search-contractors-btn']) ?>
	    </div>
	    <div class="clearfix"></div>
		<?= $form->field($model, 'type')->label(Yii::t('procedures', 'CONTRACTOR_TYPE'))
                        ->dropDownList($contractor_types /*, ['prompt' => Yii::t('procedures', 'CHOOSE_ONE')]*/); ?>
	<?php ActiveForm::end(); ?>
</div>