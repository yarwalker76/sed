<?php

namespace app\modules\procedures\models;

use Yii;
use yii\base\Model;
use app\models\TblOrganizationsTypes;
use yii\helpers\ArrayHelper;
use app\models\AppHelper;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ContractorSearchForm extends Model
{
    public $search;
    public $type;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['search'], 'string'],
            ['type', 'in', 'range' => array_keys( $this->getContractorTypeList() )],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function getContractorTypeList()
    {
        $arr = ['0' => 'Все'];
        $types = AppHelper::getContractorTypesList();

        foreach( $types as $key => $type )
            $arr[$key] = $type;

        return $arr;
        //return array_merge(['0' => 'Все'], AppHelper::getContractorTypesList());
    }

}
