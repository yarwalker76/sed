<?php

namespace app\modules\procedures\models;

use Yii;

/**
 * This is the model class for table "last_seen_procedures".
 *
 * @property integer $user_id
 * @property integer $proc_id
 */
class LastSeenProcedures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'last_seen_procedures';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'proc_id'], 'required'],
            [['user_id', 'proc_id'], 'integer'],
            [['user_id'], 'unique'],
            [['user_id', 'proc_id'], 'unique', 'targetAttribute' => ['user_id', 'proc_id'], 'message' => 'The combination of User ID and Proc ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'proc_id' => Yii::t('app', 'Proc ID'),
        ];
    }
}
