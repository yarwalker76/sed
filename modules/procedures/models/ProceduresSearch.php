<?php

namespace app\modules\procedures\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\procedures\models\Procedures;

/**
 * ProceduresSearch represents the model behind the search form about `app\modules\procedures\models\Procedures`.
 */
class ProceduresSearch extends Procedures
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'organization_id', 'type_id', 'court_id', 'judge_id', 'manager_id', 'applicant_id', 'last_seen'], 'integer'],
            [['case_number', 'create_date', 'results_session_date', 'close_date', 'status', 'introduction_basis', 'approval_basis', 'judgment_date', 'recept_application_date', 'manager_approval_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Procedures::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'organization_id' => $this->organization_id,
            'type_id' => $this->type_id,
            'court_id' => $this->court_id,
            'judge_id' => $this->judge_id,
            'create_date' => $this->create_date,
            'results_session_date' => $this->results_session_date,
            'close_date' => $this->close_date,
            'manager_id' => $this->manager_id,
            'judgment_date' => $this->judgment_date,
            'recept_application_date' => $this->recept_application_date,
            'applicant_id' => $this->applicant_id,
            'manager_approval_date' => $this->manager_approval_date,
            'last_seen' => $this->last_seen,
        ]);

        $query->andFilterWhere(['like', 'case_number', $this->case_number])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'introduction_basis', $this->introduction_basis])
            ->andFilterWhere(['like', 'approval_basis', $this->approval_basis]);

        return $dataProvider;
    }
}
