<?php

namespace app\modules\procedures\models;

use Yii;
use app\models\TblOrganizations;
use app\models\TblOrganizationsMethods;

/**
 * This is the model class for table "procedures_additional_properties".
 *
 * @property integer $id
 * @property integer $proc_id
 * @property integer $np_id
 * @property string $np_date
 * @property string $np_number
 * @property integer $np_page_number
 * @property string $np_post_number
 * @property string $efrsb_date
 * @property string $efrsb_post_number
 * @property string $reg_credit_close_date
 * @property string $property_inventory
 * @property string $property_assessment
 * @property string $insurance_company_name
 * @property string $insurance_register_data
 * @property string $proc_introduction_background
 * @property string $proc_introduction_reason
 * @property integer $registry_holder_id
 *
 * @property Procedures $proc
 * @property TblOrganizations $np
 */
class ProceduresAdditionalProperties extends \yii\db\ActiveRecord
{
    public $name = '';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'procedures_additional_properties';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proc_id', 'np_id', 'np_page_number', 'registry_holder_id'], 'integer'],
            [['np_date', 'efrsb_date', 'reg_credit_close_date'], 'safe'],
            [['proc_introduction_background', 'proc_introduction_reason'], 'string'],
            [['np_number', 'np_post_number', 'efrsb_post_number', 'property_inventory', 'property_assessment', 'insurance_company_name', 'insurance_register_data'], 'string', 'max' => 255],
            [['proc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Procedures::className(), 'targetAttribute' => ['proc_id' => 'id']],
            [['np_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblOrganizations::className(), 'targetAttribute' => ['np_id' => 'id']],
            [['name'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'proc_id' => Yii::t('app', 'Proc ID'),
            'np_id' => Yii::t('app', 'Np ID'),
            'np_date' => Yii::t( 'procedures', 'NP_DATE' ),
            'np_number' => Yii::t( 'procedures', 'NP_NUMBER' ),
            'np_page_number' => Yii::t( 'procedures', 'NP_PAGE_NUMBER' ),
            'np_post_number' => Yii::t( 'procedures', 'NP_POST_NUMBER' ),
            'efrsb_date' => Yii::t( 'procedures', 'EFRSB_DATE' ),
            'efrsb_post_number' => Yii::t( 'procedures', 'EFRSB_POST_NUMBER' ),
            'reg_credit_close_date' => Yii::t('procedures', 'REG_CREDIT_CLOSE_DATE'),
            'property_inventory' => Yii::t('procedures', 'PROPERTY_INVENTORY'),
            'property_assessment' => Yii::t('procedures', 'PROPERTY_ASSESSMENT'),
            'insurance_company_name' => Yii::t('procedures', 'INSURANCE_COMPANY_NAME'),
            'insurance_register_data' => Yii::t('procedures', 'INSURANCE_REGISTER_DATA'),
            'proc_introduction_background' => Yii::t('procedures', 'PROC_INTRODUCTION_BACKGROUND'),
            'proc_introduction_reason' => Yii::t('procedures', 'PROC_INTRODUCTION_REASON'),
            'registry_holder_id' => Yii::t('procedures', 'Registry Holder ID'),
            
            'name' => Yii::t('procedures', 'NP_NAME'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProc()
    {
        return $this->hasOne(Procedures::className(), ['id' => 'proc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNp()
    {
        return $this->hasOne(TblOrganizations::className(), ['id' => 'np_id']);
    }
    
    public function getName()
    {
        $name = '';
        $params = [];

        if( $params = TblOrganizationsMethods::getParams($this->np_id) ):

             foreach( $params as $param ):
                if( in_array( 'Название', $param ) ):
                    $name = $param['value'];
                    break;
                endif;    
            endforeach; 
            
        endif;    

        return $name;
    }
    
    public function beforeValidate()
    {
        if ( isset($this->np_date) && !empty($this->np_date) ) {
            $new_date_format = date('Y-m-d', strtotime($this->np_date));
            $this->np_date = $new_date_format;
        }
        
        if ( isset($this->efrsb_date) && !empty($this->efrsb_date) ) {
            $new_date_format = date('Y-m-d', strtotime($this->efrsb_date));
            $this->efrsb_date = $new_date_format;
        }
        
        if ( isset($this->reg_credit_close_date) && !empty($this->reg_credit_close_date) ) {
            $new_date_format = date('Y-m-d', strtotime($this->reg_credit_close_date));
            $this->reg_credit_close_date = $new_date_format;
        }
               
        return parent::beforeValidate();
    }
}
