<?php

namespace app\modules\procedures\models;

use Yii;
use app\models\TblOrganizations;
use budyaga\users\models\User;
use app\modules\procedures\models\ProcedureTypes;
use yii\helpers\ArrayHelper;
use app\models\TblOrganizationsMethods;

/**
 * This is the model class for table "procedures".
 *
 * @property integer $id
 * @property integer $organization_id
 * @property integer $type_id
 * @property string $case_number
 * @property integer $court_id
 * @property integer $judge_id
 * @property string $create_date
 * @property string $results_session_date
 * @property string $close_date
 * @property string $status
 * @property integer $manager_id
 * @property string $introduction_basis
 * @property string $approval_basis
 * @property string $judgment_date
 * @property string $recept_application_date
 * @property integer $applicant_id
 * @property string $manager_approval_date
 * @property integer $last_seen
 *
 * @property TblOrganizations $applicant
 * @property TblOrganizations $court
 * @property TblOrganizations $judge
 * @property TblOrganizations $manager
 * @property TblOrganizations $organization
 * @property ProcedureTypes $type
 */
class Procedures extends \yii\db\ActiveRecord
{
    public $name;
    public $court;
    public $judge;
    public $manager;
    public $applicant;

    const SCENARIO_ARCHIVE = 'archive';
    const SCENARIO_DEFAULT = 'default';

    /**
     * @var array|null
     */
    private $_params;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'procedures';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_ARCHIVE => [],
            self::SCENARIO_DEFAULT => ['id', 'organization_id', 'type_id',
                    'case_number',
                    'court_id',
                    'judge_id',
                    'create_date',
                    'results_session_date',
                    'close_date',
                    'status',
                    'manager_id',
                    'introduction_basis',
                    'approval_basis',
                    'judgment_date',
                    'recept_application_date',
                    'applicant_id',
                    'manager_approval_date',
                    'last_seen',]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id', 'type_id', 'case_number', 'court_id', 'judge_id', 'create_date', 'manager_id'], 'required'],
            [['organization_id', 'type_id', 'court_id', 'judge_id', 'manager_id', 'applicant_id', 'last_seen'], 'integer'],
            [['create_date', 'results_session_date', 'close_date', 'judgment_date', 'recept_application_date', 'manager_approval_date'], 'safe'],
            [['status', 'introduction_basis', 'approval_basis'], 'string'],
            [['case_number'], 'string', 'max' => 14],
            //['case_number', 'match', 'pattern'=>'/[a-zA-Zа-яА-Я]\d{2}-\d{5}\/\d{4}/u'],
            [['applicant_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblOrganizations::className(), 'targetAttribute' => ['applicant_id' => 'id']],
            [['court_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblOrganizations::className(), 'targetAttribute' => ['court_id' => 'id']],
            [['judge_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblOrganizations::className(), 'targetAttribute' => ['judge_id' => 'id']],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblOrganizations::className(), 'targetAttribute' => ['manager_id' => 'id']],
            [['organization_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblOrganizations::className(), 'targetAttribute' => ['organization_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProcedureTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['type_id'], 'in', 'range' => array_keys( $this->getProcedureTypeList() )],

            [['name', 'court', 'judge', 'manager', 'manager_approval_date'], 'required'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organization_id' => Yii::t('app', 'Organization ID'),
            'type_id' => Yii::t('procedures', 'TYPE_ID'),
            'case_number' => Yii::t('procedures', 'CASE_NUMBER'),
            'court_id' => Yii::t('procedures', 'COURT'),
            'judge_id' => Yii::t('procedures', 'JUDGE'),
            'create_date' => Yii::t('procedures', 'CREATE_DATE'),
            'results_session_date' => Yii::t('procedures', 'RESULTS_SESSION_DATE'),
            'close_date' => Yii::t('procedures', 'CLOSE_DATE'),
            'status' => Yii::t('app', 'Status'),
            'manager_id' => Yii::t('procedure', 'MANAGER'),
            'introduction_basis' => Yii::t('procedures', 'INTRODUCTION_BASIS'),
            'approval_basis' => Yii::t('procedures', 'APPROVAL_BASIS'),
            'judgment_date' => Yii::t('procedures', 'JUDGMENT_DATE'),
            'recept_application_date' => Yii::t('procedures', 'RECEPT_APPLICATION_DATE'),
            'applicant_id' => Yii::t('app', 'Applicant ID'),
            'manager_approval_date' => Yii::t('procedures', 'MANAGER_APPROVAL_DATE'),
            'last_seen' => Yii::t('app', 'Last Seen'),

            'name' => Yii::t('procedures', 'NAME'),
            'court' => Yii::t('procedures', 'COURT'),
            'judge' => Yii::t('procedures', 'JUDGE'),
            'manager' => Yii::t('procedures', 'MANAGER'),
            'applicant' => Yii::t('procedures', 'APPLICANT'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplicant()
    {
        return $this->hasOne(TblOrganizations::className(), ['id' => 'applicant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourt()
    {
        return $this->hasOne(TblOrganizations::className(), ['id' => 'court_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudge()
    {
        return $this->hasOne(TblOrganizations::className(), ['id' => 'judge_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(TblOrganizations::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(TblOrganizations::className(), ['id' => 'organization_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ProcedureTypes::className(), ['id' => 'type_id']);
    }
    
    /**
    * get list of procedures type for dropdown
    */
    public static function getProcedureTypeList()
    {
        $droptions = ProcedureTypes::find()->asArray()->all();
        return ArrayHelper::map($droptions, 'id', 'type');
    }

    /**
     * @return array|null
     */
    public function getParams()
    {
        if ($this->_params === null) {
            $this->_params = TblOrganizationsMethods::getParams($this->organization_id);
        }

        return $this->_params;
    }

    /**
     * @return string
     */
    public function getName()
    {
        $name = '';
        $params = $this->getParams();

        if($params):
            foreach( $params as $param ):
                if( in_array( 'Название', $param ) ):
                    $name = $param['value'];
                    break;
                endif;    
            endforeach;    
        endif;    

        return $name;
    }

    /**
     * @return string|null
     */
    public function getShortName()
    {
        $params = $this->getParams();

        $shortNameLabel = 'Сокр. наименование';

        foreach ($params as $param) {
            if ($param['label'] === $shortNameLabel) {
                return $param['value'];
            }
        }

        return null;
    }

    public function getJudgeName()
    {
        $name = $lastname = $firstname = $patronymic = '';
        $params = [];

        if( $params = TblOrganizationsMethods::getParams($this->judge_id) ):

            foreach( $params as $param ):
                if( in_array( 'Фамилия', $param ) ) $lastname = $param['value'];
                if( in_array( 'Имя', $param ) ) $firstname = $param['value'];
                if( in_array( 'Отчество', $param ) ) $patronymic = $param['value'];
            endforeach; 
            
            $name = $lastname . ( $firstname ? ' ' . $firstname : '' ) . ( $patronymic ? ' ' . $patronymic : '' );
        endif;    

        return $name;
    }

    public function getCourtName()
    {
        $name = '';
        $params = [];

        if( $params = TblOrganizationsMethods::getParams($this->court_id) ):

             foreach( $params as $param ):
                if( in_array( 'Название', $param ) ):
                    $name = $param['value'];
                    break;
                endif;    
            endforeach; 
            
        endif;    

        return $name;
    }
    
    public function getEnumList($field)
    {
        $params = [ ':field' => $field ];
        $values = Yii::$app->db->createCommand("SHOW COLUMNS FROM procedures like :field", $params )->queryAll();
        if ($values) {
            $option_array = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $values[0]['Type']));
        }
        //echo '<pre>'.print_r($values,true).'</pre>';
        //exit('<pre>'.print_r($option_array,true).'</pre>');
        return $option_array;
    }
    
    public function beforeValidate()
    {
        if ( isset($this->create_date) && !empty($this->create_date) ) {
            $new_date_format = date('Y-m-d', strtotime($this->create_date));
            $this->create_date = $new_date_format;
        }
        
        if ( isset($this->manager_approval_date) && !empty($this->manager_approval_date) ) {
            $new_date_format = date('Y-m-d', strtotime($this->manager_approval_date));
            $this->manager_approval_date = $new_date_format;
        }
        
        if ( isset($this->judgment_date) && !empty($this->judgment_date != null) ) {
            $new_date_format = date('Y-m-d', strtotime($this->judgment_date));
            $this->judgment_date = $new_date_format;
        }
        
        if ( isset($this->results_session_date) && !empty($this->results_session_date != null) ) {
            $new_date_format = date('Y-m-d', strtotime($this->results_session_date));
            $this->results_session_date = $new_date_format;
        }
        
        if ( isset($this->close_date) && !empty($this->close_date != null) ) {
            $new_date_format = date('Y-m-d', strtotime($this->close_date));
            $this->close_date = $new_date_format;
        }
        
        if ( isset($this->recept_application_date) && !empty($this->recept_application_date != null) ) {
            $new_date_format = date('Y-m-d', strtotime($this->recept_application_date));
            $this->recept_application_date = $new_date_format;
        }
        
        return parent::beforeValidate();
    }
    
   
}
