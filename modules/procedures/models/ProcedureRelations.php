<?php

namespace app\modules\procedures\models;

use Yii;
use app\modules\procedures\models\Procedures;
use budyaga\users\models\User;

/**
 * This is the model class for table "procedure_relations".
 *
 * @property integer $id
 * @property integer $proc_id
 * @property integer $user_id
 *
 * @property Procedures $proc
 * @property User $user
 */
class ProcedureRelations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'procedure_relations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proc_id', 'user_id'], 'required'],
            [['proc_id', 'user_id'], 'integer'],
            [['proc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Procedures::className(), 'targetAttribute' => ['proc_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'proc_id' => Yii::t('app', 'Proc ID'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProc()
    {
        return $this->hasOne(Procedures::className(), ['id' => 'proc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /*public static function getProcRelations()
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand(
        "SELECT proc.id as procedure_id, CONCAT_WS(', ', proc.type_abbr, v.value) name, proc.user_id FROM tbl_values v 
        INNER JOIN (SELECT P.*, IFNULL(pr.user_id,0) AS user_id FROM 
        (SELECT p.id, p.organization_id, pt.type_abbr FROM procedures p INNER JOIN procedure_types pt ON(p.type_id=pt.id)) AS P 
        LEFT JOIN procedure_relations pr ON(P.id=pr.proc_id) ) AS proc ON(v.organizations_id=proc.organization_id) WHERE v.params_id IN(SELECT id FROM `tbl_params` WHERE `params_types_id` = '10')");

        return $command->queryAll();
    }*/

    public function getProcRelations($id)
    {
        $command = Yii::$app->db->createCommand('SELECT P.id AS procedure_id, CONCAT_WS(\' \', P.type_abbr, v.value) AS name, IFNULL(P.user_id,0) AS user_id
                      FROM tbl_values v
                    INNER JOIN
                    (SELECT p.id, p.organization_id, pt.type_abbr, pr.user_id
                           FROM procedures p
                          INNER JOIN procedure_types pt ON (p.type_id = pt.id)
                           LEFT JOIN procedure_relations AS pr ON p.id = pr.proc_id
                          WHERE p.status = \'in_work\'
                    ) AS P ON v.organizations_id = P.organization_id
                    WHERE v.params_id IN ( SELECT id FROM `tbl_params` WHERE `params_types_id` = \'10\' )
                    ');

        return $command->queryAll();
    }

    public function getAssignedUserProcs($id)
    {
        $command = Yii::$app->db->createCommand('');

        return $command->queryAll();
    }
    
}
