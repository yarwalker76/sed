<?php

namespace app\modules\procedures\models;

use Yii;

/**
 * This is the model class for table "procedures_prolongation".
 *
 * @property integer $id
 * @property string $date_with
 * @property string $date_till
 * @property string $date_dssz
 * @property integer $proc_id
 */
class ProceduresProlongations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'procedures_prolongations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_with', 'date_till', 'date_dssz'], 'safe'],
            [['proc_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date_with' => Yii::t('app', 'Date With'),
            'date_till' => Yii::t('app', 'Date Till'),
            'date_dssz' => Yii::t('app', 'Date Dssz'),
            'proc_id' => Yii::t('app', 'Proc ID'),
        ];
    }
    
    public static function getList()
    {
        return self::findAll(['proc_id' => Yii::$app->session->get('current_proc_id')]);
    }
    
    public function beforeValidate()
    {
        if ( isset($this->date_with) && !empty($this->date_with) ) {
            $new_date_format = date('Y-m-d', strtotime($this->date_with));
            $this->date_with = $new_date_format;
        }
        
        if ( isset($this->date_till) && !empty($this->date_till) ) {
            $new_date_format = date('Y-m-d', strtotime($this->date_till));
            $this->date_till = $new_date_format;
        }
        
        if ( isset($this->date_dssz) && !empty($this->date_dssz != null) ) {
            $new_date_format = date('Y-m-d', strtotime($this->date_dssz));
            $this->date_dssz = $new_date_format;
        }
        
        return parent::beforeValidate();
    }
}
