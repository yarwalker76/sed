<?php

namespace app\modules\procedures\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\models\AppHelper;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ManagerSearchForm extends Model
{
    public $search;
    public $type;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['search'], 'string'],
        ];
    }

}
