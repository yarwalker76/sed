<?php

namespace app\modules\procedures;

use Yii;

/**
 * Procedures module definition class
 */
class Procedures extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\procedures\controllers';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        $this->setAliases(['@procedures-assets' => __DIR__ . '/assets']);
      
        self::registerTranslations();
    }

    public static function registerTranslations() {
        if (!isset(Yii::$app->i18n->translations['procedures']) && !isset(Yii::$app->i18n->translations['procedures/*'])) {
            Yii::$app->i18n->translations['procedures'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/modules/procedures/messages',
                'sourceLanguage' => Yii::$app->language,
                'forceTranslation' => true,
                'fileMap' => [
                    'procedures' => 'procedures.php'
                ]
            ];
        }
    }
}
