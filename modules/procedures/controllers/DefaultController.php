<?php

namespace app\modules\procedures\controllers;

use Yii;
use yii\web\Controller;
use app\models\TblOrganizations;
use app\models\TblOrganizationsMethods;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\filters\AccessControl;

use app\modules\procedures\models\Procedures;
use app\modules\procedures\models\LastSeenProcedures;
use app\modules\procedures\models\ProceduresSearch;
use app\modules\procedures\models\ProcedureTypes;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\procedures\models\ContractorSearchForm;
use app\modules\procedures\models\ManagerSearchForm;
use app\modules\procedures\models\ProcsHelper;
use app\modules\procedures\models\ProceduresProlongations;
use app\modules\procedures\models\ProceduresAdditionalProperties;
use app\models\AppHelper;
use app\modules\procedures\models\ProcedureRelations;
/**
 * Default controller for the `Procedures` module
 */
class DefaultController extends Controller
{
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
               'class' => AccessControl::className(),
               'rules' => [
                       [
                           'allow' => true,
                           'roles' => ['manager', 'backOffice'],
                       ],
                       [
                           'actions' => ['view', 'change-current-procedure', 'get-search-form', 'search-contractor', 'search-manager'],
                           'allow' => true,
                           'roles' => ['officeManager', 'lawyer', 'bookkeeping']
                       ]
                   ],
            ],/*
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],*/
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {


        $searchModel = new ProceduresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Procedures model.
     * @param integer $id
     * @return mixed
     */
    public function actionView() 
    {
    	$id = Yii::$app->session->get('current_proc_id');
        $model = $this->findModel($id);
        $model->name = $model->getName();
        $model->judge = $model->getJudgeName();
        $model->court = $model->getCourtName();
        $prolongations_model = ProceduresProlongations::getList();
        
        $model->manager = AppHelper::getFIO($model->manager_id);
        $search_model = new ContractorSearchForm();
        $manager_search_model = new ManagerSearchForm();
        $contractor_types = ProcsHelper::getContractorTypesList();
        
        $additional_properties_model = ProceduresAdditionalProperties::findOne(['proc_id' => $id]);
        if( is_object($additional_properties_model) ):
            $additional_properties_model->name = $additional_properties_model->getName();
        else:
            $additional_properties_model = new ProceduresAdditionalProperties();
        endif;
        
        return $this->render('view', [
            'model' => $model,
            'search_model' => $search_model,
            'manager_search_model' => $manager_search_model,
            'contractor_types' => $contractor_types,
            'prolongations_model' => $prolongations_model,
            'additional_properties_model' => $additional_properties_model,
        ]);
    }
    
    public function actionUpdateCommonProperties()
    {
        //echo '<pre>'.print_r(Yii::$app->request->post(),true).'</pre>';
        $proc_id = Yii::$app->session->get('current_proc_id');
        $result = [];
        
        $model = $this->findModel($proc_id);
        //$result = ['msg' => '<pre>' . print_r(Yii::$app->request->post(), true) . '</pre>'];
       
        if( $model->load(Yii::$app->request->post()) && $model->save() ):
            
            ProceduresProlongations::deleteAll([ 'proc_id' => $proc_id ]);
            $prolongations = Yii::$app->request->post('ProceduresProlongations');
            foreach( $prolongations as $p ):
                if( !empty($p['date_with']) || !empty($p['date_till']) || !empty($p['date_dssz']) ):
                    $p_model = new ProceduresProlongations();
                    $p_model->proc_id = $proc_id;
                    $p_model->date_with = $p['date_with'];
                    $p_model->date_till = $p['date_till'];
                    $p_model->date_dssz = $p['date_dssz'];
                    
                    if( !$p_model->save() ):
                        if( isset($result['error']) ):
                            $result['error'] .=  Yii::t('procedures', 'ERR_PROLONG_PROC_SAVE') . '<br>';
                        else:
                            $result['error'] =  Yii::t('procedures', 'ERR_PROLONG_PROC_SAVE') . '<br>';
                        endif;
                    endif;
                endif;
            endforeach;
            
            isset($result['error']) || $result = ['msg' => Yii::t('procedures', 'SAVE_SUCCESS') ];
        else:
            $result = [ 'error' => Yii::t('procedures', 'ERR_SAVE_PROC') ];
            $errors = $model->getErrors();
            $str = '';

            if( !empty($errors) ):
                foreach ($errors as $value) {
                    foreach( $value as $v ):
                            $str .= '<br>' . $v;
                    endforeach;
                }
            endif;
            $result['error'] .= $str;
        endif; 
        
        echo json_encode($result);
    }
    
    // Сохраняем дополнительные свойства процедуры
    public function actionUpdateAdditionalProperties()
    {
        $proc_id = Yii::$app->session->get('current_proc_id');
        $result = [];
        $id = ( !is_null(Yii::$app->request->post('ProceduresAdditionalProperties')['id']) ? Yii::$app->request->post('ProceduresAdditionalProperties')['id'] : 0 );
        
        if( $id ):
            $model = ProceduresAdditionalProperties::findOne($id);
        else:
            $model = new ProceduresAdditionalProperties();
        endif;
        
        $model->proc_id = $proc_id;
        
        if( $model->load( Yii::$app->request->post() ) && $model->save() ):
            $result = ['msg' => Yii::t('procedures', 'SAVE_SUCCESS')];
        else:
            $result = [ 'error' => Yii::t('procedures', 'ERR_SAVE_PROC') ];
            $errors = $model->getErrors();
            $str = '';

            if( !empty($errors) ):
                foreach ($errors as $value) {
                    foreach( $value as $v ):
                            $str .= '<br>' . $v;
                    endforeach;
                }
            endif;
            $result['error'] .= $str;
        endif;
        
        echo json_encode($result);
    }
    
    public function actionSaveDialog()
    {
        echo $this->renderPartial('_save_dialog');
    }

    /**
     * Creates a new Procedures model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Procedures();
        $model->scenario = Procedures::SCENARIO_DEFAULT;
        $search_model = new ContractorSearchForm();
        $manager_search_model = new ManagerSearchForm();
        $contractor_types = ProcsHelper::getContractorTypesList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // сохраним данные по процедуре и пользователю
            $proc_relations_model = new ProcedureRelations();
            $proc_relations_model->user_id = Yii::$app->user->identity->id;
            $proc_relations_model->proc_id = $model->id;
            // $proc_relations_model->save();
            if( $proc_relations_model->save() ):
                return $this->goHome(); //return $this->redirect( Yii::$app->request->referrer ); //$this->redirect(['view', 'id' => $model->id]);
            else:
                return $this->render('create', [
                'model' => $model,
                'search_model' => $search_model,
                'manager_search_model' => $manager_search_model,
                'contractor_types' => $contractor_types,
            ]);
            endif;

        } else {
            return $this->render('create', [
                'model' => $model,
                'search_model' => $search_model,
                'manager_search_model' => $manager_search_model,
                'contractor_types' => $contractor_types,
            ]);
        }
    }

    /**
     * Updates an existing Procedures model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->scenario = Procedures::SCENARIO_DEFAULT;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Procedures model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $proc = $this->findModel($id); 
        $proc->scenario = Procedures::SCENARIO_ARCHIVE;
        $proc->status = 'archive';
        $proc->save();

        // удаляем связи процедуры
        ProcedureRelations::deleteAll(['proc_id' => $id]);
        LastSeenProcedures::deleteAll(['proc_id' => $id]);
        
        return $this->redirect(['/site/index']);
    }

    /**
     * Finds the Procedures model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Procedures the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Procedures::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSearchContractor()
	{
		$str = '';
		$params = [];

		if( Yii::$app->request->isAjax ):

			$search = Yii::$app->request->post('ContractorSearchForm')['search'];
			$type = Yii::$app->request->post('ContractorSearchForm')['type'];

			empty($search) || $params[':search'] = '%' . mb_strtoupper($search, 'UTF-8') . '%';
			empty($type) || $params[':type'] = $type;

			$contractors = Yii::$app->db
				->createCommand("
				    select g.title, g.inn, g.lastname, g.firstname, g.patronymic,  o.*
				  	  from
						(select t.organizations_id id, max(t.title) title, max(t.inn) inn, 
                                                        max(lastname) lastname, 
                                                        max(firstname) firstname,
                                                        max(patronymic) patronymic
                                                   from
							(select v.organizations_id, 
								    if(p.title = 'Название', v.value, null) as 'title', 
								    if(p.title = 'ИНН', v.value, null) as 'inn',
                                                                    if(p.title = 'Фамилия', v.value, null) as 'lastname',
                                                                    if(p.title = 'Имя', v.value, null) as 'firstname',
                                                                    if(p.title = 'Отчество', v.value, null) as 'patronymic'
							   from tbl_values as v
							   left join (SELECT id, title FROM tbl_params WHERE title in ('Название', 'ИНН', 'Фамилия', 'Имя', 'Отчество')) as p
								 on v.params_id = p.id
							  where p.id is not null) as t 
						  group by t.organizations_id) as g
						left join tbl_organizations as o on g.id = o.id " .
						( !empty( $search ) || !empty( $type ) ? 'WHERE ' : '' ) .
						( !empty( $search ) ? "( UPPER(g.title) LIKE :search OR g.inn LIKE :search OR UPPER(g.lastname) LIKE :search OR UPPER(g.firstname) LIKE :search OR UPPER(g.patronymic) LIKE :search) " . ( !empty( $type ) ? ' AND ' : '' ) : '' ) .
						( !empty( $type ) ? " o.organizations_types_id = :type" : "" ),
					$params )
				->queryAll();
			
			if( count( $contractors ) ):
				$str .= '<div id="contractors-list">';
			
				foreach( $contractors as $c ):
					$str .= '<div class="form-group">
								<div class="radio">
								    <label>
								      <input type="radio" name="contractor" 
                                                                        value="' . ( $c['title'] ? Html::encode($c['title']) : $c['lastname'] . ' ' . $c['firstname'] . ' ' .$c['patronymic'] ) . '" ' .
                                                                      ' data-inn="' . Html::encode($c['inn']) . '" ' .
                                                                      ' data-id="' . $c['id'] . '"> ' . ( $c['title'] ? Html::encode($c['title']) : $c['lastname'] . ' ' . $c['firstname'] . ' ' .$c['patronymic'] ) .
								    '</label>
								</div>    
							  </div>';
				endforeach;	

				$str .= '</div><div class="form-group"><button type="submit" class="btn btn-primary">' . Yii::t('procedures', 'CHOOSE') .'</button></div>';
			else:
				$str = '<p>' . Yii::t('procedures', 'NOT_FOUND') . '</p>';
			endif;	
		else:
            // not ajax request
            throw new NotFoundHttpException('The requested page does not exist.');
		endif;

		echo json_encode( [ 'contractors' => $str ] ); 
	}

    public function actionChangeCurrentProcedure()
    {
        $result = [];
        $proc_id = Yii::$app->request->post('proc_id');

        $last_procedure = LastSeenProcedures::findOne([ 'user_id' => Yii::$app->user->identity->id ]);
        $last_procedure->proc_id = $proc_id;

        if ($last_procedure->save() !== false) {
            // update successful
            // сохраним выбранную процедуру в сессию
            Yii::$app->session->set('current_proc_id', $proc_id);

            $result = ['msg' => 0];
        } else {
            // update failed
            $result = ['msg' => Yii::t('procedures', 'ERR_CHANGE_CUR_PROC')];
        }

        echo json_encode($result);
    }

    public function actionSearchManager()
    {
        $str = '';
        $params = [];

        if( Yii::$app->request->isAjax ):

                $search = Yii::$app->request->post('ManagerSearchForm')['search'];


                empty($search) || $params[':search'] = '%' . mb_strtoupper($search, 'UTF-8') . '%';

                $managers = Yii::$app->db
                        ->createCommand("SELECT u.id, concat(u.lastname, ' ', u.firstname, ' ', u.patronymic) fio  
                                                           FROM auth_assignment AS a
                                                           LEFT JOIN user AS u ON a.user_id = u.id
                                                          WHERE a.item_name IN ('manager', 'backOffice') " .
                                        ( !empty( $search ) ? ' AND ( UPPER(u.lastname) LIKE :search OR UPPER(u.firstname) LIKE :search )' : '' ),

                                $params )
                        ->queryAll();

                if( count( $managers ) ):
                        $str .= '<div id="managers-list">';

                        foreach( $managers as $m ):
                                $str .= '<div class="form-group">
                                                        <div class="radio">
                                                            <label>
                                                              <input type="radio" name="manager" value="' . Html::encode($m['fio']) . '" data-id="' . $m['id'] . '"> ' . Html::encode($m['fio']) .
                                                            '</label>
                                                        </div>    
                                                  </div>';
                        endforeach;	

                        $str .= '</div><div class="form-group"><button type="submit" class="btn btn-primary">' . Yii::t('procedures', 'CHOOSE') .'</button></div>';
                else:
                        $str = '<p>' . Yii::t('procedures', 'NOT_FOUND') . '</p>';
                endif;	
        else:
            // not ajax request
            throw new NotFoundHttpException('The requested page does not exist.');
        endif;

        echo json_encode( [ 'managers' => $str ] ); 
    }
    
    public function actionGetSearchForm()
    {
        $search_model = new ContractorSearchForm();
        $type = Yii::$app->request->post('type');
        $types = AppHelper::getContractorTypesList();
        $contractor_types = [];
        
        switch ($type):
            case 'choose_pd':
                /*foreach( $types as $key => $value ):
                    if( $key == 9 || $key == 10 ):
                        $contractor_types[$key] = $value;
                    endif;
                endforeach; */
                $contractor_types[9] = $types[9];
                $contractor_types[10] = $types[10];
                arsort($contractor_types);
                break;
                
            case 'choose_court':
                //$key = array_search('Суд', $types); 
                //!$key || $contractor_types[$key] = $types[$key];
                $contractor_types[4] = $types[4];
                break;
            
            case 'choose_judge':
                //$key = array_search('Судьи', $types); 
                //!$key || $contractor_types[$key] = $types[$key];
                $contractor_types[13] = $types[13];
                break;
            
            case 'choose_manager':
                //$key = array_search('АУ', $types); 
                //!$key || $contractor_types[$key] = $types[$key];
                $contractor_types[11] = $types[11];
                break;
            
            case 'choose_newspaper':
                //$key = array_search('АУ', $types); 
                //!$key || $contractor_types[$key] = $types[$key];
                $contractor_types[3] = $types[3];
                break;
            default:
                $contractor_types = $types;
        endswitch;
        
        //echo '<pre>'.print_r($type,true).'</pre>';
        echo $this->renderPartial('contractor-search-form', ['model' => $search_model, 'contractor_types' => $contractor_types]);
    }
    
    public function actionTest()
    {
            
            echo '<pre>'.print_r(AppHelper::getUserProceduresIDList(), true).'</pre>';
    }
}
