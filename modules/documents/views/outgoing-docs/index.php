<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\documents\models\OutgoingDocs;
use yii\bootstrap\Alert;
use yii\bootstrap\Modal;

use app\components\exportFile\ExportFile;

// add module assets
use app\modules\documents\assets\DocumentsAsset;
DocumentsAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\modules\documents\models\OutgoingDocsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('documents', 'OUTGOING');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outgoing-docs-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="docs-controls">
        <?= Html::a( Yii::t('documents', 'CREATE_RECORD'), Url::to('/documents/create-outgoing-record'), ['class' => 'btn btn-primary', 'role' => 'button']) ?>
        <?= Html::a( Yii::t('documents', 'CLONE_RECORD'), Url::to('/documents/clone-outgoing-record?id=0'), ['class' => 'btn btn-warning disabled', 'role' => 'button', 'id' => 'clone-doc-record', 'data-doc-type' => 'outgoing', 'data-method' => 'post']) ?>
        <?= Html::a( Yii::t('documents', 'DELETE_RECORDS'), Url::to('/documents/delete-outgoing-records'), ['class' => 'btn btn-danger', 'role' => 'button', 'id' => 'delete-doc-records', 'data-doc-type' => 'outgoing']) ?>
        <?= Html::a( Yii::t('documents', 'PRINT_ENVELOPS'), Url::to('/printdocs/envelope/create-from-outgoing-docs'), ['class' => 'btn btn-default', 'role' => 'button', 'id' => 'print-doc-records', 'data-doc-type' => 'outgoing']) ?>
        <?= Html::a( Yii::t('documents', 'PRINT_REGISTER'), Url::to('/documents/print-outgoing-register'), ['class' => 'btn btn-default disabled', 'role' => 'button', 'id' => 'export-register']) ?>
    </p>

    <?php 
    if ( $msg = Yii::$app->session->getFlash('success') || $msg = Yii::$app->session->getFlash('error') ) :
        echo Alert::widget([ 'body' => $msg, ]);
    endif;
    ?>

<?php Pjax::begin(['id' => 'outgoing-docs-pjax']); ?>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['id' => 'outgoing-docs-grid'],
        'layout'=>"{pager}\n{summary}\n{items}\n{pager}",
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [ 'class' => 'yii\grid\CheckboxColumn', 'headerOptions' => [ 'width' => '30' ] ],
            [
                //'encodeLabel' => false,
                //'label' => Yii::t('documents', 'SERIAL_NUMBER'),
                'attribute' => 'serial_number', 
                //'format'=>'raw',
                'headerOptions' => [ 'width' => '50' ], 
                'contentOptions' => [ 'style' => 'text-align: center' ]
            ],
            //'serial_number',
            [
                'attribute' => 'send_date',
                'format' => ['date', 'php:d.m.Y'],
                'headerOptions' => [ 'width' => '130' ],
            ], 
            'name',
            'contractor',
            'storage',
           /* [
                'attribute' => 'request_type_id',
                'label' => Yii::t('documents', 'REQUEST_TYPE_ID'),
                'format' => 'text', // Возможные варианты: raw, html
                'content' => function($data){
                    return $data->getRequestTypeName();
                },
                'filter' => OutgoingDocs::getRequestTypeList()
            ],  */

            //'id',
            //'proc_id',
            
            
            //'name',
            // 'contractor',
            // 'send_date',
            // 'receive_date',
            // 'delivery_address',
            // 'notes:ntext',
            // 'files:ntext',

           //['class' => 'yii\grid\ActionColumn'],
            //'buttons' => ['update', 'delete'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update} {delete}',
            ]
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php Modal::begin([
    'header' => '<h2>Message</h2>',
    'headerOptions' => ['id' => 'modalMessageHeader'],
    'id' => 'modalMessage',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

Modal::end();
?>

<?php Modal::begin([
    'header' => '<h2>Message</h2>',
    'headerOptions' => ['id' => 'modalMessageHeader'],
    'id' => 'modalExportRegister',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

Modal::end();
?>
