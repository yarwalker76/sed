<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;

// add module assets
use app\modules\documents\assets\DocumentsAsset;
DocumentsAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\documents\models\OutgoingDocs */

$this->title = Yii::t('documents', 'CREATE_OUTGOING_RECORD');
$this->params['breadcrumbs'][] = ['label' => Yii::t('documents', 'OUTGOING'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outgoing-docs-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'contractor_types' => $contractor_types,
        'search_model' => $search_model,
        'files' => $files,
        'response_on_name' => $response_on_name,
    ]) ?>

</div>

<?php
Modal::begin([
    'header' => '<h2>Message</h2>',
    'headerOptions' => ['id' => 'modalMessageHeader'],
    'id' => 'modalMessage',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

Modal::end();
?>