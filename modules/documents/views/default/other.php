<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('documents', 'OTHER');

?>
<h2><?= $this->title; ?></h2>

<div>
	<?= Html::a( Yii::t('documents', 'CREATE_RECORD'), Url::to('create-other-record'), ['class' => 'btn btn-primary']) ?>
    <?= Html::a( Yii::t('documents', 'CLONE_RECORD'), Url::to('clone-other-record'), ['class' => 'btn btn-warning']) ?>
    <?= Html::a( Yii::t('documents', 'DELETE_RECORD'), Url::to('clone-other-record'), ['class' => 'btn btn-danger']) ?>
</div>