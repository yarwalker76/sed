<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="docs-form">
	<?php $search_form = ActiveForm::begin(['id' => 'contractor-search-form', 'action' => '/documents/default/search-contractor']); ?>
		<?= $search_form->field($model, 'search', ['options' => ['class' => 'col-lg-8 form-group']])->label(Yii::t('documents', 'SEARCH'))->textInput() ?>
		<div class="form-group col-lg-4">
	        <?= Html::submitButton(Yii::t('documents', 'SEARCH'), ['class' => 'btn btn-primary', 'id' => 'search-contractors-btn']) ?>
	    </div>
	    <div class="clearfix"></div>
		<?= $search_form->field($model, 'type')->label(Yii::t('documents', 'CONTRACTOR_TYPE'))->dropDownList($contractor_types, ['prompt' => Yii::t('documents', 'CHOOSE_ONE')]); ?>
	<?php ActiveForm::end(); ?>
</div>