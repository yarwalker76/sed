<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('documents', 'OUTGOING');

?>
<h2><?= $this->title; ?></h2>

<div>
	<?= Html::a( Yii::t('documents', 'CREATE_RECORD'), Url::to('create-outgoing-record'), ['class' => 'btn btn-primary']) ?>
    <?= Html::a( Yii::t('documents', 'CLONE_RECORD'), Url::to('clone-outgoing-record'), ['class' => 'btn btn-warning']) ?>
    <?= Html::a( Yii::t('documents', 'DELETE_RECORD'), Url::to('clone-outgoing-record'), ['class' => 'btn btn-danger']) ?>
    <?= Html::a( Yii::t('documents', 'PRINT_ENVELOPS'), Url::to('print-outgoing-envelops'), ['class' => 'btn btn-default']) ?>
    <?= Html::a( Yii::t('documents', 'PRINT_REGISTER'), Url::to('print-outgoing-register'), ['class' => 'btn btn-default']) ?>
</div>