<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

// add module assets
use app\modules\documents\assets\DocumentsAsset;
DocumentsAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\modules\documents\models\InternalDocsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('documents', 'INTERNAL');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="internal-docs-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="docs-controls">
        <?= Html::a( Yii::t('documents', 'CREATE_RECORD'), Url::to('/documents/create-internal-record'), ['class' => 'btn btn-primary', 'role' => 'button']) ?>
        <?= Html::a( Yii::t('documents', 'CLONE_RECORD'), Url::to('/documents/clone-internal-record?id=0'), ['class' => 'btn btn-warning disabled', 'role' => 'button', 'id' => 'clone-doc-record', 'data-doc-type' => 'internal', 'data-method' => 'post']) ?>
        <?= Html::a( Yii::t('documents', 'DELETE_RECORDS'), Url::to('/documents/delete-internal-record'), ['class' => 'btn btn-danger', 'role' => 'button', 'id' => 'delete-doc-records', 'data-doc-type' => 'internal']) ?>
    </p>
<?php Pjax::begin(['id' => 'internal-docs-pjax']); ?>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['id' => 'internal-docs-grid'],
        'layout'=>"{pager}\n{summary}\n{items}\n{pager}",
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [ 'class' => 'yii\grid\CheckboxColumn', 'headerOptions' => [ 'width' => '30' ] ],
            [
                //'encodeLabel' => false,
                //'label' => Yii::t('documents', 'SERIAL_NUMBER'),
                'attribute' => 'serial_number', 
                //'format'=>'raw',
                'headerOptions' => [ 'width' => '50' ], 
                'contentOptions' => [ 'style' => 'text-align: center' ]
            ],
            //'serial_number',
            [
                'attribute' => 'date',
                'format' => ['date', 'php:d.m.Y'],
                'headerOptions' => [ 'width' => '130' ],
            ], 
            //'id',
            //'proc_id',
            
            'name',
            'storage',
            
            // 'files:ntext',
            // 'notes:ntext',

            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}' ]
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php Modal::begin([
        'header' => '<h2>Message</h2>',
        'headerOptions' => ['id' => 'modalMessageHeader'],
        'id' => 'modalMessage',
        'size' => 'modal-md',
        //keeps from closing modal with esc key or by clicking out of the modal.
        // user must click cancel or X to close
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

Modal::end();
?>