<?php

use yii\helpers\Html;

// add module assets
use app\modules\documents\assets\DocumentsAsset;
DocumentsAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\documents\models\IncomingDocs */

$this->title = Yii::t('documents', 'EDIT_RECORD') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Incoming Docs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="incoming-docs-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'outgoing_docs' => $outgoing_docs,
        'contractor_types' => $contractor_types,
        'search_model' => $search_model,
        'files' => $files,
        'response_on_name' => $response_on_name,
    ]) ?>

</div>
