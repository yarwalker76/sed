<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Alert;
use yii\bootstrap\Modal;

// add module assets
use app\modules\documents\assets\DocumentsAsset;
DocumentsAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\modules\documents\models\IncomingDocsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('documents', 'INCOMING');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="incoming-docs-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="docs-controls">
        <?= Html::a( Yii::t('documents', 'CREATE_RECORD'), Url::to('/documents/create-incoming-record'), ['class' => 'btn btn-primary', 'role' => 'button']) ?>
        <?= Html::a( Yii::t('documents', 'CLONE_RECORD'), Url::to('/documents/clone-incoming-record?id=0'), ['class' => 'btn btn-warning disabled', 'role' => 'button', 'id' => 'clone-doc-record', 'data-doc-type' => 'incoming', 'data-method' => 'post']) ?>
        <?= Html::a( Yii::t('documents', 'DELETE_RECORDS'), Url::to('/documents/delete-incoming-record'), ['class' => 'btn btn-danger', 'role' => 'button', 'id' => 'delete-doc-records', 'data-doc-type' => 'incoming']) ?>
    </p>
<?php Pjax::begin(['id' => 'incoming-docs-pjax']); ?>    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['id' => 'incoming-docs-grid'],
        'layout'=>"{pager}\n{summary}\n{items}\n{pager}",
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [ 'class' => 'yii\grid\CheckboxColumn', 'headerOptions' => [ 'width' => '30' ] ],
            [
                //'encodeLabel' => false,
                //'label' => Yii::t('documents', 'SERIAL_NUMBER'),
                'attribute' => 'serial_number', 
                //'format'=>'raw',
                'headerOptions' => [ 'width' => '50' ], 
                'contentOptions' => [ 'style' => 'text-align: center' ]
            ],
            //'serial_number',
            [
                'attribute' => 'receive_date',
                'format' => ['date', 'php:d.m.Y'],
                'headerOptions' => [ 'width' => '130' ],
            ], 
            'name',
            'contractor',
            'storage',

            //'response_type_id',
            

            //'id',
            //'proc_id',
            //'response_on_id',
            //'response_type_id',
            //'serial_number',
            // 'name',
            // 'contractor',
            // 'send_date',
            // 'receive_date',
            // 'notes:ntext',
            // 'files:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update} {delete}',
            ],

        ],
    ]); ?>
<?php Pjax::end(); ?></div>

<?php Modal::begin([
    'header' => '<h2>Message</h2>',
    'headerOptions' => ['id' => 'modalMessageHeader'],
    'id' => 'modalMessage',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

Modal::end();
?>