<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\bootstrap\Alert;

// add module assets
use app\modules\documents\assets\DocumentsAsset;
DocumentsAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\documents\models\OtherDocs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="other-docs-form">
    <?php
    $errors = $model->getErrors();
    $str = '';

    if( !empty($errors) ):
            foreach ($errors as $value) {
                    foreach( $value as $v ):
                            $str .= $v . '<br/>';
                    endforeach;
            }

            echo Alert::widget([
        'options' => [
            'class' => 'alert-danger'
        ],
        'body' => '<b>Ошибка!</b> ' . $str
    ]);
    endif;	
    ?>

    <?php $form = ActiveForm::begin(['options'=> ['enctype'=> 'multipart/form-data']]); ?>

    <input type="hidden" id="otherdocs-id" class="form-control" name="OtherDocs[id]" value="<?= $model->id ?>">
    <input type="hidden" value="other" id="doc-type">

    <?= $form->field($model, 'serial_number', ['options' => ['class' => 'col-lg-2 form-group']])->textInput(['value' => ( $model->serial_number ? $model->serial_number : $model->maxSerialNumber + 1)] ) ?>
    <div class="clearfix"></div>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date', ['options' => ['class' => 'col-lg-3 form-group']])->widget(DatePicker::classname(), [
        'options' => ['value' => ( $model->date ? date('d.m.Y', strtotime($model->date)) : date('d.m.Y') )],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy'
        ]
    ]); ?>
    <?= $form->field($model, 'storage', ['options' => ['class' => 'col-lg-5 col-lg-offset-1 form-group']])->textInput(['maxlength' => true]) ?>
    <div class="clearfix"></div>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'files[]')->widget(FileInput::classname(), [
        'options' => [ 'multiple' => true ],
        'pluginOptions' => [
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
            'overwriteInitial' => true,
            'allowedFileExtensions' => ['pdf','doc','docx'],
            'maxFileSize' => 20971520
        ]
    ]); ?>

    <?php if( !empty($files) ): ?>
        <div class="form-group">
            <ul class="doc-files">
                <?php foreach( $files as $file ): ?>
                    <li>
                        <?= Html::a( $file->name, Url::to('/documents/download-file?id=' . $file->id) ); ?>
                        <a href="#" class="del-file" data-id="<?= $file->id; ?>"><span class="glyphicon glyphicon-remove del" aria-hidden="true" data-placement="top" title="<?= Yii::t('documents', 'DELETE') ?>"></span></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>

    <input type="hidden" id="otherdocs-proc_id" class="form-control" name="OtherDocs[proc_id]" value="<?= Yii::$app->session->get('current_proc_id'); ?>">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('documents', 'CREATE') : Yii::t('documents', 'UPDATE'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
