<?php

namespace app\modules\documents\models;

use Yii;

/**
 * This is the model class for table "doc_types".
 *
 * @property integer $id
 * @property string $type
 * @property string $name
 *
 * @property IncomingDocs[] $incomingDocs
 * @property OutgoingDocs[] $outgoingDocs
 */
class DocTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'string'],
            [['name'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingDocs()
    {
        return $this->hasMany(IncomingDocs::className(), ['response_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoingDocs()
    {
        return $this->hasMany(OutgoingDocs::className(), ['request_type_id' => 'id']);
    }
}
