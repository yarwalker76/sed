<?php

namespace app\modules\documents\models;

use Yii;

/**
 * This is the model class for table "doc_files".
 *
 * @property integer $id
 * @property string $doc_type
 * @property string $name
 * @property string $path
 * @property integer $doc_id
 */
class DocFiles extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['doc_type'], 'string'],
            [['doc_id'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['path'], 'string', 'max' => 255],
            [['file'], 'safe'],
            [['file'], 'file', 'extensions' => 'pdf,doc,docx'],
            [['file'], 'file', 'maxSize' => '20971520'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'doc_type' => Yii::t('app', 'Doc Type'),
            'name' => Yii::t('app', 'Name'),
            'path' => Yii::t('app', 'Path'),
            'doc_id' => Yii::t('app', 'Doc ID'),
        ];
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // удаляем файл

            if( !unlink($this->path) ):
                throw new Exception('Не удалось удалить файл ' . $this->path);
                return false;
            endif;    

            return true;
        } else {
            return false;
        }
    }
}
