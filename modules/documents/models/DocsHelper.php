<?php
namespace app\modules\documents\models;

use Yii;
use yii\helpers\Url;
use app\models\TblOrganizations;
use app\models\TblOrganizationsSearch;
use app\models\TblOrganizationsTypes;
use yii\helpers\ArrayHelper;

class DocsHelper {
	public static function getContractorsList()
	{
		//$contractors = TblOrganizations::find()->all();
		$searchModel = new TblOrganizationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			];
	}

	public static function getContractorTypesList()
	{
		$dropdowns = TblOrganizationsTypes::find()->asArray()->all();
		return ArrayHelper::map($dropdowns, 'id', 'type');
	}

	public static function searchContractor( $search = '', $type = 0)
	{
		$contractors = TblOrganizations::find()->where('( `title` LIKE \'%' . $search . '%\' OR `INN` LIKE \'%' . $search . '%\' )' . ( $type ? ' AND `organizations_types_id` = ' . $type : '' ) )->asArray()->all();
		return $contractors; 
	}
}