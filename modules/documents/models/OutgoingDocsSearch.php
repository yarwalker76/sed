<?php

namespace app\modules\documents\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\documents\models\OutgoingDocs;

/**
 * OutgoingDocsSearch represents the model behind the search form about `app\modules\documents\models\OutgoingDocs`.
 */
class OutgoingDocsSearch extends OutgoingDocs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proc_id', 'serial_number', 'request_type_id'], 'integer'], //[['id', 'proc_id', 'serial_number', 'request_type_id'], 'integer'],
            [['id', 'name', 'contractor', 'send_date', 'delivery_address', 'notes', 'files'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OutgoingDocs::find();

        // add conditions that should always apply here
        $totalCount = Yii::$app->db->createCommand('SELECT COUNT(*) FROM ' . OutgoingDocs::tableName() . ' WHERE proc_id = :proc_id', [':proc_id' => Yii::$app->session->get('current_proc_id')])->queryScalar();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => (int)$totalCount,
            'pagination' => [
                // количество пунктов на странице
                'pageSize' => 20,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'proc_id' => $this->proc_id,
            'serial_number' => $this->serial_number,
            'request_type_id' => $this->request_type_id,
            'send_date' => $this->send_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'contractor', $this->contractor])
            ->andFilterWhere(['like', 'delivery_address', $this->delivery_address])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'files', $this->files]);

        return $dataProvider;
    }

    public function exportFields()
    {
        return [
            /*'id' => function ($model) {
                // @var $model User 
                return $model->id;
            },*/
            'serial_number',
            'name',
            'contractor',
            'delivery_address'
        ];
    }
}
