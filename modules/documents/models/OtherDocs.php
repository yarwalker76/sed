<?php

namespace app\modules\documents\models;

use Yii;

/**
 * This is the model class for table "other_docs".
 *
 * @property integer $id
 * @property integer $proc_id
 * @property integer $serial_number
 * @property string $name
 * @property string $date
 * @property string $files
 * @property string $notes
 */
class OtherDocs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'other_docs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proc_id'], 'integer'],
            [['date'], 'safe'],
            [['notes', 'serial_number'], 'string'],
            [['storage'], 'string', 'max' => 20],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('documents', 'ID'),
            'proc_id' => Yii::t('documents', 'PROC_ID'),
            'serial_number' => Yii::t('documents', 'SERIAL_NUMBER'),
            'name' => Yii::t('documents', 'NAME'),
            'date' => Yii::t('documents', 'DATE'),
            'files' => Yii::t('documents', 'FILES'),
            'notes' => Yii::t('documents', 'NOTES'),
            'storage' => Yii::t('documents', 'STORAGE'),
        ];
    }

    /**
     * get maximum serial number
     */
    public function getMaxSerialNumber()
    {
        return $this->find()->where(['proc_id' => Yii::$app->session->get('current_proc_id')])->max('convert(serial_number, unsigned)');
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // удалим прикрепленные к записи файлы
            try {
                $files = DocFiles::find()->where(['doc_id' => $this->id, 'doc_type' => 'other'])->all();

                foreach( $files as $file ):
                    $file->delete();
                endforeach;
            } catch (Exception $e) {
                Yii::$app->session->setFlash('alert-error', $e->getMessage());
            }

            return true;
        } else {
            return false;
        }
    }

    public function beforeValidate()
    {
        if ($this->date != null) {
            $new_date_format = date('Y-m-d', strtotime($this->date));
            $this->date = $new_date_format;
        }
        
        return parent::beforeValidate();
    }
}
