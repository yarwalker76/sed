<?php

namespace app\modules\documents\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\documents\models\IncomingDocs;

/**
 * IncomingDocsSearch represents the model behind the search form about `app\modules\documents\models\IncomingDocs`.
 */
class IncomingDocsSearch extends IncomingDocs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'proc_id', 'response_on_id', 'response_type_id', 'serial_number'], 'integer'],
            [['name', 'contractor', 'receive_date', 'notes', 'files'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = IncomingDocs::find();

        // add conditions that should always apply here

        $totalCount = Yii::$app->db->createCommand('SELECT COUNT(*) FROM ' . IncomingDocs::tableName() . ' WHERE proc_id = :proc_id', [':proc_id' => Yii::$app->session->get('current_proc_id')])->queryScalar();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => (int)$totalCount,
            'pagination' => [
                // количество пунктов на странице
                'pageSize' => 20,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'proc_id' => $this->proc_id,
            'response_on_id' => $this->response_on_id,
            'response_type_id' => $this->response_type_id,
            'serial_number' => $this->serial_number,
            'receive_date' => $this->receive_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'contractor', $this->contractor])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'files', $this->files]);

        return $dataProvider;
    }
}
