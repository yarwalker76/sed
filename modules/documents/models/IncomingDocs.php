<?php

namespace app\modules\documents\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\documents\models\DocFiles;

/**
 * This is the model class for table "incoming_docs".
 *
 * @property integer $id
 * @property integer $proc_id
 * @property integer $response_on_id
 * @property integer $response_type_id
 * @property integer $serial_number
 * @property string $name
 * @property string $contractor
 * @property string $receive_date
 * @property string $notes
 * @property string $files
 *
 * @property OutgoingDocs $responseOn
 * @property DocTypes $responseType
 */
class IncomingDocs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'incoming_docs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proc_id', 'response_on_id', 'response_type_id'], 'integer'],
            [['receive_date'], 'safe'],
            [['notes', 'serial_number'], 'string'],
            [['name'], 'required'],
            [['name', 'contractor'], 'string', 'max' => 255],
            [['storage'], 'string', 'max' => 20],
            //[['response_on_id'], 'exist', 'skipOnError' => true, 'targetClass' => OutgoingDocs::className(), 'targetAttribute' => ['response_on_id' => 'id']],
            [['response_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocTypes::className(), 'targetAttribute' => ['response_type_id' => 'id']],
            [['response_type_id'], 'in', 'range' => array_keys( $this->getResponseTypeList() )],
            [['response_on_id'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('documents', 'ID'),
            'proc_id' => Yii::t('documents', 'PROC_ID'),
            'response_on_id' => Yii::t('documents', 'RESPONSE_ON_ID'),
            'response_type_id' => Yii::t('documents', 'RESPONSE_TYPE_ID'),
            'serial_number' => Yii::t('documents', 'SERIAL_NUMBER'),
            'name' => Yii::t('documents', 'NAME'),
            'contractor' => Yii::t('documents', 'SEND_CONTRACTOR'),
            'receive_date' => Yii::t('documents', 'RECEIVE_DATE'),
            'notes' => Yii::t('documents', 'NOTES'),
            'files' => Yii::t('documents', 'FILES'),
            'storage' => Yii::t('documents', 'STORAGE'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponseOn()
    {
        return $this->hasOne(OutgoingDocs::className(), ['id' => 'response_on_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponseType()
    {
        return $this->hasOne(DocTypes::className(), ['id' => 'response_type_id']);
    }

    /**
    * uses magic getResponseType on return statement
    *
    */
    public function getResponseTypeName()
    {
        return $this->responseType->name;
    }

    /**
    * get maximum serial number
    */
    public function getMaxSerialNumber()
    {
        return $this->find()->where(['proc_id' => Yii::$app->session->get('current_proc_id')])->max('convert(serial_number, unsigned)');
    }

    /**
    * get list of request type for dropdown
    */
    public static function getResponseTypeList()
    {
        $droptions = DocTypes::find()->where(['type' => 'incoming'])->asArray()->all();
        return ArrayHelper::map($droptions, 'id', 'name');
    }

    public function getResponseOnName()
    {
        $response_on_model = OutgoingDocs::findOne($this->response_on_id); 
        return ( is_object($response_on_model) ? '№' . $response_on_model->serial_number . ', ' . $response_on_model->name . ', от ' . $response_on_model->send_date : '' );
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // удалим прикрепленные к записи файлы
            try {
                $files = DocFiles::find()->where(['doc_id' => $this->id, 'doc_type' => 'incoming'])->all();

                foreach( $files as $file ):
                    $file->delete();
                endforeach;    
             } catch (Exception $e) {
                Yii::$app->session->setFlash('alert-error', $e->getMessage());
             }

            return true;
        } else {
            return false;
        }
    }

    public function beforeValidate()
    {
        if ($this->receive_date != null) {
            $new_date_format = date('Y-m-d', strtotime($this->receive_date));
            $this->receive_date = $new_date_format;
        }
        
        return parent::beforeValidate();
    }
}
