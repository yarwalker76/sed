<?php

namespace app\modules\documents\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\documents\models\DocFiles;
use app\modules\documents\models\IncomingDocs;

/**
 * This is the model class for table "outgoing_docs".
 *
 * @property integer $id
 * @property integer $proc_id
 * @property integer $serial_number
 * @property integer $request_type_id
 * @property string $name
 * @property string $contractor
 * @property string $send_date
 * @property string $delivery_address
 * @property string $notes
 * @property string $files
 *
 * @property IncomingDocs[] $incomingDocs
 * @property DocTypes $requestType
 */
class OutgoingDocs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outgoing_docs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['proc_id', 'request_type_id'], 'integer'],
            [['send_date', 'reg_date'], 'safe'],
            [['notes', 'serial_number'], 'string'],
            [['name'], 'required'],
            [['storage'], 'string', 'max' => 20],
            [['name', 'contractor', 'delivery_address'], 'string', 'max' => 255],
            [['request_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocTypes::className(), 'targetAttribute' => ['request_type_id' => 'id']],
            [['request_type_id'], 'in', 'range' => array_keys( $this->getRequestTypeList() )],
            [['response_on_id'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('documents', 'ID'),
            'proc_id' => Yii::t('documents', 'PROC_ID'),
            'serial_number' => Yii::t('documents', 'SERIAL_NUMBER'),
            'request_type_id' => Yii::t('documents', 'REQUEST_TYPE_ID'),
            'name' => Yii::t('documents', 'NAME'),
            'contractor' => Yii::t('documents', 'RECEIVE_CONTRACTOR'),
            'send_date' => Yii::t('documents', 'SEND_DATE'),
            'reg_date' => Yii::t('documents', 'REG_DATE'),
            'delivery_address' => Yii::t('documents', 'DELIVERY_ADDRESS'),
            'notes' => Yii::t('documents', 'NOTES'),
            'files' => Yii::t('documents', 'FILES'),
            'storage' => Yii::t('documents', 'STORAGE'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomingDocs()
    {
        return $this->hasMany(IncomingDocs::className(), ['response_on_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestType()
    {
        return $this->hasOne(DocTypes::className(), ['id' => 'request_type_id']);
    }

    /**
    * get list of request type for dropdown
    */
    public static function getRequestTypeList()
    {
        $droptions = DocTypes::find()->where(['type' => 'outgoing'])->asArray()->all();
        return ArrayHelper::map($droptions, 'id', 'name');
    }

    /**
    * get maximum serial number
    */
    public function getMaxSerialNumber()
    {
        return $this->find()->where(['proc_id' => Yii::$app->session->get('current_proc_id')])->max('convert(serial_number, unsigned)');
    }

    /**
     * @return string
     */
    public function getNextSerialNumber()
    {
        $current = (int) $this->getMaxSerialNumber() + 1;

        return (string) $current;
    }

    /**
    * uses magic getRequestType on return statement
    *
    */
    public function getRequestTypeName()
    {
        return $this->requestType->name;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // удалим прикрепленные к записи файлы
            try {
                $files = DocFiles::find()->where(['doc_id' => $this->id, 'doc_type' => 'outgoing'])->all();

                foreach( $files as $file ):
                    $file->delete();
                endforeach;    
             } catch (Exception $e) {
                Yii::$app->session->setFlash('alert-error', $e->getMessage());
             }

             // Обновим связанные исходящие записи
             IncomingDocs::updateAll(['response_on_id' => 0], 'response_on_id = ' . $this->id);

            return true;
        } else {
            return false;
        }
    }

    public function getResponseOnName()
    {
        $response_on_model = IncomingDocs::findOne($this->response_on_id);
        return ( is_object($response_on_model) ? '№' . $response_on_model->serial_number . ', ' . $response_on_model->name . ', от ' . $response_on_model->receive_date : '' );
    }

    public function beforeValidate()
    {
        if ($this->send_date != null) {
            $new_date_format = date('Y-m-d', strtotime($this->send_date));
            $this->send_date = $new_date_format;
        }

        if ($this->reg_date != null) {
            $new_date_format = date('Y-m-d', strtotime($this->reg_date));
            $this->reg_date = $new_date_format;
        }
        
        return parent::beforeValidate();
    }
}
