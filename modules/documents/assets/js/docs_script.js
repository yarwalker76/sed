(function($){

        /*$('.nav-vertical a.dropdown-toggle').on('click', function(ev){
            ev.preventDefault();
            $(this).next('ul.dropdown-menu').slideToggle(350);
        }); */

        $('#contractor-search-form').on('submit', function (e) {
            e.preventDefault();

            $.post( $(this).attr('action'), $(this).serialize(), function(data){

                if( data.error !== undefined ) {
                    $('#contractors-list-form').html('<p>' + data.error + '</p>');
                  //  $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                  //  $('#modalMessage').modal('show');
                } else {
                    $('#contractors-list-form').html(data.contractors);
                }

            }, "json")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                //$('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                //$('#modalMessage').modal('show');
            });
        });

        $('#contractors-list-form').on('submit', function(ev){
            ev.preventDefault();

            $('#' + $(':hidden#doc-type').val() + 'docs-contractor').val($('#contractors-list :radio:checked').val());
            $('#' + $(':hidden#doc-type').val() + 'docs-delivery_address').val($('#contractors-list :radio:checked').data('address'));

            // на закрытие модального окна очищаем форму
            $('#modalContractors').modal('hide');
            $('#contractors-list').html('');
            $('#contractor-search-form input').val('');
        });

        // на закрытие модального окна очищаем форму
        $('#modalContractors').on('hide.bs.modal', function(){
            // очистим форму
            $('#contractor-search-form input').val('');
            $('#contractors-list').html('');
        });

        $(document).on('click', '.del-file', function(ev){
            ev.preventDefault();

            $.post( '/documents/default/del-file', { 'id': $(this).data('id') }, function(data){

                if( data.error !== undefined ) {
                    //$('#contractors-list').html('<p>' + data.error + '</p>');
                    $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                    $('#modalMessage').modal('show');
                } else {
                    $('#modalMessage .modal-body').html('<p>' + data.result + '</p>');
                    $('#modalMessage').modal('show');

                    // обновим список файлов в записи
                    $.post('/documents/default/get-files-list', { 'id': $('#' + $('#doc-type').val() + 'docs-id').val(), 'doc_type': $('#doc-type').val() }, function(data){
                        $('ul.doc-files li').remove();
                        $('ul.doc-files').html(data.files);
                        console.info('data', data);
                    }, "json")
                    .fail(function(error){
                        //$('#modal').modal('hide');
                        console.info('func error', error);
                        $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                        $('#modalMessage').modal('show');
                    });
                }

            }, "json")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            });
        });


        $(document).on('click', '#delete-doc-records', function(ev){
            ev.preventDefault();

            $.post('/documents/default/get-delete-confirm', {}, function(data){
                $('#modalMessage .modal-body').html(data);
                $('#modalMessage').modal('show');
            }, "html")
            .fail(function(error){
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            });


        });

        $(document).on('click', 'button#cancel', function(){
            $('#modalMessage').modal('hide');
            $('#modalExportRegister').modal('hide');
        });

        $(document).on('click', 'a#print-doc-records', function(e){
            e.preventDefault();

            var keys = $('#' + $('#print-doc-records').data('doc-type') + '-docs-grid').yiiGridView('getSelectedRows');

            window.location.href = $('#print-doc-records').attr('href') + '?ids=' + keys.join(',');
        });


        $(document).on('click', 'button#delete-selected-records', function(){

            var keys = $('#' + $('#delete-doc-records').data('doc-type') + '-docs-grid').yiiGridView('getSelectedRows');
            
            $.post( '/documents/default/delete-doc-records', { 'ids': keys, 'doc_type': $('#delete-doc-records').data('doc-type') }, function(data){
                if( data.error !== undefined ) {
                    //$('#contractors-list').html('<p>' + data.error + '</p>');
                    $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                    $('#modalMessage').modal('show');
                } else {
                    $('#modalMessage .modal-body').html('<p>' + data.result + '</p>');
                    $('#modalMessage').modal('show');
                } 

                // обновим GridView с записями
                $.pjax.reload({container: '#' + $('#delete-doc-records').data('doc-type') + '-docs-pjax'});
                
            }, "json")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }); 
        });

        // дизейблим кнопку клонирования если выделено больше одной записи
        $(document).on('click','#outgoing-docs-grid tbody :checkbox, #incoming-docs-grid tbody :checkbox, #internal-docs-grid tbody :checkbox, #other-docs-grid tbody :checkbox', function(){ 
            if( $('#outgoing-docs-grid tbody :checkbox:checked').length ) {
                $('.outgoing-docs-index #export-register').removeClass('disabled');
            } else {
                $('.outgoing-docs-index #export-register').addClass('disabled');
            }

            if( $('#outgoing-docs-grid tbody :checkbox:checked').length != 1) {
                $('.outgoing-docs-index #clone-doc-record').addClass('disabled');
            } else {
                $('.outgoing-docs-index #clone-doc-record').removeClass('disabled');
                var keys = $('#outgoing-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);
                var url = $('.outgoing-docs-index #clone-doc-record').attr('href'); console.info('url out', url + ' - ' + url.replace(/id=\d+/, 'id=' + keys[0]));
                $('.outgoing-docs-index #clone-doc-record').attr('href', url.replace(/id=\d+/, 'id=' + keys[0]));
            }

            if( $('#incoming-docs-grid tbody :checkbox:checked').length != 1) {
                $('.incoming-docs-index #clone-doc-record').addClass('disabled');
            } else {
                $('.incoming-docs-index #clone-doc-record').removeClass('disabled');
                var keys = $('#incoming-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);
                var url = $('.incoming-docs-index #clone-doc-record').attr('href'); console.info('url in', url + ' - ' + url.replace(/id=\d+/, 'id=' + keys[0]));
                $('.incoming-docs-index #clone-doc-record').attr('href', url.replace(/id=\d+/, 'id=' + keys[0]));
            }

            if( $('#internal-docs-grid tbody :checkbox:checked').length != 1) {
                $('.internal-docs-index #clone-doc-record').addClass('disabled');
            } else {
                $('.internal-docs-index #clone-doc-record').removeClass('disabled');
                var keys = $('#internal-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);
                var url = $('.internal-docs-index #clone-doc-record').attr('href'); console.info('url out', url + ' - ' + url.replace(/id=\d+/, 'id=' + keys[0]));
                $('.internal-docs-index #clone-doc-record').attr('href', url.replace(/id=\d+/, 'id=' + keys[0]));
            }

            if( $('#other-docs-grid tbody :checkbox:checked').length != 1) {
                $('.other-docs-index #clone-doc-record').addClass('disabled');
            } else {
                $('.other-docs-index #clone-doc-record').removeClass('disabled');
                var keys = $('#other-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);
                var url = $('.other-docs-index #clone-doc-record').attr('href'); console.info('url out', url + ' - ' + url.replace(/id=\d+/, 'id=' + keys[0]));
                $('.other-docs-index #clone-doc-record').attr('href', url.replace(/id=\d+/, 'id=' + keys[0]));
            }
        });

        

        $(document).on('click','#outgoing-docs-grid thead :checkbox, #incoming-docs-grid thead :checkbox, #internal-docs-grid thead :checkbox, #other-docs-grid thead :checkbox, #delete-doc-records', function(){
            $('#clone-doc-record').addClass('disabled');
            

            setTimeout(function(){ 
                if( $('#outgoing-docs-grid tbody :checkbox:checked').length ) {
                    $('.outgoing-docs-index #export-register').removeClass('disabled');
                } else {
                    $('.outgoing-docs-index #export-register').addClass('disabled');
                }  
            }, 100);
            
        });

        // обнуляем поле ID исходящего документа в форме входящего документа, если поле поиска пустое
        $('#fake-response_on_name').on('change', function(){
            if( $('#fake-response_on_name').val() == ''){
                $('#incomingdocs-response_on_id').val(0);
            }
        });

        $('#export-register').on('click', function(ev){
            ev.preventDefault();

            var keys = $('#outgoing-docs-grid').yiiGridView('getSelectedRows'); console.info('keys', keys);

            $.post('/documents/outgoing-docs/get-export-register-confirm', { 'ids': keys }, function(data){
                $('#modalExportRegister .modal-body').html(data);
                $('#modalExportRegister').modal('show');
            }, "html")
            .fail(function(error){
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }); 
        });

        $(document).on('click', '#print-register-btn', function(ev){
            var keys = $('#outgoing-docs-grid').yiiGridView('getSelectedRows');

            $.post( '/documents/outgoing-docs/export-register', { 'ids': keys }, function(data){
                console.info('data', data);

                if( data.error !== undefined ) {
                    //$('#contractors-list').html('<p>' + data.error + '</p>');
                    //$('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                    //$('#modalMessage').modal('show');
                } else {
                    //$('#modalMessage .modal-body').html('<p>' + data.result + '</p>');
                    //$('#modalMessage').modal('show');
                } 

                // обновим GridView с записями
                //$.pjax.reload({container: '#' + $('#delete-doc-records').data('doc-type') + '-docs-pjax'});
                
            }, "json")
            .fail(function(error){
                //$('#modal').modal('hide');
                console.info('func error', error);
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }); 
        });
        
        $('input[id$=serial_number]').on('blur', function(){
            if( $(this).val().length == 0 ){
                $(this).val('б.н.');
            }
        });
        
        $('input[id$=serial_number]').on('keypress', function(e){
            e = e || event;
            
            if (e.ctrlKey || e.altKey || e.metaKey) return;

            var chr = getChar(e);
            // с null надо осторожно в неравенствах,
            // т.к. например null >= '0' => true
            // на всякий случай лучше вынести проверку chr == null отдельно
            if (chr == null) return;

            if (chr < '0' || chr > '9') {
              return false;
            }
        });
        
        function getChar(event) {
            if (event.which == null) {
              if (event.keyCode < 32) return null;
              return String.fromCharCode(event.keyCode) // IE
            }

            if (event.which != 0 && event.charCode != 0) {
              if (event.which < 32) return null;
              return String.fromCharCode(event.which) // остальные
            }

            return null; // специальная клавиша
        }

})(jQuery);