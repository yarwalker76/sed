<?php
namespace app\modules\documents\controllers;

use Yii;
use yii\web\Controller;
use app\models\TblOrganizations;
use yii\web\NotFoundHttpException;
use app\modules\documents\models\DocFiles;
use yii\helpers\Html;


class DefaultController extends Controller
{
	public function  actionIndex()
	{
		return $this->render('index');
	}

	public function actionIncoming()
	{
		return $this->render('incoming');
	}

	public function actionOutgoing()
	{
		return $this->render('outgoing');
	}

	public function actionInternal()
	{
		return $this->render('internal');
	}

	public function actionOther()
	{
		return $this->render('other');
	}

	public function actionSearchContractor()
	{
		$str = '';
		$params = [];

		if( Yii::$app->request->isAjax ):

			$search = Yii::$app->request->post('ContractorSearchForm')['search'];
			$type = Yii::$app->request->post('ContractorSearchForm')['type'];

			empty($search) || $params[':search'] = '%' . mb_strtoupper($search, 'UTF-8') . '%';
			empty($type) || $params[':type'] = $type;

			$contractors = Yii::$app->db
				->createCommand("
				    select g.title, g.lastname, g.firstname, g.patronymic, g.inn, o.*, make_address_string(o.addresses_id_3) as address
				  	  from
						(select t.organizations_id id, max(t.title) title, 
                                                        max(t.lastname) lastname, 
                                                        max(t.firstname) firstname, 
                                                        max(t.patronymic) patronymic, 
                                                        max(t.inn) inn from
							(select v.organizations_id, 
								    if(p.title = 'Название', v.value, null) as 'title', 
                                                                    if(p.title = 'Фамилия', v.value, null) as 'lastname', 
                                                                    if(p.title = 'Имя', v.value, null) as 'firstname', 
                                                                    if(p.title = 'Отчество', v.value, null) as 'patronymic',
								    if(p.title = 'ИНН', v.value, null) as 'inn' 
							   from tbl_values as v
							   left join (SELECT id, title FROM tbl_params WHERE title in ('Название', 'ИНН', 'Фамилия', 'Имя', 'Отчество')) as p
								 on v.params_id = p.id
							  where p.id is not null) as t 
						  group by t.organizations_id) as g
						left join tbl_organizations as o on g.id = o.id " .
						( !empty( $search ) || !empty( $type ) ? 'WHERE ' : '' ) .
						( !empty( $search ) ? "( UPPER(g.title) LIKE :search OR g.inn LIKE :search OR UPPER(g.lastname) LIKE :search OR UPPER(g.firstname) LIKE :search OR UPPER(g.patronymic) LIKE :search ) " . ( !empty( $type ) ? ' AND ' : '' ) : '' ) .
						( !empty( $type ) ? " o.organizations_types_id = :type" : "" ),
					$params )
				->queryAll();
			
			if( count( $contractors ) ):
                            $str .= '<div id="contractors-list">';

                            foreach( $contractors as $c ):
                                    $str .= '<div class="form-group">
                                                <div class="radio">
                                                    <label>
                                                      <input type="radio" name="contractor" value="' . 
                                                        ( $c['title'] ? Html::encode($c['title']) : Html::encode($c['lastname'] .' '.$c['firstname'].' '.$c['patronymic'])) . '" '
                                                        . ' data-address="' . Html::encode($c['address']) . '"> ' . 
                                                        ( $c['title'] ? Html::encode($c['title']) : Html::encode($c['lastname'] .' '.$c['firstname'].' '.$c['patronymic'])) .
                                                    '</label>
                                                </div>    
                                          </div>';
                            endforeach;	

                            $str .= '</div><div class="form-group"><button type="submit" class="btn btn-primary">' . Yii::t('documents', 'CHOOSE') .'</button></div>';
			else:
                            $str = '<p>' . Yii::t('documents', 'NOT_FOUND') . '</p>';
			endif;	
		else:
            // not ajax request
            throw new NotFoundHttpException('The requested page does not exist.');
		endif;

		echo json_encode( [ 'contractors' => $str ] ); 
	}

	/**
    * Удаляет файл из записи реестра
    */
    public function actionDelFile()
    {
    	if( Yii::$app->request->isAjax ):
	    	if( is_integer((int) Yii::$app->request->post('id')) ):
	    		$result = DocFiles::findOne(Yii::$app->request->post('id'))->delete();
	    		if( $result === FALSE || $result == 0 ):
	    			$msg = ['error' => Yii::t('documents', 'DELETE_FILE_ERROR')];
	    		else:
	    			$msg = ['result' => Yii::t('documents', 'DELETE_FILE_SUCCESS')];
	    		endif;	
	    	else:
	    		$msg = ['error' => Yii::t('documents', 'MUST_BE_INTEGER')];
	    	endif;	
    	else:
            // not ajax request
            throw new NotFoundHttpException('The requested page does not exist.');
		endif;
    	
        echo json_encode($msg);
    }
    
    /**
    * Генерирует список прикрепленных к записи файлов
    */
    public function actionGetFilesList()
    {
    	$str = '';
    	if( Yii::$app->request->isAjax ):
	        $id = Yii::$app->request->post('id');
	        $doc_type = Yii::$app->request->post('doc_type');

	        $files = DocFiles::find()->where(['doc_id' => $id, 'doc_type' => $doc_type])->all();

	        foreach( $files as $file ): 
	            $str .= '<li><a href="http://' . $_SERVER['SERVER_NAME'] . '/' . str_replace( Yii::$app->basePath . '/web/', '', $file->path ) . '">' . $file->name . '</a> 
	            <a href="#" class="del-file" data-id="' . $file->id . '"><span class="glyphicon glyphicon-remove del" aria-hidden="true" data-placement="top" title="' . Yii::t('documents', 'DELETE') . '"></span></a></li>';
	        endforeach; 
        else:
            // not ajax request
            throw new NotFoundHttpException('The requested page does not exist.');
		endif;

        echo json_encode( [ 'files' => $str ] );
    }

    /**
    *	Удаляет выделенные записи реестра
    */
    public function actionDeleteDocRecords()
    {
    	$msg = [];
    	$str = '';

    	if( Yii::$app->request->isAjax ):
    		
    		$ids = Yii::$app->request->post('ids');
    		$model_name = 'app\modules\documents\models\\'.ucfirst( Yii::$app->request->post('doc_type') ) . 'Docs';

    		if( count($ids) ):
    			$records = $model_name::find()->where('id in (' . implode(',', $ids) . ')')->all();
    			
    			foreach( $records as $rec ):
    				if( $rec->delete() !== FALSE ):
    					$str .= '<p>' . Yii::t('documents', 'RECORD') . ' ' . Html::encode($rec->name) . ' ' . Yii::t('documents', 'DELETED') .'</p>';
    				else:
    					$str .= '<p>' . Yii::t('documents', 'RECORD') . ' ' . Html::encode($rec->name) . ' ' . Yii::t('documents', 'NOT_DELETE') .'</p>';
    				endif;	
    			endforeach;	

    			$msg = [ 'result' => $str ];
    		else:
    			$msg = [ 'error' => Yii::t('documents', 'NO_RECORDS_SELECTED') ];
    		endif;	
    	else:
            // not ajax request
            throw new NotFoundHttpException('The requested page does not exist.');
		endif;

        echo json_encode( $msg );	
    }

    /**
    * Генерирует сообщение для подтверждения
    */
    public function actionGetDeleteConfirm()
    {
    	echo '<p>' . Yii::t('documents', 'CONFIRM_DELETE_TEXT') . '</p>
    		  <div style="text-align: right"><button id="delete-selected-records" class="btn btn-primary">' . Yii::t('documents', 'DELETE') . '</button>
    		  <button id="cancel" class="btn btn-default">' . Yii::t('documents', 'CANCEL') . '</button></div>';
    }

    public function actionDownloadFile($id)
    {
    	$file = DocFiles::findOne($id);
        
        if ($file === null) {
	    throw new NotFoundHttpException('Image not found');
	}
	
        return Yii::$app->response->sendFile($file->path, $file->name ); 
    }
}