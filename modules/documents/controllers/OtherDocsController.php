<?php

namespace app\modules\documents\controllers;

use Yii;
use app\modules\documents\models\OtherDocs;
use app\modules\documents\models\OtherDocsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use app\modules\documents\models\DocsHelper;
use app\modules\documents\models\DocFiles;

/**
 * OtherDocsController implements the CRUD actions for OtherDocs model.
 */
class OtherDocsController extends Controller
{
    const DOC_TYPE = 'other';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['index', 'error'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer', 'officeManager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OtherDocs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OtherDocsSearch( ['proc_id' => ( Yii::$app->session->get('current_proc_id') ? Yii::$app->session->get('current_proc_id') : -1 )] );
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OtherDocs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Сохраняет запись реестра с файлами в рамках транзакции
     */
    private function _changeTransaction($model){
        $orig_path = Yii::$app->params['uploadPath'] . self::DOC_TYPE . '/';

        if( $model->load( Yii::$app->request->post() ) ) {
            if( is_writable($orig_path) ):
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if( $model->save() ):
                        // get inserted row ID
                        $doc_id = $model->id;

                        $files = UploadedFile::getInstances($model, 'files');

                        // store the source file name
                        foreach( $files as $key => &$f ):
                            $doc_file = new DocFiles();

                            $filename = $f->name;
                            $arr = explode(".", $f->name);
                            $ext = end($arr);

                            $file = Yii::$app->security->generateRandomString().".{$ext}";
                            $path = $orig_path . $file;

                            $doc_file->setAttributes([
                                'path' => $path,
                                'name' => $filename,
                                'doc_id' => $doc_id,
                                'doc_type' => self::DOC_TYPE,
                                'file' => $f
                            ]);

                            if( !$doc_file->save() ):
                                // не удалось сохранить запись документа
                                throw new \Exception( AppHelper::makeErrorString($doc_file->getErrors()) );
                            else:
                                if( !$f->saveAs($path) ):
                                    // не удалось сохранить файл
                                    throw new \Exception( Yii::t('documents', 'FILE_ERR_' . $f->error ) );
                                endif;
                            endif;
                        endforeach;

                        $transaction->commit();

                        return $this->redirect(['/documents/other']);
                    else:
                        // ошибка сохранения записи реестра
                        throw new \Exception( AppHelper::makeErrorString($model->getErrors()) );
                    endif;
                } catch(\Exception $e) {
                    // ошибка транзакции
                    $transaction->rollBack();
                    $model->addErrors(['exception' => $e->getMessage()]);
                }
            else:
                // директория защищена от записи
                $model->addErrors(['files' => Yii::t('documents', 'ERR_DIR_NOT_WRITABLE')]);
            endif;
        }
    }

    /**
     * Creates a new OtherDocs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OtherDocs();

        $this->_changeTransaction($model);

        return $this->render('create', [
            'model' => $model,
            'files' => [],
        ]);
    }

    /**
     * Клонирует выбранную запись
     * @param integer $id
     */
    public function actionClone($id)
    {
        $obj = $this->findModel($id);
        $model = clone $obj;
        $model->id = $model->serial_number = null;
        $model->isNewRecord = true;

        $this->_changeTransaction($model);

        return $this->render('create', [
            'model' => $model,
            'files' => [],
        ]);
    }

    /**
     * Updates an existing OtherDocs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $files = DocFiles::find()->where(['doc_id' => $id, 'doc_type' => self::DOC_TYPE])->all();

        $this->_changeTransaction($model);

        return $this->render('update', [
            'model' => $model,
            'files' => $files,
        ]);
    }

    /**
     * Deletes an existing OtherDocs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OtherDocs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OtherDocs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OtherDocs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
