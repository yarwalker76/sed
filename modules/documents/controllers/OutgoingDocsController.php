<?php

namespace app\modules\documents\controllers;

use app\models\AppHelper;
use app\modules\printdocs\models\forms\LetterForm;
use Yii;
use app\modules\documents\models\OutgoingDocs;
use app\modules\documents\models\OutgoingDocsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\documents\models\DocsHelper;
use app\modules\documents\models\ContractorSearchForm;
use app\modules\documents\models\DocFiles;

use yii\web\UploadedFile;

use app\components\exportFile\ExportFile;
use app\modules\procedures\models\Procedures;
use app\models\TblOrganizationsMethods;

/**
 * OutgoingDocsController implements the CRUD actions for OutgoingDocs model.
 */
class OutgoingDocsController extends Controller
{
    const DOC_TYPE = 'outgoing';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['index', 'error'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer', 'officeManager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OutgoingDocs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OutgoingDocsSearch( ['proc_id' => ( Yii::$app->session->get('current_proc_id') ? Yii::$app->session->get('current_proc_id') : -1 )] );
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OutgoingDocs model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Сохраняет запись реестра с файлами в рамках транзакции
     */
    private function _changeTransaction($model){
        $orig_path = Yii::$app->params['uploadPath'] . self::DOC_TYPE . '/';

        if( $model->load( Yii::$app->request->post() ) ) {
            if( is_writable($orig_path) ):
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if( $model->save() ):
                        // get inserted row ID
                        $doc_id = $model->id;

                        $files = UploadedFile::getInstances($model, 'files');

                        // store the source file name
                        foreach( $files as $key => &$f ):
                            $doc_file = new DocFiles();

                            $filename = $f->name;
                            $arr = explode(".", $f->name);
                            $ext = end($arr);

                            $file = Yii::$app->security->generateRandomString().".{$ext}";
                            $path = $orig_path . $file;

                            $doc_file->setAttributes([
                                'path' => $path,
                                'name' => $filename,
                                'doc_id' => $doc_id,
                                'doc_type' => self::DOC_TYPE,
                                'file' => $f
                            ]);

                            if( !$doc_file->save() ):
                                // не удалось сохранить запись документа
                                throw new \Exception( AppHelper::makeErrorString($doc_file->getErrors()) );
                            else:
                                if( !$f->saveAs($path) ):
                                    // не удалось сохранить файл
                                    throw new \Exception( Yii::t('documents', 'FILE_ERR_' . $f->error ) );
                                endif;
                            endif;
                        endforeach;

                        $transaction->commit();

                        return $this->redirect(['/documents/outgoing']);
                    else:
                        // ошибка сохранения записи реестра
                        throw new \Exception( AppHelper::makeErrorString($model->getErrors()) );
                    endif;
                } catch(\Exception $e) {
                    // ошибка транзакции
                    $transaction->rollBack();
                    $model->addErrors(['exception' => $e->getMessage()]);
                }
            else:
                // директория защищена от записи
                $model->addErrors(['files' => Yii::t('documents', 'ERR_DIR_NOT_WRITABLE')]);
            endif;
        }
    }

    /**
     * Creates a new OutgoingDocs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OutgoingDocs();
        $contractor_types = DocsHelper::getContractorTypesList();
        $search_model = new ContractorSearchForm();
        $response_on_name = '';

        $this->_changeTransaction($model);

        return $this->render('create', [
            'model' => $model,
            'contractor_types' => $contractor_types,
            'search_model' => $search_model,
            'files' => [],
            'response_on_name' => $response_on_name,
        ]);
    }

    public function actionClone($id)
    {
        $obj = $this->findModel($id);
        $model = clone $obj;
        $model->id = $model->serial_number = null;
        $model->isNewRecord = true;
        $contractor_types = DocsHelper::getContractorTypesList();
        $search_model = new ContractorSearchForm();
        $files = [];
        $response_on_name = $model->responseOnName;

        $this->_changeTransaction($model);

        return $this->render('create', [
            'model' => $model,
            'contractor_types' => $contractor_types,
            'search_model' => $search_model,
            'files' => [],
            'response_on_name' => $response_on_name,
        ]);
    }

    /**
     * Updates an existing OutgoingDocs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $contractor_types = DocsHelper::getContractorTypesList();
        $search_model = new ContractorSearchForm();
        $files = DocFiles::find()->where(['doc_id' => $id, 'doc_type' => self::DOC_TYPE])->all();
        $response_on_name = $model->responseOnName;

        $this->_changeTransaction($model);

        return $this->render('update', [
            'model' => $model,
            'contractor_types' => $contractor_types,
            'search_model' => $search_model,
            'files' => $files,
            'response_on_name' => $response_on_name,
        ]);
    }

    /**
     * Deletes an existing OutgoingDocs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //echo '213';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OutgoingDocs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OutgoingDocs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OutgoingDocs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetExportRegisterConfirm()
    {
        $ids = Yii::$app->request->post('ids');
        $ids_str = implode(',', $ids);
        //echo '<pre>' . print_r(Yii::$app->request->post(), true) . '</pre>';

        $searchModel = new OutgoingDocsSearch();
        $dataProvider = $searchModel->search([
            'OutgoingDocsSearch' => [ 'id' => $ids ],
        ]);

        $pd_name = Procedures::findOne(Yii::$app->session->get('current_proc_id'))->getName();

        $serials_range_str = '';

        foreach ($dataProvider->getModels() as $k => $m):
            $k != 0 || $min = $max = $m->serial_number;
            $m->serial_number >= $min || $min = $m->serial_number;
            $m->serial_number <= $max || $max = $m->serial_number;
        endforeach;
        //exit();

        echo '<p>' . Yii::t('documents', 'CONFIRM_EXPORT_REGISTER_TEXT') . '</p>' .
             '<div style="text-align: right">' .
             ExportFile::widget([
                'model'             => 'app\modules\documents\models\OutgoingDocsSearch',   // путь к модели
                'searchAttributes'  => $searchModel,                    // фильтр
                'title'             => 'Реестр писем №',
                'enterprise'        => $pd_name,
                'notes'             => 'от ' . date('d.m.Y') . ' исх. № ' . ( $min == $max ? $min : $min . ' - ' . $max),

                'getAll'            => true,                       // все записи - true, учитывать пагинацию - false
                'csvCharset'        => 'Windows-1251',              // кодировка csv файла: 'UTF-8' (по умолчанию) или 'Windows-1251'

                'buttonClass'       => 'btn btn-primary',           // класс кнопки
                'blockClass'        => '',                 // класс блока в котором кнопка
                'blockStyle'        => 'display: inline-block;',             // стиль блока в котором кнопка

                // экспорт в следующие файлы (true - разрешить, false - запретить)
                'xls'               => false,
                'csv'               => false,
                'word'              => true,
                'html'              => false,
                'pdf'               => false,

                // шаблоны кнопок
                //'xlsButtonName'     => \Yii::t('app', 'MS Excel'),
                //'csvButtonName'     => \Yii::t('app', 'CSV'),
                'wordButtonName'    => \Yii::t('documents', 'PRINT'),
                //'htmlButtonName'    => \Yii::t('app', 'HTML'),
                //'pdfButtonName'     => \Yii::t('app', 'PDF')
            ]) .
            '<button id="cancel" class="btn btn-default">' . Yii::t('documents', 'CANCEL') . '</button></div>';
    }

    public function actionGetDocsBy()
    {
        $search = Yii::$app->request->post('search');

        $docs = OutgoingDocs::find()
                ->select(['id', 'concat("№", serial_number, ", ", name, if(send_date, concat(", от ", send_date), "")) as value', 'concat("№", serial_number, ", ", name, if(send_date, concat(", от ", send_date), "")) as label'])
                ->where(['like', 'name', $search])
                ->orWhere(['like', 'serial_number', $search])
                ->andWhere(['proc_id' => Yii::$app->session->get('current_proc_id') ])
                ->asArray()
                ->all();

        echo json_encode( $docs );
    }
}
