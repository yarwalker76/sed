<?php

namespace app\modules\documents;

use Yii;

class Documents extends \yii\base\Module
{
	public $controllerNamespace = 'app\modules\documents\controllers';

    public function init()
    {
        parent::init();

        // инициализация модуля с помощью конфигурации, загруженной из config.php
    	\Yii::configure($this, require(__DIR__ . '/config/config.php'));

    	$this->setAliases(['@documents-assets' => __DIR__ . '/assets']);

        self::registerTranslations();
    }

    public static function registerTranslations() {
        if (!isset(Yii::$app->i18n->translations['documents']) && !isset(Yii::$app->i18n->translations['documents/*'])) {
            Yii::$app->i18n->translations['documents'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/modules/documents/messages',
                'sourceLanguage' => Yii::$app->language,
                'forceTranslation' => true,
                'fileMap' => [
                    'documents' => 'documents.php'
                ]
            ];
        }
    }
}