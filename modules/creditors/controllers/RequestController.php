<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/28/17
 * Time: 12:48 AM
 */

namespace app\modules\creditors\controllers;


use app\modules\creditors\models\Amortization;
use app\modules\creditors\models\Guarantee;
use app\modules\creditors\models\Request;
use app\modules\creditors\models\forms\RequestForm;
use app\modules\creditors\models\RequestType;
use app\modules\creditors\services\RequestService;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class RequestController extends Controller
{
    /**
     * @var RequestService
     */
    private $requestService;

    /**
     * RequestController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param RequestService $requestService
     * @param array $config
     */
    public function __construct($id, $module, RequestService $requestService, array $config = [])
    {
        $this->requestService = $requestService;

        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'get-as-json',
                        ],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['manager', 'administrator'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new RequestForm();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($newModel = $this->requestService->createFromForm($model)) {
                return $this->redirect(['view', 'id' => $newModel->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'typeDropdown' => RequestType::getDropdown(),
            'queueDropdown' => Request::getQueueDropdown(),
            'thirdQueueDropdown' => $this->requestService->getThirdQueueDropdown(),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = RequestForm::findOne($id);

        if ($model == false) {
            throw new NotFoundHttpException(Yii::t('creditors', 'Cannot find request'));
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($newModel = $this->requestService->update($model)) {
                return $this->redirect(['view', 'id' => $newModel->id]);
            }
        }

        Url::remember();

        return $this->render('update', [
            'model' => $model,
            'typeDropdown' => RequestType::getDropdown(),
            'queueDropdown' => Request::getQueueDropdown(),
            'thirdQueueDropdown' => $this->requestService->getThirdQueueDropdown(),
            'guarantee' => new Guarantee([
                'request_id' => $model->id,
            ]),
            'amortization' => new Amortization([
                'request_id' => $model->id,
            ]),
        ]);
    }

    public function actionIndex()
    {
        $models = $this->requestService->getRequestQuery()->all();
        $mapping = Request::getQueueDropdown();

        return $this->render('index', [
            'models' => $this->requestService->getMappedModels($models, $mapping),
            'total' => $this->requestService->getTotal($models),
            'queueMapping' => $mapping,
        ]);
    }

    public function actionView($id)
    {
        $model = Request::findOne($id);

        if ($model == false) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $model,
            'typeMapping' => RequestType::getDropdown(),
            'queueMapping' => Request::getQueueDropdown(),
            'guarantee' => new Guarantee([
                'request_id' => $model->id,
            ]),
            'amortization' => new Amortization([
                'request_id' => $model->id,
                'total' => $model->total,
            ]),
        ]);
    }

    public function actionGetAsJson()
    {
        $model = $this->requestService->getRequest(Yii::$app->request->post('id'));

        if ($model) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $model;
        }

        throw new NotFoundHttpException();
    }

    public function actionDelete($id)
    {
        $model = Request::findOne($id);

        if ($model) {
            $this->requestService->delete($model);
        }

        return $this->redirect(['index']);
    }
}