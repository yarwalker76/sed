<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/30/17
 * Time: 2:10 AM
 */

namespace app\modules\creditors\controllers;


use app\modules\creditors\behaviors\UpdateBehavior;
use app\modules\creditors\models\Amortization;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

class AmortizationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'create' => ['POST'],
                    'update' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager', 'administrator'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new Amortization();

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }

        return $this->goBack();
    }

    public function actionUpdate($id)
    {
        $model = Amortization::findOne($id);

        if ($model && $model->load(Yii::$app->request->post())) {
            if ($model->isJustUpdated()) {
                $newModel = new Amortization($model->getAttributes(null, UpdateBehavior::getIgnoredAttributes()));
                $newModel->save();

                $model->is_updated = true;
                $model->updated_id = $newModel->id;
                $model->save();
            } else {
                $model->save();
            }
        }

        return $this->goBack();
    }

    public function actionDelete($id)
    {
        $model = Amortization::findOne($id);

        if ($model) {
            $model->delete();
        }

        return $this->goBack();
    }
}