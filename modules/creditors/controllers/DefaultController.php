<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/26/17
 * Time: 9:20 PM
 */

namespace app\modules\creditors\controllers;

use Yii;
use app\modules\creditors\models\Creditor;
use app\modules\creditors\services\CreditorService;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends Controller
{
    /**
     * @var CreditorService
     */
    private $creditorService;

    /**
     * DefaultController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param CreditorService $creditorService
     * @param array $config
     */
    public function __construct($id, $module, CreditorService $creditorService, array $config = [])
    {
        $this->creditorService = $creditorService;

        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                            'find-by-title',
                        ],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['manager', 'administrator'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->creditorService->getFindQuery(),
        ]);

        return $this->render('index', [
            'provider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Creditor();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $creditor = $this->creditorService->create($model);

            if ($creditor) {
                return $this->redirect(['view', 'id' => $creditor->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->creditorService->findById($id);

        if ($model == false) {
            throw new NotFoundHttpException('Cannot Find Creditor');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->creditorService->findById($id);

        if ($model == false) {
            throw new NotFoundHttpException('Cannot Find Creditor');
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()) {
            $model = $this->creditorService->update($model);

            if ($model) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            throw new NotFoundHttpException('Невозможно обновить кредитора!');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionFindByTitle()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->enableCsrfValidation = false;

        $query = Yii::$app->request->get('title');

        return [
            'query' => $query,
            'suggestions' => $this->creditorService->findByTitle($query),
        ];
    }

    public function actionDelete($id)
    {
        $model = Creditor::findOne($id);

        if ($model) {
            if (count($model->getRequests()->all())) {
                return $this->render('delete-error');
            } else {
                $this->creditorService->delete($model);
            }
        }

        return $this->redirect(['index']);
    }

    public function actionUpdateDetails($id)
    {
        $model = Creditor::findOne($id);

        if ($model) {
            $this->creditorService->updateDetails($model);
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }
}