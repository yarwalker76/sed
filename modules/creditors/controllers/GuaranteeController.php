<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/30/17
 * Time: 2:10 AM
 */

namespace app\modules\creditors\controllers;


use app\modules\creditors\behaviors\UpdateBehavior;
use app\modules\creditors\models\Guarantee;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

class GuaranteeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'create' => ['POST'],
                    'update' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['manager', 'administrator'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new Guarantee();

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }

        return $this->goBack();
    }

    public function actionUpdate($id)
    {
        $model = Guarantee::findOne($id);

        if ($model && $model->load(Yii::$app->request->post())) {
            if ($model->isJustUpdated()) {
                $newModel = new Guarantee($model->getAttributes(null, UpdateBehavior::getIgnoredAttributes()));
                $newModel->save();

                $model->refresh();
                $model->is_updated = true;
                $model->updated_id = $newModel->id;
                $model->save();
            } else {
                $model->save();
            }
        }

        return $this->goBack();
    }

    public function actionDelete($id)
    {
        $model = Guarantee::findOne($id);

        if ($model) {
            $model->delete();
        }

        return $this->goBack();
    }
}