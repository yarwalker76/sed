<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/10/17
 * Time: 11:41 PM
 */

namespace app\modules\creditors\controllers;


use app\modules\creditors\models\forms\PrintForm;
use app\modules\creditors\models\Request;
use app\modules\creditors\services\PrintService;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\Response;

class PrintController extends Controller
{
    /**
     * @var PrintService
     */
    private $printService;

    /**
     * PrintController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param PrintService $printService
     * @param array $config
     */
    public function __construct($id, $module, PrintService $printService, array $config = [])
    {
        $this->printService = $printService;

        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionReport()
    {
        $model = new PrintForm([
            'queues' => array_keys(Request::getQueueDropdown()),
        ]);

        if (Yii::$app->request->isPost && $model->validate()) {
            $resPath = $this->printService->buildReport($model);

            Yii::$app->response->on(Response::EVENT_AFTER_SEND, function () use ($resPath) {
                unlink($resPath);
            });

            return Yii::$app->response->sendFile($resPath);
        }

        return $this->render('report', [
            'model' => $model,
        ]);
    }
}