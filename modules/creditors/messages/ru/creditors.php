<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/2/17
 * Time: 5:58 PM
 */

return [
    'Amortization' => 'Погашение',
    'Behind Queue' => 'За реестром',
    'Calculate Percents' => 'Подсчитать проценты',
    'Cannot find request' => 'Невозможно найти требование',
    'Create amortization' => 'Создать погашение',
    'Create Creditor' => 'Создать кредитора',
    'Create guarantee' => 'Создать залоговое обязательство',
    'Create Request' => 'Создать требование',
    'Create time' => 'Время создания',
    'Creditor' => 'Кредитор',
    'Creditors' => 'Реестр Кредиторов',
    'Details' => 'Детали',
    'First Queue' => '1-я очередь',
    'Guarantees' => 'Залоговые обязательства',
    'ID' => '№П.З.',
    'Initiation Time' => 'Дата',
    'Is Updated' => 'Отметка об изменениях',
    'Linked Request ID' => 'Связанное обязательство',
    'Organization ID' => '№Кредитора',
    'Param' => 'Параметр',
    'Penalty Queue' => '3.4 - очередь, штрафные санкции',
    'Registry Number' => 'Номер по реестру',
    'Request' => 'Требование',
    'Requests' => 'Требования',
    'Request Types' => 'Виды требований',
    'Request Type' => 'Вид требования',
    'rub' => 'рублей',
    'Second Queue' => '2-я очередь',
    'Select queue type' => 'Выберите очередь',
    'Queue Type' => 'Вид очереди',
    'Select request' => 'Выберете требование',
    'Select type' => 'Выберете вид',
    'Type' => 'Вид обязательств',
    'Submit' => 'Создать',
    'Update' => 'Обновить',
    'Create' => 'Создать',
    'Third Queue With Guarantee' => '3.1 - очередь, обеспеченная залогом',
    'Third Queue Without Guarantee' => '3.2 - очередь не обеспеченная залогом',
    'Third Queue With Percents' => '3.3 - очередь, проценты на сумму требований',
    'Title' => 'Кредитор',
    'Total' => 'Всего',
    'Request Total' => 'Размер требований',
    'Amortization Total Ratio' => 'Отношение к сумме требований в очереди',
    'Total Payed' => 'Сумма погашения',
    'Request Details' => 'Основание требования',
    'Uodate' => 'Обновить',
    'Update Creditor' => 'Обновить кредитора',
    'Updated ID' => '№Н.П.З.',
    'Update Initiator' => 'Инициатор обновления',
    'Update Reason' => 'Основание изменения',
    'Value' => 'Значение',
    'Is Enabled' => 'Включено',
    'Is Disabled' => 'Исключено',
    'Creditor Details Update' => 'Изменение сведений кредитора',
    'Creditor Change' => 'Изменение кредитора',
    'Print Requests' => 'Печать реестра требований',
    'Request Type Title' => 'Вид обязательств',
    'Enable Reason' => 'Основание включения',
    'Disable Reason' => 'Основание исключения',
    'Enable Time' => 'Дата включения',
    'Disable Time' => 'Дата исключения',
    'Amortization Details' => 'Реквизиты документа о погашении',
    'Save' => 'Сохранить',
    'Create Time' => 'Дата внесения записи в реестр',
    'Amortization Time' => 'Дата Погашения',
    'Amortization Total' => 'Размер непогашенного требования',
    'Guarantee Total' => 'Размер залогового обеспечения',
    'Guarantee Details' => 'Реквизиты договора займа',
    'Guarantee Total Ratio' => '% - отношение размера залога',
    'Percentage' => 'Процентная Ставка',
    'Day Count' => 'Дней',
    'Total Accrued' => 'Размер начисленных требований',
];