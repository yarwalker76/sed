<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/4/17
 * Time: 11:31 PM
 */

namespace app\modules\creditors\repositories;


use yii\base\Object;
use app\modules\creditors\models\Creditor;
use app\modules\creditors\models\CreditorDetails;
use app\modules\creditors\models\CreditorBankAccount;
use app\modules\creditors\models\CreditorAddress;

class OrganizationDetailsRepository extends Object
{
    /**
     * @param $creditor Creditor
     * @param $params array
     * @return CreditorDetails[]
     */
    public function createDetails($creditor, $params)
    {
        $res = [];

        foreach ($params as $param) {
            $model = new CreditorDetails([
                'params_id' => $param['params_id'],
                'value' => $param['value'],
                'creditor_id' => $creditor->id,
            ]);

            if ($model->save()) {
                $res[] = $model;
            }
        }

        return $res;
    }

    /**
     * @param $creditor Creditor
     * @param $bankAccounts array
     * @return CreditorBankAccount[]
     */
    public function createBankAccounts($creditor, $bankAccounts)
    {
        $res = [];

        foreach ($bankAccounts as $bankAccount) {
            $model = new CreditorBankAccount([
                'creditor_id' => $creditor->id,
                'banks_id' => $bankAccount['banks_id'],
                'number' => $bankAccount['number'],
                'is_closed' => $bankAccount['closed'],
                'close_time' => $bankAccount['closing'],
                'comment' => $bankAccount['comment'],
            ]);

            if ($model->save()) {
                $res[] = $model;
            }
        }

        return $res;
    }

    public function createAddresses($creditor, $addresses)
    {
        $res = [];

        foreach ($addresses as $type => $address) {
            $type = (int) $type;
            if (in_array($type, CreditorAddress::getTypes())) {
                $model = new CreditorAddress([
                    'creditor_id' => $creditor->id,
                    'locality' => $address['locality'],
                    'address' => $address['address'],
                    'postcode' => $address['postcode'],
                    'type' => $type,
                ]);

                if ($model->save()) {
                    $res[] = $model;
                }
            }
        }

        return $res;
    }
}