<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/26/17
 * Time: 6:02 PM
 */

namespace app\modules\creditors\repositories;

use app\models\TblOrganizationsMethods;
use app\modules\creditors\models\Creditor;
use app\modules\creditors\models\CreditorDetails;
use yii\db\ActiveQuery;

class CreditorRepository extends \yii\base\Object
{
    /**
     * @param $attributes array
     * @param $organization array
     * @param $details array
     * @return Creditor|false
     */
    public function createFromOrganization($attributes, $organization, $details)
    {
        $model = new Creditor($attributes);

        $model->organization_id = $organization['id'];
        $model->organization_type_id = $organization['organizations_types_id'];
        $model->title = $this->extractTitleFromParams($details);

        if ($model->save()) {
            return $model;
        }

        return false;
    }

    /**
     * @param $params array
     * @return string
     */
    private function extractTitleFromParams($params)
    {
        return TblOrganizationsMethods::extractNameFromParams($params);
    }

    /**
     * @return ActiveQuery
     */
    public function find()
    {
        return Creditor::find()
            ->joinWith([
                'indexedList'
            ]);
    }

    /**
     * @param $model Creditor
     * @return Creditor
     */
    public function createFromUpdated($model)
    {
        $newModel = new Creditor();

        $newModel->setAttributes($model->getAttributes(null, [
            'is_updated',
            'update_reason',
            'update_initiator',
            'updated_id',
        ]));

        return $newModel;
    }

    /**
     * @param $title string
     * @return ActiveQuery
     */
    public function findByTitle($title)
    {
        return $this->find()
            ->select([
                'creditor.id',
                'title AS value',
            ])
            ->asArray()
            ->andFilterWhere(['LIKE', 'title', $title]);
    }
}