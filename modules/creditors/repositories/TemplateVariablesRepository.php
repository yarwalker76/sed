<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/14/17
 * Time: 12:38 AM
 */

namespace app\modules\creditors\repositories;


use app\modules\creditors\models\CreditorAddress;
use Yii;
use app\models\AddressHelper;
use app\models\AppHelper;
use app\models\DateHelper;
use app\models\TblOrganizationsMethods;
use app\modules\creditors\models\Creditor;
use app\modules\creditors\models\CreditorBankAccount;
use app\modules\creditors\models\PrintTable;
use app\modules\creditors\models\Request;
use app\modules\procedures\models\Procedures;
use PhpOffice\PhpWord\TemplateProcessor;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class TemplateVariablesRepository extends Object
{
    /**
     * @var TemplateProcessor
     */
    public $template;

    public function setCommon()
    {
        $procedure = Procedures::findOne(AppHelper::getLastSeenProcedureID());

        $this->template->setValue('${ПДНазвание}', $procedure->getName());
        list($day, $month, $year) = explode('.', date('d.m.Y'));
        $this->template->setValue('${ТекДата}', sprintf('%s %s %s г', $day, DateHelper::getMonth()[(int) $month], $year));

        $name = AppHelper::getFIO( $procedure->manager_id );
        list($lastname, $firstname, $patronymic) = explode(' ', $name);
        $shortName = $lastname . ' ' . mb_substr($firstname, 0, 1, 'UTF-8') . '.' . mb_substr($patronymic, 0, 1, 'UTF-8');
        $this->template->setValue('${АУФамилияИнициалы}', $shortName);
        $this->template->setValue('${ФИОАУПолное}', $name);
    }

    public function getQueueMapping()
    {
        return [
            Request::QUEUE_TYPE_FIRST => '1',
            Request::QUEUE_TYPE_SECOND => '2',
            Request::QUEUE_TYPE_THIRD_GUARANTEE => '3.1',
            Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE => '3.2',
            Request::QUEUE_TYPE_PERCENTS => '3.3',
            Request::QUEUE_TYPE_PENALTY => '3.4',
            Request::QUEUE_TYPE_DEFAULT => 'ЗР',
        ];
    }

    public function executeQueueBaseReplaces($queue, $creditors, $requests, $amortizations)
    {
        $id = $this->getQueueMapping()[$queue];

        $total = array_sum(ArrayHelper::getColumn($requests, function ($model) {
            return ($model->isDisabled() == false) ? $model->total : 0;
        }));
        $totalPayed = array_sum(ArrayHelper::getColumn($amortizations, function ($model) {
            return ($model->isUpdated() == false) ? $model->total_payed : 0;
        }));

        $ratio = $total ? ($totalPayed / $total * 100) : 0;

        $exluded = array_filter($requests, function ($model) {
            return $model->isDisabled();
        });

        $this->template->setValue(sprintf('${Кол-воКредиторовОчер%s}', $id), count($creditors));
        $this->template->setValue(sprintf('${Кол-воТребованийКредиторовОчер%s}', $id), count($requests));
        $this->template->setValue(sprintf('${СумТребОчер%s}', $id), Yii::$app->formatter->asDecimal($total));
        $this->template->setValue(sprintf('${СумПогашенийОчер%s}', $id), Yii::$app->formatter->asDecimal($totalPayed));
        $this->template->setValue(sprintf('${%%Отношение%s}', $id), Yii::$app->formatter->asDecimal($ratio));
        $this->template->setValue(sprintf('${Кол-воИсключенныхОчер%s}', $id), count($exluded));
    }

    public function getCreditorMapping()
    {
        return [
            'i' => 'ПП',
            'initiation_time' => 'Дата',
            'registry_number' => 'НомерПоРеестру',
            'name' => 'ФИО',
            'passport' => 'Паспорт',
            'addressFirst' => 'Адрес',
            'bankAccount' => 'Реквизиты',
            'changes' => 'Изменения',
            'addressSecond' => 'Местонахождение',
            'chiefName' => 'ФИОРуководителя',
        ];
    }

    public function getRequestMapping()
    {
        return [
            'i' => 'ПП',
            'create_time' => 'ДатаВнесения',
            'creditor_id' => 'НомерКредитора',
            'registry_number' => 'НомерПоРеестру',
            'type' => 'ВидОбязательство',
            'details' => 'Основание',
            'initiation_time' => 'Дата',
            'total' => 'РазмерТребования',
            'enable_reason' => 'ОснованиеВключения',
            'changes' => 'Изменения',
            'linked_request_id' => 'НомерСвязанногоТребования',
            'percentage' => 'СтавкаПроцента',
            'day_count' => 'ПериодНачисления',
            'total_accrued' => 'РазмерПоПроцентам',
        ];
    }

    public function getAmortizationMapping()
    {
        return [
            'i' => 'ПП',
            'create_time' => 'Дата',
            'creditor_id' => 'НомерКредитора',
            'request_id' => 'НомерТребования',
            'details' => 'Реквизиты',
            'total_payed' => 'РазмерПогашения',
            'ratio' => 'ОтношениеКСумме',
            'initiation_time' => 'ДатаПогашения',
            'total_left' => 'РазмерОстатка',
            'request_disable_time' => 'ДатаИсключенияТребования',
            'request_disable_details' => 'РеквизитыИсключенияТребования',
            'changes' => 'Изменения',
        ];
    }

    public function getGuaranteeMapping()
    {
        return [
            'i' => 'ПП',
            'create_time' => 'Дата',
            'creditor_id' => 'НомерКредитора',
            'request_id' => 'НомерТребования',
            'details' => 'Реквизиты',
            'total' => 'Размер',
            'totalRatio' => 'ОтношениеКТребованию',
            'changes' => 'Изменения',
        ];
    }

    public function buildCreditorRow($model, $models)
    {
        return [
            'i' => $model->index,
            'initiation_time' => $this->buildDate($model->initiation_time),
            'registry_number' => $model->registry_number,
            'name' => $model->title,
            'passport' => $this->extractPassport($model),
            'addressFirst' => @$model->addresses[CreditorAddress::TYPE_CORRESPONDENTION] ? $this->buildAddress($model->addresses[CreditorAddress::TYPE_CORRESPONDENTION]) : null,
            'bankAccount' => @$model->bankAccounts[0] ? $this->buildBankAccount($model->bankAccounts[0]) : null,
            'addressSecond' => @$model->addresses[CreditorAddress::TYPE_LEGAL] ? $this->buildAddress($model->addresses[CreditorAddress::TYPE_LEGAL]) : null,
            'chiefName' => $this->findCreditorDetail($model, 'Руководитель'),
            'changes' => $this->buildChanges($model, $models),
        ];
    }

    /**
     * @param $model Request
     * @return array
     */
    public function buildRequestRow($model, $models)
    {
        if ($model->queue_type === Request::QUEUE_TYPE_PERCENTS) {
            return $this->buildPercentRequestRow($model, $models);
        }

        return [
            'i' => $model->index,
            'create_time' => $this->buildDate($model->create_time),
            'creditor_id' => $model->creditor->registry_number,
            'registry_number' => $model->registry_number,
            'type' => @$model->requestType->title,
            'details' => $model->details,
            'initiation_time' => $this->buildDate($model->initiation_time),
            'total' => Yii::$app->formatter->asDecimal($model->total),
            'enable_reason' => $model->enable_reason,
            'changes' => $this->buildChanges($model, $models),
        ];
    }

    /**
     * @param $model Request
     * @return array
     */
    public function buildPercentRequestRow($model, $models)
    {
        return [
            'i' => $model->index,
            'create_time' => $this->buildDate($model->create_time),
            'creditor_id' => $model->creditor->registry_number,
            'linked_request_id' => $model->linkedRequest->registry_number,
            'total' => Yii::$app->formatter->asDecimal($model->total),
            'percentage' => Yii::$app->formatter->asDecimal($model->percentage),
            'day_count' => $model->day_count,
            'total_accrued' => Yii::$app->formatter->asDecimal($model->total_accrued),
            'registry_number' => $model->registry_number,
            'details' => $model->details,
            'changes' => $this->buildChanges($model, $models),
        ];
    }

    public function buildAmortizationRow($model, $models)
    {
        return [
            'i' => $model->index,
            'create_time' => $this->buildDate($model->create_time),
            'creditor_id' => $model->request->creditor->registry_number,
            'request_id' => $model->request->registry_number,
            'details' => $model->details,
            'total_payed' => Yii::$app->formatter->asDecimal($model->total_payed),
            'ratio' => Yii::$app->formatter->asDecimal($model->totalRatio),
            'initiation_time' => $this->buildDate($model->initiation_time),
            'total_left' => Yii::$app->formatter->asDecimal($model->request->calculateAmortizationPayedTotal() - $model->total_payed),
            'request_disable_time' => $this->buildDate($model->request->disable_time),
            'request_disable_details' => $model->request->disable_reason,
            'changes' => $this->buildChanges($model, $models),
        ];
    }

    public function buildGuaranteeRow($model, $models)
    {
        return [
            'i' => $model->index,
            'create_time' => $this->buildDate($model->create_time),
            'creditor_id' => $model->request->creditor->registry_number,
            'request_id' => $model->request->registry_number,
            'details' => $model->details,
            'total' => Yii::$app->formatter->asDecimal($model->total),
            'totalRatio' => Yii::$app->formatter->asDecimal($model->totalRatio),
            'changes' => $this->buildChanges($model, $models),
        ];
    }

    /**
     * @param $printTable PrintTable
     */
    public function buildTable($printTable)
    {
        if ($printTable->number == false) {
            return ;
        }

        $callback = null;
        $mapping = null;

        switch ($printTable->type) {
            case PrintTable::TYPE_REQUEST:
                $callback = [$this, 'buildRequestRow'];
                $mapping = $this->getRequestMapping();
                break;
            case PrintTable::TYPE_CREDITOR:
                $callback = [$this, 'buildCreditorRow'];
                $mapping = $this->getCreditorMapping();
                break;
            case PrintTable::TYPE_GUARANTEE:
                $callback = [$this, 'buildGuaranteeRow'];
                $mapping = $this->getGuaranteeMapping();
                break;
            case PrintTable::TYPE_AMORTIZATION:
                $callback = [$this, 'buildAmortizationRow'];
                $mapping = $this->getAmortizationMapping();
                break;
        }

        $rows = [];

        if ($callback && $mapping) {
            for ($i = 0; $i < count($printTable->models); $i++) {
                $rows[] = call_user_func($callback, $printTable->models[$i], $printTable->models);
            }
        }

        $this->fillTables($rows, $mapping, $printTable->number);
    }

    protected function fillTables($rows, $mapping, $tableNum)
    {
        if (count($rows)) {
            $cloned = false;

            try {
                $cloneReplace = sprintf('${Таблица%s%s}', $tableNum, $mapping['i']);
                $this->template->cloneRow($cloneReplace, count($rows));

                $cloned = true;
            } catch (\Exception $exception) {
                $this->clearTable($mapping, $tableNum);
            }

            if ($cloned) {
                $i = 0;
                foreach ($rows as $row) {
                    $i++;

                    foreach ($row as $attr => $value) {
                        if (isset($mapping[$attr])) {
                            $replace = sprintf('${Таблица%s%s#%s}', $tableNum, $mapping[$attr], $i);
                            $this->template->setValue($replace, $this->buildValue($attr, $value, $row));
                        }
                    }

                    $this->clearRow($mapping, $tableNum, $i);
                }
            }
        }

        $this->clearTable($mapping, $tableNum);
    }

    public function clearRow($mapping, $tableNum, $rowNum)
    {
        foreach ($mapping as $attr => $value) {
            $replace = sprintf('${Таблица%s%s#%s}', $tableNum, $mapping[$attr], $rowNum);
            $this->template->setValue($replace,  '-');
        }
    }

    public function clearTable($mapping, $tableNum)
    {
        foreach ($mapping as $attr => $value) {
            $replace = sprintf('${Таблица%s%s}', $tableNum, $mapping[$attr]);
            $this->template->setValue($replace,  '-');
        }
    }

    /**
     * @param $replace
     * @return string
     */
    public function cross($replace)
    {
        return '</w:t></w:r><w:r><w:rPr><w:strike/><w:sz w:val="19"/></w:rPr><w:t xml:space="preserve">' . $replace . '</w:t></w:r><w:r><w:t>';
    }

    /**
     * @param $model Creditor
     * @return string
     */
    private function buildChanges($model, $models)
    {
        if ($model->getAttribute('is_updated') || $model->getAttribute('is_details_updated')) {
            $num = $model->updated ? $model->updated->index : $model->id;
            $i = 0;

            foreach ($models as $possibleUpdated) {
                $i++;

                if ($possibleUpdated->id === $model->updated_id) {
                    $num = $possibleUpdated->index;
                    break;
                }
            }

            return sprintf('%s %s', $num, $model->is_updated ? $model->update_reason : $model->update_details_reason);
        }

        return null;
    }

    private function buildAddress($address)
    {
        return AddressHelper::build([
            $address->postcode,
            $address->address,
            $address->region ? $address->region->title : null,
            $address->locality
        ]);
    }

    /**
     * @param $bankAccount CreditorBankAccount
     * @return mixed
     */
    private function buildBankAccount($bankAccount)
    {
        return sprintf('%s %s', $bankAccount->number, $bankAccount->bankTitle);
    }

    /**
     * @param $date string
     * @return mixed
     */
    private function buildDate($date)
    {
        return Yii::$app->formatter->asDate($date, 'php:d.m.Y');
    }

    private function extractPassport($model)
    {
        $details = [
            'серия',
            'номер',
            'выдан'
        ];
        $res = [];

        foreach ($details as $detail) {
            $value = $this->findCreditorDetail($model, $detail);

            if ($value) {
                $res[] = $value;
            }
        }

        return implode(', ', $res);
    }

    private function findCreditorDetail($model, $detail)
    {
        foreach ($model->organizationDetails as $storedDetail) {
            if ($storedDetail->tblParams && $storedDetail->tblParams->title === $detail) {
                return $storedDetail->value;
            }
        }

        return null;
    }

    private function buildValue($attribute, $value, $row)
    {
        $value = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $value);

        if ($value === null || trim($value) === '') {
            return '-';
        }

        if ($this->getIsCrossed($attribute, $row)) {
            return $this->cross($value);
        }

        return $value;
    }

    private function getNotCrossingAttributes()
    {
        return [
            'request_disable_time',
            'request_disable_details',
            'changes',
        ];
    }

    private function getIsCrossed($attribute, $row)
    {
        if (!empty($row['changes'])) {
            foreach ($this->getNotCrossingAttributes() as $notCrossingAttribute) {
                if ($attribute === $notCrossingAttribute) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }
}