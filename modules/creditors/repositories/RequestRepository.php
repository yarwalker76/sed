<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/26/17
 * Time: 6:02 PM
 */

namespace app\modules\creditors\repositories;

use app\modules\creditors\behaviors\UpdateBehavior;
use app\modules\creditors\models\Amortization;
use app\modules\creditors\models\Guarantee;
use yii\base\Object;
use app\modules\creditors\models\Request;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;


class RequestRepository extends Object
{
    /**
     * @return ActiveQuery
     */
    public function getQuery()
    {
        return Request::find()
            ->joinWith([
                'indexedList',
                'creditor',
                'requestType'
            ]);
    }

    /**
     * @param $old Request
     * @param $reason string
     * @return Request
     */
    public function createUpdated($old, $reason)
    {
        $new = UpdateBehavior::createUpdated($old, $reason);

        $this->updateGuarantees($old, $new);
        $this->updateAmortizations($old, $new);

        return $new;
    }

    private function updateGuarantees($old, $new)
    {
        Guarantee::updateAll([
            'request_id' => $new->id,
        ], [
            'id' => ArrayHelper::getColumn($old->guarantees, 'id'),
        ]);
    }

    private function updateAmortizations($old, $new)
    {
        Amortization::updateAll([
            'request_id' => $new->id,
        ], [
            'id' => ArrayHelper::getColumn($old->amortizations, 'id'),
        ]);
    }

    public function delete($model)
    {
        foreach (array_merge($model->guarantees, $model->amortizations) as $child) {
            $child->delete();
        }

        $model->delete();
    }
}