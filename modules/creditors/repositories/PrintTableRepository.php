<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/10/17
 * Time: 11:59 PM
 */

namespace app\modules\creditors\repositories;


use app\modules\creditors\models\Amortization;
use app\modules\creditors\models\Creditor;
use app\modules\creditors\models\Guarantee;
use app\modules\creditors\models\PrintTable;
use app\modules\creditors\models\Request;
use yii\base\Model;
use yii\base\Object;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class PrintTableRepository extends Object
{
    /**
     * @var integer
     */
    public $procedures_id;

    /**
     * @param $queue
     * @return array
     */
    public function getTables($queue)
    {
        if ($queue === Request::QUEUE_TYPE_THIRD_GUARANTEE) {
            return $this->buildGuaranteeTables();
        } else {
            return $this->buildBaseTables($queue);
        }
    }

    /**
     * @param $queue
     * @return ActiveQuery
     */
    protected function getRequestQuery($queue)
    {
        return Request::find()
            ->joinWith([
                'contextIndexedList',
                'creditor',
            ])
            ->with([
                'creditor.contextIndexedList',
                'requestType',
                'guarantees',
                'guarantees.contextIndexedList',
                'guarantees.request',
                'amortizations',
                'amortizations.request',
                'amortizations.contextIndexedList',
            ])
            ->andWhere([
                'creditor.procedures_id' => $this->procedures_id,
                'creditor_request.queue_type' => $queue,
            ]);
    }

    /**
     * @param $models array
     * @return array
     */
    private function getFlatAmortizations($models)
    {
        $amortizations = ArrayHelper::getColumn($models, 'amortizations');

        if (count($amortizations)) {
            return call_user_func_array('array_merge', $amortizations);
        } else {
            return [];
        }
    }

    /**
     * @param $models array
     * @return array
     */
    private function getFlatGuarantees($models)
    {
        $guarantees = ArrayHelper::getColumn($models, 'guarantees');

        if (count($guarantees)) {
            return call_user_func_array('array_merge', $guarantees);
        } else {
            return [];
        }
    }

    /**
     * @param $models array
     * @return array
     */
    private function getFlatCreditors($models)
    {
        $creditors = ArrayHelper::getColumn($models, 'creditor');
        $creditors = ArrayHelper::index($creditors, 'id');

        if (count($creditors)) {
            return array_values($creditors);
        } else {
            return [];
        }
    }

    /**
     * @param $queue integer
     * @return array
     */
    public function buildBaseTables($queue)
    {
        $models = $this->getRequestQuery($queue)->all();

        return [
            new PrintTable([
                'queue' => $queue,
                'models' => $this->getFlatCreditors($models),
                'type' => PrintTable::TYPE_CREDITOR,
            ]),
            new PrintTable([
                'queue' => $queue,
                'models' => $models,
                'type' => PrintTable::TYPE_REQUEST,
            ]),
            new PrintTable([
                'queue' => $queue,
                'models' => $this->getFlatAmortizations($models),
                'type' => PrintTable::TYPE_AMORTIZATION,
            ]),
        ];
    }

    /**
     * @return array
     */
    public function buildGuaranteeTables()
    {
        $queue = Request::QUEUE_TYPE_THIRD_GUARANTEE;
        $models = $this->getRequestQuery($queue)->all();

        return [
            new PrintTable([
                'queue' => $queue,
                'models' => $this->getFlatCreditors($models),
                'type' => PrintTable::TYPE_CREDITOR,
            ]),
            new PrintTable([
                'queue' => $queue,
                'models' => $models,
                'type' => PrintTable::TYPE_REQUEST,
            ]),
            new PrintTable([
                'queue' => $queue,
                'models' => $this->getFlatAmortizations($models),
                'type' => PrintTable::TYPE_AMORTIZATION,
            ]),
            new PrintTable([
                'queue' => $queue,
                'models' => $this->getFlatGuarantees($models),
                'type' => PrintTable::TYPE_GUARANTEE,
            ]),
        ];
    }
}