<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/26/17
 * Time: 9:32 PM
 */

namespace app\modules\creditors\services;

use app\models\AppHelper;
use app\models\TblAccountsSearch;
use app\models\TblOrganizationsMethods;
use app\modules\creditors\models\Creditor;
use app\modules\creditors\models\CreditorDetails;
use app\modules\creditors\models\Request;
use app\modules\creditors\repositories\CreditorRepository;
use app\modules\creditors\repositories\OrganizationDetailsRepository;
use app\modules\creditors\repositories\RequestRepository;
use yii\base\Object;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class CreditorService extends Object
{
    /**
     * @var CreditorRepository
     */
    private $creditorRepository;

    /**
     * @var OrganizationDetailsRepository
     */
    private $organizationDetailsRepository;

    /**
     * @var RequestRepository
     */
    private $requestRepository;

    /**
     * CreditorService constructor.
     * @param CreditorRepository $creditorRepository
     * @param OrganizationDetailsRepository $organizationDetailsRepository
     * @param RequestRepository $requestRepository
     * @param array $config
     */
    public function __construct(CreditorRepository $creditorRepository, OrganizationDetailsRepository $organizationDetailsRepository, RequestRepository $requestRepository, array $config = [])
    {
        $this->creditorRepository = $creditorRepository;
        $this->organizationDetailsRepository = $organizationDetailsRepository;
        $this->requestRepository = $requestRepository;

        parent::__construct($config);
    }

    /**
     * @param Creditor $model
     * @return Creditor|bool
     */
    public function create($model)
    {
        $organization = TblOrganizationsMethods::getOrganization($model->organization_id);

        if ($organization) {
            $params = TblOrganizationsMethods::getParams($model->organization_id);
            $accountSearchModel = new TblAccountsSearch();
            $bankAccounts = $accountSearchModel->search($organization['id'])->query->all();
            $addresses = $this->extractAddressesFromOrganization($organization);

            if ($model->procedures_id == false && ($procedure = AppHelper::getCurrentProcedure())) {
                $model->procedures_id = $procedure->id;
            }

            $creditor = $this->creditorRepository->createFromOrganization($model->getAttributes(), $organization, $params);
            $this->organizationDetailsRepository->createDetails($creditor, $params);
            $this->organizationDetailsRepository->createBankAccounts($creditor, $bankAccounts);
            $this->organizationDetailsRepository->createAddresses($creditor, $addresses);

            return $creditor;
        }

        return false;
    }

    private function extractAddressesFromOrganization($organization)
    {
        $regexp = '/address_(\d+)_/';
        $res = [];

        foreach ($organization as $attribute => $value)
        {
            if (preg_match($regexp, $attribute, $data)) {
                $index = (int) $data[1];
                $key = preg_replace($regexp, '', $attribute);

                if (!isset($res[$index])) {
                    $res[$index] = [];
                }

                $res[$index][$key] = $value;
            }
        }

        return $res;
    }

    /**
     * @param $id integer
     * @return array|null|Creditor
     */
    public function findById($id)
    {
        return $this->creditorRepository
            ->find()
            ->andWhere([
                'creditor.id' => $id,
            ])
            ->with([
                'organizationDetails',
                'organizationDetails.tblParams',
            ])
            ->one();
    }

    /**
     * @return ActiveQuery
     */
    public function getFindQuery()
    {
        return $this->creditorRepository
            ->find()
            ->andFilterWhere([
                'creditor.procedures_id' => @AppHelper::getCurrentProcedure()->id,
            ]);
    }

    /**
     * @param $updating Creditor
     * @return Creditor|boolean
     */
    public function update($updating)
    {
        if ($updating->isJustUpdated()) {
            $new = new Creditor([
                'organization_id' => $updating->organization_id,
                'initiation_time' => new Expression('NOW()'),
                'registry_number' => $updating->update_details_reason ? $updating->registry_number : null,
            ]);
            $new = $this->create($new);

            if ($new->validate() && $new->save()) {
                if ($updating->is_updated) {
                    $reason = $updating->update_reason;
                    $updating->refresh();
                    $updating->is_updated = true;
                    $updating->update_reason = $reason;
                } else {
                    $reason = $updating->update_details_reason;
                    $updating->refresh();
                    $updating->is_details_updated = true;
                    $updating->update_details_reason = $reason;
                }

                $updating->updated_id = $new->id;
                $updating->save();

                if ($updating->is_updated) {
                    $this->updateRequests($this->createUpdatedRequest($updating, $reason), $new);
                } else {
                    $this->updateRequests($updating->requests, $new);
                }

                return $new;
            }
        } else {
            $updating->save();
        }

        return $updating;
    }

    private function createUpdatedRequest($old, $reason)
    {
        $requests = [];

        foreach ($old->requests as $request) {
            $requests[] = $this->requestRepository->createUpdated($request, $reason);
        }

        return $requests;
    }

    private function updateRequests($requests, $creditor)
    {
        if (count($requests)) {
            Request::updateAll([
                'creditor_id' => $creditor->id,
            ], [
                'id' => ArrayHelper::getColumn($requests, 'id'),
            ]);
        }
    }

    /**
     * @param $title
     * @return array
     */
    public function findByTitle($title)
    {
        return $this->creditorRepository
            ->findByTitle($title)
            ->andFilterWhere([
                'procedures_id' => @AppHelper::getCurrentProcedure()->id,
            ])
            ->andWhere([
                'OR',
                ['IS', 'is_updated', null],
                ['is_updated' => false]
            ])
            ->andWhere([
                'OR',
                ['IS', 'is_details_updated', null],
                ['is_details_updated' => false]
            ])
            ->all();
    }

    /**
     * @param $model Creditor
     */
    public function delete($model)
    {
        $this->deleteData($model);

        foreach ($model->requests as $request) {
            $this->requestRepository->delete($request);
        }

        $model->delete();
    }

    /**
     * @param $model
     */
    public function deleteData($model)
    {
        foreach (array_merge($model->organizationDetails, $model->addresses, $model->bankAccounts) as $child) {
            $child->delete();
        }
    }

    public function updateDetails($model)
    {
        $organization = TblOrganizationsMethods::getOrganization($model->organization_id);

        if ($organization) {
            $this->deleteData($model);
            $params = TblOrganizationsMethods::getParams($model->organization_id);
            $accountSearchModel = new TblAccountsSearch();
            $bankAccounts = $accountSearchModel->search($organization['id'])->query->all();
            $addresses = $this->extractAddressesFromOrganization($organization);

            $this->organizationDetailsRepository->createDetails($model, $params);
            $this->organizationDetailsRepository->createBankAccounts($model, $bankAccounts);
            $this->organizationDetailsRepository->createAddresses($model, $addresses);
        }

        return false;
    }
}