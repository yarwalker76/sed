<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/28/17
 * Time: 12:54 AM
 */

namespace app\modules\creditors\services;


use app\models\AppHelper;
use app\modules\creditors\behaviors\UpdateBehavior;
use app\modules\creditors\models\Creditor;
use app\modules\creditors\models\forms\RequestForm;
use app\modules\creditors\models\Request;
use app\modules\creditors\repositories\RequestRepository;
use yii\base\Object;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class RequestService extends Object
{
    /**
     * @var RequestRepository
     */
    private $requestRepository;

    /**
     * RequestService constructor.
     * @param RequestRepository $requestRepository
     * @param array $config
     */
    public function __construct(RequestRepository $requestRepository, array $config = [])
    {
        $this->requestRepository = $requestRepository;

        parent::__construct($config);
    }

    /**
     * @param $form RequestForm
     * @return Request|boolean
     */
    public function createFromForm($form)
    {
        $model = $this->buildFromForm($form);

        if ($model->validate() && $model->save()) {
            return $model;
        }

        return false;
    }

    /**
     * @param $form RequestForm
     * @return Request
     */
    protected function buildFromForm($form)
    {
        $formAttributes = $form->getAttributes(null, [
            'linkedTitle',
            'linkedId',
            'is_disabled',
        ]);

        if ($form->isNewRecord) {
            $model = new Request($formAttributes);
        } else {
            $model = Request::findOne($form->id);
            $model->setAttributes($formAttributes);
        }

        if ($model && $form->is_disabled) {
            $model->is_enabled = false;
        }

        return $model;
    }

    /**
     * @param $form RequestForm
     * @return Request
     */
    public function update($form)
    {
        $updating = $this->buildFromForm($form);

        if ($form->isJustUpdated()) {
            return $this->requestRepository->createUpdated($updating, $updating->update_reason);
        } else {
            $updating->save();
        }

        return $updating;
    }

    /**
     * @return ActiveQuery
     */
    public function getRequestQuery()
    {
        return $this->requestRepository
            ->getQuery()
            ->andWhere([
                'creditor.procedures_id' => @AppHelper::getCurrentProcedure()->id,
            ]);
    }

    /**
     * @param $requests array
     * @param $mapping array
     * @return array
     */
    public function getMappedModels($requests, $mapping)
    {
        $ordered = [];
        $unordered = ArrayHelper::index($requests, null, 'queue_type');

        foreach ($mapping as $type => $name) {
            if (isset($unordered[$type])) {
                $ordered[$type] = $unordered[$type];
            }
        }

        return $ordered;
    }

    public function getTotal($requests)
    {
        return array_sum(ArrayHelper::getColumn($requests, function ($request) {
            return $request->calculateTotal();
        }));
    }

    public function getThirdQueueDropdown()
    {
        $requests = $this->getRequestQuery()
            ->andWhere([
                'creditor_request.queue_type' => [
                    Request::QUEUE_TYPE_THIRD_GUARANTEE,
                    Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE,
                ],
                'creditor_request.is_enabled' => true,
            ])
            ->andWhere([
                'OR',
                ['IS', 'creditor_request.is_updated', null],
                ['creditor_request.is_updated' => false]
            ])
            ->all();

        return ArrayHelper::map($requests, 'id', function ($model) {
            return sprintf('№%d - %s - %s - %s р.', $model->id, Request::getQueueDropdown()[$model->queue_type], $model->creditor->title, $model->total);
        });
    }

    /**
     * @param $id integer
     * @return mixed
     */
    public function getRequest($id)
    {
        return $this->getRequestQuery()
            ->where([
                'creditor_request.id' => $id
            ])
            ->one();
    }

    /**
     * @param $model Request
     */
    public function delete($model)
    {
        $this->requestRepository->delete($model);
    }
}