<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/10/17
 * Time: 11:35 PM
 */

namespace app\modules\creditors\services;


use app\models\AppHelper;
use app\modules\creditors\helpers\TablePrintHelper;
use app\modules\creditors\models\forms\PrintForm;
use app\modules\creditors\models\PrintTable;
use app\modules\creditors\models\Request;
use app\modules\creditors\repositories\TemplateVariablesRepository;
use app\modules\printdocs\models\DocTemplates;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use yii\base\Object;
use Yii;
use app\modules\creditors\repositories\PrintTableRepository;
use yii\helpers\ArrayHelper;

class PrintService extends Object
{
    const CREDITOR_GROUP_ID = 7;

    /**
     * @var PrintTableRepository
     */
    private $reportTableRepository;

    /**
     * @var TemplateVariablesRepository
     */
    private $templateVariablesRepository;

    /**
     * PrintService constructor.
     * @param PrintTableRepository $reportTableRepository
     * @param TemplateVariablesRepository $templateVariablesRepository
     * @param array $config
     */
    public function __construct(PrintTableRepository $reportTableRepository, TemplateVariablesRepository $templateVariablesRepository, array $config = [])
    {
        $this->reportTableRepository = $reportTableRepository;
        $this->templateVariablesRepository = $templateVariablesRepository;

        parent::__construct($config);
    }

    /**
     * @param PrintForm $queueForm
     * @return string
     */
    public function buildReport($queueForm)
    {
        Settings::setTempDir($this->getTempDir());
        $templateProcessor = new TemplateProcessor($this->getTemplate()->path);
        $this->templateVariablesRepository->template = $templateProcessor;
        $this->templateVariablesRepository->setCommon();
        $this->reportTableRepository->procedures_id = AppHelper::getCurrentProcedure()->id;

        foreach ($queueForm->queues as $queue) {
            $tables = $this->reportTableRepository->getTables($queue);
            $tables = ArrayHelper::index($tables, 'type');
            $this->templateVariablesRepository->executeQueueBaseReplaces(
                $queue,
                $tables[PrintTable::TYPE_CREDITOR]->models,
                $tables[PrintTable::TYPE_REQUEST]->models,
                $tables[PrintTable::TYPE_AMORTIZATION]->models
            );
            foreach ($tables as $table) {
                $this->templateVariablesRepository->buildTable($table);
            }
        }

        $path = $this->getTempPath('.docx');

        $templateProcessor->saveAs($path);

        return $path;
    }

    /**
     * @param $section mixed
     * @param $tables
     */
    public function populateTables($section, $tables)
    {
        foreach ($tables as $tableRows) {
            if (count($tableRows)) {
                TablePrintHelper::populateTable($section, $tableRows);
            }
        }
    }

    /**
     * @param string $extension
     * @return string
     */
    public function getTempPath($extension = '.docx')
    {
        return $this->getTempDir() . md5(uniqid()) . $extension;
    }

    /**
     * @return string
     */
    public function getTempDir()
    {
        return sprintf('%s/web/uploads/templates/', Yii::getAlias('@app'));
    }

    /**
     * @return null|DocTemplates
     */
    public function getTemplate()
    {
        return DocTemplates::find()
            ->where(['group_id' => self::CREDITOR_GROUP_ID])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }
}