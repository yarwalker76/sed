<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/26/17
 * Time: 6:03 PM
 */

namespace app\modules\creditors\behaviors;

use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Expression;
use Yii;

/**
 * Class UpdateBehavior
 * @package app\modules\creditors\behaviors
 * @property Model $owner
 */
class UpdateBehavior extends \yii\base\Behavior
{
    const SCENARIO_UPDATE = 'update';

    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        parent::attach($owner);

        $this->owner->on(ActiveRecord::EVENT_AFTER_FIND, [$this, 'initUpdatedParam']);
    }

    public function initUpdatedParam()
    {
        $this->owner->is_updated = $this->isUpdated();
    }

    public function isJustUpdated()
    {
        if (@$this->owner->oldAttributes['is_updated'] == false && $this->owner->is_updated) {
            return true;
        }

        return false;
    }

    public function checkUpdated()
    {
        if ($this->isJustUpdated()) {
            $this->owner->setScenario(self::SCENARIO_UPDATE);
        }
    }

    public function isUpdated()
    {
        return $this->owner->is_updated;
    }

    public function events()
    {
        return [
            Model::EVENT_AFTER_VALIDATE => 'beforeValidate',
        ];
    }

    public function validateUpdatedId()
    {
        if ($this->owner->updated_id) {
            return (bool) $this->owner->findOne($this->owner->updated_id);
        }
    }

    public function beforeValidate()
    {
        if ($this->isJustUpdated()) {
            $this->owner->update_time = new Expression('NOW()');
            $this->owner->update_initiator = (string) Yii::$app->user->id;
        }
    }

    public static function getIgnoredAttributes()
    {
        return [
            'id',
            'is_updated',
            'update_reason',
            'update_time',
            'update_initiator',
        ];
    }

    public static function getRules()
    {
        return [['update_reason', 'update_time', 'update_initiator'], 'required', 'on' => self::SCENARIO_UPDATE];
    }

    /**
     * @param $old ActiveRecord
     * @param $reason string
     * @return mixed
     */
    public static function createUpdated($old, $reason)
    {
        $new = Yii::createObject($old::className(), [
            $old->getAttributes(null, self::getIgnoredAttributes())
        ]);
        $new->id = null;

        if ($new->validate() && $new->save()) {
            $old->refresh();

            $old->is_updated = true;
            $old->update_reason = $reason;
            $old->updated_id = $new->id;
            $old->save();

            return $new;
        }

        return $new;
    }

    public function getUpdated()
    {
        return $this->owner->hasOne($this->owner->className(), ['id' => 'updated_id']);
    }
}