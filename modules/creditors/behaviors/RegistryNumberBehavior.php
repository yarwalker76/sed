<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/2/17
 * Time: 2:18 PM
 */

namespace app\modules\creditors\behaviors;


use app\models\AppHelper;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use Yii;

class RegistryNumberBehavior extends Behavior
{
    /**
     * @var array
     */
    static $incrementNumbers = [];

    public function attach($owner)
    {
        parent::attach($owner);

        $this->initIncrementNumbers();
    }

    protected function initIncrementNumbers()
    {
        if ($this->owner->isNewRecord) {
            if ($this->isCanSetAttribute('registry_number')) {
                $this->owner->registry_number = self::getNextRegistryNumber($this->owner);
            }
        }
    }

    private function isCanSetAttribute($attribute)
    {
        $attributes = $this->owner->getAttributes();

        if (array_key_exists($attribute, $attributes) && empty($attributes[$attribute])) {
            return true;
        }

        return false;
    }

    /**
     * @param $model ActiveRecord
     * @return false|null|string
     */
    public static function getNextRegistryNumber($model)
    {
        $table = $model->tableName();
        $attribute = 'registry_number';

        if (!isset(self::$incrementNumbers[$table][$attribute])) {
            $query = $model->find();

            if ($model->hasMethod('getCreditor')) {
                $query->leftJoin('creditor', 'creditor.id = creditor_id');
            } elseif ($model->hasMethod('getRequest')) {
                $query->leftJoin('creditor_request', 'creditor_request.id = creditor_request_id');
                $query->leftJoin('creditor', 'creditor.id = creditor_id');
            }

            $query->andWhere([
                'creditor.procedures_id' => AppHelper::getCurrentProcedure()->id,
            ]);

            self::$incrementNumbers[$table][$attribute] = $query->max(sprintf('%s.%s', $table, $attribute)) + 1;
        }

        return self::$incrementNumbers[$table][$attribute];
    }
}