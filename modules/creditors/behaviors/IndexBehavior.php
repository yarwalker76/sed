<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 5/5/17
 * Time: 10:46 PM
 */

namespace app\modules\creditors\behaviors;


use app\models\AppHelper;
use app\modules\creditors\models\Request;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveQuery;
use yii\db\Expression;

class IndexBehavior extends Behavior
{
    public function getIndex()
    {
        if ($this->owner->isNewRecord) {
            return $this->calculateNextIndex();
        } elseif ($this->owner->isRelationPopulated('contextIndexedList')) {
            return $this->owner->contextIndexedList['index'];
        } else {
            return $this->owner->indexedList['index'];
        }
    }

    public function getUpdatedIndex()
    {
        if ($this->owner->isUpdated() && $this->owner->updated) {
            return $this->owner->updated->index;
        }

        return $this->getNextIndex();
    }

    public function getNextIndex()
    {
        return $this->calculateNextIndex();
    }

    public function getIndexedList()
    {
        return $this->getIndexedListQuery(false);
    }

    public function getContextIndexedList()
    {
        return $this->getIndexedListQuery(true);
    }

    protected function getIndexedListQuery($context)
    {
        if ($this->owner->className() === Request::className()) {
            return $this->getRequestQuery($context);
        }

        /** @var ActiveQuery $query */
        $query = $this->owner->find();

        if ($context) {
            $query->select([
                'owner.id',
                new Expression('@rowNumber := @rowNumber + 1 `index`')
            ]);
            $query->from([
                $this->owner->tableName() . ' AS owner',
                '(SELECT @rowNumber:=0) r'
            ]);
            $query->orderBy = [
                'owner.id' => SORT_ASC,
            ];
        } else {
            $joined = $this->getNonContextQuery();
            $joined->select([
                'joined.id',
                new Expression('@rowNumber := @rowNumber + 1 `index`')
            ]);
            $joined->from([
                $this->owner->tableName() . ' AS joined',
                '(SELECT @rowNumber:=0) r'
            ]);
            $joined->orderBy = [
                'joined.id' => SORT_ASC,
            ];

            $query->select([
                'owner.id',
                'owner.index',
            ]);
            $query->from([
                'owner' => $joined,
            ]);
        }

        $query->primaryModel = $this->owner;
        $query->link = [
            'id' => 'id',
        ];
        $query->multiple = false;
        $query->asArray = true;

        return $query;
    }

    protected function getRequestQuery($context = false)
    {
        /** @var ActiveQuery $query */
        $query = $this->owner->find();
        $query->select([
            'id',
            new Expression($this->getRequestExpressionSql())
        ]);
        $query->from([
            $this->owner->tableName() . ' AS owner',
            '(SELECT @rowNumber:=0) r',
            '(SELECT @queueNumber:=0) e'
        ]);
        $query->link = [
            'id' => 'id',
        ];
        $query->orderBy = [
            'owner.queue_type' => SORT_ASC,
            'owner.id' => SORT_ASC,
        ];
        $query->multiple = false;
        $query->asArray = true;

        return $query;
    }

    protected function getRequestExpressionSql()
    {
        return <<<SQL
( 
    CASE queue_type 
    WHEN @queueNumber 
    THEN @rowNumber := @rowNumber + 1 
    ELSE @rowNumber := 1 AND @queueNumber := queue_type END
) AS `index`
SQL;
    }


    /**
     * @return false|null|string
     */
    public function calculateNextIndex()
    {
        return $this->getNonContextQuery()->count() + 1;
    }

    protected function getNonContextQuery()
    {
        $query = $this->owner->find();

        if ($this->owner->hasMethod('getRequest')) {
            $query->andWhere([
                'request_id' => $this->owner->request_id,
            ]);
        } elseif ($this->owner->hasMethod('getCreditor')) {
            $query->andWhere([
                'queue_type' => $this->owner->queue_type,
            ]);
        } else {
            $query->andWhere([
                'procedures_id' => AppHelper::getCurrentProcedure()->id,
            ]);
        }

        return $query;
    }
}