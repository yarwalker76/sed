<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/19/17
 * Time: 10:55 PM
 */

namespace app\modules\creditors\behaviors;


use yii\behaviors\AttributeTypecastBehavior;

class TimecastBehavior extends AttributeTypecastBehavior
{
    public $attributeTypes = [];

    public $typecastBeforeSave = true;

    public $typecastAfterFind = false;

    public $typecastAfterValidate = false;

    private $possibleAttributes = [
        'create_time',
        'initiation_time',
        'enable_time',
        'disable_time',
    ];

    public function attach($owner)
    {
        parent::attach($owner);

        $this->initTimeAttributes();
    }

    public function castTime($value)
    {
        if (is_string($value)) {
            $date = new \DateTime();
            $date->setTimestamp(strtotime($value));
            return $date->format('Y-m-d H:i:s');
        }

        return $value;
    }

    protected function initTimeAttributes()
    {
        foreach ($this->possibleAttributes as $attribute) {
            if ($this->owner->canSetProperty($attribute)) {
                $this->attributeTypes[$attribute] = [$this, 'castTime'];
            }
        }
    }
}