<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/11/17
 * Time: 12:18 AM
 */

namespace app\modules\creditors\helpers;

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Element\Table;

class TablePrintHelper
{
    /**
     * @param $section mixed
     * @param $tableRows array
     */
    public static function populateTable($section, $tableRows)
    {
        $table = $section->addTable();
        $heading = array_shift($tableRows);
        self::pasteHeading($table, $heading);
        foreach ($tableRows as $row) {
            self::pasteRow($table, $row);
        }
        $section->addText('');
    }

    /**
     * @param Table $table
     * @param $cells
     */
    public static function pasteHeading($table, $cells)
    {
        $table->addRow();

        foreach ($cells as $value) {
            $table
                ->addCell()
                ->addText($value, [
                    'bold' => true,
                ]);
        }
    }

    /**
     * @param Table $table
     * @param $cells
     */
    public function pasteRow($table, $cells)
    {
        $table->addRow();

        foreach ($cells as $value) {
            $table
                ->addCell()
                ->addText($value);
        }
    }
}