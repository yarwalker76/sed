<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/26/17
 * Time: 1:02 PM
 */

namespace app\modules\creditors;

use NumberFormatter;
use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\creditors\controllers';

    public function init()
    {
        parent::init();

        Yii::$app->formatter->dateFormat = 'dd.MM.yyyy';
        Yii::$app->formatter->numberFormatterOptions[NumberFormatter::MAX_FRACTION_DIGITS] = 2;
        Yii::$app->formatter->numberFormatterOptions[NumberFormatter::MIN_FRACTION_DIGITS] = 2;
        Yii::$app->formatter->decimalSeparator = '.';

        $this->registerTranslations();
    }

    public function registerTranslations() {
        if (!isset(Yii::$app->i18n->translations['creditors'])) {
            Yii::$app->i18n->translations['creditors'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/modules/creditors/messages',
                'sourceLanguage' => Yii::$app->language,
                'forceTranslation' => true,
            ];
        }
    }}