<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/30/17
 * Time: 1:26 AM
 */

namespace app\modules\creditors\widgets;

use yii\base\Widget;

class GuaranteeForm extends Widget
{
    public $model;

    public $formOptions = [];

    public function run()
    {
        return $this->render('guarantee-form', [
            'model' => $this->model,
            'formOptions' => $this->formOptions,
        ]);
    }
}