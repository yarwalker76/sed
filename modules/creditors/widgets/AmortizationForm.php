<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/30/17
 * Time: 1:27 AM
 */

namespace app\modules\creditors\widgets;


use yii\base\Widget;

class AmortizationForm extends Widget
{
    public $model;

    public $formOptions;

    public function run()
    {
        return $this->render('amortization-form', [
            'model' => $this->model,
            'formOptions' => $this->formOptions,
        ]);
    }
}