<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/5/17
 * Time: 11:19 PM
 */

namespace app\modules\creditors\widgets;


use app\modules\creditors\models\CreditorDetails;
use yii\base\Widget;

class CreditorDetailsView extends Widget
{
    /**
     * @var CreditorDetails[]
     */
    public $details;

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('details-view', [
            'attributes' => $this->buildDetailWidgetAttributes(),
        ]);
    }

    /**
     * @return array
     */
    private function buildDetailWidgetAttributes()
    {
        $res = [];

        foreach ($this->details as $detail) {
            if ($detail->tblParams) {
                $res[] = [
                    'label' => $detail->tblParams->title,
                    'value' => $detail->value,
                ];
            }
        }

        return $res;
    }
}