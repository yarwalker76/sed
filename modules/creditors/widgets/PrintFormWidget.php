<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/11/17
 * Time: 12:58 AM
 */

namespace app\modules\creditors\widgets;


use app\modules\creditors\models\forms\PrintForm;
use app\modules\creditors\models\Request;
use yii\base\Widget;

class PrintFormWidget extends Widget
{
    /**
     * @var PrintForm
     */
    public $model;

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('print-form', [
            'model' => $this->model,
            'queues' => Request::getQueueDropdown(),
        ]);
    }
}