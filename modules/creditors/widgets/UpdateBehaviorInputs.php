<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/30/17
 * Time: 11:16 PM
 */

namespace app\modules\creditors\widgets;


use app\modules\creditors\behaviors\RegistryNumberBehavior;
use yii\base\Widget;

class UpdateBehaviorInputs extends Widget
{
    public $model;

    public $form;

    public function run()
    {
        return $this->render('update-behavior-inputs', [
            'model' => $this->model,
            'form' => $this->form,
        ]);
    }
}