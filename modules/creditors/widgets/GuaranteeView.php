<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/30/17
 * Time: 1:49 AM
 */

namespace app\modules\creditors\widgets;

use yii\base\Widget;

class GuaranteeView extends Widget
{
    public $model;

    public function run()
    {
        return $this->render('guarantee-view', [
            'model' => $this->model,
        ]);
    }
}