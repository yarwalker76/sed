<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/9/17
 * Time: 11:22 PM
 */

namespace app\modules\creditors\widgets;


use yii\base\Widget;

class GuaranteeModal extends Widget
{
    public $model;

    public $id = 'create-guarantee';

    public $formOptions = [
        'action' => ['/creditors/guarantee/create'],
    ];

    public function run()
    {
        return $this->render('guarantee-modal', [
            'model' => $this->model,
            'id' => $this->id,
            'formOptions' => $this->formOptions,
        ]);
    }
}