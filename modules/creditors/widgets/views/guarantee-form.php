<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/30/17
 * Time: 1:29 AM
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\creditors\widgets\UpdateBehaviorInputs;
use yii\jui\DatePicker;
?>

<?php $form = ActiveForm::begin($formOptions); ?>
    <?= $form->field($model, 'request_id')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'nextIndex')->textInput([
        'disabled' => true,
    ]) ?>

    <?= $form->field($model, 'create_time')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'options' => [
            'id' => uniqid(),
            'class' => 'form-control',
        ]
    ]) ?>

    <?= $form->field($model, 'total')->textInput([
        'id' => 'guarantee-total',
    ]); ?>

    <?= $form->field($model, 'details') ?>

    <div class="form-group">
        <label for="guarantee-total-ratio"><?= Yii::t('creditors', 'Guarantee Total Ratio') ?></label>
        <input class="form-control" id="guarantee-total-ratio" disabled>
    </div>

    <?= UpdateBehaviorInputs::widget([
        'form' => $form,
        'model' => $model,
    ]) ?>

    <?= Html::submitButton(Yii::t('creditors', $model->isNewRecord ? 'Submit' : 'Update'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>