<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/11/17
 * Time: 12:59 AM
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/creditors/print/report'],
]) ?>
    <?= $form->field($model, 'queues')->checkboxList($queues, [
        'itemOptions' => [
            'disabled' => true,
        ]
    ])->label(false) ?>
    <?= Html::submitButton(Yii::t('creditors', 'Submit'), ['class' => 'btn btn-primary']); ?>`
<?php ActiveForm::end() ?>
