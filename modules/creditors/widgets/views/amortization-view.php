<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/31/17
 * Time: 12:39 AM
 */

use yii\widgets\DetailView;
use app\modules\creditors\widgets\AmortizationModal;
use yii\helpers\Html;


?>

<?= DetailView::widget([
    'model' => $model,
    'options' => [
        'class' => 'table table-bordered detail-view' . ($model->is_updated ? ' bg-danger' : ''),
    ],
    'attributes' => [
        'initiation_time',
        'total:decimal',
        'total_payed:decimal',
        'totalRatio:decimal',
        'details',
        [
            'attribute' => '',
            'format' => 'raw',
            'value' => Yii::$app->user->can('manager')  || Yii::$app->user->can('administrator') ? (
                    Html::a('<i class="glyphicon glyphicon-pencil"></i>', '#', [
                        'data-target' => '#amortization-' . $model->id,
                        'data-toggle' => 'modal',
                    ]) 
                    .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', ['/creditors/amortization/delete', 'id' => $model->id])
            ) : '',
        ]
    ],
]) ?>

<?= AmortizationModal::widget([
    'model' => $model,
    'id' => 'amortization-' . $model->id,
    'formOptions' => [
        'action' => ['/creditors/amortization/update', 'id' => $model->id],
    ]
]) ?>

