<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/30/17
 * Time: 1:49 AM
 */

use yii\widgets\DetailView;
use yii\helpers\Html;
use app\modules\creditors\widgets\GuaranteeModal;
?>

<?= DetailView::widget([
    'model' => $model,
    'options' => [
        'class' => 'table table-bordered detail-view' . ($model->is_updated ? ' bg-danger' : ''),
    ],
    'attributes' => [
        'total:decimal',
        'details',
        'totalRatio:decimal',
        [
            'attribute' => '',
            'format' => 'raw',
            'value' => Yii::$app->user->can('manager') || Yii::$app->user->can('administrator') ? (
                Html::a('<i class="glyphicon glyphicon-pencil"></i>', '#', [
                    'data-target' => '#guarantee-' . $model->id,
                    'data-toggle' => 'modal',
                ])
                .
                Html::a('<i class="glyphicon glyphicon-trash"></i>', ['/creditors/guarantee/delete', 'id' => $model->id])
            ) : '',
        ]
    ],
]) ?>

<?= GuaranteeModal::widget([
    'model' => $model,
    'id' => 'guarantee-' . $model->id,
    'formOptions' => [
        'action' => ['/creditors/guarantee/update', 'id' => $model->id],
    ]
]) ?>

