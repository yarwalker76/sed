<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/30/17
 * Time: 11:17 PM
 */

?>

<?php if ($model->isNewRecord == false): ?>
    <?= $form->field($model, 'is_updated')->checkbox([
        'id' => 'update-initiation',
    ]) ?>

    <div id="update-container" style="<?= $model->isUpdated() ? '' : 'display: none;' ?>">
        <?= $form->field($model, 'updatedIndex')->textInput([
            'disabled' => true,
        ]) ?>

        <?= $form->field($model, 'update_reason') ?>
    </div>
<?php endif; ?>