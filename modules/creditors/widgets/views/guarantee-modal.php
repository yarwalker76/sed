<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/9/17
 * Time: 11:23 PM
 */

use app\modules\creditors\widgets\GuaranteeForm;

?>

<div class="modal fade" id="<?= $id ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= Yii::t('creditors', $model->isNewRecord ? 'Create guarantee' : 'Update guarantee') ?></h4>
            </div>
            <div class="modal-body">
                <?= GuaranteeForm::widget([
                    'model' => $model,
                    'formOptions' => $formOptions,
                ]) ?>
            </div>
        </div>
    </div>
</div>

