<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/28/17
 * Time: 12:57 AM
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('creditors', 'Create Request');

$this->params['breadcrumbs'][] = ['label' => Yii::t('creditors', 'Requests'), 'url' => '/creditors/request'];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>

<?php $form = ActiveForm::begin() ?>

    <?= $this->render('_form', [
        'form' => $form,
        'model' => $model,
        'typeDropdown' => $typeDropdown,
        'queueDropdown' => $queueDropdown,
        'thirdQueueDropdown' => $thirdQueueDropdown,
    ]) ?>


    <?= Html::submitButton(Yii::t('creditors', 'Submit'), [
        'class' => 'btn btn-default',
    ]); ?>

<?php ActiveForm::end() ?>


<?php
    $this->registerJsFile('js/creditors.js', ['depends' => 'yii\web\JqueryAsset']);
    $this->registerJsFile('js/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset']);
    $this->registerJsFile('js/creditors_autocomplete.js', ['depends' => 'yii\web\JqueryAsset']);
?>
