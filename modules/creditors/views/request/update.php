<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/29/17
 * Time: 12:16 AM
 */


use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\creditors\widgets\GuaranteeView;
use app\modules\creditors\widgets\GuaranteeModal;
use app\modules\creditors\widgets\AmortizationView;
use app\modules\creditors\widgets\AmortizationModal;
use app\modules\creditors\widgets\UpdateBehaviorInputs;

$this->title = sprintf('%s #%s', Yii::t('creditors', 'Uodate'), $model->id);

$this->params['breadcrumbs'][] = ['label' => Yii::t('creditors', 'Requests'), 'url' => '/creditors/request'];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>

<ul class="nav nav-tabs creditor-request__tabs">
    <li class="active"><a data-toggle="tab" href="#home"><?= Yii::t('creditors', 'Request') ?></a></li>
    <?php if ($model->isWithGuarantee()): ?>
        <li id="guarantee-tab"><a data-toggle="tab" href="#guarantee"><?= Yii::t('creditors', 'Guarantees') ?></a></li>
    <?php endif; ?>
    <li><a data-toggle="tab" href="#amortization"><?= Yii::t('creditors', 'Amortization') ?></a></li>
</ul>

<div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        <?php $form = ActiveForm::begin() ?>

            <?= $this->render('_form', [
                'form' => $form,
                'model' => $model,
                'typeDropdown' => $typeDropdown,
                'queueDropdown' => $queueDropdown,
                'thirdQueueDropdown' => $thirdQueueDropdown,
            ]) ?>

            <?= UpdateBehaviorInputs::widget([
                'form' => $form,
                'model' => $model,
            ]) ?>

        <?= Html::submitButton(Yii::t('creditors', 'Save'), [
            'class' => 'btn btn-default',
        ]); ?>

        <?php ActiveForm::end() ?>
    </div>
    <div id="guarantee" class="tab-pane fade">
        <?php foreach ($model->guarantees as $item): ?>
            <?= GuaranteeView::widget([
                'model' => $item,
            ]) ?>
        <?php endforeach; ?>

        <?= GuaranteeModal::widget([
            'model' => $guarantee,
        ]); ?>

        <button class="btn btn-default" data-toggle="modal" data-target="#create-guarantee"><i class="glyphicon glyphicon-plus-sign"></i></button>
    </div>
    <div id="amortization" class="tab-pane fade">
        <?php foreach ($model->amortizations as $item): ?>
            <?= AmortizationView::widget([
                'model' => $item,
            ]) ?>
        <?php endforeach; ?>

        <?= AmortizationModal::widget([
            'model' => $amortization,
        ]) ?>

        <button class="btn btn-default" data-toggle="modal" data-target="#create-amortization"><i class="glyphicon glyphicon-plus-sign"></i></button>
    </div>
</div>

<?php
    $this->registerJsFile('js/creditors.js', ['depends' => 'yii\web\JqueryAsset']);
    $this->registerJsFile('js/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset']);
    $this->registerJsFile('js/creditors_autocomplete.js', ['depends' => 'yii\web\JqueryAsset']);
?>

