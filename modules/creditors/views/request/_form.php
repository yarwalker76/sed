<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/29/17
 * Time: 12:15 AM
 */

use yii\jui\DatePicker;
use app\modules\creditors\models\Request;
?>

<?= $form->field($model, 'index')->textInput([
    'disabled' => true,
]) ?>

<?= $form->field($model, 'queue_type')->dropDownList($queueDropdown, [
    'id' => 'queue-dropdown',
    'prompt' => Yii::t('creditors', 'Select queue type'),
]) ?>

<div id="creditor-select-params" style="<?= ($model->isNewRecord || $model->isUpdated()) ?: 'display:none;' ?>">
    <div id="find-creditor-by-title-container">
        <?= $form->field($model, 'linkedTitle')->textInput([
            'id' => 'find-creditor-by-title',
        ]); ?>
    </div>

    <div id="find-creditor-by-request-container" style="display: none;">
        <?= $form->field($model, 'linkedTitle')->dropDownList($thirdQueueDropdown, [
            'prompt' => Yii::t('creditors', 'Select request'),
            'id' => 'creditor-request-dropdown',
        ])->label(Yii::t('creditors', 'Request')); ?>
    </div>

    <?= $form->field($model, 'linkedId')->hiddenInput([
        'id' => 'found-creditor'
    ])->label(false) ?>
</div>
<?php if ($model->isNewRecord == false && $model->isUpdated() == false): ?>
    <div id="creditor-current-params">
        <?= $form->field($model, 'creditorTitle')->textInput([
            'value' => $model->creditor->title,
            'disabled' => true,
        ])->label(Yii::t('creditors', 'Title')); ?>
    </div>
<?php endif; ?>

<?= $form->field($model, 'type_id')->dropDownList($typeDropdown, [
    'id' => 'request-type',
    'prompt' => Yii::t('creditors', 'Select type'),
    'style' => $model->queue_type === Request::QUEUE_TYPE_PERCENTS ? 'display:none;' : '',
]) ?>

<?= $form->field($model, 'registry_number') ?>

<?= $form->field($model, 'total')->textInput([
    'id' => 'total',
]) ?>

<div id="requirement-fields" style="<?= $model->queue_type === Request::QUEUE_TYPE_PERCENTS ?: 'display:none;' ?>">
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'percentage')->textInput([
                'id' => 'requirement-percentage',
            ]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'day_count')->textInput([
                'id' => 'requirement-day-count',
            ]) ?>
        </div>
        <div class="col-sm-4" style="padding-top: 20px;">
            <button id="calculate-percents" class="btn btn-default">
                <?= Yii::t('creditors', 'Calculate Percents') ?>
            </button>
        </div>
    </div>

    <?= $form->field($model, 'total_accrued')->textInput([
        'id' => 'requirement-total-accrued',
        'readonly' => true,
    ]) ?>
</div>



<?= $form->field($model, 'initiation_time')->widget(DatePicker::classname(), [
    'language' => 'ru',
    'options' => [
        'class' => 'form-control',
    ]
]) ?>

<?= $form->field($model, 'details')->textarea() ?>

<?= $form->field($model, 'is_enabled')->checkbox([
    'id' => 'enable-toggle',
    'disabled' => $model->is_disabled,
]) ?>

<div id="enabled-block" style="<?= $model->is_enabled ? '' : 'display: none;' ?>">
    <?= $form->field($model, 'enable_time')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ]
    ]) ?>
    <?= $form->field($model, 'enable_reason') ?>
</div>

<?= $form->field($model, 'is_disabled')->checkbox([
    'id' => 'disable-toggle',
    'disabled' => ($model->is_enabled === null),
]) ?>

<div id="disabled-block" style="<?= $model->is_disabled ? '' : 'display: none;' ?>">
    <?= $form->field($model, 'disable_time')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'options' => [
            'class' => 'form-control',
        ],
    ]) ?>
    <?= $form->field($model, 'disable_reason') ?>
</div>

