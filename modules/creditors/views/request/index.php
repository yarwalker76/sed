<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/29/17
 * Time: 9:51 PM
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>

<h1><?= Yii::t('creditors', 'Requests') ?></h1>

<div>
    <?= Html::a(Yii::t('creditors', 'Create Request'), ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('creditors', 'Print Requests'), ['/creditors/print/report'], ['class' => 'btn btn-primary']) ?>
    <?php if (Yii::$app->user->can('administrator')): ?>
        <?= Html::a(Yii::t('creditors', 'Request Types'), ['/creditors/request-type'],  ['class' => 'btn btn-default']) ?>
    <?php endif; ?>
</div>
<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th><?= Yii::t('creditors', 'Registry Number') ?></th>
            <th><?= Yii::t('creditors', 'Title') ?></th>
            <th><?= Yii::t('creditors', 'Request Type') ?></th>
            <th><?= Yii::t('creditors', 'Request Total') ?></th>
            <th colspan="2"><?= Yii::t('creditors', 'Initiation Time') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($models as $queueType => $requests): ?>
            <tr>
                <td colspan="5">
                    <span class="h4"><?= Yii::t('creditors', $queueMapping[$queueType]) ?> - <?= array_sum(ArrayHelper::getColumn($requests, function ($model) {
                        return $model->calculateTotal();
                    }))?> <?= Yii::t('creditors', 'rub') ?>.</span>
                </td>
            </tr>
            <?php foreach ($requests as $request): ?>
            <tr class="<?= $request->is_enabled ?: 'bg-warning' ?> <?= $request->isUpdated() ? 'bg-danger' : '' ?>">
                <td><?= $request->index ?></td>
                <td>
                    <?= $request->registry_number ?>
                </td>
                <td>
                    <strong><?= $request->creditor->title ?></strong>
                </td>
                <td>
                    <?= $request->requestType ? $request->requestType->title : '' ?>
                </td>
                <td>
                    <?= Yii::$app->formatter->asDecimal($request->getTotal()) ?>
                </td>
                <td>
                    <?= Yii::$app->formatter->asDate($request->initiation_time) ?>
                </td>
                <td>
                    <?= Html::a('<i class="glyphicon glyphicon-eye-open"></i>', ['view', 'id' => $request->id]) ?>
                    <?php if (Yii::$app->user->can('manager') || Yii::$app->user->can('administrator')): ?>
                        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $request->id]) ?>
                        <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $request->id], [
                            'data-confirm' => "Вы уверены, что хотите удалить этот элемент?"
                        ]) ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </tbody>
</table>
<span class="h3"><?= Yii::t('creditors', 'Total') ?> - <?= $total ?> <?= Yii::t('creditors', 'rub') ?>.</span>
