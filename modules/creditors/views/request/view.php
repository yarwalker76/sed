<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/29/17
 * Time: 12:54 AM
 */

use app\modules\creditors\models\Request;
use app\modules\creditors\widgets\GuaranteeView;
use app\modules\creditors\widgets\AmortizationView;
use app\modules\creditors\widgets\GuaranteeModal;
use app\modules\creditors\widgets\AmortizationModal;
use yii\widgets\DetailView;

$this->title = sprintf('%s #%s', Yii::t('creditors', 'Request'), $model->registry_number);

$this->params['breadcrumbs'][] = ['label' => Yii::t('creditors', 'Requests'), 'url' => '/creditors/request'];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>

<ul class="nav nav-tabs creditor-request__tabs">
    <li class="active"><a data-toggle="tab" href="#home"><?= Yii::t('creditors', 'Request') ?></a></li>
    <?php if ($model->queue_type === Request::QUEUE_TYPE_THIRD_GUARANTEE): ?>
        <li id="guarantee-tab" style="display: none;"><a data-toggle="tab" href="#guarantee"><?= Yii::t('creditors', 'Guarantees') ?></a></li>
    <?php endif; ?>
    <li><a data-toggle="tab" href="#amortization"><?= Yii::t('creditors', 'Amortization') ?></a></li>
</ul>

<div class="tab-content">
    <div id="home" class="tab-pane fade in active">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => array_merge([
                'index',
                'create_time',
                'initiation_time',
                [
                    'attribute' => 'type_id',
                    'value' => @$typeMapping[$model->type_id],
                ],
                [
                    'attribute' => 'queue_type',
                    'value' => @$queueMapping[$model->queue_type],
                ],
                'creditor.title',
                'registry_number',
                'total',
            ], ($model->queue_type === Request::QUEUE_TYPE_PERCENTS) ? [
                'percentage',
                'day_count',
                'total_accrued',
            ] : [], [
                'details',
                'is_enabled:boolean',
                'enable_time',
                'enable_reason',
                'disable_time',
                'disable_reason',
            ]),
        ]) ?>
    </div>
    <?php if ($model->queue_type === Request::QUEUE_TYPE_THIRD_GUARANTEE): ?>
        <div id="guarantee" class="tab-pane fade">
            <?php foreach ($model->guarantees as $item): ?>
                <?= GuaranteeView::widget([
                    'model' => $item,
                ]) ?>
            <?php endforeach; ?>

            <?= GuaranteeModal::widget([
                'model' => $guarantee,
            ]); ?>

            <button class="btn btn-default" data-toggle="modal" data-target="#create-guarantee"><i class="glyphicon glyphicon-plus-sign"></i></button>
        </div>
    <?php endif; ?>
    <div id="amortization" class="tab-pane fade">
        <?php foreach ($model->amortizations as $item): ?>
            <?= AmortizationView::widget([
                'model' => $item,
            ]) ?>
        <?php endforeach; ?>

        <?= AmortizationModal::widget([
            'model' => $amortization,
        ]) ?>

        <button class="btn btn-default" data-toggle="modal" data-target="#create-amortization"><i class="glyphicon glyphicon-plus-sign"></i></button>
    </div>
</div>

<?php
    $this->registerJsFile('js/creditors.js', ['depends' => 'yii\web\JqueryAsset']);
?>

