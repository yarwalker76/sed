<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/11/17
 * Time: 1:04 AM
 */

use app\modules\creditors\widgets\PrintFormWidget;

$this->title = Yii::t('creditors', 'Print Requests');

$this->params['breadcrumbs'][] = ['label' => Yii::t('creditors', 'Requests'), 'url' => '/creditors/request'];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>

<?= PrintFormWidget::widget([
    'model' => $model
]); ?>

