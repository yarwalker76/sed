<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/27/17
 * Time: 11:26 PM
 */

use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = sprintf('%s #%s', Yii::t('creditors', 'Creditor'), $model->registry_number);

$this->params['breadcrumbs'][] = ['label' => Yii::t('creditors', 'Creditors'), 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;

use app\modules\creditors\widgets\CreditorDetailsView;

?>

<h1><?= Yii::t('creditors', 'Creditor')?>: <?= $model->title ?></h1>
<a href="<?= Url::to(['/creditors/default/update', 'id' => $model->id]) ?>" class="btn btn-default"><i class="glyphicon glyphicon-pencil"></i></a>
<hr />
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'index',
        'title',
        'initiation_time',
        [
            'attribute' => 'organization_id',
            'format' => 'raw',
            'value' => Html::a('#' . $model->organization_id, ['/references/view', 'id' => $model->organization_id]),
        ],
    ],
]) ?>

<h2><?= Yii::t('creditors', 'Details') ?></h2>
<?= CreditorDetailsView::widget([
        'details' => $model->organizationDetails,
]) ?>
<?php if (count($model->addresses)): ?>
    <h2><?= Yii::t('creditors', 'Addresses') ?></h2>
<?php endif; ?>
<div class="row">

    <?php foreach ($model->addresses as $address): ?>
        <div class="col-sm-6">
            <?= DetailView::widget([
                'model' => $address,
                'attributes' => [
                    'postcode',
                    'locality',
                    'address',
                ]
            ]) ?>
        </div>
    <?php endforeach; ?>
</div>
<?php if (count($model->bankAccounts)): ?>
    <h2><?= Yii::t('creditors', 'Bank Accounts') ?></h2>
<?php endif; ?>
<div class="row">
    <?php foreach ($model->bankAccounts as $bankAccount): ?>
        <div class="col-sm-6">
            <?= DetailView::widget([
                'model' => $bankAccount,
                'attributes' => [
                    [
                        'attribute' => 'banks_id',
                        'format' => 'raw',
                        'value' => Html::a('#' . $bankAccount->banks_id, ['/references/view', 'id' => $model->organization_id]),
                    ],
                    'bankTitle',
                    'number',
                    'comment',
                    'is_closed:boolean',
                    'close_time',
                ]
            ]) ?>
        </div>
    <?php endforeach; ?>
</div>
