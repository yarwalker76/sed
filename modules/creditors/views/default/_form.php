<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/29/17
 * Time: 12:12 AM
 */

use yii\jui\DatePicker;
use yii\helpers\Url;

?>

<?= $form->field($model, 'index')->textInput([
    'disabled' => true,
]) ?>

<?= $form->field($model, 'registry_number') ?>

<div class="row">
    <div class="<?= $model->isNewRecord ? 'col-sm-12' : 'col-sm-9' ?>">
        <?= $form->field($model, 'title')->textInput([
            'id' => 'find-organization-by-title',
            'readonly' => !$model->isNewRecord,
        ]); ?>
    </div>
    <?php if ($model->isNewRecord == false): ?>
        <div class="col-sm-3" style="padding-top: 21px;">
            <a href="<?= Url::to(['update-details', 'id' => $model->id]) ?>" class="btn btn-default">Обновить данные</a>
        </div>
    <?php endif; ?>
</div>

<?= $form->field($model, 'organization_id')->textInput([
    'id' => 'found-organization',
    'readonly' => true,
]) ?>

<?= $form->field($model, 'initiation_time')->widget(DatePicker::classname(), [
    'language' => 'ru',
    'options' => [
        'class' => 'form-control',
    ]
]) ?>