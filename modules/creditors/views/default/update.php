<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/28/17
 * Time: 12:19 AM
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\creditors\widgets\UpdateBehaviorInputs;

$this->title = sprintf('%s #%s', Yii::t('creditors', 'Creditor'), $model->registry_number);

$this->params['breadcrumbs'][] = ['label' => Yii::t('creditors', 'Creditors'), 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;

?>

<h1><?= Yii::t('creditors', 'Update Creditor') . ' #' . $model->registry_number ?></h1>

<?php $form = ActiveForm::begin() ?>
    <div class="base-attributes">
        <?= $this->render('_form', [
            'form' => $form,
            'model' => $model,
        ]) ?>

        <?= UpdateBehaviorInputs::widget([
            'form' => $form,
            'model' => $model,
        ]) ?>
    </div>

    <?= $form->field($model, 'is_details_updated')->checkbox([
        'id' => 'update-details-initiation'
    ]) ?>

    <div id="update-details-container" style="<?= $model->is_details_updated ? '' : 'display: none;' ?>">
        <?= $form->field($model, 'update_details_reason') ?>
    </div>

    <?= Html::submitButton(Yii::t('creditors', 'Save'), ['class' => 'btn btn-default']) ?>
<?php ActiveForm::end() ?>

<?php
    $this->registerJsFile( 'js/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset'] );
    $this->registerJsFile( 'js/creditors.js', ['depends' => 'yii\web\JqueryAsset'] );
?>
