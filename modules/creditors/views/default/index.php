<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/27/17
 * Time: 11:44 PM
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\grid\ActionColumn;

$this->title = Yii::t('creditors', 'Creditors');

$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= $this->title ?></h1>

<?= Html::a(Yii::t('creditors', 'Create Creditor'), ['create'], ['class' => 'btn btn-success']) ?>

<?= GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        'index',
        'registry_number',
        'title',
        'initiation_time:date',
        'is_updated:boolean',
        [
            'class' => ActionColumn::className(),
        ],
    ],
    'rowOptions' => function($model) {
        if ($model->is_updated || $model->is_details_updated) {
            return ['class' => 'danger'];
        }

        return [];
    },
]) ?>
