<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/26/17
 * Time: 9:37 PM
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('creditors', 'Create Creditor');

$this->params['breadcrumbs'][] = ['label' => Yii::t('creditors', 'Creditors'), 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;

use app\modules\creditors\widgets\CreditorDetailsView;

?>

<h1><?= $this->title ?></h1>

<?php $form = ActiveForm::begin(); ?>
    <?= $this->render('_form', [
        'form' => $form,
        'model' => $model,
    ]) ?>

    <?= Html::submitButton(Yii::t('creditors', 'Create Creditor'), ['class' => 'btn btn-default']) ?>
<?php ActiveForm::end(); ?>

<?php
    $this->registerJsFile( 'js/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset'] );
    $this->registerJsFile( 'js/creditors.js', ['depends' => 'yii\web\JqueryAsset'] );
?>