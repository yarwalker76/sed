<?php

namespace app\modules\creditors\models;

use app\modules\creditors\behaviors\IndexBehavior;
use app\modules\creditors\behaviors\RegistryNumberBehavior;
use app\modules\creditors\behaviors\TimecastBehavior;
use app\modules\creditors\behaviors\UpdateBehavior;
use Yii;
use yii\behaviors\AttributeTypecastBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "creditor".
 *
 * @property integer $id
 * @property integer $organization_id
 * @property integer $organization_type_id
 * @property string $title
 * @property string $create_time
 * @property string $initiation_time
 * @property integer $is_updated
 * @property string $update_reason
 * @property string $update_initiator
 * @property integer $updated_id
 * @property integer $procedures_id
 * @property boolean $is_details_updated
 * @property string $update_details_reason
 * @property integer $registry_number
 */
class Creditor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'creditor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_id', 'organization_type_id', 'title', 'initiation_time'], 'required'],
            [['organization_id', 'updated_id', 'registry_number', 'id', 'procedures_id'], 'integer'],
            [['updated_id'], 'validateUpdatedId'],
            [['create_time', 'initiation_time'], 'safe'],
            [['initiation_time'], 'default', 'value' => date('Y-M-d')],
            [['update_reason', 'update_details_reason'], 'string', 'max' => 512],
            [['update_initiator', 'title', 'update_details_initiator'], 'string', 'max' => 256],
            [['is_updated', 'is_details_updated'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('creditors', 'ID'),
            'title' => Yii::t('creditors', 'Title'),
            'organization_id' => Yii::t('creditors', 'Organization ID'),
            'create_time' => Yii::t('creditors', 'Create time'),
            'is_updated' => Yii::t('creditors', 'Creditor Change'),
            'update_reason' => Yii::t('creditors', 'Update Reason'),
            'update_initiator' => Yii::t('creditors', 'Update Initiator'),
            'updated_id' => Yii::t('creditors', 'Updated ID'),
            'registry_number' => Yii::t('creditors', 'Registry Number'),
            'initiation_time' => Yii::t('creditors', 'Initiation Time'),
            'is_details_updated' => Yii::t('creditors', 'Creditor Details Update'),
            'update_details_reason' => Yii::t('creditors', 'Update Reason'),
            'index' => Yii::t('creditors', 'ID'),
            'nextIndex' => Yii::t('creditors', 'Updated ID'),
            'updatedIndex' => Yii::t('creditors', 'Updated ID'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            'update' => UpdateBehavior::className(),
            RegistryNumberBehavior::className(),
            TimecastBehavior::className(),
            IndexBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizationDetails()
    {
        return $this->hasMany(CreditorDetails::className(), ['creditor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankAccounts()
    {
        return $this->hasMany(CreditorBankAccount::className(), ['creditor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(CreditorAddress::className(), ['creditor_id' => 'id'])->indexBy('type');
    }

    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['creditor_id' => 'id']);
    }

    public function isJustUpdated()
    {
        return $this->getBehavior('update')->isJustUpdated() || (@$this->oldAttributes['is_details_updated'] == false && $this->is_details_updated);
    }
}