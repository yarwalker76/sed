<?php

namespace app\modules\creditors\models;

use app\modules\creditors\behaviors\IndexBehavior;
use app\modules\creditors\behaviors\RegistryNumberBehavior;
use app\modules\creditors\behaviors\TimecastBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\modules\creditors\behaviors\UpdateBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "creditor_request".
 *
 * @property integer $id
 * @property integer $creditor_id
 * @property integer $registry_number
 * @property integer $queue_type
 * @property integer $request_type_id
 * @property string $create_time
 * @property string $initiation_time
 * @property string $details
 * @property string $total
 * @property integer $is_enabled
 * @property string $enable_reason
 * @property string $disable_reason
 * @property integer $is_updated
 * @property string $update_reason
 * @property string $update_initiator
 * @property integer $updated_id
 * @property string $enable_time
 * @property string $disable_time
 * @property string $update_time
 * @property float $percentage
 * @property float $total_accrued
 */
class Request extends \yii\db\ActiveRecord
{
    const QUEUE_TYPE_FIRST = 1;

    const QUEUE_TYPE_SECOND = 2;

    const QUEUE_TYPE_THIRD_GUARANTEE = 3;

    const QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE = 4;

    const QUEUE_TYPE_PERCENTS = 5;

    const QUEUE_TYPE_PENALTY = 6;

    const QUEUE_TYPE_DEFAULT = 7;

    const SCENARIO_REQUIREMENT = 'requirement';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'creditor_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['total', 'queue_type', 'creditor_id'], 'required'],
            [['creditor_id', 'registry_number', 'queue_type', 'type_id', 'updated_id', 'day_count', 'linked_request_id'], 'integer'],
            [['update_time', 'create_time', 'initiation_time', 'enable_time', 'disable_time'], 'safe'],
            [['total', 'total_accrued', 'percentage'], 'number'],
            [['details', 'enable_reason', 'disable_reason', 'update_reason'], 'string', 'max' => 512],
            [['update_initiator'], 'string', 'max' => 256],
            [['is_enabled', 'is_updated'], 'boolean'],
            [['linked_request_id'], 'required', 'on' => self::SCENARIO_REQUIREMENT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('creditors','ID'),
            'creditor_id' => Yii::t('creditors','Creditor ID'),
            'registry_number' => Yii::t('creditors','Registry Number'),
            'queue_type' => Yii::t('creditors','Queue Type'),
            'type_id' => Yii::t('creditors','Type'),
            'create_time' => Yii::t('creditors','Create Time'),
            'initiation_time' => Yii::t('creditors','Initiation Time'),
            'details' => Yii::t('creditors','Request Details'),
            'total' => Yii::t('creditors','Request Total'),
            'is_enabled' => Yii::t('creditors','Is Enabled'),
            'enable_reason' => Yii::t('creditors','Enable Reason'),
            'enable_time' => Yii::t('creditors','Enable Time'),
            'disable_reason' => Yii::t('creditors','Disable Reason'),
            'disable_time' => Yii::t('creditors','Disable Time'),
            'is_updated' => Yii::t('creditors','Is Updated'),
            'update_reason' => Yii::t('creditors','Update Reason'),
            'update_initiator' => Yii::t('creditors','Update Initiator'),
            'updated_id' => Yii::t('creditors','Updated ID'),
            'linked_request_id' => Yii::t('creditors', 'Linked Request ID'),
            'percentage' => Yii::t('creditors', 'Percentage'),
            'day_count' => Yii::t('creditors', 'Day Count'),
            'total_accrued' => Yii::t('creditors', 'Total Accrued'),
            'index' => Yii::t('creditors', 'ID'),
            'nextIndex' => Yii::t('creditors', 'Updated ID'),
            'updatedIndex' => Yii::t('creditors', 'Updated ID'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            UpdateBehavior::className(),
            RegistryNumberBehavior::className(),
            TimecastBehavior::className(),
            IndexBehavior::className()
        ];
    }

    public static function getQueueDropdown()
    {
        return [
            self::QUEUE_TYPE_FIRST => Yii::t('creditors', 'First Queue'),
            self::QUEUE_TYPE_SECOND => Yii::t('creditors', 'Second Queue'),
            self::QUEUE_TYPE_THIRD_GUARANTEE => Yii::t('creditors', 'Third Queue With Guarantee'),
            self::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE => Yii::t('creditors', 'Third Queue Without Guarantee'),
            self::QUEUE_TYPE_PERCENTS => Yii::t('creditors', 'Third Queue With Percents'),
            self::QUEUE_TYPE_PENALTY => Yii::t('creditors', 'Penalty Queue'),
            self::QUEUE_TYPE_DEFAULT => Yii::t('creditors', 'Behind Queue'),
        ];
    }

    public function getCreditor()
    {
        return $this->hasOne(Creditor::className(), ['id' => 'creditor_id']);
    }

    public function getRequestType()
    {
        return $this->hasOne(RequestType::className(), ['id' => 'type_id']);
    }

    public function getGuarantees()
    {
        return $this->hasMany(Guarantee::className(), ['request_id' => 'id']);
    }

    public function getAmortizations()
    {
        return $this->hasMany(Amortization::className(), ['request_id' => 'id']);
    }

    public function isWithPercentage()
    {
        return $this->queue_type == self::QUEUE_TYPE_PERCENTS;
    }

    public function isWithGuarantee()
    {
        return $this->queue_type == self::QUEUE_TYPE_THIRD_GUARANTEE;
    }

    public function getLinkedRequest()
    {
        return $this->hasOne(self::className(), ['id' => 'linked_request_id']);
    }

    public function beforeValidate()
    {
        if ($this->isWithPercentage()) {
            $this->setScenario(self::SCENARIO_REQUIREMENT);
        }

        return parent::beforeValidate();
    }

    public function calculateAmortizationPayedTotal()
    {
        return array_sum(ArrayHelper::getColumn($this->amortizations,'total_payed'));
    }

    public function isDisabled()
    {
        return ($this->is_enabled !== null && $this->is_enabled == false) || $this->isUpdated();
    }

    public function getTotal()
    {
        if ($this->queue_type == self::QUEUE_TYPE_PERCENTS) {
            return $this->total_accrued;
        }

        return $this->total;
    }

    public function calculateTotal()
    {
        if ($this->isDisabled()) {
            return 0;
        }

        return $this->getTotal();
    }
}