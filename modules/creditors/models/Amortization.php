<?php

namespace app\modules\creditors\models;

use app\modules\creditors\behaviors\IndexBehavior;
use app\modules\creditors\behaviors\RegistryNumberBehavior;
use app\modules\creditors\behaviors\TimecastBehavior;
use app\modules\creditors\behaviors\UpdateBehavior;
use app\modules\creditors\models\Request;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "creditor_request_amortization".
 *
 * @property integer $id
 * @property integer $request_id
 * @property string $total
 * @property mixed $totalRatio
 * @property string $total_payed
 * @property string $details
 * @property string $repeat_details
 * @property string $create_time
 * @property string $initiation_time
 * @property integer $is_updated
 * @property string $update_reason
 * @property string $update_initiator
 * @property integer $updated_id
 */
class Amortization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'creditor_request_amortization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id'], 'required'],
            [['request_id', 'is_updated', 'updated_id', 'id'], 'integer'],
            [['total', 'total_payed'], 'number'],
            [['create_time', 'initiation_time', 'update_time'], 'safe'],
            [['details', 'repeat_details', 'update_reason'], 'string', 'max' => 512],
            [['update_initiator'], 'string', 'max' => 256],
            [['total', 'total_payed', 'create_time', 'initiation_time'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->total = $this->getTotalDefaults();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            RegistryNumberBehavior::className(),
            TimecastBehavior::className(),
            UpdateBehavior::className(),
            IndexBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('creditors','ID'),
            'request_id' => Yii::t('creditors','Request ID'),
            'total' => Yii::t('creditors','Amortization Total'),
            'totalRatio' => Yii::t('creditors','Amortization Total Ratio'),
            'total_payed' => Yii::t('creditors','Total Payed'),
            'details' => Yii::t('creditors','Amortization Details'),
            'repeat_details' => Yii::t('creditors','Repeat Details'),
            'create_time' => Yii::t('creditors','Create Time'),
            'initiation_time' => Yii::t('creditors','Amortization Time'),
            'is_updated' => Yii::t('creditors','Is Updated'),
            'update_reason' => Yii::t('creditors','Update Reason'),
            'update_initiator' => Yii::t('creditors','Update Initiator'),
            'updated_id' => Yii::t('creditors','Updated ID'),
            'index' => Yii::t('creditors', 'ID'),
            'nextIndex' => Yii::t('creditors', 'Updated ID'),
            'updatedIndex' => Yii::t('creditors', 'Updated ID'),
        ];
    }

    public function getTotalRatio()
    {
        $total = Request::find()
            ->select('SUM(total)')
            ->where([
                'queue_type' => $this->getRequest()->one()->queue_type,
            ])
            ->scalar();

        return $this->total_payed / $total * 100;
    }

    public function getTotalDefaults()
    {
        if ($this->isNewRecord && $this->request) {
            $total = $this->request->total;
            $payed = array_sum(ArrayHelper::getColumn($this->request->amortizations, 'total_payed'));

            return $total - $payed;
        }

        return null;
    }

    public function getRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'request_id']);
    }
}