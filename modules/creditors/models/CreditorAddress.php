<?php

namespace app\modules\creditors\models;

use app\models\TblRegionsSearch;
use Yii;

/**
 * This is the model class for table "creditor_organization_address".
 *
 * @property integer $id
 * @property integer $creditor_id
 * @property integer $regions_id
 * @property string $postcode
 * @property string $locality
 * @property string $address
 * @property integer $type
 */
class CreditorAddress extends \yii\db\ActiveRecord
{
    const TYPE_LEGAL = 0;

    const TYPE_CORRESPONDENTION = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'creditor_organization_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creditor_id'], 'required'],
            [['creditor_id', 'regions_id', 'type'], 'integer'],
            [['postcode'], 'string', 'max' => 30],
            [['locality'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creditor_id' => 'Creditor ID',
            'regions_id' => 'Regions ID',
            'postcode' => 'Postcode',
            'locality' => 'Locality',
            'address' => 'Address',
            'type' => 'Type',
        ];
    }

    public function getRegion()
    {
        return $this->hasOne(TblRegionsSearch::className(), ['id' => 'regions_id']);
    }

    public static function getTypes()
    {
        return [
            self::TYPE_CORRESPONDENTION,
            self::TYPE_LEGAL,
        ];
    }
}