<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/16/17
 * Time: 10:53 PM
 */

namespace app\modules\creditors\models;


use yii\base\Model;

class PrintTable extends Model
{
    const TYPE_CREDITOR = 'creditor';

    const TYPE_REQUEST = 'request';

    const TYPE_AMORTIZATION = 'amortization';

    const TYPE_GUARANTEE = 'guarantee';

    /**
     * @var string
     */
    public $type;

    /**
     * @var array
     */
    public $models;

    /**
     * @var integer
     */
    public $queue;

    /**
     * @return int|false
     */
    public function getNumber()
    {
        if (isset($this->getMapping()[$this->queue . $this->type])) {
            return $this->getMapping()[$this->queue . $this->type];
        }

        return false;
    }

    protected function getMapping()
    {
        return [
            Request::QUEUE_TYPE_FIRST . PrintTable::TYPE_CREDITOR => 1,
            Request::QUEUE_TYPE_FIRST . PrintTable::TYPE_REQUEST => 2,
            Request::QUEUE_TYPE_FIRST . PrintTable::TYPE_AMORTIZATION => 3,
            Request::QUEUE_TYPE_SECOND . PrintTable::TYPE_CREDITOR => 4,
            Request::QUEUE_TYPE_SECOND . PrintTable::TYPE_REQUEST => 5,
            Request::QUEUE_TYPE_SECOND . PrintTable::TYPE_AMORTIZATION => 6,
            Request::QUEUE_TYPE_THIRD_GUARANTEE . PrintTable::TYPE_CREDITOR => 7,
            Request::QUEUE_TYPE_THIRD_GUARANTEE . PrintTable::TYPE_REQUEST => 8,
            Request::QUEUE_TYPE_THIRD_GUARANTEE . PrintTable::TYPE_AMORTIZATION => 10,
            Request::QUEUE_TYPE_THIRD_GUARANTEE . PrintTable::TYPE_GUARANTEE => 9,
            Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE . PrintTable::TYPE_CREDITOR => 11,
            Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE . PrintTable::TYPE_REQUEST => 12,
            Request::QUEUE_TYPE_THIRD_WITHOUT_GUARANTEE . PrintTable::TYPE_AMORTIZATION => 13,
            Request::QUEUE_TYPE_PERCENTS . PrintTable::TYPE_REQUEST => 14,
            Request::QUEUE_TYPE_PERCENTS . PrintTable::TYPE_AMORTIZATION => 15,
            Request::QUEUE_TYPE_PENALTY . PrintTable::TYPE_CREDITOR => 16,
            Request::QUEUE_TYPE_PENALTY . PrintTable::TYPE_REQUEST => 17,
            Request::QUEUE_TYPE_PENALTY . PrintTable::TYPE_AMORTIZATION => 18,
            Request::QUEUE_TYPE_DEFAULT . PrintTable::TYPE_CREDITOR => 19,
            Request::QUEUE_TYPE_DEFAULT . PrintTable::TYPE_REQUEST => 20,
            Request::QUEUE_TYPE_DEFAULT . PrintTable::TYPE_AMORTIZATION => 21,
        ];
    }
}