<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/10/17
 * Time: 11:39 PM
 */

namespace app\modules\creditors\models\forms;


use app\modules\creditors\models\Request;
use yii\base\Model;

class PrintForm extends Model
{
    /**
     * @var array
     */
    public $queues = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['queues'], 'safe'],
        ];
    }
}