<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 3/28/17
 * Time: 1:27 AM
 */

namespace app\modules\creditors\models\forms;

use app\modules\creditors\models\Request;
use Yii;

class RequestForm extends Request
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['linkedTitle'], 'safe'],
            [['linkedId'], 'integer'],
            [['linkedId'], 'required'],
            [['is_disabled'], 'boolean'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_FIND, [$this, 'initParams']);
    }

    /**
     * @return void
     */
    public function initParams()
    {
        if ($this->isWithPercentage()) {
            $this->linkedId = $this->linkedRequest->id;
        } else {
            $this->linkedId = $this->creditor_id;
            $this->linkedTitle = $this->creditor->title;
        }
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'linkedTitle',
            'linkedId',
            'is_disabled'
        ]);
    }

    public function afterFind()
    {
        parent::afterFind();

        if ($this->isWithPercentage()) {
            $this->linkedId = $this->linked_request_id;
        } else {
            $this->linkedId = $this->creditor_id;
        }
    }

    public function beforeValidate()
    {
        if ($this->isWithPercentage()) {
            $this->linked_request_id = $this->linkedId;
            $this->creditor_id = $this->linkedRequest->creditor->id;
        } else {
            $this->creditor_id = $this->linkedId;
            $this->cleanRequirementAttributes();
        }

        return parent::beforeValidate();
    }

    public function attributeLabels()
    {
        return array_merge([
            'linkedTitle' => Yii::t('creditors', 'Title'),
            'linkedId' => Yii::t('creditors', 'Linked ID'),
            'is_disabled' => Yii::t('creditors', 'Is Disabled'),
        ], parent::attributeLabels());
    }

    protected function cleanRequirementAttributes()
    {
        $this->setAttributes([
            'linked_request_id' => null,
            'percentage' => null,
            'day_count' => null,
            'total_accrued' => null,
        ]);
    }
}