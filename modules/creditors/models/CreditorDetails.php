<?php

namespace app\modules\creditors\models;

use app\models\TblParams;
use Yii;

/**
 * This is the model class for table "creditor_organization_details".
 *
 * @property integer $id
 * @property integer $creditor_id
 * @property integer $params_id
 * @property string $value
 */
class CreditorDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'creditor_organization_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creditor_id', 'params_id'], 'required'],
            [['creditor_id', 'params_id'], 'integer'],
            [['value'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('creditors', 'Id'),
            'creditor_id' => Yii::t('creditors', 'Organization'),
            'params_id' => Yii::t('creditors', 'Param'),
            'value' => Yii::t('creditors', 'Value'),
        ];
    }

    public function getTblParams()
    {
        return $this->hasOne(TblParams::className(), ['id' => 'params_id']);
    }
}
