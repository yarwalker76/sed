<?php

namespace app\modules\creditors\models;

use app\controllers\ReferencesController;
use app\models\TblOrganizationsMethods;
use Yii;

/**
 * This is the model class for table "creditor_organization_bank_account".
 *
 * @property integer $id
 * @property integer $creditor_id
 * @property integer $banks_id
 * @property string $close_time
 * @property integer $is_closed
 * @property string $comment
 * @property string $number
 * @property array $bank
 * @property string $bankTitle
 */
class CreditorBankAccount extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    private $_bank;

    /**
     * @var string
     */
    private $_bankTitle;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'creditor_organization_bank_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creditor_id', 'banks_id'], 'required'],
            [['creditor_id', 'banks_id', 'is_closed'], 'integer'],
            [['close_time'], 'safe'],
            [['comment'], 'string', 'max' => 512],
            [['number'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creditor_id' => 'Creditor ID',
            'banks_id' => 'Banks ID',
            'close_time' => 'Close Time',
            'is_closed' => 'Is Closed',
            'comment' => 'Comment',
            'number' => 'Number',
        ];
    }

    /**
     * @return array|false
     */
    public function getBank()
    {
        if ($this->_bank === null) {
            $this->_bank = TblOrganizationsMethods::getOrganization($this->banks_id);
        }

        return $this->_bank;
    }

    /**
     * @return null|string
     */
    public function getBankTitle()
    {
        if ($this->_bankTitle === null) {
            $this->_bankTitle = TblOrganizationsMethods::extractNameFromParams(TblOrganizationsMethods::getParams($this->banks_id));
        }

        return $this->_bankTitle;
    }
}