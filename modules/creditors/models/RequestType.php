<?php

namespace app\modules\creditors\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "creditor_request_type".
 *
 * @property integer $id
 * @property string $title
 */
class RequestType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'creditor_request_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('creditors', 'Request Type Title'),
        ];
    }

    /**
     * @return array
     */
    public static function getDropdown()
    {
        $items = self::find()
            ->asArray()
            ->all();

        return ArrayHelper::map($items, 'id', 'title');
    }
}