<?php

namespace app\modules\creditors\models;

use app\modules\creditors\behaviors\IndexBehavior;
use app\modules\creditors\behaviors\RegistryNumberBehavior;
use app\modules\creditors\behaviors\TimecastBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\modules\creditors\behaviors\UpdateBehavior;

/**
 * This is the model class for table "creditor_request_guarantee".
 *
 * @property integer $id
 * @property integer $request_id
 * @property string $total
 * @property string $details
 * @property Request $request
 * @property mixed $totalRatio
 * @property string $create_time
 * @property integer $is_updated
 * @property string $update_reason
 * @property string $update_initiator
 * @property integer $updated_id
 */
class Guarantee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'creditor_request_guarantee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_id'], 'required'],
            [['request_id', 'updated_id'], 'integer'],
            [['total'], 'number'],
            [['details', 'update_reason', 'update_initiator'], 'string', 'max' => 512],
            [['create_time'], 'safe'],
            [['is_updated'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('creditors', 'ID'),
            'request_id' => Yii::t('creditors', 'Request ID'),
            'create_time' => Yii::t('creditors', 'Create Time'),
            'total' => Yii::t('creditors', 'Guarantee Total'),
            'details' => Yii::t('creditors', 'Guarantee Details'),
            'totalRatio' => Yii::t('creditors', 'Guarantee Total Ratio'),
            'is_updated' => Yii::t('creditors', 'Is Updated'),
            'update_reason' => Yii::t('creditors', 'Update Reason'),
            'update_initiator' => Yii::t('creditors', 'Update Initiator'),
            'updated_id' => Yii::t('creditors', 'Updated ID'),
            'index' => Yii::t('creditors', 'ID'),
            'nextIndex' => Yii::t('creditors', 'Updated ID'),
            'updatedIndex' => Yii::t('creditors', 'Updated ID'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ],
            RegistryNumberBehavior::className(),
            UpdateBehavior::className(),
            TimecastBehavior::className(),
            IndexBehavior::className(),
        ];
    }

    public function getRequest()
    {
        return $this->hasOne(Request::className(), ['id' => 'request_id']);
    }

    public function getTotalRatio()
    {
        if ($this->request) {
            return $this->total / $this->request->total * 100;
        }

        return null;
    }
}