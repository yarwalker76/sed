<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 2/15/17
 * Time: 8:12 AM
 */

namespace app\modules\printdocs\services;

use app\modules\printdocs\models\DocTemplates;
use app\modules\printdocs\repositories\EnvelopeRepository;
use app\modules\printdocs\repositories\LetterFormRepository;
use DocxMerge\DocxMerge;

class EnvelopeService extends \yii\base\Object
{
    const ENVELOPE_GROUP_ID = 6;

    /**
     * @var LetterFormRepository
     */
    private $letterFormRepository;

    /**
     * @var EnvelopeRepository
     */
    private $envelopeRepository;

    public function __construct(LetterFormRepository $letterFormRepository, EnvelopeRepository $envelopeRepository, array $config = [])
    {
        $this->letterFormRepository = $letterFormRepository;
        $this->envelopeRepository = $envelopeRepository;

        parent::__construct($config);
    }

    /**
     * @param $ids string[]
     * @return bool|string
     */
    public function createEnvelopsFromOutgoingDocs($ids)
    {
        $forms = $this->letterFormRepository->createFromOutgoingDocs($ids);

        if (count($forms) === 0) {
            return false;
        }

        $docPaths = $this->envelopeRepository->createLetterFormArray($forms, $this->findDocTemplate());

        $resPath = $this->envelopeRepository->getTempPath();

        $dm = new DocxMerge();
        $dm->merge($docPaths, $resPath);

        $this->envelopeRepository->deleteDocs($docPaths);

        return $resPath;
    }

    /**
     * @return null|DocTemplates
     */
    protected function findDocTemplate()
    {
        return DocTemplates::findOne(['group_id' => self::ENVELOPE_GROUP_ID]);
    }
}