<?php

namespace app\modules\printdocs\models;

use Yii;

/**
 * This is the model class for table "doc_groups".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DocTemplates[] $docTemplates
 */
class DocGroups extends \yii\db\ActiveRecord
{
    const CREDITORS_ID = 7;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100],
            ['form_view', 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocTemplates()
    {
        return $this->hasMany(DocTemplates::className(), ['group_id' => 'id']);
    }
}
