<?php

namespace app\modules\printdocs\models;

use Yii;

/**
 * This is the model class for table "doc_templates".
 *
 * @property integer $id
 * @property string $name
 * @property string $path
 * @property integer $group_id
 * @property integer $template_type
 * @property integer $proc_type
 * @property integer $question_type
 *
 * @property DocGroups $group
 */
class DocTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id'], 'required'],
            [['group_id', 'question_type'], 'integer'],
            [['name', 'path', 'template_type', 'proc_type'], 'string', 'max' => 100],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocGroups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('printdocs', 'ID'),
            'name' => Yii::t('printdocs', 'Name'),
            'path' => Yii::t('printdocs', 'Path'),
            'group_id' => Yii::t('printdocs', 'Group ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(DocGroups::className(), ['id' => 'group_id']);
    }
    
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            // удаляем файл
            @unlink($this->path);
            /*
            if( !unlink($this->path) ):
                throw new Exception('Не удалось удалить файл ' . $this->path);
                return false;
            endif;    
            */
            return true;
        } else {
            return false;
        }
    }
}
