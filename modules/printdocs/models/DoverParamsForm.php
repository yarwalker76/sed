<?php

namespace app\modules\printdocs\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class DoverParamsForm extends Model
{
    public $date;
    public $tpl_id;
    public $for_whom;
    //public $on_whom;
    public $for_whom_fio;
    /*public $on_whom_fio;*/
    public $actions;
    public $basis;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['for_whom_fio', /*'on_whom_fio'*/], 'string'],
            [['for_whom_fio', 'basis'/*'on_whom_fio'*/], 'required'],
            [['for_whom', /*'on_whom',*/ 'tpl_id'], 'integer'],
            [['basis'], 'string', 'max' => 512],
            [['date'], 'safe'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('printdocs', 'DOVER_DATE'),
            'for_whom_fio' => Yii::t('printdocs', 'FOR_WHOM'),
            'on_whom_fio' => Yii::t('printdocs', 'ON_WHOM'),
            'basis' => Yii::t('printdocs', 'BASIS'),
        ];
    }
}
