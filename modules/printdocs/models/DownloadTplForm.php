<?php

namespace app\modules\printdocs\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class DownloadTplForm extends Model
{
    public $tpl;
    public $group_id;
    public $template_type;
    public $proc_type;
    public $question_type;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['group_id', 'template_type', 'proc_type', 'question_type'], 'integer'],
            [['group_id', 'tpl'], 'required'],
            [['group_id', 'tpl'], 'safe'],
            [['tpl'], 'file', 'skipOnEmpty' => false, 'extensions' => 'doc,docx'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'tpl' => 'Файл',
            'template_type' => 'Тип шаблона',
            'proc_type' => 'Тип процедуры',
            'question_type' => 'Тип вопроса'
        ];
    }
}
