<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 2/10/17
 * Time: 12:30 AM
 */

namespace app\modules\printdocs\models\forms;

use yii\base\Model;

class LetterForm extends Model
{
    /**
     * @var string
     */
    public $senderTitle ;

    /**
     * @var string
     */
    public $senderAddress;

    /**
     * @var string
     */
    public $senderZip;

    /**
     * @var string
     */
    public $receiverTitle;

    /**
     * @var string
     */
    public $receiverAddress;

    /**
     * @var string
     */
    public $receiverZip;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['senderTitle', 'senderAddress', 'senderZip', 'receiverTitle', 'receiverAddress', 'receiverZip'], 'string'],
        ];
    }
}