<?php

namespace app\modules\printdocs\models;

use Yii;
use yii\base\Model;
use app\models\TblOrganizationsTypes;
use yii\helpers\ArrayHelper;
use app\models\AppHelper;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CollectDebtorInfoParamsForm extends Model
{
    public $tpl_id;
    public $addressee_id;
    public $addressee;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['addressee'], 'string'],
            [['tpl_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'addressee' => Yii::t('printdocs', 'ADDRESSEE'),
        ];
    }

}
