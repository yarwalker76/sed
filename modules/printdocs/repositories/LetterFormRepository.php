<?php

/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 2/15/17
 * Time: 8:15 AM
 */

namespace app\modules\printdocs\repositories;

use app\modules\documents\models\OutgoingDocs;
use app\models\AppHelper;
use app\models\TblOrganizationsMethods;
use app\modules\printdocs\models\forms\LetterForm;

class LetterFormRepository extends \yii\base\Object
{
    public function createFromOutgoingDocs($outgoingDocIds)
    {
        $models = OutgoingDocs::findAll(['id' => $outgoingDocIds]);

        $res = [];

        foreach ($models as $model) {
            $form = $this->createLetterForm($model);

            if ($form) {
                $res[] = $form;
            }
        }

        return $res;
    }

    private function createLetterForm($outgoingDoc)
    {
        $currentProcedure = AppHelper::getCurrentProcedure();
        $organization = TblOrganizationsMethods::getOrganization($currentProcedure->manager_id);

        $printModel = new LetterForm([
            'senderTitle' => AppHelper::getFIO($currentProcedure->manager_id),
            'senderAddress' => $this->buildAddress([
                @$organization['address_2_address'],
                @$organization['address_2_region'],
                @$organization['address_2_locality'],
            ]),
            'senderZip' => $organization['address_2_postcode'],

            'receiverTitle' => $outgoingDoc->contractor,
            'receiverZip' => $this->extractZipFromAddress($outgoingDoc->delivery_address),
            'receiverAddress' => $this->removeZipFromAddress($outgoingDoc->delivery_address),
        ]);

        return $printModel;
    }

    /**
     * @param $attributes
     * @return string
     */
    private function buildAddress($attributes)
    {
        $res = '';

        foreach ($attributes as $key => $value) {
            $comma = ', ';

            if ($key === count($attributes) - 1) {
                $comma = '';
            }

            if ($value) {
                $res .= $value . $comma;
            }
        }

        return $res;
    }

    /**
     * @param $address
     * @return mixed
     */
    private function removeZipFromAddress($address)
    {
        return trim(preg_replace('/^\d+/ims', '', $address), '., ');
    }

    /**
     * @param $address
     * @return mixed
     */
    private function extractZipFromAddress($address)
    {
        preg_match('/^(\d+)/ims', $address, $params);

        return @$params[1];
    }
}