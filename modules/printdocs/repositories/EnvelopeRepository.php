<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 2/15/17
 * Time: 8:20 AM
 */

namespace app\modules\printdocs\repositories;


use app\modules\printdocs\models\DocTemplates;
use app\modules\printdocs\models\forms\LetterForm;
use yii\base\Object;
use PhpOffice\PhpWord\TemplateProcessor;
use Yii;

class EnvelopeRepository extends Object
{
    /**
     * @param $letterForms LetterForm[]
     * @param $template DocTemplates
     * @return array
     */
    public function createLetterFormArray($letterForms, $template)
    {
        $res = [];

        foreach ($letterForms as $letterForm) {
            $res[] = $this->createFromLetterForm($letterForm, $template);
        }

        return $res;
    }

    /**
     * @param $letterForm LetterForm
     * @param $template DocTemplates
     * @return string
     */
    public function createFromLetterForm($letterForm, $template)
    {
        $templateProcessor = new TemplateProcessor($template->path);
        $letterFormAttributes = $letterForm->getAttributes();

        foreach ($this->getLetterFormMapping() as $letterFormAttribute => $templaterVar) {
            $templateProcessor->setValue($templaterVar, $letterFormAttributes[$letterFormAttribute]);
        }

        $path = $this->getTempPath();

        $templateProcessor->saveAs($path);

        return $path;
    }

    /**
     * @param string $extension
     * @return string
     */
    public function getTempPath($extension = '.docx')
    {
        return sprintf('%s/web/uploads/templates/%s%s', Yii::getAlias('@app'),  md5(uniqid()), $extension);
    }

    /**
     * @param $paths string[]
     */
    public function deleteDocs($paths)
    {
        foreach ($paths as $path) {
            unlink($path);
        }
    }

    /**
     * @return array
     */
    protected function getLetterFormMapping()
    {
        return [
            'senderTitle' => 'ФИОАУПолное',
            'senderAddress' => 'АУАдрКор',
            'senderZip' => 'АУИндекс',
            'receiverTitle' => 'АдрКому',
            'receiverAddress' => 'АдрКуда',
            'receiverZip' => 'КомуИндекс',
        ];
    }
}