(function($){
    
    $('.add-printdoc-item').on('click', function(ev){
        ev.preventDefault();

        $.post("/printdocs/default/add-template-form", {'id': $(this).data('id')}, function(data){
            $('#modalDownloadTpl .modal-body').html(data);
            $('#modalDownloadTpl').modal('show');
        }, 'html')
        .fail(function(error){
            $('#modalDownloadTpl .modal-body').html('<p>' + error.responseText + '</p>');
            $('#modalDownloadTpl').modal('show');
        });



        /*$('#downloadtplform-group_id').val( $(this).data('id') );
        $('#dwnd-form').attr('data-id', $(this).data('id') );*/
       
    });

    // на закрытие модального окна обновляем шаблоны
    $('#modalDownloadTpl').on('hide.bs.modal', function(){
        /*$.post('/printdocs/default/get-dwn-tpl-form', function(data){
            $('#new-template').html(data);
        }, 'html'); */
    });  
    
    $(document).on('submit', '#dwnd-form', function(ev){
        ev.preventDefault();
        console.log('submit' + $(this).attr('data-id'));
        var id = $(this).attr('data-id');
        var form = document.getElementById('dwnd-form');
        var formData = new FormData(form);
        
        formData.append('tpl', $('#downloadtplform-tpl')[0].files[0]);
        
        $('#modalDownloadTpl').modal('hide');
        
        $.ajax({
            url: '/printdocs/add-template',
            type: 'POST',
            async: false,
            dataType: "json",
            data: formData, 
            processData: false,
            contentType: false,
            success: function(data) {
                console.info('data', data);
            	if( data.error !== undefined ) {
                    $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                    $('#modalMessage').modal('show');
                } else {
                    $.pjax.reload({container: '#templates' + id});
                } 
            },
            error: function(error) {
                //$('#modal').modal('hide');
                console.info('func error', error);
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            }
        }); 
        
        
    });
    
    $(document).on('click', 'a.del-tpl', function(ev){
        ev.preventDefault();
        var grid = $(this).attr('data-grid');
        
        $.post($(this).attr('href'), {'id': $(this).data('id')}, function(data){
            console.info('data', data);
            if( data.error !== undefined ) {
                $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                $('#modalMessage').modal('show');
            } else {
                $('#modalMessage .modal-body').html('<p>' + data.msg + '</p>');
                $('#modalMessage').modal('show');
                $.pjax.reload({container: '#templates' + grid});
            } 
        }, 'json')
        .fail(function(error){
            console.info('func error', error);
            $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
            $('#modalMessage').modal('show');
        });
    });
    
    $(document).on('click', 'a.download-tpl-link', function(ev){
        var $this = $(this);
        ev.preventDefault();
        console.info('grid', $this.data('grid'));

        $this
            .closest('div.grid-view')
            .find('#params-form-wrap')
            .slideUp(250, function(){ $(this).remove()});

        if( !$this.hasClass('active') ) {
            $this.closest('div.grid-view').find('a.download-tpl-link').removeClass('active');
        }

        var dataGridNumber = $this.data('grid');

        if (dataGridNumber == 1
            || dataGridNumber == 2
            || dataGridNumber == 5)
        {
            if( $this.hasClass('active') ) {
                $this.removeClass('active');
                $this.next('#params-form-wrap').slideUp(250, function(){ $(this).remove()});
            } else {
                // получаем форму с параметрами
                $.post('/printdocs/get-params-form', {tpl_id: $this.data('id'), grid: $this.data('grid')}, function(data){
                    if( data.error !== undefined ) {
                        $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
                        $('#modalMessage').modal('show');
                    } else {
                        $this.addClass('active');

                        var formHtml = $('<div id="params-form-wrap">' + data.msg + '</div>');

                        if (dataGridNumber == 1 || dataGridNumber == 2)
                        {
                            var documentCheckbox = $('#createDocumentsCheckbox').clone();
                            formHtml.find('button[type=submit]').before(documentCheckbox);
                        }

                        formHtml.insertAfter($this);
                    }
                }, 'json')
                    .fail(function(error){
                        console.info('func error', error);
                        $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                        $('#modalMessage').modal('show');
                    });
            }
            //window.location = '/printdocs/make-proc-holding?tpl_id=' + $this.data('id') + 'grid=' + $this.data('grid');
        } else {
            // вариант без формы параметров
            window.location = $this.attr('href') + '?tpl_id=' + $this.data('id') + 'grid=' + $this.data('grid');
        }
    });

    $(document).on('click','.choose-contractor-btn', function(){
        $('#contractors-list-form').data('type', $(this).data('type'));    
        console.info('form type', $('#contractors-list-form').data('type') );

        // запрос на выбор типов справочников в форме поиска 
        $.post( '/printdocs/default/get-search-form', {type: $(this).data('type')}, function(data){
            $('#modalContractors .modal-body #contractor-search-form').remove();
            $('#modalContractors .modal-body').prepend(data);
        }, "html")
        .fail(function(error){
            //$('#modal').modal('hide');
            console.info('func error', error);
            $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
            $('#modalMessage').modal('show');
        }); 
    });
    
    $(document).on('submit', '#contractor-search-form', function (e) {
        e.preventDefault();

        $.post( $(this).attr('action'), $(this).serialize(), function(data){

            if( data.error !== undefined ) {
                $('#contractors-list-form').html('<p>' + data.error + '</p>');
              //  $('#modalMessage .modal-body').html('<p>' + data.error + '</p>');
              //  $('#modalMessage').modal('show');
            } else {
                $('#contractors-list-form').html(data.contractors);
            } 

        }, "json")
        .fail(function(error){
            //$('#modal').modal('hide');
            console.info('func error', error);
            //$('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
            //$('#modalMessage').modal('show');
        });

    });
    
    $(document).on('submit','#contractors-list-form', function(ev){
        ev.preventDefault();

        var type = $(this).data('type');
        console.info('submit form type', $(this).data('type'));
        switch (type) {
            case 'for_whom':
                $('#doverparamsform-for_whom_fio').val( $('#contractors-list :radio:checked').val() );
                $('#doverparamsform-for_whom').val( $('#contractors-list :radio:checked').data('id') );
                break;
            case 'on_whom':
                $('#doverparamsform-on_whom_fio').val( $('#contractors-list :radio:checked').val() );
                $('#doverparamsform-on_whom').val( $('#contractors-list :radio:checked').data('id') );
                break;
            case 'addressee':
                $('#collectdebtorinfoparamsform-addressee').val( $('#contractors-list :radio:checked').val() );
                $('#collectdebtorinfoparamsform-addressee_id').val( $('#contractors-list :radio:checked').data('id') );
                break;
        }

        // на закрытие модального окна очищаем форму
        $('#modalContractors').modal('hide');
        $('#contractors-list-form').html('');
    });

    $(document).on('change', '#downloadtplform-template_type', function(){
        switch( parseInt($(this).val()) ) {
            case 0:
                $('#wrap-proc-type').show();
                $('#wrap-question-type').hide();
                break;

            case 1:
                $('#wrap-proc-type').hide();
                $('#wrap-question-type').show();
                break;

            case 2:
                $('#wrap-proc-type').show();
                $('#wrap-question-type').hide();
                break;

            default:
                $('#wrap-proc-type').hide();
                $('#wrap-question-type').hide();
        }
    });

    
})(jQuery);