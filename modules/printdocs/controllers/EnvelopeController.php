<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 2/15/17
 * Time: 9:50 AM
 */

namespace app\modules\printdocs\controllers;


use app\modules\printdocs\services\EnvelopeService;
use yii\web\Controller;
use yii\web\Response;
use Yii;
use yii\web\NotFoundHttpException;

class EnvelopeController extends Controller
{
    /**
     * @var EnvelopeService
     */
    private $envelopeService;

    /**
     * EnvelopeController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param EnvelopeService $envelopeService
     * @param array $config
     */
    public function __construct($id, $module, EnvelopeService $envelopeService, array $config = [])
    {
        $this->envelopeService = $envelopeService;

        parent::__construct($id, $module, $config);
    }

    public function actionCreateFromOutgoingDocs($ids)
    {
        $outgoingDocsIds = explode(',', $ids);

        $resPath = $this->envelopeService->createEnvelopsFromOutgoingDocs($outgoingDocsIds);

        if ($resPath == false) {
            throw new NotFoundHttpException('Please, select at least one message!');
        }

        Yii::$app->response->on(Response::EVENT_AFTER_SEND, function () use ($resPath) {
            unlink($resPath);
        });

        return Yii::$app->response->sendFile($resPath);
    }
}