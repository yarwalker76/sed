<?php

namespace app\modules\printdocs\controllers;


use app\models\AddressHelper;
use app\models\TblOrganizations;
use app\modules\meetings\models\MeetingBulletinForms;
use app\modules\procedures\models\ProceduresProlongations;
use app\modules\procedures\models\ProcedureTypes;
use PhpOffice\PhpWord\Settings;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\printdocs\models\DownloadTplForm;
use app\modules\printdocs\models\DocGroups;
use app\modules\printdocs\models\DocTemplates;
use app\modules\printdocs\models\DocTemplatesSearch;
use app\modules\printdocs\models\ContractorSearchForm;
use PhpOffice\PhpWord\TemplateProcessor;
use app\modules\printdocs\models\DoverParamsForm;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\AppHelper;
use app\models\TblValues;
use app\modules\procedures\models\Procedures;
use app\models\TblOrganizationsMethods;
use yii\helpers\Json;
use app\models\LegalService;
use app\models\BackOffice;
use app\modules\documents\models\OutgoingDocs;
use yii\db\Query;
use app\modules\documents\models\DocTypes;
use app\modules\documents\models\IncomingDocs;
use app\modules\procedures\models\ProceduresAdditionalProperties;
use app\modules\printdocs\models\CollectDebtorInfoParamsForm;


use app\components\petrovich\Petrovich;
use app\components\petrovich\traits\TraitPetrovich;


/**
 * Default controller for the `print_docs` module
 */
class DefaultController extends Controller
{
    /**
     * input name for creating documents
     */
    const NAME_CREATE_DOCUMENTS = 'isCreateDocuments';

    use TraitPetrovich;
    
    private $_days = [ 'Нулевое', 'Первое', 'Второе', 'Третье', 'Четвертое', 'Пятое', 'Шестое', 'Седьмое', 'Восьмое', 'Девятое', "Десятое", 
                "Одиннадцатое", "Двенадцатое", "Тринадцатое", "Четырнадцатое", "Пятнадцатое", "Шестнадцатое", "Семнадцатое", "Восемнадцатое", "Девятнадцатое", "Двадцатое",
                "Двадцать первое", "Двадцать второе", "Двадцать третье", "Двадцать четвертое", "Двадцать пятое", "Двадцать шестое", "Двадцать седьмое", "Двадцать восьмое", "Двадцать девятое", 
                "Тридцатое", "Тридцать первое"
            ];
    private $_months = ["нулября", "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];
    private $_decs = [ 16 => 'шестнадцатого', 17 => 'семнадцатого', 18 => 'восемнадцатого', 19 => 'девятнадцатого', 20 => 'давдацтого', 21 => 'двадцать первого',
                22 => 'двадцать второго', 23 => 'двадцать третьего', 24 => 'двадцать четвертого', 25 => 'двадцать пятого', 26 => 'двадцать шестого',
                27 => 'двадцать седьмого', 28 => 'двадцать восьмого', 29 => 'двадцать девятого', 30 => 'тридцатого'
            ];

    private $_meeting_template_types_list = ['Уведомление', 'Бюллетень', 'Протокол', 'Журнал'];
    private $_proc_types = ['Н, ВУ', 'КП'];

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['index', 'error'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer', 'officeManager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $orig_path = Yii::$app->params['uploadPath'] . 'templates/';

        $tplFormModel = new DownloadTplForm();
        $dataProviders = [];
        $group_model = $this->orderTemplateGroups(DocGroups::find()->all());
            
        foreach( $group_model as $g ):
            $searchModel = new DocTemplatesSearch( ['group_id' => $g->id] );
            $dataProviders[ $g->id ] = [
                'searchModel' => $searchModel,
                'dataProvider' => $searchModel->search(Yii::$app->request->queryParams)
            ];
        endforeach;
        
        /*echo 'cur proc ID ' . Yii::$app->session->get('current_proc_id') . '<br>';
        
        $procedure = Procedures::findOne(Yii::$app->session->get('current_proc_id'));
        $org = TblOrganizationsMethods::getOrganization( $procedure->organization_id );
        echo '<pre>'.print_r($org,true).'</pre>';
        echo '<pre>' . print_r( TblOrganizationsMethods::getParams( $procedure->organization_id ), true ) . '</pre>';*/
        
        return $this->render('index', [
                    'doc_groups' => $group_model,
                    'tplFormModel' => $tplFormModel,
                    'dataProviders' => $dataProviders,
                    'nameCreateDocuments' => self::NAME_CREATE_DOCUMENTS,
        ]);
    }

    public function getOrder()
    {
        return [
            1,
            2,
            7,
            3,
            4,
            5,
            6,
        ];
    }

    public function orderTemplateGroups($models)
    {
        $res = [];
        $models = ArrayHelper::index($models, 'id');

        foreach ($this->getOrder() as $position => $id) {
            $res[$id] = @$models[$id];
            unset($models[$id]);
        }

        $res = array_merge($res, $models);

        return $res;
    }
    
    public function actionGetDwnTplForm()
    {
        $tplFormModel = new DownloadTplForm();
        echo $this->renderPartial('_dwn_tpl_form', ['tplFormModel' => $tplFormModel]);
    }
    
    public function actionAddTemplate()
    {
        $absolutePath = Yii::$app->params['uploadPath'] . 'templates/';
        $result = [];
        
        if(is_writable($absolutePath) ):
            $tplFormModel = new DownloadTplForm();

            if ( $tplFormModel->load( Yii::$app->request->post() ) ):
                $tplFormModel->tpl = UploadedFile::getInstanceByName('DownloadTplForm[tpl]');
                //echo '<pre>'.print_r($_FILES,true).'</pre>';
                
                if( $tplFormModel->validate() ):
                    $filename = $tplFormModel->tpl->name;
                    $ext = $tplFormModel->tpl->extension;
                    $file = Yii::$app->security->generateRandomString().".{$ext}";
                    $path = $absolutePath . $file;

                    if( $tplFormModel->tpl->saveAs($path) ):
                        $tpl_model = new DocTemplates();

                        $tpl_model->setAttributes([
                            'group_id' => $tplFormModel->group_id,
                            'path' => str_replace(Yii::getAlias('@app') . '/web/', '', $path),
                            'name' => $filename,
                            'template_type' => ($tplFormModel->group_id == 8 ? (isset($tplFormModel->template_type) ? $this->_meeting_template_types_list[$tplFormModel->template_type] : null) : null),
                            'proc_type' => ($tplFormModel->group_id == 8 ? (isset($tplFormModel->proc_type) && !empty($tplFormModel->proc_type) ? $this->_proc_types[$tplFormModel->proc_type] : null) : null),
                            'question_type' => ($tplFormModel->group_id == 8 ? (isset($tplFormModel->question_type) && !empty($tplFormModel->question_type) ? $tplFormModel->question_type : null) : null)
                        ]);

                        if( !$tpl_model->save() ):
                            $str = '';
                            $errors = $tpl_model->getErrors();

                            !$errors || $str = AppHelper::makeErrorString($errors);

                            //\Yii::$app->getSession()->setFlash('download_error', $str);
                            $result = ['error' => $str];
                            //return $this->redirect(Url::to(['/printdocs']));
                            //echo $str;
                        endif;
                    else:
                        $errors = $tplFormModel->getErrors();
                        $str = '';

                        !$errors || $str = AppHelper::makeErrorString($errors);

                        $result = ['error' => $str];
                        //\Yii::$app->getSession()->setFlash('download_error', $str);
                        //return $this->redirect(Url::to(['/printdocs']));
                        //echo $str;
                    endif;
                else:
                    $errors = $tplFormModel->getErrors();
                    $str = '';

                    if( !empty($errors) ):
                        foreach ($errors as $value) {
                            foreach( $value as $v ):
                                    $str .= '<br>' . $v;
                            endforeach;
                        }
                    endif;

                    $result = ['error' => $str];
                endif;
            else:
                // ошибки при загрузки формы
                //echo 'Error<br><pre>'.print_r($tplFormModel->errors,true).'</pre>';
                $errors = $tplFormModel->getErrors();
                $str = '';

                !$errors || $str = AppHelper::makeErrorString($errors);

                //\Yii::$app->getSession()->setFlash('download_error', $str);
                $result = ['error' => $str];
                //return $this->redirect(Url::to(['/printdocs']));
                //echo $str;
            endif;
        else:
            // оставили до прояснения
            //\Yii::$app->getSession()->setFlash('download_error', Yii::t('printdocs', 'ERR_DIR_NOT_WRITABLE'));
            $result = ['error' => Yii::t('printdocs', 'ERR_DIR_NOT_WRITABLE')];
            // return $this->redirect(Url::to(['/printdocs']));
             //echo Yii::t('printdocs', 'ERR_DIR_NOT_WRITABLE');

        endif;    
        
        echo json_encode($result);
    }
    
    public function actionDeleteTemplate()
    {
        $result = [];
        
        if( Yii::$app->request->isAjax ):
            $res = DocTemplates::findOne(['id' => Yii::$app->request->post('id')])->delete();
            if( $res == 0 || $res === false ):
                $result = ['error' => Yii::t('printdocs', 'ERR_DEL_TPL')];
            else:
                $result = ['msg' => Yii::t('printdocs', 'DEL_TPL_SUCCESS')];
            endif;
            
            echo json_encode($result);
        else:
            // not ajax request
            throw new NotFoundHttpException('The requested page does not exist.');
        endif;
    }
    
    public function actionGetParamsForm()
    {
        if( Yii::$app->request->isAjax ):
            if( !is_null(AppHelper::getLastSeenProcedureID()) ):
                $group = DocGroups::findOne(['id' => Yii::$app->request->post('grid')]);
                if( is_object($group) && !empty($group) && !is_null($group->form_view) ):
                    // запись найдена
                    if( file_exists(Yii::getAlias('@app') . '/modules/' . \Yii::$app->controller->module->id . '/views/default/' . $group->form_view . '.php') ):
                        switch( Yii::$app->request->post('grid') ):
                            case 1:
                                $model = new \app\modules\printdocs\models\DoverParamsForm();
                                $result = ['msg' => $this->renderAjax($group->form_view, ['model' => $model, 'tpl_id' => Yii::$app->request->post('tpl_id')]) ];
                                break;

                            case 2:
                                $model = new \app\modules\printdocs\models\CollectDebtorInfoParamsForm();
                                $result = ['msg' => $this->renderAjax($group->form_view, ['model' => $model, 'tpl_id' => Yii::$app->request->post('tpl_id')]) ];
                                break;

                            case 5:
                                // dataProvider для исходящего реестра
                                $od_dataProvider = new ActiveDataProvider([
                                    'query' => /*OutgoingDocs::find()
                                                ->select()
                                                ->where(['proc_id' => Yii::$app->session->get('current_proc_id')])*/
                                        ( new Query() )
                                            ->select( '`o`.*, `i`.`response_on_id` \'incoming_response_on_id\'' )
                                            ->from( OutgoingDocs::tableName() . ' as `o`')
                                            ->leftJoin( IncomingDocs::tableName() . ' as `i`', '`o`.`id` = `i`.`response_on_id`' )
                                            ->where( ['`o`.`proc_id`' => Yii::$app->session->get('current_proc_id')] )
                                            ->distinct(),
                                    'sort' => false,
                                    'pagination' => [
                                        'pagesize' => -1,    //Alternate method of disabling paging
                                    ],
                                ]);

                                // dataProvider для входящего реестра
                                $id_dataProvider = new ActiveDataProvider([
                                    'query' =>
                                        ( new Query() )
                                            ->select( '`i`.*, `i`.`response_on_id` \'outgoing_response_on_id\'' )
                                            //->select( '`i`.*, `o`.`response_on_id` \'outgoing_response_on_id\'' )
                                            ->from( IncomingDocs::tableName() . ' as `i`')
                                            //->leftJoin( OutgoingDocs::tableName() . ' as `o`', '`i`.`id` = `o`.`response_on_id`' )
                                            ->where( ['`i`.`proc_id`' => Yii::$app->session->get('current_proc_id'), 'i.response_on_id' => 0] )
                                            ->distinct(),
                                    'sort' => false,
                                    'pagination' => [
                                        'pagesize' => -1,    //Alternate method of disabling paging
                                    ],
                                ]);


                                // dataProvider для бэк-офиса
                                $bo_dataProvider = new ActiveDataProvider([
                                    'query' => BackOffice::find()->where(['proc_id' => Yii::$app->session->get('current_proc_id')]),
                                    'sort' => false,
                                    'pagination' => [
                                        'pagesize' => -1,    //Alternate method of disabling paging
                                    ],
                                ]);

                                // dataProvider для юридической службы
                                $ls_dataProvider = new ActiveDataProvider([
                                    'query' => LegalService::find()->where(['proc_id' => Yii::$app->session->get('current_proc_id')]),
                                    'sort' => false,
                                    'pagination' => [
                                        'pagesize' => -1,    //Alternate method of disabling paging
                                    ],
                                ]);
                                
                                $result = ['msg' => $this->renderAjax($group->form_view, [
                                        'od_dataProvider' => $od_dataProvider, // outgoing docs
                                        'id_dataProvider' => $id_dataProvider, // incoming docs
                                        'bo_dataProvider' => $bo_dataProvider, // back-office
                                        'ls_dataProvider' => $ls_dataProvider, // legal service
                                        'tpl_id' => Yii::$app->request->post('tpl_id')
                                    ])
                                ];
                                break;
                        endswitch;
                    else:
                        $result = ['error' => Yii::t('printdocs', 'ERR_VIEW_NOT_EXISTS') ];
                    endif;
                else:
                    // такой записи нет
                    $result = ['error' => Yii::t('printdocs', 'ERR_FORM_PARAMS')];
                endif;
            else:
                // не удалось определить текущую процедуру
                $result = ['error' => Yii::t('printdocs', 'ERR_NO_PROCEDURES')];
            endif;
            
            echo json_encode($result);
        else:
            // not ajax request
            throw new NotFoundHttpException('The requested page does not exist.');
        endif;
    }
    
    public function actionGetSearchForm()
    {
        $search_model = new ContractorSearchForm();
        $types = AppHelper::getContractorTypesList();

        switch( Yii::$app->request->post('type') ):
            case 'for_whom':
                $contractor_types[7] = $types[7];
                break;
            /*case 'addressee':
                $contractor_types[7] = $types[7];
                $contractor_types[6] = $types[6];
                break;*/
            default:
                $contractor_types = $types;
        endswitch;
        
        return $this->renderAjax('contractor-search-form', ['model' => $search_model, 'contractor_types' => $contractor_types]);
    }
    
    public function actionSearchContractor()
	{
            $str = '';
            $params = [];

            if( Yii::$app->request->isAjax ):

                $search = Yii::$app->request->post('ContractorSearchForm')['search'];
                $type = Yii::$app->request->post('ContractorSearchForm')['type'];

                empty($search) || $params[':search'] = '%' . mb_strtoupper($search, 'UTF-8') . '%';
                empty($type) || $params[':type'] = $type;

                $contractors = Yii::$app->db
                    ->createCommand("
                        select g.title, g.inn, g.lastname, g.firstname, g.patronymic,  o.*
                              from
                                    (select t.organizations_id id, max(t.title) title, max(t.inn) inn, 
                                            max(lastname) lastname, 
                                            max(firstname) firstname,
                                            max(patronymic) patronymic
                                       from
                                            (select v.organizations_id, 
                                                        if(p.title = 'Название', v.value, null) as 'title', 
                                                        if(p.title = 'ИНН', v.value, null) as 'inn',
                                                        if(p.title = 'Фамилия', v.value, null) as 'lastname',
                                                        if(p.title = 'Имя', v.value, null) as 'firstname',
                                                        if(p.title = 'Отчество', v.value, null) as 'patronymic'
                                               from tbl_values as v
                                               left join (SELECT id, title FROM tbl_params WHERE title in ('Название', 'ИНН', 'Фамилия', 'Имя', 'Отчество')) as p
                                                     on v.params_id = p.id
                                              where p.id is not null) as t 
                                      group by t.organizations_id) as g
                                    left join tbl_organizations as o on g.id = o.id " .
                                    ( !empty( $search ) || !empty( $type ) ? 'WHERE ' : '' ) .
                                    ( !empty( $search ) ? "( UPPER(g.title) LIKE :search OR g.inn LIKE :search OR UPPER(g.lastname) LIKE :search OR UPPER(g.firstname) LIKE :search OR UPPER(g.patronymic) LIKE :search ) " . ( !empty( $type ) ? ' AND ' : '' ) : '' ) .
                                    ( !empty( $type ) ? " o.organizations_types_id = :type" : "" ),
                            $params )
                    ->queryAll();

                if( count( $contractors ) ):
                        $str .= '<div id="contractors-list">';

                        foreach( $contractors as $c ):

                            $str .= '<div class="form-group">
                                                    <div class="radio">
                                                        <label>
                                                          <input type="radio" name="contractor" 
                                                            value="' . ( $c['title'] ? Html::encode($c['title']) : trim($c['lastname']) . ' ' . trim($c['firstname']) . ' ' . trim($c['patronymic']) ) . '" ' .
                                                          ' data-inn="' . Html::encode($c['inn']) . '" ' .
                                                          ' data-id="' . $c['id'] . '"> ' . ( $c['title'] ? Html::encode($c['title']) : trim($c['lastname']) . ' ' . trim($c['firstname']) . ' ' . trim($c['patronymic']) ) .
                                                        '</label>
                                                    </div>    
                                              </div>';
                        endforeach;	

                        $str .= '</div><div class="form-group"><button type="submit" class="btn btn-primary">' . Yii::t('printdocs', 'CHOOSE') .'</button></div>';
                else:
                        $str = '<p>' . Yii::t('printdocs', 'NOT_FOUND') . '</p>';
                endif;	
            else:
                // not ajax request
                throw new NotFoundHttpException('The requested page does not exist.');
            endif;

        echo json_encode( [ 'contractors' => $str ] );
    }

    public function actionDownloadTemplate()
    {
        // пытаемсо обработать и сохранить шаблон
        $tpl = DocTemplates::findOne(Yii::$app->request->get('tpl_id'));

        if ($tpl->group_id === DocGroups::CREDITORS_ID) {
            return $this->redirect(['/creditors/print/report']);
        }

        Settings::setTempDir(Yii::getAlias('@app') . '/web/uploads/templates/');
        $templateProcessor = new TemplateProcessor($tpl->path);

        list($day, $month, $year) = explode('.', date('d.m.Y'));
        $templateProcessor->setValue('ТекДата', $day . ' ' . $this->_months[(int)$month] . ' ' . $year . ' г.');

        $proc_params = $this->getCurrentProcInfo();
        $templateProcessor->setValue('ПДНазвание', $proc_params['org_name']);
        $templateProcessor->setValue('ПДНазваниеПолное', $proc_params['org_full_name']);
        $templateProcessor->setValue('ПДНазваниеСокр', $proc_params['org_short_name']);
        $templateProcessor->setValue('ОГРНПредприятия', $proc_params['org_ogrn']);
        $templateProcessor->setValue('ИННПредприятия', $proc_params['org_inn']);
        $templateProcessor->setValue('КПППредприятия', $proc_params['org_kpp']);
        $templateProcessor->setValue('ОКВЭДПредприятия', $proc_params['org_okved']);
        $templateProcessor->setValue('ПДЮрАдрес', $proc_params['org_legal_address']);
        $templateProcessor->setValue('ФИОАУПолное', $proc_params['au_fio']);
        $templateProcessor->setValue('ПроцПред', $proc_params['introduction_background']);
        $templateProcessor->setValue('ПроцОснов', $proc_params['introduction_basis']);

        list($lastname, $firstname, $patronymic) = explode(' ', $proc_params['au_fio']);
        $templateProcessor->setValue('АУФамилияИнициалы', $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.');

        //${ФИОАУПолноеРП}
        $templateProcessor->setValue('ФИОАУПолноеРП', $this->_getAccusativeFIO($proc_params['au_id']));

        // АУФамилияИнициалыДП
        //$lastname = $this->lastname(Petrovich::CASE_INSTRUMENTAL);
        $templateProcessor->setValue('АУКому', $this->_getDativeShortFIO($proc_params['au_id'])); //$lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.');

        $templateProcessor->setValue('АУФамилияИнициалыТП', $this->_getInstrumentalShortFIO($proc_params['au_id']));

        // ${ВводПроцПубликация} в газете «Коммерсантъ», № 93 от 28.05.2016, стр. 49 сообщение № 77031876447
        $templateProcessor->setValue('ВводПроцПубликация', $proc_params['proc_publication']);

        // ЕФРСБСообщение
        $templateProcessor->setValue('ЕФРСБСообщение', ( $proc_params['efrsb_post_number'] ? 'в сообщении  №' . $proc_params['efrsb_post_number'] : '' ) .
            ( $proc_params['efrsb_date'] ? ', ' . $proc_params['efrsb_date'] . ' года' : '' ) );

        $templateProcessor->setValue('ФИОАУПолное', $proc_params['au_fio']);
        $templateProcessor->setValue('АУАдрКор', $proc_params['au_post_address']);
        $templateProcessor->setValue('АУМобТелефон', $proc_params['au_mobile']);
        $templateProcessor->setValue('АУПочта', $proc_params['au_email']);
        $templateProcessor->setValue('АУИНН', $proc_params['au_inn']);
        $templateProcessor->setValue('АУСНИЛС', $proc_params['au_snils']);
        $templateProcessor->setValue('ПроцСуд', $proc_params['court_name']);
        $templateProcessor->setValue('Проц№Дела', $proc_params['case_number']);
        $templateProcessor->setValue('ПроцДатаСудАкт', $proc_params['create_date']);
        $templateProcessor->setValue('ПроцДатаНазнАУ', $proc_params['manager_approval_date']);
        $templateProcessor->setValue('АУСРО', $proc_params['au_cro']);
        $templateProcessor->setValue('АУСРОНомДата', $proc_params['au_cro_num_date']);
        $templateProcessor->setValue('АУСтрахНаим', $proc_params['au_insurance_company_name']);
        $templateProcessor->setValue('АУСтрахНомДог', $proc_params['au_insurance_register_data']);

        // Мероприятия АУ
        if( $proc_params['outgoing_and_backoffice'] ):
            try {
                $templateProcessor->cloneRow('МеропрАУ№п/п', count($proc_params['outgoing_and_backoffice']));

                foreach ($proc_params['outgoing_and_backoffice'] as $key => $m):
                    $templateProcessor->setValue('МеропрАУ№п/п#' . ($key + 1), ($key + 2));
                    $templateProcessor->setValue('МеропрАУМероприятие#' . ($key + 1), $m['event']);
                    $templateProcessor->setValue('МеропрАУРезультат#' . ($key + 1), $m['result']);
                endforeach;
            } catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
                // do nothing
            }

        else:
            $templateProcessor->setValue('МеропрАУ№п/п', '');
            $templateProcessor->setValue('МеропрАУМероприятие', '');
            $templateProcessor->setValue('МеропрАУРезультат', '');
        endif;

        // ЮрЗаписьДата ЮрЗаписьМероп ЮрЗаписьРезульт
        if( $proc_params['legal_services'] ):
            try {
                $templateProcessor->cloneRow('ЮрЗаписьДата', count($proc_params['legal_services']));

                foreach ($proc_params['legal_services'] as $key => $ls):
                    $templateProcessor->setValue('ЮрЗаписьДата#' . ($key + 1), Yii::$app->formatter->asDate($ls['data_time'], 'php:d.m.Y'));
                    $templateProcessor->setValue('ЮрЗаписьМероп#' . ($key + 1), $ls['event']);
                    $templateProcessor->setValue('ЮрЗаписьРезульт#' . ($key + 1), $ls['result']);
                endforeach;
            } catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
                // do nothing
            }
        else:
            $templateProcessor->setValue('ЮрЗаписьДата', '');
            $templateProcessor->setValue('ЮрЗаписьМероп', '');
            $templateProcessor->setValue('ЮрЗаписьРезульт', '');
        endif;

        // ДССЗ
        $templateProcessor->setValue('ДССЗ', $proc_params['date_dssz']);
        // ПроцСтрахНаим
        $templateProcessor->setValue('ПроцСтрахНаим', $proc_params['insurance_company_name']);

        // ПроцСтрахНомДог
        $templateProcessor->setValue('ПроцСтрахНомДог', $proc_params['insurance_company_data']);

        // АУСРОПолн
        $templateProcessor->setValue('АУСРОПолн', $proc_params['au_cro_full']);
        // АУСРОАдрес
        $templateProcessor->setValue('АУСРОАдрес', $proc_params['au_cro_post_address']);

        $path = Yii::getAlias('@app') .'/web/uploads/templates/'.$tpl->name;
        $templateProcessor->saveAs($path);
        $this->_file_force_download($path);
    }

    /**
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * Формирует шаблон доверенностей
     */
    public function actionMakeDover()
    {
        $org_params = [];
        
        $model = new DoverParamsForm();
        if( $model->load(Yii::$app->request->post()) && $model->validate() ):
            // пытаемсо обработать и сохранить шаблон
            $tpl = DocTemplates::findOne(Yii::$app->request->post('DoverParamsForm')['tpl_id']);

            $templateProcessor = new TemplateProcessor($tpl->path);

            list($day, $month, $year) = explode('.', date('d.m.Y'));
            $templateProcessor->setValue('ДатаПрописью', $this->_days[(int)$day] . ' ' . $this->_months[(int)$month] . ' две тысячи ' . $this->_decs[($year - 2000)] . ' года');

            $proc_params = $this->getCurrentProcInfo();

            // creating outgoing document && setting letter variable in template
            $this->registerLetterVariable($templateProcessor, $tpl->name, $this->_getAccusativeFIO($model->for_whom), $proc_params['au_envelope_address']);

            $templateProcessor->setValue('ПДПолноеНазвание', $proc_params['org_full_name']);
            $templateProcessor->setValue('ОГРНПредприятия', $proc_params['org_ogrn']);
            $templateProcessor->setValue('ИННПредприятия', $proc_params['org_inn']);
            $templateProcessor->setValue('ПДЮрАдрес', $proc_params['org_legal_address']);

            $templateProcessor->setValue('ФИОАУПолное', $this->_getGenitiveFIO($proc_params['au_id']));
            $templateProcessor->setValue('ФИОКому', $this->_getAccusativeFIO(Yii::$app->request->post('DoverParamsForm')['for_whom']));

            $templateProcessor->setValue('АУКому', $this->_getDativeFIO($proc_params['au_id']));
            $templateProcessor->setValue('АУПочта', $proc_params['au_email']);
            $templateProcessor->setValue('АУИНН', $proc_params['au_inn']);
            $templateProcessor->setValue('АУСНИЛС', $proc_params['au_snils']);
            $templateProcessor->setValue('АУАдрКор', $proc_params['au_post_address']);
            $templateProcessor->setValue('АУМобТелефон', $proc_params['au_mobile']);
            $templateProcessor->setValue('АУСРО', $proc_params['au_cro']);
            $templateProcessor->setValue('АУСРОПолн', $proc_params['au_cro_full']);
            $templateProcessor->setValue('АУСРОАдрес', $proc_params['au_cro_post_address']);
            $templateProcessor->setValue('АУСРОНомДата', $proc_params['au_cro_num_date']);
            $templateProcessor->setValue('АУСтрахНаим', $proc_params['au_insurance_company_name']);
            $templateProcessor->setValue('АУСтрахНомДог', $proc_params['au_insurance_register_data']);

            $templateProcessor->setValue('ОснованиеАУ', Yii::$app->request->post('DoverParamsForm')['basis']);
            
            $man_params = $this->getManInfo(Yii::$app->request->post('DoverParamsForm')['for_whom']);
            $templateProcessor->setValue('УдостДок', $man_params['certificate']);
            $templateProcessor->setValue('УдостДок№Серия', $man_params['cert_series'] . ' №' . $man_params['cert_number']);
            $templateProcessor->setValue('УдостДокВыдан', $man_params['cert_given']);
            $templateProcessor->setValue('АдрессДляКорреспонденцииКому', $man_params['post_address']);

            $actions_str = file_get_contents(Yii::getAlias('@app') . '/modules/printdocs/assets/js/dover_actions.json');
            $arr = Json::decode($actions_str)['actions'];

            foreach ($arr as $key => $action):
                $actions[$key] = $action['value'];
            endforeach;

            try {
                $templateProcessor->cloneRow('Действие', count(Yii::$app->request->post('DoverParamsForm')['actions']));

                foreach (Yii::$app->request->post('DoverParamsForm')['actions'] as $key => $value):
                    $templateProcessor->setValue('Действие#' . ($key + 1), $actions[$value]);
                endforeach;
            } catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
                // do nothing
            }

            $templateProcessor->setValue('ПДНазвание', $proc_params['org_name']);
            
            list($lastname, $firstname, $patronymic) = explode(' ', $proc_params['au_fio']);
            $templateProcessor->setValue('АУФамилияИнициалы', $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.');

            //$templateProcessor->setValue('for_whom', Yii::$app->request->post('DoverParamsForm')['for_whom_fio']);
            //$templateProcessor->setValue('on_whom', Yii::$app->request->post('DoverParamsForm')['on_whom_fio']);
            //$templateProcessor->setValue('date', Yii::$app->request->post('DoverParamsForm')['date']);       

            $path = Yii::getAlias('@app') .'/web/uploads/templates/'.$tpl->name;
            $templateProcessor->saveAs($path);
            $this->_file_force_download($path);
        else:
            $errors = $model->getErrors();
            $str = '';

            if( !empty($errors) ):
                foreach ($errors as $value) {
                    foreach( $value as $v ):
                            $str .= '<br>' . $v;
                    endforeach;
                }
            endif;

            $result = ['error' => $str];
        endif;
    }

    /*public function actionDownloadTemplate(){
        echo '<pre>'.print_r(Yii::$app->request->post(),true).'</pre>';
    }*/

    /**
     * Формирует шаблон Сбор сведений о должнике
     */
    public function actionMakeCollectDebtorInfo() {
        $model = new CollectDebtorInfoParamsForm();
        if( $model->load(Yii::$app->request->post()) && $model->validate() ):

            $tpl = DocTemplates::findOne(Yii::$app->request->post('CollectDebtorInfoParamsForm')['tpl_id']);

            $templateProcessor = new TemplateProcessor($tpl->path);

            list($day, $month, $year) = explode('.', date('d.m.Y'));
            $templateProcessor->setValue('ТекДата', $day . ' ' . $this->_months[(int)$month] . ' ' . $year . ' г.');

            $proc_params = $this->getCurrentProcInfo();

            // creating outgoing document && setting letter variable in template
            $this->registerLetterVariable($templateProcessor, $tpl->name, $model->addressee, $proc_params['au_envelope_address']);

            $templateProcessor->setValue('ПДНазвание', $proc_params['org_name']);
            $templateProcessor->setValue('ФИОАУПолное', $proc_params['au_fio']);
            $templateProcessor->setValue('АУАдрКор', $proc_params['au_post_address']);
            $templateProcessor->setValue('АУМобТелефон', $proc_params['au_mobile']);
            $templateProcessor->setValue('АУПочта', $proc_params['au_email']);
            $templateProcessor->setValue('АУИНН', $proc_params['au_inn']);
            $templateProcessor->setValue('АУСНИЛС', $proc_params['au_snils']);
            $templateProcessor->setValue('ПроцПред', $proc_params['introduction_background']);
            $templateProcessor->setValue('ПроцСудРП', $proc_params['court_name_rp']);
            $templateProcessor->setValue('Проц№Дела', $proc_params['case_number']);
            $templateProcessor->setValue('ПроцДатаСудАкт', $proc_params['create_date']);
            $templateProcessor->setValue('АУСРО', $proc_params['au_cro']);
            $templateProcessor->setValue('АУСРОПолн', $proc_params['au_cro_full']);
            $templateProcessor->setValue('АУСРОАдрес', $proc_params['au_cro_post_address']);
            $templateProcessor->setValue('АУСРОНомДата', $proc_params['au_cro_num_date']);
            $templateProcessor->setValue('АУСтрахНаим', $proc_params['au_insurance_company_name']);
            $templateProcessor->setValue('АУСтрахНомДог', $proc_params['au_insurance_register_data']);
            $templateProcessor->setValue('ИННПредприятия', $proc_params['org_inn']);
            $templateProcessor->setValue('ПДЮрАдрес', $proc_params['org_legal_address']);
            $templateProcessor->setValue('ОГРНПредприятия', $proc_params['org_ogrn']);
            $templateProcessor->setValue('ПроцОснов', $proc_params['introduction_basis']);
            $templateProcessor->setValue('ИННПредприятия', $proc_params['org_inn']);
            $templateProcessor->setValue('ПДНазваниеСокр', $proc_params['org_short_name']);

            list($lastname, $firstname, $patronymic) = explode(' ', $proc_params['au_fio']);
            $templateProcessor->setValue('АУФамилияИнициалы', $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.');

            // АУФамилияИнициалыДП
            //$lastname = $this->lastname(Petrovich::CASE_INSTRUMENTAL);
            //$templateProcessor->setValue('АУКому', $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.');
            $templateProcessor->setValue('АУКому', $this->_getDativeShortFIO($proc_params['au_id']));

            $templateProcessor->setValue('АУФамилияИнициалыТП', $this->_getInstrumentalShortFIO($proc_params['au_id']));

            // ${ВводПроцПубликация} в газете «Коммерсантъ», № 93 от 28.05.2016, стр. 49 сообщение № 77031876447
            $templateProcessor->setValue('ВводПроцПубликация', $proc_params['proc_publication']);

            // ЕФРСБСообщение
            $templateProcessor->setValue('ЕФРСБСообщение', ( $proc_params['efrsb_post_number'] ? 'в сообщении  №' . $proc_params['efrsb_post_number'] : '' ) .
                ( $proc_params['efrsb_date'] ? ', ' . $proc_params['efrsb_date'] . ' года' : '' ) );

            if( $addressee = TblOrganizationsMethods::getOrganization( Yii::$app->request->post('CollectDebtorInfoParamsForm')['addressee_id'] ) ):
                switch( $addressee['organizations_types_id'] ):
                    /*case 6:
                        $templateProcessor->setValue( 'Адресат', TblValues::find()->where(['params_id' => 108, 'organizations_id' => $addressee['id']])->one()->value );
                        break;*/
                    case 7:
                    case 11:
                        $templateProcessor->setValue( 'Адресат', AppHelper::getFIO($addressee['id']) );
                        break;
                    default:
                        if( $params = TblOrganizationsMethods::getParams( $addressee['id'] ) ):
                            foreach( $params as $param ):
                                if( in_array( 'Название', $param ) ):
                                    $templateProcessor->setValue( 'Адресат', $param['value'] );
                                    break;
                                endif;
                            endforeach;
                        endif;
                endswitch;

                $templateProcessor->setValue('АдресАдресатаДляКорреспонденции', ( $addressee['address_2_postcode'] ? $addressee['address_2_postcode'] . ', ' : '' ) .
                    ( $addressee['address_2_region'] ? $addressee['address_2_region'] . ', ' : '' ) .
                    ( $addressee['address_2_locality'] ? $addressee['address_2_locality'] . ', ' : '' ) .
                    $addressee['address_2_address'] );
            endif;

            $path = Yii::getAlias('@app') . '/web/uploads/templates/' . $tpl->name;
            $templateProcessor->saveAs($path);
            $this->_file_force_download($path);
        else:
            $errors = $model->getErrors();
            $str = '';

            if( !empty($errors) ):
                foreach ($errors as $value) {
                    foreach( $value as $v ):
                        $str .= '<br>' . $v;
                    endforeach;
                }
            endif;

            $result = ['error' => $str];
        endif;
    }

    /**
     * Формирует шаблон Отчет о проведении процедуры
     */
    public function actionMakeProcHolding() {

        $tpl = DocTemplates::findOne(Yii::$app->request->post('tpl_id'));

        $templateProcessor = new TemplateProcessor($tpl->path);

        list($day, $month, $year) = explode('.', date('d.m.Y'));
        $templateProcessor->setValue('ТекДата', $day . ' ' . $this->_months[(int)$month] . ' ' . $year . ' г.');

        $proc_params = $this->getCurrentProcInfo();
        $templateProcessor->setValue('ПДНазвание', $proc_params['org_name']);
        $templateProcessor->setValue('ПДНазваниеПолное', $proc_params['org_full_name']);
        $templateProcessor->setValue('ПДНазваниеСокр', $proc_params['org_short_name']);
        $templateProcessor->setValue('ОГРНПредприятия', $proc_params['org_ogrn']);
        $templateProcessor->setValue('ИННПредприятия', $proc_params['org_inn']);
        $templateProcessor->setValue('КПППредприятия', $proc_params['org_kpp']);
        $templateProcessor->setValue('ОКВЭДПредприятия', $proc_params['org_okved']);
        $templateProcessor->setValue('ПДЮрАдрес', $proc_params['org_legal_address']);
        $templateProcessor->setValue('ФИОАУПолное', $proc_params['au_fio']);
        $templateProcessor->setValue('ПроцПред', $proc_params['introduction_background']);
        $templateProcessor->setValue('ПроцОснов', $proc_params['introduction_basis']);

        list($lastname, $firstname, $patronymic) = explode(' ', $proc_params['au_fio']);
        $templateProcessor->setValue('АУФамилияИнициалы', $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.');

        //${ФИОАУПолноеРП}
        $templateProcessor->setValue('ФИОАУПолноеРП', $this->_getAccusativeFIO($proc_params['au_id']));

        // АУФамилияИнициалыТП
        //$lastname = $this->lastname(Petrovich::CASE_INSTRUMENTAL);
        //$templateProcessor->setValue('АУКому', $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . mb_substr($patronymic, 0, 1) . '.');
        $templateProcessor->setValue('АУФамилияИнициалыТП', $this->_getInstrumentalShortFIO($proc_params['au_id']));
        $templateProcessor->setValue('АУКому', $this->_getDativeFIO($proc_params['au_id']));

        // ${ВводПроцПубликация} в газете «Коммерсантъ», № 93 от 28.05.2016, стр. 49 сообщение № 77031876447
        $templateProcessor->setValue('ВводПроцПубликация', $proc_params['proc_publication']);

        $templateProcessor->setValue('АУПочта', $proc_params['au_email']);
        $templateProcessor->setValue('АУИНН', $proc_params['au_inn']);
        $templateProcessor->setValue('АУСНИЛС', $proc_params['au_snils']);
        $templateProcessor->setValue('АУМобТелефон', $proc_params['au_mobile']);

        // ЕФРСБСообщение
        $templateProcessor->setValue('ЕФРСБСообщение', ( $proc_params['efrsb_post_number'] ? 'в сообщении  №' . $proc_params['efrsb_post_number'] : '' ) .
            ( $proc_params['efrsb_date'] ? ', ' . $proc_params['efrsb_date'] . ' года' : '' ) );

        $templateProcessor->setValue('АУАдрКор', $proc_params['au_post_address']);
        $templateProcessor->setValue('ПроцСуд', $proc_params['court_name']);
        $templateProcessor->setValue('Проц№Дела', $proc_params['case_number']);
        $templateProcessor->setValue('ПроцДатаСудАкт', $proc_params['create_date']);
        $templateProcessor->setValue('ПроцДатаНазнАУ', $proc_params['manager_approval_date']);
        $templateProcessor->setValue('АУСРО', $proc_params['au_cro']);
        $templateProcessor->setValue('АУСРОНомДата', $proc_params['au_cro_num_date']);
        $templateProcessor->setValue('АУСтрахНаим', $proc_params['au_insurance_company_name']);
        $templateProcessor->setValue('АУСтрахНомДог', $proc_params['au_insurance_register_data']);

        // Мероприятия АУ
        if( $proc_params['outgoing_and_backoffice'] ):
            try{
                $templateProcessor->cloneRow('МеропрАУ№п/п', count($proc_params['outgoing_and_backoffice']));

                foreach($proc_params['outgoing_and_backoffice'] as $key => $m):
                    //echo '<pre>'; print_r($m); echo '</pre>'; 
                    $templateProcessor->setValue('МеропрАУ№п/п#' . ($key + 1), ($key + 2));
                    $templateProcessor->setValue('МеропрАУМероприятие#' . ($key + 1), $m['event']);
                    $templateProcessor->setValue('МеропрАУРезультат#' . ($key + 1), $m['result']);
                    $templateProcessor->setValue('СсылкаНаПриложение#' . ($key + 1), $m['notes']);
                endforeach;
                //exit();
            }
            catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
                // do nothing
            }
        else:
            $templateProcessor->setValue('МеропрАУ№п/п', '');
            $templateProcessor->setValue('МеропрАУМероприятие', '');
            $templateProcessor->setValue('МеропрАУРезультат', '');
            $templateProcessor->setValue('СсылкаНаПриложение', '');
        endif;

        // ЮрЗаписьДата ЮрЗаписьМероп ЮрЗаписьРезульт
        if( $proc_params['legal_services'] ):
            try {
                $templateProcessor->cloneRow('ЮрЗаписьДата', count($proc_params['legal_services']));

                foreach ($proc_params['legal_services'] as $key => $ls):
                    $templateProcessor->setValue('ЮрЗаписьДата#' . ($key + 1), Yii::$app->formatter->asDate($ls['data_time'], 'php:d.m.Y'));
                    $templateProcessor->setValue('ЮрЗаписьМероп#' . ($key + 1), $ls['event']);
                    $templateProcessor->setValue('ЮрЗаписьРезульт#' . ($key + 1), $ls['result']);
                endforeach;
            } catch ( \PhpOffice\PhpWord\Exception\Exception $e ) {
                // do nothing
            }
        else:
            $templateProcessor->setValue('ЮрЗаписьДата', '');
            $templateProcessor->setValue('ЮрЗаписьМероп', '');
            $templateProcessor->setValue('ЮрЗаписьРезульт', '');
        endif;

        // ДССЗ
        $templateProcessor->setValue('ДССЗ', $proc_params['date_dssz']);
        // ПроцСтрахНаим
        $templateProcessor->setValue('ПроцСтрахНаим', $proc_params['insurance_company_name']);

        // ПроцСтрахНомДог
        $templateProcessor->setValue('ПроцСтрахНомДог', $proc_params['insurance_company_data']);

        // АУСРОПолн
        $templateProcessor->setValue('АУСРОПолн', $proc_params['au_cro_full']);
        // АУСРОАдрес
        $templateProcessor->setValue('АУСРОАдрес', $proc_params['au_cro_post_address']);


        $path = Yii::getAlias('@app') .'/web/uploads/templates/'.$tpl->name;
        $templateProcessor->saveAs($path);
        $this->_file_force_download($path);

    }
    
    private function _file_force_download($file) {
        if (file_exists($file)) {
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
              ob_end_clean();
            }
            $arr = explode('/', $file);
            $filename = str_replace(' ', '_', array_pop($arr));
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            //header('Content-Type: application/octet-stream');
            header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment; filename=' . $filename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            readfile($file);
            unlink($file);
            exit;
        }
    }
    
    public function getCurrentProcInfo()
    {
        $proc_params = [];
        $procedure = Procedures::findOne(AppHelper::getLastSeenProcedureID());

        if( is_object($procedure) && !empty($procedure) ):
        
            if( $params = TblOrganizationsMethods::getParams( $procedure->organization_id ) ):

                //exit('<pre>'.print_r($params,true).'</pre>');
                
                // Начальное присваивание (зануление)
                $proc_params['org_full_name'] = '';
                $proc_params['org_name'] = '';
                $proc_params['org_short_name'] = '';
                $proc_params['org_ogrn'] = '';
                $proc_params['org_inn'] = '';
                $proc_params['org_okved'] = '';
                $proc_params['org_kpp'] = '';
                
                foreach( $params as $param ):
                    if( in_array( 'Полное наименование', $param ) ):
                        $proc_params['org_full_name'] = $param['value'];
                    endif;

                    if( in_array( 'Название', $param ) ):
                        $proc_params['org_name'] = $param['value'];
                    endif;

                    if( in_array( 'Сокр. наименование', $param ) ):
                        $proc_params['org_short_name'] = $param['value'];
                    endif;

                    if( in_array( 'ОГРН', $param ) ):
                        $proc_params['org_ogrn'] = $param['value'];
                    endif;

                    if( in_array( 'ИНН', $param ) ):
                        $proc_params['org_inn'] = $param['value'];
                    endif;

                    if( in_array( 'ОКВЭД', $param ) ):
                        $proc_params['org_okved'] = $param['value'];
                    endif;

                    if( in_array( 'КПП', $param ) ):
                        $proc_params['org_kpp'] = $param['value'];
                    endif;
                endforeach;
            endif;

            // юридический адрес ПД
            $org = TblOrganizationsMethods::getOrganization( $procedure->organization_id );

            $proc_params['org_legal_address'] = ( $org['address_0_postcode'] ? $org['address_0_postcode'] . ', ' : '' ) .
                    ( $org['address_0_region'] ? $org['address_0_region'] . ', ' : '' ) .
                    ( $org['address_0_locality'] ? $org['address_0_locality'] . ', ' : '' ) .
                    $org['address_0_address'];

            // address for outgoing documents
            $proc_params['au_envelope_address'] = AddressHelper::build([
                @$org['address_0_postcode'],
                @$org['address_0_address'],
                @$org['address_0_region'],
                @$org['address_0_locality'],
            ]);

            // ФИО АУ
            $proc_params['au_fio'] = AppHelper::getFIO( $procedure->manager_id );
            $proc_params['au_id'] = $procedure->manager_id;

            // адрес для корреспонденции АУ
            $arr = TblOrganizationsMethods::getOrganization( $procedure->manager_id );
            $proc_params['au_post_address'] = ( $arr['address_2_postcode'] ? $arr['address_2_postcode'] . ', ' : '' ) .
                ( $arr['address_2_region'] ? $arr['address_2_region'] . ', ' : '' ) .
                ( $arr['address_2_locality'] ? $arr['address_2_locality'] . ', ' : '' ) .
                $arr['address_2_address'];

            $proc_params['au_phone'] = $arr['phone'];
            $proc_params['au_mobile'] = $arr['mobile'];
            $proc_params['au_email'] = $arr['email'];
            $proc_params['au_inn'] = TblValues::find()->where(['params_id' => 90, 'organizations_id' => $procedure->manager_id])->one()->value;
            $proc_params['au_snils'] = TblValues::find()->where(['params_id' => 92, 'organizations_id' => $procedure->manager_id])->one()->value;

            // название суда
            $court_name = TblValues::find()->where(['params_id' => 104, 'organizations_id' => $procedure->court_id])->one()->value;
            $proc_params['court_name'] = ( $court_name ? $court_name : '' );
            $court_name_rp = TblValues::find()->where(['params_id' => 18, 'organizations_id' => $procedure->court_id])->one()->value;
            $proc_params['court_name_rp'] = ( $court_name_rp ? $court_name_rp : '' );
            /*if( $params = TblOrganizationsMethods::getParams( $procedure->court_id ) ):

                //exit('<pre>'.print_r($params,true).'</pre>');
                foreach( $params as $param ):
                    if( in_array( 'Название', $param ) ):
                        $proc_params['court_name'] = $param['value'];
                        break;
                    endif;
                endforeach;
            endif;*/

            // основание введения процедуры
            $proc_additional_properties = ProceduresAdditionalProperties::find()->where(['proc_id' => $procedure->id])->one();
            if( is_object( $proc_additional_properties ) ):
                $proc_params['introduction_basis'] = $proc_additional_properties->proc_introduction_reason;
                $proc_params['introduction_background'] = $proc_additional_properties->proc_introduction_background;

                // ${ВводПроцПубликация}
                if( $proc_additional_properties->np_id ):
                    //$params = TblOrganizationsMethods::getParams( $proc_additional_properties->np_id );
                    $np_name = TblValues::find()->where(['params_id' => 12, 'organizations_id' => $proc_additional_properties->np_id])->one()->value;
                    $proc_params['proc_publication'] = $np_name .
                        ( $proc_additional_properties->np_number ? ', №' . $proc_additional_properties->np_number : '' ) .
                        ( $proc_additional_properties->np_date ? ' от ' . Yii::$app->formatter->asDate( $proc_additional_properties->np_date, 'php:d.m.Y') : '' ) .
                        ( $proc_additional_properties->np_page_number ? ', стр. ' . $proc_additional_properties->np_page_number : '' ) .
                        ( $proc_additional_properties->np_post_number ? ' сообщение №' . $proc_additional_properties->np_post_number : '' );
                else:
                    $proc_params['proc_publication'] = '';
                endif;

                $proc_params['efrsb_date'] = ( $proc_additional_properties->efrsb_date ? Yii::$app->formatter->asDate( $proc_additional_properties->efrsb_date, 'php:d.m.Y') : '' );
                $proc_params['efrsb_post_number'] = ( $proc_additional_properties->efrsb_post_number ? $proc_additional_properties->efrsb_post_number : '' );

                $proc_params['insurance_company_name'] = ( $proc_additional_properties->insurance_company_name ? $proc_additional_properties->insurance_company_name : '' );
                $proc_params['insurance_company_data'] = ( $proc_additional_properties->insurance_register_data ? $proc_additional_properties->insurance_register_data : '' );
            else:
                $proc_params['introduction_basis'] = '';
                $proc_params['proc_publication'] = '';
                $proc_params['introduction_background'] = '';

                $proc_params['efrsb_date'] = '';
                $proc_params['efrsb_post_number'] = '';

                $proc_params['insurance_company_name'] = '';
                $proc_params['insurance_company_data'] = '';
            endif;

            // номер дела
            $proc_params['case_number'] = $procedure->case_number;

            // ПроцДатаСудАкт
            $proc_params['create_date'] = Yii::$app->formatter->asDate( $procedure->create_date, 'php:d.m.Y');

            // ПроцДатаНазнАУ
            $proc_params['manager_approval_date'] = Yii::$app->formatter->asDate( $procedure->manager_approval_date, 'php:d.m.Y');

            // АУСРО
            if( $cro_id = TblValues::find()->where(['params_id' => 117, 'organizations_id' => $procedure->manager_id])->one()->value ):
                if( is_object(TblValues::find()->where(['params_id' => 106, 'organizations_id' => $cro_id])->one()) ):
                    $proc_params['au_cro'] = TblValues::find()->where(['params_id' => 106, 'organizations_id' => $cro_id])->one()->value;
                else:
                    $proc_params['au_cro'] = '';
                endif;

                if( is_object(TblValues::find()->where(['params_id' => 24, 'organizations_id' => $cro_id])->one()) ):
                    $proc_params['au_cro_full'] = TblValues::find()->where(['params_id' => 24, 'organizations_id' => $cro_id])->one()->value;
                else:
                    $proc_params['au_cro_full'] = '';
                endif;

                $cro = TblOrganizationsMethods::getOrganization( $cro_id );

                $proc_params['au_cro_post_address'] = ( $cro['address_2_postcode'] ? $cro['address_2_postcode'] . ', ' : '' ) .
                    ( $cro['address_2_region'] ? $cro['address_2_region'] . ', ' : '' ) .
                    ( $cro['address_2_locality'] ? $cro['address_2_locality'] . ', ' : '' ) .
                    $cro['address_2_address'];
            else:
                $proc_params['au_cro'] = '';
                $proc_params['au_cro_full'] = '';
                $proc_params['au_cro_post_address'] = '';
            endif;

            // АУСРОНомДата
            $proc_params['au_cro_num_date'] = TblValues::find()->where(['params_id' => 93, 'organizations_id' => $procedure->manager_id])->one()->value;

            //exit('<pre>'.print_r(TblValues::find()->where(['params_id' => 94, 'organizations_id' => $procedure->manager_id])->one(), true).'</pre>');

            // АУСтрахНаим
            $ensurance_company_name = TblValues::find()->where(['params_id' => 94, 'organizations_id' => $procedure->manager_id])->one()->value;
            $proc_params['au_insurance_company_name'] = ( $ensurance_company_name ? $ensurance_company_name : '' );

            // АУСтрахНомДог
            $insurance_register_data = TblValues::find()->where(['params_id' => 95, 'organizations_id' => $procedure->manager_id])->one()->value;
            $proc_params['au_insurance_register_data'] = ( $insurance_register_data ? $insurance_register_data : '' );

            $proc_params['legal_services'] = ( Yii::$app->request->post('ls-grid-items') ?
                                                LegalService::find()
                                                    ->where(['proc_id' => $procedure->id])
                                                    ->andWhere('id in (' . implode(',', Yii::$app->request->post('ls-grid-items')) . ')')
                                                    ->orderBy('data_time')
                                                    ->all() : '' );

           /* $outgoing_docs_query = ( new Query() )
                ->select( ['ev_date' => 'o.send_date', 'event' => "concat('Направлен(о) ', dto1.name, ' в ', o.contractor, ' исх. №', o.serial_number,
                        ' от ', date_format(o.send_date,'%d.%m.%Y'), 'г. ', o.name )",
                        'result' => "if( i.id is not null, concat( 'Поступил ',  dto2.name, ' ', i.name, ' от ', i.contractor ), '')",
                        'notes' => "concat_ws(CHAR(10 using utf8), o.notes, i.notes)"] )
                ->from( OutgoingDocs::tableName() . " as o")
                ->leftJoin( DocTypes::tableName() . " as dto1", 'dto1.id = o.request_type_id')
                ->leftJoin( IncomingDocs::tableName() . " as i", 'o.id = i.response_on_id' )
                ->leftJoin( DocTypes::tableName() . " as dto2", 'dto2.id = i.response_type_id')
                ->where( ['o.proc_id' => AppHelper::getLastSeenProcedureID() ] )
                ->andWhere('o.id in (' . ( Yii::$app->request->post('od-grid-items') ? implode(',', Yii::$app->request->post('od-grid-items')) : '-1' ) . ')');

            $bo_query = ( new Query() )
                ->select( /*['ev_date' => "bo.data_time", "event" => 'CONCAT_WS(" ", date_format(bo.data_time,\'%d.%m.%Y\'), event)', "result", 'notes' => 'null']*/
                  //  "bo.data_time as ev_date, CONCAT_WS(' ', date_format(bo.data_time,'%d.%m.%Y'), event) as 'event', result, null as 'notes'")
                //->from( BackOffice::tableName() . ' as `bo`' )
               // ->where(['bo.proc_id' => AppHelper::getLastSeenProcedureID()])
               // ->andWhere('bo.id in (' . ( Yii::$app->request->post('bo-grid-items') ? implode(',', Yii::$app->request->post('bo-grid-items')) : '-1' ) . ')');

            //$outgoing_docs_query->union( $bo_query, false );
            //$sql = $outgoing_docs_query->createCommand()->getRawSql();
            //$sql .= ' order by ev_date';*/

            $sql = 
                " select 
                    o.send_date as ev_date, 
                    concat(
                        'Направлен(о) ', ifnull(dto1.name,''), 
                        ' в ', ifnull(o.contractor,''), 
                        ' исх. №', ifnull(o.serial_number,''), 
                        ' от ', ifnull(date_format(o.send_date,'%d.%m.%Y'),''), 'г. ', ifnull(o.name,'') 
                    ) as event, 
                    if( i.id is not null, concat( 'Поступил(о) ',  ifnull(dto2.name, ''), ' ', ifnull(i.name, ''), ' от ', ifnull(i.contractor,'') ), '') as result,
                    concat_ws( CHAR(10 using utf8), ifnull(o.notes, ''), ifnull(i.notes, '') ) as notes" .
                " from " . OutgoingDocs::tableName() . " as o " .
                " left join " . DocTypes::tableName() . " as dto1 on dto1.id = o.request_type_id " .
                " left join " . IncomingDocs::tableName() . " as i on o.id = i.response_on_id " .
                " left join " . DocTypes::tableName() . " as dto2 on dto2.id = i.response_type_id " .
                " where o.proc_id = " . AppHelper::getLastSeenProcedureID() .
                    " and " . "o.id in (" . ( Yii::$app->request->post('od-grid-items') ? implode(',', Yii::$app->request->post('od-grid-items')) : '-1' ) . ") " .
            
                " union " .
                    
                " select 
                    i.receive_date as ev_date, 
                    concat(
                        'Получен(о) ', ifnull(dto1.name,''), 
                        ' от ', ifnull(i.contractor,''), 
                        ' вх. №', ifnull(i.serial_number,''), 
                        ' от ', ifnull(date_format(i.receive_date,'%d.%m.%Y'),''), 'г. ', ifnull(i.name,'') 
                    ) as event, 
                    if( o.id is not null, 
                        concat( 
                            'Отправлен(о) ',  ifnull(dto2.name, ''), 
                            ' №', ifnull(o.serial_number,''), 
                            ' от ', ifnull(date_format(o.send_date,'%d.%m.%Y'),''), 'г. ',
                            ' ', ifnull(o.name, '') 
                            
                            " .
                            //' к ', ifnull(o.contractor,'') 
                            "
                        ), 
                        ''
                    ) as result,
                    concat_ws( CHAR(10 using utf8), ifnull(i.notes, ''), ifnull(o.notes, '') ) as notes" .
                " from " . IncomingDocs::tableName() . " as i " .
                " left join " . DocTypes::tableName() . " as dto1 on dto1.id = i.response_type_id " . 
                " left join " . OutgoingDocs::tableName() . " as o on i.id = o.response_on_id " .
                " left join " . DocTypes::tableName() . " as dto2 on dto2.id = o.request_type_id " .
                " where i.proc_id = " . AppHelper::getLastSeenProcedureID() .
                    " and " . "i.id in (" . ( Yii::$app->request->post('ind-grid-items') ? implode(',', Yii::$app->request->post('ind-grid-items')) : '-1' ) . ") " .
            
                " union " .
                    
                    
                " select bo.data_time as ev_date, CONCAT_WS(' ', ifnull(date_format(bo.data_time,'%d.%m.%Y'), ''), ifnull(event, '')) as event, result, null as notes " .
                " from " . BackOffice::tableName() . " as bo " .
                " where bo.proc_id = " . AppHelper::getLastSeenProcedureID() .
                " and bo.id in (" . ( Yii::$app->request->post('bo-grid-items') ? implode(',', Yii::$app->request->post('bo-grid-items')) : '-1' ) . ')  ' .
                " order by ev_date";

            $proc_params['outgoing_and_backoffice'] = Yii::$app->db->createCommand($sql)->queryAll();

            $proc_prolongation = ProceduresProlongations::find()
                ->select(['date_dssz' => 'date_format(date_dssz,\'%d.%m.%Y\')'])
                ->where(['proc_id' => $procedure->id])
                ->orderBy(['date_dssz' => SORT_DESC])
                ->one();

            if( !is_null($proc_prolongation) ):
                $proc_params['date_dssz'] = $proc_prolongation->date_dssz;
            else:
                $proc_params['date_dssz'] = '';
            endif;

        endif;

        //var_dump($proc_params);
        //exit();
        
        return $proc_params;
    }
    
    public function getManInfo($id) {
        $params = $man_params = [];
        
        if( $params = TblOrganizationsMethods::getParams($id) ):
            foreach( $params as $param ):
                if( in_array( 'Документ подтверждающий личность', $param ) ):
                    $man_params['certificate'] = $param['value'];
                endif;    
                
                if( in_array( 'серия', $param ) ):
                    $man_params['cert_series'] = $param['value'];
                endif; 
                
                if( in_array( 'номер', $param ) ):
                    $man_params['cert_number'] = $param['value'];
                endif;    
                
                if( in_array( 'выдан', $param ) ):
                    $man_params['cert_given'] = $param['value'];
                endif; 
            endforeach; 
        endif;
        
        $arr = TblOrganizationsMethods::getOrganization( $id ); 
        $man_params['post_address'] = ( $arr['address_2_postcode'] ? $arr['address_2_postcode'] . ', ' : '' ) .
                ( $arr['address_2_region'] ? $arr['address_2_region'] . ', ' : '' ) .
                ( $arr['address_2_locality'] ? $arr['address_2_locality'] . ', ' : '' ) .
                $arr['address_2_address'];
        return $man_params;
    }

    protected function _getAccusativeFIO($id)
    {
        $lastname = $firstname = $patronymic = '';

        $arr = explode(' ', AppHelper::getFIO($id));

       // var_dump($arr); echo '<br>';


        if( $arr[2] ):
            $this->gender = null;
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_ACCUSATIVE);
            $firstname = $this->firstname(Petrovich::CASE_ACCUSATIVE);
            $patronymic = $this->middlename(Petrovich::CASE_ACCUSATIVE);
           // echo 'middlename exists<br>';
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );
            /*
              const GENDER_MALE = 1; // Мужской
              const GENDER_FEMALE = 2; // Женский
            */
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_ACCUSATIVE);
            $firstname = $this->firstname(Petrovich::CASE_ACCUSATIVE);
            //$patronymic = $this->middlename(Petrovich::CASE_ACCUSATIVE);
        endif;

       // echo $lastname . ' ' . $firstname . ' ' . $patronymic . '<br>';

        return $lastname . ' ' . $firstname . ' ' . $patronymic;
    }

    protected function _getAccusativeShortFIO($id)
    {
        $lastname = $firstname = $patronymic = '';

        $arr = explode(' ', AppHelper::getFIO($id));

        // var_dump($arr); echo '<br>';


        if( $arr[2] ):
            $this->gender = null;
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_ACCUSATIVE);
            $firstname = $this->firstname(Petrovich::CASE_ACCUSATIVE);
            $patronymic = $this->middlename(Petrovich::CASE_ACCUSATIVE);
        // echo 'middlename exists<br>';
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );
            /*
              const GENDER_MALE = 1; // Мужской
              const GENDER_FEMALE = 2; // Женский
            */
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_ACCUSATIVE);
            $firstname = $this->firstname(Petrovich::CASE_ACCUSATIVE);
            //$patronymic = $this->middlename(Petrovich::CASE_ACCUSATIVE);
        endif;

        // echo $lastname . ' ' . $firstname . ' ' . $patronymic . '<br>';

        return $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . ( $patronymic ? mb_substr($patronymic, 0, 1) . '.' : '' );
    }

    protected function _getGenitiveFIO($id)
    {
        $lastname = $firstname = $patronymic = '';

        $arr = explode(' ', AppHelper::getFIO($id));

         //var_dump($arr); echo '<br>';



        if( !empty($arr[2]) ):
            $this->gender = null;
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_GENITIVE);
            $firstname = $this->firstname(Petrovich::CASE_GENITIVE);
            $patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
         //echo 'middlename exists<br>';
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );
            /*
              const GENDER_MALE = 1; // Мужской
              const GENDER_FEMALE = 2; // Женский
            */
            //echo '<br>printdocs controller gender '.$this->gender.'<br>';

            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_GENITIVE);
            $firstname = $this->firstname(Petrovich::CASE_GENITIVE);
            //$patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
        endif;

       // echo $lastname . ' ' . $firstname . ' ' . $patronymic . '<br>'; exit();

        return $lastname . ' ' . $firstname . ' ' . $patronymic;
    }

    protected function _getGenitiveShortFIO($id)
    {
        $lastname = $firstname = $patronymic = '';

        $arr = explode(' ', AppHelper::getFIO($id));

        //  var_dump($arr); echo '<br>';



        if( !empty($arr[2]) ):
            $this->gender = null;
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_GENITIVE);
            $firstname = $this->firstname(Petrovich::CASE_GENITIVE);
            $patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
        // echo 'middlename exists<br>';
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );
            /*
              const GENDER_MALE = 1; // Мужской
              const GENDER_FEMALE = 2; // Женский
            */
            // echo '<br>printdocs controller gender '.$this->gender.'<br>';

            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_GENITIVE);
            $firstname = $this->firstname(Petrovich::CASE_GENITIVE);
            //$patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
        endif;

        // echo $lastname . ' ' . $firstname . ' ' . $patronymic . '<br>'; exit();

        return $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . ( $patronymic ? mb_substr($patronymic, 0, 1) . '.' : '' );
    }

    protected function _getInstrumentalFIO($id)
    {
        $lastname = $firstname = $patronymic = '';

        $arr = explode(' ', AppHelper::getFIO($id));

        //  var_dump($arr); echo '<br>';



        if( !empty($arr[2]) ):
            $this->gender = null;
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_INSTRUMENTAL);
            $firstname = $this->firstname(Petrovich::CASE_INSTRUMENTAL);
            $patronymic = $this->middlename(Petrovich::CASE_INSTRUMENTAL);
        // echo 'middlename exists<br>';
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );
            /*
              const GENDER_MALE = 1; // Мужской
              const GENDER_FEMALE = 2; // Женский
            */
            // echo '<br>printdocs controller gender '.$this->gender.'<br>';

            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_INSTRUMENTAL);
            $firstname = $this->firstname(Petrovich::CASE_INSTRUMENTAL);
            //$patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
        endif;

        // echo $lastname . ' ' . $firstname . ' ' . $patronymic . '<br>'; exit();

        return $lastname . ' ' . $firstname . ' ' . $patronymic;
    }

    protected function _getInstrumentalShortFIO($id)
    {
        $lastname = $firstname = $patronymic = '';

        $arr = explode(' ', AppHelper::getFIO($id));

        //  var_dump($arr); echo '<br>';



        if( !empty($arr[2]) ):
            $this->gender = null;
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_INSTRUMENTAL);
            $firstname = $this->firstname(Petrovich::CASE_INSTRUMENTAL);
            $patronymic = $this->middlename(Petrovich::CASE_INSTRUMENTAL);
        // echo 'middlename exists<br>';
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );
            /*
              const GENDER_MALE = 1; // Мужской
              const GENDER_FEMALE = 2; // Женский
            */
            // echo '<br>printdocs controller gender '.$this->gender.'<br>';

            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_INSTRUMENTAL);
            $firstname = $this->firstname(Petrovich::CASE_INSTRUMENTAL);
            //$patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
        endif;

        // echo $lastname . ' ' . $firstname . ' ' . $patronymic . '<br>'; exit();

        return $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . ( $patronymic ? mb_substr($patronymic, 0, 1) . '.' : '' );
    }

    protected function _getDativeFIO($id)
    {
        $lastname = $firstname = $patronymic = '';

        $arr = explode(' ', AppHelper::getFIO($id));

        if( !empty($arr[2]) ):
            $this->gender = null;
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_DATIVE);
            $firstname = $this->firstname(Petrovich::CASE_DATIVE);
            $patronymic = $this->middlename(Petrovich::CASE_DATIVE);
        // echo 'middlename exists<br>';
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );
            /*
              const GENDER_MALE = 1; // Мужской
              const GENDER_FEMALE = 2; // Женский
            */
            // echo '<br>printdocs controller gender '.$this->gender.'<br>';

            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_DATIVE);
            $firstname = $this->firstname(Petrovich::CASE_DATIVE);
            //$patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
        endif;

        // echo $lastname . ' ' . $firstname . ' ' . $patronymic . '<br>'; exit();

        return $lastname . ' ' . $firstname . ' ' . $patronymic;
    }

    protected function _getDativeShortFIO($id)
    {
        $lastname = $firstname = $patronymic = '';

        $arr = explode(' ', AppHelper::getFIO($id));

        if( !empty($arr[2]) ):
            $this->gender = null;
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_DATIVE);
            $firstname = $this->firstname(Petrovich::CASE_DATIVE);
            $patronymic = $this->middlename(Petrovich::CASE_DATIVE);
        // echo 'middlename exists<br>';
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );
            /*
              const GENDER_MALE = 1; // Мужской
              const GENDER_FEMALE = 2; // Женский
            */
            // echo '<br>printdocs controller gender '.$this->gender.'<br>';

            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_DATIVE);
            $firstname = $this->firstname(Petrovich::CASE_DATIVE);
            //$patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
        endif;

        // echo $lastname . ' ' . $firstname . ' ' . $patronymic . '<br>'; exit();

        return $lastname . ' ' . mb_substr($firstname, 0, 1) . '.' . ( $patronymic ? mb_substr($patronymic, 0, 1) . '.' : '' );
    }

    public function actionTest($id)
    {
        //list($this->lastname, $this->firstname, $this->middlename) =
        //$lastname = $firstname = $patronymic = '';
        //$org = TblOrganizationsMethods::getParams( $id );
        //echo '<pre>'.print_r($org,true).'</pre>';
       /* $arr = explode(' ', AppHelper::getFIO($id));

        if( $arr[2] ):
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_ACCUSATIVE);
            $firstname = $this->firstname(Petrovich::CASE_ACCUSATIVE);
            $patronymic = $this->middlename(Petrovich::CASE_ACCUSATIVE);
        else:
            $this->gender = ( TblValues::find()->where(['params_id' => 41, 'organizations_id' => $id])->one()->value == 1 ? 1 : 2 );
            list($this->lastname, $this->firstname, $this->middlename) = $arr;
            $lastname = $this->lastname(Petrovich::CASE_ACCUSATIVE);
            $firstname = $this->firstname(Petrovich::CASE_ACCUSATIVE);
            //$patronymic = $this->middlename(Petrovich::CASE_GENITIVE);
        endif;
        echo '|' . $lastname . ' ' . $firstname . ' ' . $patronymic . '|';
        */

        $au = TblOrganizationsMethods::getParams($id);

        echo '<pre>'.print_r($au, true).'</pre>';

        /*echo $this->_getGenitiveFIO($id) . '<br><hr>';
        echo $this->_getGenitiveFIO(73) . '<br><hr>';
        echo $this->_getGenitiveFIO($id) . '<br><hr>';*/

    }

    /**
     * creates outgoing document with given parameters and register letter variable in document template
     * @param $templateProcessor TemplateProcessor
     * @param $name string
     * @param $contractor string
     * @param $address string
     */
    private function registerLetterVariable($templateProcessor, $name, $contractor, $address)
    {
        if (Yii::$app->request->post(self::NAME_CREATE_DOCUMENTS)) {
            $outgoingDocument = $this->createOutgoingDocument($name, $contractor, $address);

            if ($outgoingDocument) {
                $templateProcessor->setValue('№Письма', $outgoingDocument->serial_number);
            }
        }
    }

    /**
     * create outgoing document with given params
     * @param $name string
     * @param $contractor string
     * @param $address string
     * @return OutgoingDocs|null
     */
    private function createOutgoingDocument($name, $contractor, $address)
    {
        $model = new OutgoingDocs();

        $model->setAttributes([
            'serial_number' => $model->getNextSerialNumber(),
            'proc_id' => Yii::$app->session->get('current_proc_id'),
            'name' => $this->buildOutgoingDocName($name),
            'contractor' => $contractor,
            'delivery_address' => $address,
            'send_date' => date('d.m.Y'),
        ]);

        if ($model->validate() && $model->save()) {
            return $model;
        }

        return null;
    }

    /**
     * @param $name
     * @return string
     */
    private function buildOutgoingDocName($name)
    {
        return preg_replace('/\..*?$/ims', '', $name);
    }

    public function actionAddTemplateForm()
    {
        if( Yii::$app->request->isAjax ){
            $tplFormModel = new DownloadTplForm();
            $tplFormModel->group_id = Yii::$app->request->post('id');

            if( Yii::$app->request->post('id') != 8 ) {
                echo $this->renderAjax('_dwn_tpl_form', [
                    'tplFormModel' => $tplFormModel,
                    //'group_id' => Yii::$app->request->post('id'),
                ]);
            } else {
                echo $this->renderAjax('_dwn_meeting_tpl_form', [
                    'tplFormModel' => $tplFormModel,
                    'proc_types' => $this->_proc_types,
                    'template_types' => $this->_meeting_template_types_list,
                    //'proc_types' => ArrayHelper::map(ProcedureTypes::find()->where(['<=', 'id', 3])->asArray()->all(), 'id', 'type'),
                    'question_types' => ArrayHelper::map(MeetingBulletinForms::find()->asArray()->all(), 'id', 'name')
                ]);
            }

        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }


    }
}
