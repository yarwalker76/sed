<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\printdocs\models\DocTemplates */

$this->title = Yii::t('procedures', 'Update {modelClass}: ', [
    'modelClass' => 'Doc Templates',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('procedures', 'Doc Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('procedures', 'Update');
?>
<div class="doc-templates-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
