<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\printdocs\models\DocTemplates */

$this->title = Yii::t('procedures', 'Create Doc Templates');
$this->params['breadcrumbs'][] = ['label' => Yii::t('procedures', 'Doc Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doc-templates-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
