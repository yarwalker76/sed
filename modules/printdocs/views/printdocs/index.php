<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\printdocs\models\DocTemplatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('procedures', 'Doc Templates');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doc-templates-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('procedures', 'Create Doc Templates'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'path',
            'group_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
