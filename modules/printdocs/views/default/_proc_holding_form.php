<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Tabs;


//Pjax::begin(['id' => 'proc-holding-form-wrap', 'enablePushState' => false, 'timeout' => 1000]);

$form = ActiveForm::begin([
    'action' => [ '/' . \Yii::$app->controller->module->id . '/make-proc-holding' ],
    'options' => [
        'id' => 'proc-holding-params-form',
        'data-id' => '',
    ] // important
]);
?>

<?= Tabs::widget([
    'items' => [
        [
            'label' => Yii::t('printdocs', 'OUTGOING_TAB'),
            'content' => GridView::widget([
                'id' => 'od-grid',
                'dataProvider' => $od_dataProvider,
                'layout' => "{items}",
                'showHeader' => true,
                'showFooter' => false,

                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'headerOptions' => [ 'width' => '30' ],
                        'name' => 'od-grid-items',
                        'checkboxOptions' => function ($model, $key, $index, $column) {
                            return ['value' => $model['id'], 'checked' => true];
                        }
                    ],
                    [
                        'attribute' => 'send_date',
                        'format' => ['date', 'php:d.m.Y'],
                        'headerOptions' => [ 'width' => '100' ],
                        'label' => 'Дата',
                        'encodeLabel' => false,
                    ],
                    [
                        'attribute' => 'serial_number',
                        'label' => Yii::t('printdocs', 'SERIAL_NUMBER'),
                    ],
                    [
                        'attribute' => 'contractor',
                        'label' => Yii::t('printdocs', 'ADDRESSEE'),
                    ],
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('printdocs', 'OUTGOING_NAME'),
                    ],
                    [
                        'attribute' => 'incoming_response_on_id',
                        'label' => Yii::t('printdocs', 'OUTGOING_RESPONSE'),
                        'content' => function($data) {
                            return ( !is_null($data['incoming_response_on_id']) ? '-> <-' : '' );
                        },
                    ],

                ],
            ]),
            'active' => true,
        ],
        [
            'label' => Yii::t('printdocs', 'INCOMING_TAB'),
            'content' => GridView::widget([
                'id' => 'ind-grid',
                'dataProvider' => $id_dataProvider,
                'layout' => "{items}",
                'showHeader' => true,
                'showFooter' => false,

                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'headerOptions' => [ 'width' => '30' ],
                        'name' => 'ind-grid-items',
                        'checkboxOptions' => function ($model, $key, $index, $column) {
                            return ['value' => $model['id'], 'checked' => true];
                        }
                    ],
                    [
                        'attribute' => 'receive_date',
                        'format' => ['date', 'php:d.m.Y'],
                        'headerOptions' => [ 'width' => '100' ],
                        'label' => 'Дата',
                        'encodeLabel' => false,
                    ],
                    [
                        'attribute' => 'serial_number',
                        'label' => Yii::t('printdocs', 'SERIAL_NUMBER'),
                    ],
                    [
                        'attribute' => 'contractor',
                        'label' => Yii::t('printdocs', 'ADDRESSEE'),
                    ],
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('printdocs', 'OUTGOING_NAME'),
                    ],
                    /*[
                        'attribute' => 'outgoing_response_on_id',
                        'label' => Yii::t('printdocs', 'OUTGOING_RESPONSE'),
                        'content' => function($data) {
                            return ( !is_null($data['outgoing_response_on_id']) ? '-> <-' : '' );
                        },
                    ],*/

                ],
            ]),
            'active' => false,
        ],
        [
            'label' => Yii::t('menu', 'BackOffice'),
            'content' => GridView::widget([
                            'id' => 'bo-grid',
                            'dataProvider' => $bo_dataProvider,
                            'layout' => "{items}",
                            'showHeader' => true,
                            'showFooter' => false,

                            'columns' => [
                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                    'headerOptions' => [ 'width' => '30' ],
                                    'name' => 'bo-grid-items',
                                    'checkboxOptions' => [ 'checked' => true ],
                                ],
                                [
                                    'attribute' => 'data_time',
                                    'format' => ['date', 'php:d.m.Y'],
                                    'headerOptions' => [ 'width' => '100' ],
                                    'label' => 'Дата',
                                    'encodeLabel' => false,
                                ],
                                'event',
                                'result',
                            ],
                        ]),
        ],
        [
            'label' => Yii::t('menu', 'LegalService'),
            'content' => GridView::widget([
                            'id' => 'ls-grid',
                            'dataProvider' => $ls_dataProvider,
                            'layout'=>"{items}",
                            'showHeader' => true,
                            'showFooter' => false,

                            'columns' => [
                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                    'headerOptions' => [ 'width' => '30' ],
                                    'name' => 'ls-grid-items',
                                    'checkboxOptions' => [ 'checked' => true ],
                                ],
                                [
                                    'attribute' => 'data_time',
                                    'format' => ['date', 'php:d.m.Y'],
                                    'headerOptions' => [ 'width' => '100' ],
                                    'label' => 'Дата',
                                    'encodeLabel' => false,
                                ],
                                'event',
                                'result',
                            ],
                        ]),
        ],

    ],
    'options' => ['tag' => 'div'],
    'itemOptions' => ['tag' => 'div'],
    'headerOptions' => ['class' => 'my-class'],
    'clientOptions' => ['collapsible' => false],
    ]);
?>

<input name="tpl_id" value="<?= $tpl_id ?>" type="hidden" />

<?php
echo Html::submitButton(Yii::t('printdocs', 'GENERATE'), ['class' => 'btn btn-primary']);

ActiveForm::end();

//Pjax::end();
