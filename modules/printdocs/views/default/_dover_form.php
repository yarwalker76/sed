<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use yii\helpers\Json;

//$this->registerAssetBundle(DatePicker::className());
//DatePicker::register($this);

Pjax::begin(['id' => 'dover-form-wrap', 'enablePushState' => false, 'timeout' => 1000]);

$form = ActiveForm::begin([
    'action' => [ '/' . \Yii::$app->controller->module->id . '/make-dover' ],
    'options' => [
        'id' => 'dover-params-form',
        'data-id' => '',
        ] // important
]);
?>

<?= $form->field($model, 'date', ['options' => ['class' => 'col-lg-5 form-group']])
            ->widget(DatePicker::classname(), [
                'options' => [ 'value' => date('d.m.Y') ],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy'
                ]
            ]); ?>
<div class="clearfix"></div>

<?= $form->field($model, 'for_whom_fio', ['options' => ['class' => 'col-lg-8 form-group']])->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
<div class="form-group col-lg-4">
    <?php echo Html::button( Yii::t('printdocs', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-toggle' => 'modal', 'data-type'  => 'for_whom', 'data-target' => '#modalContractors'] ); ?>
</div>
<input type="hidden" id="doverparamsform-for_whom" class="form-control" name="DoverParamsForm[for_whom]">
<div class="clearfix"></div>

<?php //echo $form->field($model, 'on_whom_fio', ['options' => ['class' => 'col-lg-8 form-group']])->textInput(['maxlength' => true]) ?>
<!--div class="form-group col-lg-4">
    <?php //echo Html::button( Yii::t('printdocs', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-type'  => 'on_whom', 'data-toggle' => 'modal', 'data-target' => '#modalContractors'] ); ?>
</div>
<input type="hidden" id="doverparamsform-on_whom" class="form-control" name="DoverParamsForm[on_whom]"-->
<?= $form->field($model, 'basis')->textarea(); ?>
<div class="clearfix"></div>

<?php
$actions_str = file_get_contents(Yii::getAlias('@app') . '/modules/printdocs/assets/js/dover_actions.json');
$arr = Json::decode($actions_str)['actions'];

foreach ($arr as $key => $action):
    $actions[$key] = $action['name'];
endforeach;

echo $form->field($model, 'actions')->checkboxList($actions);
?>

<input name="DoverParamsForm[tpl_id]" value="<?= $tpl_id ?>" type="hidden" />

<?php
echo Html::submitButton(Yii::t('printdocs', 'GENERATE'), ['class' => 'btn btn-primary']);

ActiveForm::end();

Pjax::end();

