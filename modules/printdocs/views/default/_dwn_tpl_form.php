<?php

use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\file\FileInput;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'action' => [ '/' . \Yii::$app->controller->module->id . '/add-template' ],
    'options' => [
        'enctype'=>'multipart/form-data',
        'id' => 'dwnd-form',
        'data-pjax' => true,
        'data-id' => $tplFormModel->group_id,
        ] // important
]);

echo $form->field($tplFormModel, 'tpl')->label('')->widget(FileInput::classname(), [
    'options' => [ 'multiple' => false ],
    'pluginOptions' => [
        'showPreview' => false,
        'showCaption' => true,
        'showRemove' => false,
        'showUpload' => false,
        'overwriteInitial' => true,
        'allowedFileExtensions' => ['doc','docx'],
    ]
]); ?>

<?= $form->field($tplFormModel, 'group_id')->label(false)->hiddenInput(); ?>
<?= Html::submitButton(Yii::t('printdocs', 'SAVE'), ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>

 
