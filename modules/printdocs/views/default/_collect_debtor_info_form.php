<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

Pjax::begin(['id' => 'collect-debtor-info-form-wrap', 'enablePushState' => false, 'timeout' => 1000]);

$form = ActiveForm::begin([
    'action' => [ '/' . \Yii::$app->controller->module->id . '/make-collect-debtor-info' ],
    'options' => [
        'id' => 'collect-debtor-info-params-form',
        'data-id' => '',
    ] // important
]);
?>

<?= $form->field($model, 'addressee', ['options' => ['class' => 'col-lg-8 form-group']])->textInput(['maxlength' => true, 'readonly' => 'readonly']) ?>
    <div class="form-group col-lg-4">
        <?php echo Html::button( Yii::t('printdocs', 'CHOOSE'), ['class' => 'btn btn-primary choose-contractor-btn', 'data-toggle' => 'modal', 'data-type'  => 'addressee', 'data-target' => '#modalContractors'] ); ?>
    </div>
    <input type="hidden" id="collectdebtorinfoparamsform-addressee_id" class="form-control" name="CollectDebtorInfoParamsForm[addressee_id]">
    <div class="clearfix"></div>

<input name="CollectDebtorInfoParamsForm[tpl_id]" value="<?= $tpl_id ?>" type="hidden" />

<?php
echo Html::submitButton(Yii::t('printdocs', 'GENERATE'), ['class' => 'btn btn-primary']);

ActiveForm::end();

Pjax::end();
