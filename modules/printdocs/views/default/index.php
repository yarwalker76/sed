<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\AppHelper;

// add module assets
use app\modules\printdocs\assets\PrintDocsAsset;
PrintDocsAsset::register($this);

$this->title = Yii::t('printdocs', 'TITLE');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="print_docs-index">
    <h3><?= Html::encode($this->title) ?></h3>
    <?php Pjax::begin(['id' => 'pjax-templates-reestr']); ?>
   
    <?php foreach( $doc_groups as $group ): ?>
            <div class="well">
                <h4><?= $group->name; ?></h4> 
                <?php if( !is_null( AppHelper::getLastSeenProcedureID() ) ): ?>
                    <span class="glyphicon glyphicon-plus add-printdoc-item" data-id="<?= $group->id ?>"
                       aria-hidden="true" data-pjax="1" data-target="#modalDownloadTpl" data-toggle="modal" ></span>
                <?php endif; ?>

                <?php Pjax::begin(['id' => 'templates' . $group->id]); ?>
                    <?= $this->render('_grid_tpl', [ 'id' => $group->id, 'dataProvider' => $dataProviders[ $group->id ][ 'dataProvider' ], ]); ?>
                <?php Pjax::end(); ?>
                
            </div>
    <?php endforeach; ?>

    <?php Pjax::end(); ?>
</div>

<?php Modal::begin([ 
    'header' => '<h4>' . Yii::t('printdocs', 'ADD_TPL') . '</h4>', 
    'id' => 'modalDownloadTpl',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]);

//echo $this->render('_dwn_tpl_form', ['tplFormModel' => $tplFormModel]);

Modal::end(); 
?>

<?php
Modal::begin([
    'header' => '<h3>' . Yii::t('printdocs', 'CONTRACTORS') . '</h3>',
    'headerOptions' => ['id' => 'modalContractorsHeader'],
    'id' => 'modalContractors',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

//echo $this->render('/default/contractor-search-form', ['model' => $search_model, 'contractor_types' => $contractor_types]);
?>

<form id="contractors-list-form" data-type=""></form>

<?php
Modal::end();
?>

<?php Modal::begin([
    'header' => '<h2>Уведомление</h2>',
    'headerOptions' => ['id' => 'modalMessageHeader'],
    'id' => 'modalMessage',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

Modal::end();
?>

<div class="hidden">
    <div class="checkbox" id="createDocumentsCheckbox">
        <label>
            <input type="checkbox" name="<?= $nameCreateDocuments ?>"> <strong>Создать исходящее сообщение</strong>
        </label>
    </div>
</div>

<?php 
/*Modal::begin([
    'header' => '<h2>'. Yii::t('printdocs', 'FORM_PARAMS') .'</h2>',
    //'headerOptions' => ['id' => 'modalMessageHeader'],
    'id' => 'modalFormParams',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE, ],
    ]
);

Modal::end(); */
?>
