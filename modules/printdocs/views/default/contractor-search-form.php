<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="contractors-form">
	<?php $form = ActiveForm::begin(['id' => 'contractor-search-form', 'action' => '/printdocs/default/search-contractor']); ?>
		<?= $form->field($model, 'search', ['options' => ['class' => 'col-lg-8 form-group field-procedures']])->label(Yii::t('printdocs', 'SEARCH'))->textInput() ?>
		<div class="form-group col-lg-4">
	        <?= Html::submitButton(Yii::t('printdocs', 'SEARCH'), ['class' => 'btn btn-primary', 'id' => 'search-contractors-btn']) ?>
	    </div>
	    <div class="clearfix"></div>
		<?= $form->field($model, 'type')
            ->label(Yii::t('printdocs', 'CONTRACTOR_TYPE'))
            ->dropDownList($contractor_types, ['options' => [ 6 => ['selected' => true] ]]); ?>
	<?php ActiveForm::end(); ?>
</div>