<?php

use yii\grid\GridView;
use yii\helpers\Html;
use app\models\AppHelper;

?>

<?=
 GridView::widget([
    //'id' => 'templates' . $id,
    'dataProvider' => $dataProvider,
    'layout'=>"{items}",
    'showHeader' => false,
    'showFooter' => false,
    'columns' => [
        [
            'attribute' => 'name',
            'content' => function($data){
                return Html::a( $data['name'], ['/printdocs/download-template'],
                    [
                        'class' => 'download-tpl-link',
                        //'data-method' => 'post',
                        'data-id' => $data['id'],
                        'data-grid' => $data['group_id']
                    ]);
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => [ 'width' => '40' ],
            'buttons'=> [
                  'delete' => function ($url, $model) {
                        $customurl = Yii::$app->getUrlManager()->createUrl(['/printdocs/delete-template' ]); //$model->id для AR
                        return ( !is_null( AppHelper::getLastSeenProcedureID() ) ? \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl,
                                ['class' => 'del-tpl', 'data-id' => $model['id'], 'data-grid' => $model['group_id'] ]) : '' );
               }
            ],
            'template'=>'{delete}' 
        ],
    ],
 ]);
 ?>
