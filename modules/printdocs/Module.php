<?php

namespace app\modules\printdocs;

use Yii;

/**
 * print_docs module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\printdocs\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        
        $this->setAliases(['@printdocs-assets' => __DIR__ . '/assets']);

        self::registerTranslations();
    }
    
    public static function registerTranslations() {
        if (!isset(Yii::$app->i18n->translations['print_docs']) && !isset(Yii::$app->i18n->translations['printdocs/*'])) {
            Yii::$app->i18n->translations['printdocs'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/modules/printdocs/messages',
                'sourceLanguage' => Yii::$app->language,
                'forceTranslation' => true,
                'fileMap' => [
                    'events' => 'printdocs.php'
                ]
            ];
        }
    }
}
