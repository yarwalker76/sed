<?php 
use yii\helpers\Html;
use yii\widgets\MaskedInput;
?>
                <div id="account-block-<?=$i?>">
                    <p class="text-right">
                        <a class="btn btn-default delete-account-button">
                            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                            <input id="delete-account-block-id" type="hidden" value="account-block-<?=$i?>">
                            <input id="delete-account-id" type="hidden" value="<?php if (isset($data)) echo $i?>">
                        </a>
                    </p>
                    <div class="form-group">
                        <label for="accounts[<?=$i?>][banks_id]"><?php echo Yii::t('references', 'Bank'); ?> <sapn style="color: red;">*</span></label>
                        <select class="form-control" name="accounts[<?=$i?>][banks_id]">
                            <option></option>
                            <?php foreach ($banks as $key => $value) { ?>
                                <option value="<?=$value['id'];?>" <?php if (isset($data['banks_id'])) if ($value['id'] == $data['banks_id']) echo 'selected' ?>>
                                    <?=Html::encode($value['value']);?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="accounts[<?=$i?>][number]"><?php echo Yii::t('references', 'Number'); ?> <sapn style="color: red;">*</span></label>
                        <input class="form-control" name="accounts[<?=$i?>][number]" value="<?php if (isset($data['number'])) echo Html::encode($data['number']); ?>">
                    </div>
                    <div class="form-group">
                        <label for="accounts[<?=$i?>][accounts_types_id]"><?php echo Yii::t('references', 'Type'); ?></label>
                        <select class="form-control" name="accounts[<?=$i?>][accounts_types_id]">
                            <option></option>
                             <?php foreach ($accountstypes as $key => $value) { ?>
                                <option value="<?=$value['id'];?>" <?php if (isset($data['accounts_types_id'])) if ($value['id'] == $data['accounts_types_id']) echo 'selected' ?>>
                                    <?=Html::encode($value['type']);?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="accounts[<?=$i?>][closed]"><?php echo Yii::t('references', 'Closed'); ?></label>
                        <select class="form-control" name="accounts[<?=$i?>][closed]">
                            <option></option>
                            <option value="0" <?php if (isset($data['closed'])) if ($data['closed']==0) echo 'selected' ?>><?php echo \Yii::t('references', 'Open')?></option>
                            <option value="1" <?php if (isset($data['closed'])) if ($data['closed']==1) echo 'selected' ?>><?php echo \Yii::t('references', 'Close')?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="accounts[<?=$i?>][closing]"><?php echo Yii::t('references', 'Closing'); ?></label>         
                        <input class="form-control" id="a<?=$i?>" name="accounts[<?=$i?>][closing]" placeholder="ДД-ММ-ГГГГ" value="<?php if (isset($data['closing'])) echo Html::encode($data['closing']); ?>">
                        <script>$(document).ready(function(){ $("#a<?=$i?>").mask("99-99-9999"); });</script>
                    </div>
                    <div class="form-group">
                        <label for="accounts[<?=$i?>][comment]"><?php echo Yii::t('references', 'Comment'); ?></label>
                        <textarea class="form-control" name="accounts[<?=$i?>][comment]"><?php if (isset($data['comment'])) echo Html::encode($data['comment']); ?></textarea>
                    </div>
                    <hr>
                </div>