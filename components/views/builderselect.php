
    <select class="form-control" name="param[<?=$id ?>]">
        <option></option>
        <?php foreach ($data as $key => $value) { ?>
            <option value="<?php echo $value['id']; ?>" <?php if ($organizations_id != null) if ($value['id'] == $organizations_id) echo 'selected' ?>>
                <?php echo $value['value']; ?>
            </option>
        <?php } ?>
    </select>