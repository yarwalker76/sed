<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

use yii;
use app\models\TblOrganizations;
use app\models\TblValues;
use app\models\TblParams;

class GetTitleWidget extends Widget
{
    public $id;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        if ($this->id == '')
            $title = '';
        else {
            $sql = 'SELECT v.value
                    FROM ' .  TblValues::tableName() . ' as v
                    LEFT JOIN ' . TblParams::tableName() . ' as p ON (v.params_id = p.id) 
                    WHERE (v.organizations_id =' . $this->id . ') and (p.title = "Название")';
            $data = Yii::$app->db->createCommand($sql)->queryOne();
            $title = $data['value'];
        }
        return $title; 
    }
}
