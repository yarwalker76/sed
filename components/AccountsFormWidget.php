<?php
namespace app\components;

use yii\base\Widget;

use app\models\TblAccountsTypes;
use app\models\TblOrganizationsSearch;

class AccountsFormWidget extends Widget
{
    public $i = null;
    public $data = null;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('accountsform', [
            'i' => $this->i,
            'data' => $this->data,
            'accountstypes' => TblAccountsTypes::find()->all(),
            'banks' => TblOrganizationsSearch::findBanks(),
        ]); 
    }
}