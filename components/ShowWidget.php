<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class ShowWidget extends Widget
{
    // data render array
    public $dra = null;
    
    public $a = '';
    public $x = '';

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        if (!is_null($this->dra))
            if (($this->a != '') and ($this->x != ''))
                if (isset($this->dra[$this->a][$this->x]))
                    echo Html::encode($this->dra[$this->a][$this->x]);
                else
                    return 'not found';
            else 
                return '';
        else 
            return '';
    }
}
