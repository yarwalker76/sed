<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

use yii;
use app\models\TblOrganizations;
use app\models\TblValues;
use app\models\TblParams;

class BuilderSelectWidget extends Widget
{
    public $organizations_types_id;
    public $organizations_id;
    public $id;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $sql = 'SELECT o.id, v.value
                FROM ' . TblOrganizations::tableName() . ' as o
                LEFT JOIN ' . TblParams::tableName() . ' as p ON (o.organizations_types_id = p.organizations_types_id) 
                LEFT JOIN ' . TblValues::tableName() . ' as v ON (v.params_id = p.id) and (v.organizations_id = o.id)
                WHERE (o.organizations_types_id = ' . $this->organizations_types_id . ') and (p.title = "Название")';
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        return $this->render('builderselect', [
            'data' => $data,
            'organizations_id' => $this->organizations_id,
            'id' => $this->id,
        ]); 
    }
}
