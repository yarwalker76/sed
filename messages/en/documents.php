<?php

return [
    'DOCUMENTS' => 'Делопроизводство',
    'INCOMING' => 'Входящие',
    'OUTGOING' => 'Исходящие',
    'INTERNAL' => 'Внутренние',
    'OTHER' => 'Другие',
];