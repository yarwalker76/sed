<?php

return [
    'Home' => 'Главная',
    'Login' => 'Вход',
    'Logout' => 'Выход',
    'Profile' => 'Профиль',
    'Users' => 'Пользователи',
    'RBAC' => 'Управление ролями',
    'References' => 'Справочники',
    'Tasks' => 'Задачи',
    'Profile' => 'Профиль',
    'Documents' => 'Делопроизводство',
    'Events' => 'События',
    'LegalService' => 'Юридическая служба',
    'BackOffice' => 'Бэк-Офис',
    'PrintDocs' => 'Печать документов',
    'Creditor List' => 'Реестр кредиторов',
    'Creditors' => 'Кредиторы',
    'Requests' => 'Требования',
    'CreditorsMeeting' => 'Собрания кредиторов'
];