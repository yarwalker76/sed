<?php

return [
    'Organizations' => 'Организации',
    'Create organization' => 'Создать организацию',
    'Creatig organization' => 'Создание организации',
    'Title'=> 'Название',
    'Card of organization'=> 'Карточка организации',
    
    'Find' => 'Найти организацию',
    'Find by title' => 'Найти организацию по названию',
    'Find by INN' => 'Найти организацию по ИНН',
    'Show all' => 'Показать все',
    
    'Params' => 'Список параметров',
    'Dynamic params' => 'Динамические параметры',
    'Custom params' => 'Специальные параметры',

    'Main' => 'Общие',
    'Extensions' => 'Дополнительные',
    'Main params' => 'Общие параметры',
    'Type of organization' => 'Тип организации',
    'Title of organization' => 'Название организации',
    'INN' => 'ИНН',
    'Surname' => 'Фамилия',
    'man' => 'мужской',
    'woman' => 'женский',
    
    'Contact information' => 'Контактная информация',
    'Contacts' => 'Контакты',
    'Phone' => 'Телефон',
    'Mobile' => 'Мобильный',
    'Fax' => 'Факс',
    'E-mail' => 'Эл. почта',
    'Notes' => 'Примечания',
    
    'Main address' => 'Юридический адрес',
    'Additional address' => 'Фактический адрес',
    'Address for correspondence' => 'Адрес для корреспонденции ',
    
    'Full address' => 'Полный адрес',
    'Postcode' => 'Почтовый индекс',
    'Region' => 'Субъект РФ',
    'Locality' => 'Населенный пункт',
    'Address' => 'Адрес',
        
    'Accounts' => 'Счета',
    'Type' => 'Тип счета',
    'Accounts Types ID' => 'Тип счета',
    'Bank' => 'Банк',
    'Banks ID' => 'Банк',
    'Number' => 'Номер счета',
    'Closing' => 'Дата закрытия счета',
    'Closed' => 'Статус',
    'Open' => 'Открыт',
    'Close' => 'Закрыт',
    'Comment' => 'Комментарий',

    'Actions' => 'Действия',
    'Create' => 'Создать',
    'Update' => 'Изменить',
    'Delete' => 'Удалить',
    'Save' => 'Сохранить',
    
    'Saved!' => 'Операция выполнена успешно!',
    'WARNING!!!' => 'Сохранено без параметров!',
    'ERROR.' => 'Ошибка ввода данных.',
    'Account was deleted.' => 'Счет успешно удален.',
    
    'Not found!' => 'Не найдено!',
    
    'Are you sure you want to delete this item?' => 'Удалить?',
    
    'Not a full set of data.' => 'Не полный набор данных.',

    'Delete error, there is related procedure' => 'Невозможно удалить запись, т.к. она участвует в Процедуре'
    
];

