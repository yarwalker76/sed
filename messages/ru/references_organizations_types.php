<?php

return [
    'Organizations Types' => 'Типы организаций',
    'Create organizations types' => 'Создать тип организации',
    
    'Create' => 'Создать',
    'Update' => 'Изменить',
    'Delete' => 'Удалить',
    
    'Saved!' => 'Сохранено!',
    'WARNING!!!' => 'Сохранено без параметров!',
    'ERROR.' => 'Ошибка ввода данных.',
    
    'Are you sure you want to delete this item?' => 'Удалить?',
    
];

