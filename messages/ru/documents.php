<?php

return [
    'DOCUMENTS' => 'Делопроизводство',
    'INCOMING' => 'Входящие',
    'OUTGOING' => 'Исходящие',
    'INTERNAL' => 'Внутренние',
    'OTHER' => 'Другие',
    
    'EXP_ROWNUM' => '№ п/п',
    'EXP_SERIAL_NUMBER' => '№ исх',
    'EXP_ID' => "Номер идентификатора\rИнформация клиента",
    'EXP_DELIVERY_ADDRESS' => 'Адрес получателя',
    'RECEIVE_CONTRACTOR' => 'Адресат',
];