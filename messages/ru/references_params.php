<?php

return [
    'Params' => 'Параметры',
    'Create param' => 'Создать параметр',
    
    'Create' => 'Создать',
    'Update' => 'Изменить',
    'Delete' => 'Удалить',
    
    'Saved!' => 'Сохранено!',
    'WARNING!!!' => 'Сохранено без параметров!',
    'ERROR.' => 'Ошибка ввода данных.',
    
    'Are you sure you want to delete this item?' => 'Удалить?',
    
];

