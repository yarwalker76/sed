<?php

return [
    'Params Types' => 'Типы параметров',
    'Create params types' => 'Создать тип параметра',
    
    'Create' => 'Создать',
    'Update' => 'Изменить',
    'Delete' => 'Удалить',
    
    'Saved!' => 'Сохранено!',
    'WARNING!!!' => 'Сохранено без параметров!',
    'ERROR.' => 'Ошибка ввода данных.',
    
    'Are you sure you want to delete this item?' => 'Удалить?',
    
];

