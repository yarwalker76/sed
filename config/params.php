<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'admin@example.com',
    'uploadPath' =>  dirname(__DIR__) . '/web/uploads/',
];
