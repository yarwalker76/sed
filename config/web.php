<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],

    'language'=>'ru-RU',
    'controllerMap' => [
        'export' => 'app\components\exportFile\controllers\ExportController'
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'YSqvMWETggOgtoNfagEZxr5CMys9Z-Ww',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'dmitrybaikow@gmail.com',
                'password' => 'zaq12wsxcde3',
                'port' => 587,
                'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'user' => [
            'identityClass' => 'budyaga\users\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/login'],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/signup' => '/user/user/signup',
                '/login' => '/user/user/login',
                '/logout' => '/user/user/logout',
                '/requestPasswordReset' => '/user/user/request-password-reset',
                '/resetPassword' => '/user/user/reset-password',
                '/profile' => '/user/user/profile',
                '/retryConfirmEmail' => '/user/user/retry-confirm-email',
                '/confirmEmail' => '/user/user/confirm-email',
                '/unbind/<id:[\w\-]+>' => '/user/auth/unbind',
                '/oauth/<authclient:[\w\-]+>' => '/user/auth/index',
                //'/users' => '/user/admin/index',
                '/users' => '/admin/index',
                //'/admin' => '/admin/index'
                '/documents' => '/documents/incoming-docs/index',
                '/documents/incoming' => '/documents/incoming-docs/index',
                '/documents/outgoing' => '/documents/outgoing-docs/index',
                '/documents/internal' => '/documents/internal-docs/index',
                '/documents/other' => '/documents/other-docs/index',
                '/documents/create-outgoing-record' => '/documents/outgoing-docs/create',
                '/documents/clone-outgoing-record' => '/documents/outgoing-docs/clone',
                '/documents/create-incoming-record' => '/documents/incoming-docs/create',
                '/documents/clone-incoming-record' => '/documents/incoming-docs/clone',
                '/documents/create-internal-record' => '/documents/internal-docs/create',
                '/documents/clone-internal-record' => '/documents/internal-docs/clone',
                '/documents/create-other-record' => '/documents/other-docs/create',
                '/documents/clone-other-record' => '/documents/other-docs/clone',
                '/documents/download-file' => '/documents/default/download-file',
                
                '/procedures/create' => '/procedures/default/create',
                '/procedures/view' => '/procedures/default/view',
                '/procedures/update-common-properties' => '/procedures/default/update-common-properties',
                '/procedures/update-additional-properties' => '/procedures/default/update-additional-properties',
                
                '/printdocs/add-template' => '/printdocs/default/add-template',
                '/printdocs/delete-template' => '/printdocs/default/delete-template',
                '/printdocs/get-params-form' => '/printdocs/default/get-params-form',
                '/printdocs/download-template' => '/printdocs/default/download-template',
                '/printdocs/make-dover' => '/printdocs/default/make-dover',
                '/printdocs/make-proc-holding' => '/printdocs/default/make-proc-holding',
                '/printdocs/make-collect-debtor-info' => '/printdocs/default/make-collect-debtor-info',

                //'/meetings' => '/meetings/default/index',

            ],
        ],
        'authManager' => [
            'class' => 'app\modules\events\rbac\DbManagerRBAC', 
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'userss' => 'users.php',
                        'menu' => 'menu.php'
                    ],
                   // 'forceTranslation' => true,
                ],
            ],
        ],
        'assetManager'=> ['forceCopy' => true],
    ],
    'as event' => [
        'class' => 'app\modules\events\behaviors\EventBehavior',
    ],
    'modules' => [
        'user' => [
            'class' => 'budyaga\users\Module',
            'userPhotoUrl' => '/uploads/user/photo',
            'userPhotoPath' => '@app/web/uploads/user/photo',
            'customViews' => [
                'login' => '@app/views/user/login',
                'profile' => '@app/views/user/Profile',
            ],
        ],

        'documents' => [
            'class' => 'app\modules\documents\Documents',
        ],

        'events' => [
            'class' => 'app\modules\events\Module',
            'ModelClassNames' => [
                'budyaga\users\models\User' => 'Пользователь',
                'yii\rbac\Role' => 'Роль',
                'yii\rbac\Permission' => 'Разрешение',
                'app\modules\printdocs\models\DocTemplates' => 'Шаблон отчета',
            ],
            'ModelSettings' => [
                'budyaga\users\models\User' => [
                    'modelName' => 'Пользователь',
                    'modelNoAttribute' => [
                        'id', 'value'
                    ]
                ],
                'yii\rbac\Role' => ['modelName' => 'Роль'],
                'yii\rbac\Permission' => ['modelName'  => 'Разрешение'],
                'app\modules\printdocs\models\DocTemplates' => ['modelName' => 'Шаблон отчета'],
                'app\modules\procedures\models\LastSeenProcedures' => ['modelName' => 'Последняя просмотренная процедура'],
                'app\models\BackOffice' => ['modelName' => 'Бэк-Офис'],
                'app\models\LegalService' => ['modelName' => 'Юридическая служба'],
                'app\modules\documents\models\OutgoingDocs' => ['modelName' => "Исходящие записи реестра"],
                'app\modules\documents\models\IncomingDocs' => ['modelName' => "Входящие записи реестра"],
                'app\modules\documents\models\InternalDocs' => ['modelName' => "Внутренние записи реестра"],
                'app\modules\documents\models\OtherDocs' => ['modelName' => "Другие записи реестра"],
                'app\models\TblOrganizationsTypes' => ['modelName' => 'Типы справочников'],
                'app\modules\procedures\models\Procedures' => ['modelName' => 'Процедуры'],
                'app\modules\procedures\models\ProcedureRelations' => ['modelName' => 'Отношение пользователя к процедуре'],
                'app\models\TblParamsTypes' => ['modelName' => 'Типы параметров'],
                'app\models\TblParams' => ['modelName' => 'Параметры'],
                'budyaga\users\models\UserPasswordResetToken' => ['modelName' => 'Смена пароля пользователя'],
                'app\modules\documents\models\DocFiles' => ['modelName' => 'Файл в документообороте'],
                'app\models\TblAccounts' => ['modelName' => 'Аккаунты пользователей'],
                'app\models\TblRegions' => ['modelName' => 'Регионы'],
                'app\models\TblOrganizations' => ['modelName' => 'Справочники'],
                'app\models\TblAddresses' => ['modelName' => 'Адреса'],


            ],
        ],

        'procedures' => [
            'class' => 'app\modules\procedures\Procedures',
        ],
        
        'printdocs' => [
            'class' => 'app\modules\printdocs\Module',
        ],

        'creditors' => [
            'class' => 'app\modules\creditors\Module',
        ],

        'meetings' => [
            'class' => 'app\modules\meetings\Module',
            'layout' => '@app/modules/meetings/views/layouts/meetings',
        ],
    ],
    'params' => $params, //
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
