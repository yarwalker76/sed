<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblParams */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-params-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'params_types_id')->textInput() ?>

    <?= $form->field($model, 'organizations_types_id')->textInput() ?>

    <?= $form->field($model, 'tab')->textInput() ?>

    <?= $form->field($model, 'top')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('references_params', 'Create') : Yii::t('references_params', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
