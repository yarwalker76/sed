<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblOrganization */

$this->title = Yii::t('handbook', 'Update {modelClass}: ', [
    'modelClass' => 'Tbl Organization',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('handbook', 'Tbl Organizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('handbook', 'Update');
?>
<div class="tbl-organization-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
