<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblOrganization */

$this->title = Yii::t('handbook', 'Create Tbl Organization');
$this->params['breadcrumbs'][] = ['label' => Yii::t('handbook', 'Tbl Organizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-organization-create">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                <?=
                $this->render('_form', [
                    'model' => $model,
                ])
                ?>
            </div>
        </div>

    </div>
</div>
