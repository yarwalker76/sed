<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblOrganization */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-organization-form">
    <div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#general" aria-controls="general" role="tab" data-toggle="tab">Общие</a>
            </li>
            <li role="presentation">
                <a href="#contact_info" aria-controls="contact_info" role="tab" data-toggle="tab">Контактная информация</a>
            </li>
        </ul>

        <?php $form = ActiveForm::begin(); ?>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane in active" id="general">
                <div class="row">
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'name', ['template' => '<div class="col-xs-2">{label}</div><div class="col-xs-10">{input}{error}</div>'])->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'leader', ['template' => '<div class="col-xs-2">{label}</div><div class="col-xs-10">{input}{error}</div>'])->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= $form->field($model, 'inn', ['template' => '<div class="col-xs-2">{label}</div><div class="col-xs-10">{input}{error}</div>'])->textInput() ?>
                        </div>
                        <div class="col-xs-3">
                            <?= $form->field($model, 'kpp', ['template' => '<div class="col-xs-2">{label}</div><div class="col-xs-10">{input}{error}</div>'])->textInput() ?>
                        </div>
                        <div class="col-xs-6">
                            <?= $form->field($model, 'ogrn', ['template' => '<div class="col-xs-2">{label}</div><div class="col-xs-10">{input}{error}</div>'])->textInput() ?>
                        </div>  
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="contact_info">

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('handbook', 'Create') : Yii::t('handbook', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
