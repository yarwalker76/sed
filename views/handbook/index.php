<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblOrganizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('handbook', 'Tbl Organizations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-organization-index  shadow-box">
    <div class = "row">
        <div class = "col-xs-12 tables">
            <h2 class="title"><?= Html::encode($this->title) ?></h2>
            <div class="col-xs-12">
                
                <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
                <br>
                <p>
                    <?= Html::a(Yii::t('handbook', 'Create Tbl Organization'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php Pjax::begin(); ?>    <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                      //  'id',
                        'name',
                        'leader',
                        'inn',
                        'kpp',
                        // 'ogrn',
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                    'tableOptions' => [
                        'class' => 'sTables table'
                    ],
                ]);
                ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>