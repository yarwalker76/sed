<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblRegions */

$this->title = Yii::t('references_regions', 'Create region');
$this->params['breadcrumbs'][] = ['label' => Yii::t('references_regions', 'Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-regions-create">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body" id="panel-body-create-form">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>    
        </div>
    </div>
</div>
