<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblRegions */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('references_regions', 'Tbl Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('references_regions', 'Update');
?>
<div class="tbl-regions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
