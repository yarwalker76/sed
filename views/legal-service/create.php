<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LegalService */

$this->title = Yii::t('LegalService', 'Create record');
$this->params['breadcrumbs'][] = ['label' => Yii::t('LegalService', 'Legal Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="legal-service-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
