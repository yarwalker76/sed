<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LegalService */

$this->title = Yii::t('LegalService', 'Update Record', [
    'modelClass' => 'Legal Service',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('LegalService', 'Legal Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('LegalService', 'Update');
?>
<div class="legal-service-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
