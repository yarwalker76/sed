<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use budyaga\cropper\Widget;
use budyaga\users\models\User;
use yii\helpers\Url;

use budyaga\users\UsersAsset;
use yii\helpers\ArrayHelper;

$assets = UsersAsset::register($this);
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php //= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'lastname')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'firstname')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'patronymic')->textInput(['maxlength' => 255]) ?>
    
    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'password')->textInput(['maxlength' => 255]) ?>
    <?php /*
    <div class="form-group field-user-lastname required">
        <label class="control-label" for="user-lastname">Пароль</label>
        <input type="text" id="user-password" class="form-control" name="password" maxlength="255">
    </div>  */
    ?>
    
    <?php /*=
    $form->field($model, 'photo')->widget(Widget::className(), [
        'uploadUrl' => Url::toRoute('/user/user/uploadPhoto'),
    ])*/
    ?>

    <?= $form->field($model, 'sex')->dropDownList(User::getSexArray()); ?>

        <?= $form->field($model, 'status')->dropDownList(User::getStatusArray()); ?>

    <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('users', 'CREATE') : Yii::t('users', 'UPDATE'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>

<div class="user-rules">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <br><br>
        <h3>Роли</h3>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <?= $form->field($modelForm, 'assigned')->dropDownList(
                ArrayHelper::map(
                    $modelForm->model->assignedRules,
                    function ($data) {
                        return serialize([$data->name, $data->type]);
                    },
                    'description'
                ), ['multiple' => 'multiple', 'size' => '20', 'class' => 'col-xs-12'])?>
        </div>
        <div class="col-xs-2 text-center">
            <button class="btn btn-success" type="submit" name="AssignmentForm[action]" value="assign"><span class="glyphicon glyphicon-arrow-left"></span></button>
            <button class="btn btn-success" type="submit" name="AssignmentForm[action]" value="revoke"><span class="glyphicon glyphicon-arrow-right"></span></button>
        </div>
        <div class="col-xs-5">
            <?= $form->field($modelForm, 'unassigned')->dropDownList(
                ArrayHelper::map(
                    $modelForm->model->notAssignedRules,
                    function ($data) {
                        return serialize([$data->name, $data->type]);
                    },
                    'description'
                ), ['multiple' => 'multiple', 'size' => '20', 'class' => 'col-xs-12'])?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<div class="user-rules">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <br><br>
        <h3>Процедуры</h3>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <label class="control-label" for="assignmentform-unassigned">Привязанные</label>
            <select id="assignmentform-assigned" class="col-xs-12" name="ProcedureForm[assigned_proc][]" multiple="multiple" size="20">
            <?php foreach($procedures_user as $p) { ?>
                <option value="<?=$p['id']?>">(ID: <?=$p['id']?>) <?=$p['name']?></option>
            <?php } ?>
            </select>
        </div>
        <div class="col-xs-2 text-center">
            <button class="btn btn-success" type="submit" name="ProcedureForm[action]" value="assign_proc"><span class="glyphicon glyphicon-arrow-left"></span></button>
            <button class="btn btn-success" type="submit" name="ProcedureForm[action]" value="revoke_proc"><span class="glyphicon glyphicon-arrow-right"></span></button>
        </div>
        <div class="col-xs-5">
            <label class="control-label" for="assignmentform-unassigned">Непривязанные</label>
            <select id="assignmentform-assigned" class="col-xs-12" name="ProcedureForm[unassigned_proc][]" multiple="multiple" size="20">
            <?php foreach($procedures_all as $p) { ?>
                <option value="<?=$p['id']?>">(ID: <?=$p['id']?>) <?=$p['name']?></option>
            <?php } ?>
            </select>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
