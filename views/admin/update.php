<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model budyaga\users\models\User */

$this->title = Yii::t('users', 'UPDATE_USER', ['username' => $model->username]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('users', 'USERS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>
        
    <?= $this->render('_update', [
        'model' => $model,
        'modelForm' => $modelForm,
        'procedures_all' => $procedures_all,
        'procedures_user'   => $procedures_user
    ]) ?>
</div>
