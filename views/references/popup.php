<?php 
    $message = '';
    switch ($status) {
        case 1: $message = Yii::t('references', "Saved!"); break;
        case 0.5: $message = Yii::t('references', "WARNING!!!"); break;
        case 0: $message = Yii::t('references', "ERROR."); break;
    };
    $str = '';
    for ($i=0; $i<count($errors); $i++)
        foreach ($errors[$i] as $key => $value)
            $str = $str . ' ' . implode('<br>', $value) . '<br>';
    
    echo json_encode(
        array( 
            "status" => $status,
            "message" => $message, 
            "errors" => $str,
        )
    );
?>
