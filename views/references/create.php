<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('references', 'Creatig organization');
$this->params['breadcrumbs'][] = ['label' => Yii::t('references', 'Organizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-organization-create">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body" id="panel-body-create-form">
                <b><?= Yii::t('references', 'Type of organization'); ?></b>
                <form id="create-form">
                    <select class="form-control" id="create-type" name="organizations_types_id">
                        <option id="create-type-blank"></option>
                        <?php foreach ($data as $key => $value) { ?>
                            <option  value="<?=$value['id'];?>"><?=$value['type'];?></option>
                        <?php } ?>
                    </select>
                    <div class="panel-body" id="create-data"></div>
                    <div id="create-form-status"></div>
                </form>
            </div>
        </div>
    </div>
</div>
