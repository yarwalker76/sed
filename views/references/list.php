<?php
use yii\grid\GridView;

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'emptyCell' => '',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            ['header' => \Yii::t('references', 'Type of organization'), 'value' => 'organizations_type',],
            ['header' => \Yii::t('references', 'Title'),'value' => 'values_value',],
            ['header' => \Yii::t('references', 'Surname'),'value' => 'values_value2',],
            ['header' => \Yii::t('references', 'Phone'),'value' => 'phone',],
            ['header' => \Yii::t('references', 'Mobile'),'value' => 'mobile',],
            ['header' => \Yii::t('references', 'E-mail'),'value' => 'email',],
            //'fax',
            [
                'header' => \Yii::t('references', 'Actions'),
                'class' => \yii\grid\ActionColumn::className(),
                'urlCreator' => function($action, $dataProvider, $key, $index){
                       return [$action,'id'=>$dataProvider['id']];
                   },
                'template'=>'{view} {update} {delete}',
            ]
        ],
        'tableOptions' => [
            'class' => 'sTables table'
        ],

    ]);
?>
