<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\GetTitleWidget;

$this->title = Yii::t('references', 'Card of organization');
$this->params['breadcrumbs'][] = ['label' => Yii::t('references', 'Organizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-organization-view">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<div class="tbl-organizations-view">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#main" aria-controls="main" role="tab" data-toggle="tab"><?php echo Yii::t('references', 'Main'); ?></a>
        </li>
        <li role="presentation">
            <a href="#contacts" aria-controls="contacts" role="tab" data-toggle="tab"><?php echo Yii::t('references', 'Contact information'); ?></a>
        </li>
        <li role="presentation">
            <a href="#accounts" aria-controls="accounts" role="tab" data-toggle="tab"><?php echo Yii::t('references', 'Accounts'); ?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane in active" id="main">
            <p>
                <?php //echo Html::a(Yii::t('references', 'Update'), ['update', 'id' => $organization['id']], ['class' => 'btn btn-primary']) ?>
                <?php /*echo Html::a(Yii::t('references', 'Delete'), ['delete', 'id' => $organization['id']], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('references', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) */?>
            </p>
            <?= DetailView::widget([
                'model' => $organization,
                'attributes' => [
                    [ 'label' => Yii::t('references', 'Type of organization'), 'value' => $organization['organizations_types_type'] ],
                ],
            ]) ?>
            
            <?php
                foreach ($params as $key => $value) {
                    if ($value['label'] == 'Пол') {
                        if ($value['value'] === '1') 
                            $params[$key]['value'] = Yii::t('references', 'man');
                        if ($value['value'] === '0') 
                            $params[$key]['value'] = Yii::t('references', 'woman');
                    } elseif ($value['label'] == 'ИФНС'){
                        $params[$key]['value'] = GetTitleWidget::widget(['id' => $value['value'] ]); 
                    } elseif ($value['label'] == 'Наименование СРО') { 
                        $params[$key]['value'] = GetTitleWidget::widget(['id' => $value['value'] ]);  
                    }
                }
            ?>
            
            <?= DetailView::widget([
                'model' => $params,
                'attributes' => $params,
            ]) ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="contacts">
            <?= DetailView::widget([
                'model' => $organization,
                'attributes' => [
                    [ 'label' => Yii::t('references', 'Phone'), 'value' => $organization['phone'] ],
                    [ 'label' => Yii::t('references', 'Mobile'), 'value' => $organization['mobile'] ],
                    [ 'label' => Yii::t('references', 'Fax'), 'value' => $organization['fax'] ],
                    [ 'label' => Yii::t('references', 'E-mail'), 'value' => $organization['email'] ],
                ],
            ]) ?>
            <?= DetailView::widget([
                'model' => $organization,
                'attributes' => [
                    [ 'label' => Yii::t('references', 'Postcode'), 'value' => $organization['address_0_postcode'] ],
                    [ 'label' => Yii::t('references', 'Region'), 'value' => $organization['address_0_region'] ],
                    [ 'label' => Yii::t('references', 'Locality'), 'value' => $organization['address_0_locality'] ],
                    [ 'label' => Yii::t('references', 'Address'), 'value' => $organization['address_0_address'] ],
                ],
            ]) ?>
            <?= DetailView::widget([
                'model' => $organization,
                'attributes' => [
                    [ 'label' => Yii::t('references', 'Postcode'), 'value' => $organization['address_1_postcode'] ],
                    [ 'label' => Yii::t('references', 'Region'), 'value' => $organization['address_1_region'] ],
                    [ 'label' => Yii::t('references', 'Locality'), 'value' => $organization['address_1_locality'] ],
                    [ 'label' => Yii::t('references', 'Address'), 'value' => $organization['address_1_address'] ],
                ],
            ]) ?>
            <?= DetailView::widget([
                'model' => $organization,
                'attributes' => [
                    [ 'label' => Yii::t('references', 'Postcode'), 'value' => $organization['address_2_postcode'] ],
                    [ 'label' => Yii::t('references', 'Region'), 'value' => $organization['address_2_region'] ],
                    [ 'label' => Yii::t('references', 'Locality'), 'value' => $organization['address_2_locality'] ],
                    [ 'label' => Yii::t('references', 'Address'), 'value' => $organization['address_2_address'] ],
                ],
            ]) ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="accounts">
            <?php 
                //Pjax::begin();
                echo GridView::widget([
                    'dataProvider' => $accountDataProvider,
                    //'filterModel' => $accountSearchModel,
                    'emptyCell' => '',
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['label' => Yii::t('references', 'Title'), 'value' => 'title' ],
                        ['label' => Yii::t('references', 'Number'), 'value' => 'number' ],
                        ['label' => Yii::t('references', 'Closing'), 'value' =>  'closing' ],
                        [
                            'header' => Yii::t('references', 'Closed'),
                            'attribute' => 'closed',
                            'label' => Yii::t('references', 'Closed'),
                            'content'=>function($data){
                                if ($data['closed']==1) return Yii::t('references', 'Close'); else return Yii::t('references', 'Open');
                            }
                        ],
                        ['label' => Yii::t('references', 'Comment'), 'value' => 'comment' ],
                        //['class' => 'yii\grid\ActionColumn'],
                    ],
                    'tableOptions' => [
                        'class' => 'sTables table'
                    ],
                ]);
                //Pjax::end(); 
            ?>
        </div>
    </div>
</div>