<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Alert;

use app\models\TblValues;

$this->title = Yii::t('references', 'Organizations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-organization-index shadow-box">
    <div class = "row">
        <div class = "col-xs-12 tables">
            <h2 class="title"><?= Html::encode($this->title) ?></h2>
            <div class="col-xs-12">
                <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
                <br>
                <div class="form-group">
                    <?= Html::a(Yii::t('references', 'Create organization'), ['create'], ['class' => 'btn btn-success']) ?>
                </div>

                <div class="form-group" style="width: 49%; margin-right: 1%; float: left;">
                    <input id="find-by-title" class="form-control" name="find" value="<?=(is_null($search_word_name)?'':$search_word_name)?>" placeholder="<?php echo Yii::t('references', 'Find by title'); ?>">
                </div>
                <div class="form-group" style="width: 49%; margin-left: 1%; float: left;">
                    <input id="find-by-inn" class="form-control" name="find" value="<?=(is_null($search_word_inn)?'':$search_word_inn)?>" placeholder="<?php echo Yii::t('references', 'Find by INN'); ?>">
                </div>
                
                <div class="form-group" style="width: 74%; margin-right: 1%; float: left;">
                    <select class="form-control disabled" id="organizations-types-id" name="organizations_types_id">
                        <option id="create-type-blank"></option>
                        <?php foreach ($organizationsTypes as $key => $value) { ?>
                            <option value="<?=$value['id'];?>"><?=$value['type'];?></option>
                        <?php } ?>
                    </select>
                </div>    
                <div class="form-group" style="width: 24%; margin-left: 1%; float: left;">
                    <?= Html::a(Yii::t('references', 'Show all'), ['/references'], ['class' => 'btn btn-default', 'style' => 'width: 100%;']) ?>
                </div>
                <div class="clearfix"></div>
                <br>

                <div id="list">
                <?php 
                    Pjax::begin();
                    if (Yii::$app->session->hasFlash('error')): ?>
                        <div class="flash-error">
                            <?php
                            Alert::begin([
                                'options' => [
                                'class' => 'alert alert-danger',
                                ],
                            ]);

                            echo Html::encode(Yii::$app->session->getFlash('error'));

                            Alert::end();?>
                        </div><!-- /.flash-error -->
                    <?php endif;

                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'emptyCell' => '',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            //'id',
                            ['header' => \Yii::t('references', 'Type of organization'), 'value' => 'organizations_type',],
                            ['header' => \Yii::t('references', 'Title'),'value' => 'values_value',],
                            ['header' => \Yii::t('references', 'Surname'),'value' => 'values_value2',],
                            ['header' => \Yii::t('references', 'Phone'),'value' => 'phone',],
                            ['header' => \Yii::t('references', 'Mobile'),'value' => 'mobile',],
                            ['header' => \Yii::t('references', 'E-mail'),'value' => 'email',],
                            //'fax',
                            [
                                'header' => \Yii::t('references', 'Actions'),
                                'class' => \yii\grid\ActionColumn::className(),
                                'urlCreator' => function($action, $dataProvider, $key, $index){
                                       return [$action,'id'=>$dataProvider['id']];
                                   },
                                'template'=>'{view} {update} {delete}',
                            ]
                        ],
                        'tableOptions' => [
                            'class' => 'sTables table'
                        ],
                        
                    ]);

                    Pjax::end(); 
                ?>
                </div>
                
                <?php 
                    if( Yii::$app->user->can('administrator') ){ 
                ?>
                <div class="btn-toolbar">
                    <div class="btn-group center-block">
                        <?= Html::a(Yii::t('references_regions', 'Regions'), ['/regions'], ['class' => 'btn btn-default btn-md']) ?>
                        <?= Html::a(Yii::t('references_params_types', 'Params Types'), ['/params-types'], ['class' => 'btn btn-default btn-md']) ?>
                        <?= Html::a(Yii::t('references_organizations_types', 'Organizations Types'), ['/organizations-types'], ['class' => 'btn btn-default btn-md']) ?>
                        <?= Html::a(Yii::t('references', 'Конструктор форм'), ['/params'], ['class' => 'btn btn-default btn-md']) ?>
                    </div>
                </div>
                <?php } ?>
            </div>
            
        </div>
    </div>
</div>

<?php
    $this->registerJsFile( 'js/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset'] );
    $this->registerJsFile( 'js/script_autocomplete.js', ['depends' => 'yii\web\JqueryAsset'] );
    $this->registerJsFile( 'js/enter_on_title.js', ['depends' => 'yii\web\JqueryAsset'] );
?>
