<?php 
use app\components\AccountsFormWidget;
//use app\components\ShowWidget;
use yii\helpers\Html;
use app\components\BuilderSelectWidget;

?>

<?php //echo ShowWidget::widget(['dra' => $dra, 'a' => 'organization', 'x' => 'title']); ?>

<?php if (isset($id)) echo '<form id="create-form">'; ?>
<div class="tbl-organization-form tabs">
    <input type="hidden" value="<?php if (isset($id)) echo $id; ?>" id="organization_id" name="id">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#main" aria-controls="main" role="tab" data-toggle="tab"><?php echo Yii::t('references', 'Main'); ?></a>
        </li>
        <li role="presentation">
            <a href="#contacts" aria-controls="contacts" role="tab" data-toggle="tab"><?php echo Yii::t('references', 'Contact information'); ?></a>
        </li>
        <li role="presentation">
            <a href="#accounts" aria-controls="accounts" role="tab" data-toggle="tab"><?php echo Yii::t('references', 'Accounts'); ?></a>
        </li>
        <li role="presentation">
            <a href="#extensions" aria-controls="main" role="tab" data-toggle="tab"><?php echo Yii::t('references', 'Notes'); //echo Yii::t('references', 'Extensions'); ?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active fade in" id="main">
            <h4><?php echo Yii::t('references', 'Main'); ?></h4>
            
            <?php 
                foreach ($fields as $key => $value) { 
                    if ($value['tab'] == 1) {
            ?>
            
            <div class="form-group" style="float: left; <?php if ($value['type'] == 'integer') echo 'width: 32%; margin-right: 1%; '; else echo 'width: 100%;' ?>">
                <label for="<?=$value['id'] ?>">
                    <?php echo Yii::t('references', $value['title']); ?> <?php if ($value['required'] == 1){?><span style="color: red;">*</span><?php } ?>
                </label>
                
                <?php if ($value['title'] == 'Пол') { ?>
                    <select class="form-control" id="<?=$value['id'] ?>" name="param[<?=$value['id'] ?>]">
                        <option value=""></option>
                        <option value="1" <?php if (isset($params[$value['id']])) if ($params[$value['id']]==='1') echo 'selected';?>><?php echo Yii::t('references', 'man')?></option>
                        <option value="0" <?php if (isset($params[$value['id']])) if ($params[$value['id']]==='0') echo 'selected';?>><?php echo Yii::t('references', 'woman')?></option>
                    </select>
                
                <?php } elseif ($value['title'] == 'ИФНС'){ ?>
                    <?php if (isset($params[$value['id']])) $o_id = $params[$value['id']]; else $o_id = null; ?>
                    <?= BuilderSelectWidget::widget(['organizations_types_id' => 6, 'id' => $value['id'], 'organizations_id' => $o_id]) ?>
                
                <?php } elseif ($value['title'] == 'Наименование СРО'){ ?>
                    <?php if (isset($params[$value['id']])) $o_id = $params[$value['id']]; else $o_id = null; ?>
                    <?= BuilderSelectWidget::widget(['organizations_types_id' => 5, 'id' => $value['id'], 'organizations_id' => $o_id]) ?>
                
                <?php } else { ?>
                    <input class="form-control" id="<?=$value['id'] ?>" name="param[<?=$value['id'] ?>]" value="<?php if (isset($params[$value['id']])) echo Html::encode($params[$value['id']]);?>">
                <?php } ?>

            </div>
            
            <?php 
                    }
                }
            ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="extensions">
            <h4><?php Yii::t('references', 'Notes'); //echo Yii::t('references', 'Extensions'); ?></h4>
            
            <?php 
                foreach ($fields as $key => $value) { 
                    if ($value['tab'] == 2) {
            ?>
            
            <div class="form-group" style="float: left; <?php if ($value['type'] == 'integer') echo 'width: 32%; margin-right: 1%; '; else echo 'width: 100%;' ?>">
                <label for="<?=$value['id'] ?>">
                    <?php echo Yii::t('references', $value['title']); ?> <?php if ($value['required'] == 1){?><span style="color: red;">*</span><?php } ?>
                </label>
                
                <?php if ($value['title'] == 'Примечание') { ?>
                        <textarea class="form-control" id="<?=$value['id'] ?>" name="param[<?=$value['id'] ?>]" rows="20"><?php if (isset($params[$value['id']])) echo Html::encode($params[$value['id']]);?></textarea>
                <?php } else { ?>
                    <input class="form-control" id="<?=$value['id'] ?>" name="param[<?=$value['id'] ?>]" value="<?php if (isset($params[$value['id']])) echo Html::encode($params[$value['id']]);?>">
                <?php } ?>
                
            </div>
            
            <?php 
                    }
                }
            ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="contacts">
            <h4><?php echo Yii::t('references', 'Contacts'); ?></h4>
            <div class="panel-body">
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Phone'); ?></label>
                    <input class="form-control" name="phone" value="<?php if (isset($organization)) echo Html::encode($organization['phone']);?>">
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Mobile'); ?></label>
                    <input class="form-control" name="mobile" value="<?php if (isset($organization)) echo Html::encode($organization['mobile']);?>">
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'E-mail'); ?></label>
                    <input class="form-control" name="email" value="<?php if (isset($organization)) echo Html::encode($organization['email']);?>">
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Fax'); ?></label>
                    <input class="form-control" name="fax" value="<?php if (isset($organization)) echo Html::encode($organization['fax']);?>">
                </div>
            </div>
            <h4><?php echo Yii::t('references', 'Address for correspondence'); ?></h4>
            <div class="panel-body">
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Postcode'); ?></label>
                    <input class="form-control" id="postcode" name="fulladdresses[2][postcode]" value="<?php if (isset($organization)) echo Html::encode($organization['address_2_postcode']);?>">
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Region'); ?></label>
                    <select class="form-control" name="fulladdresses[2][regions_id]">
                        <option></option>
                        <?php foreach ($regions as $key => $value) { ?>
                            <option  value="<?=$value['id'];?>" <?php if (isset($organization)) if ($value['id'] == $organization['address_2_id']) echo 'selected' ?>>
                                <?=$value['title'];?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Locality'); ?></label>
                    <input class="form-control" id="locality" name="fulladdresses[2][locality]" value="<?php if (isset($organization)) echo Html::encode($organization['address_2_locality']);?>">
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Address'); ?></label>
                    <input class="form-control" id="address" name="fulladdresses[2][address]" value="<?php if (isset($organization)) echo Html::encode($organization['address_2_address']);?>">
                </div>
            </div>
            <h4><?php echo Yii::t('references', 'Main address'); ?></h4>
            <div class="panel-body">
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Postcode'); ?></label>
                    <input class="form-control" name="fulladdresses[0][postcode]" value="<?php if (isset($organization)) echo Html::encode($organization['address_0_postcode']);?>">
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Region'); ?></label>
                    <select class="form-control" name="fulladdresses[0][regions_id]">
                        <option></option>
                        <?php foreach ($regions as $key => $value) { ?>
                            <option value="<?=$value['id'];?>" <?php if (isset($organization)) if ($value['id'] == $organization['address_0_id']) echo 'selected' ?>>
                                <?=$value['title'];?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Locality'); ?></label>
                    <input class="form-control" id="locality" name="fulladdresses[0][locality]" value="<?php if (isset($organization)) echo Html::encode($organization['address_0_locality']);?>">
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Address'); ?></label>
                    <input class="form-control" id="address" name="fulladdresses[0][address]" value="<?php if (isset($organization)) echo Html::encode($organization['address_0_address']);?>">
                </div>
            </div>
            <h4><?php echo Yii::t('references', 'Additional address'); ?></h4>
            <div class="panel-body">
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Postcode'); ?></label>
                    <input class="form-control" id="postcode" name="fulladdresses[1][postcode]" value="<?php if (isset($organization)) echo Html::encode($organization['address_1_postcode']);?>">
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Region'); ?></label>
                    <select class="form-control" name="fulladdresses[1][regions_id]">
                        <option></option>
                        <?php foreach ($regions as $key => $value) { ?>
                            <option  value="<?=$value['id'];?>" <?php if (isset($organization)) if ($value['id'] == $organization['address_1_id']) echo 'selected' ?>>
                                <?=$value['title'];?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Locality'); ?></label>
                    <input class="form-control" id="locality" name="fulladdresses[1][locality]" value="<?php if (isset($organization)) echo Html::encode($organization['address_1_locality']);?>">
                </div>
                <div class="form-group">
                    <label for=""><?php echo Yii::t('references', 'Address'); ?></label>
                    <input class="form-control" id="address" name="fulladdresses[1][address]" value="<?php if (isset($organization)) echo Html::encode($organization['address_1_address']);?>">
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="accounts">
            <h4><?php echo Yii::t('references', 'Accounts'); ?></h4>
            <div class="form-group" id="add-accounts">
                <input type="hidden" id="count-accounts" value="<?php if (isset($accountDataProvider)) echo '-1'; else echo '0'; ?>">
                <?php if (isset($accountDataProvider)) foreach ($accountDataProvider as $key => $value) {?>
                    <?= AccountsFormWidget::widget(['i' => $value['id'], 'data' => $value]) ?>
                <?php } ?>
            </div>
            <p class="text-right">
                <a id="add-account-button" class="btn btn-default">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </p>
        </div>
    </div>
</div>

<div style="width: 100%; float: left;">
    <button id="create-button" type="button" class="btn btn-primary">
        <?php if (isset($id)) echo Yii::t('references', 'Save'); else echo Yii::t('references', 'Create'); ?>
    </button>
</div>

<?php if (isset($id)) echo '</form>'; ?>