<?php
    use app\components\AccountsFormWidget;
?>
<?php 
    if (isset($data))
        echo AccountsFormWidget::widget( ['i' => $i, 'data' => $data] );
    else
        echo AccountsFormWidget::widget( ['i' => $i] );
?>