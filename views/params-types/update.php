<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblParamsTypes */

$this->title = '№' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('references_params_types', 'Params Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('references_params_types', 'Update');
?>
<div class="tbl-params-types-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
