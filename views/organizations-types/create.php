<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblOrganizationsTypes */

$this->title = Yii::t('references_organizations_types', 'Create organizations types');
$this->params['breadcrumbs'][] = ['label' => Yii::t('references_organizations_types', 'Organizations Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-organizations-types-create">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body" id="panel-body-create-form">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>
