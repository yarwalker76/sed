<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblOrganizationsTypes */

$this->title = $model->type;
$this->params['breadcrumbs'][] = ['label' => Yii::t('references_organizations_types', 'Organizations Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('references_organizations_types', 'Update');
?>
<div class="tbl-organizations-types-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
