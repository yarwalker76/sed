<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TblOrganizationsTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('references_organizations_types', 'Organizations Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-organizations-types-index shadow-box">
    <div class = "row">
        <div class = "col-xs-12 tables">
            <h2 class="title"><?= Html::encode($this->title) ?></h2>
            <div class="col-xs-12">
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <br>
                <p>
                    <?= Html::a(Yii::t('references_organizations_types', 'Create organizations types'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php Pjax::begin(); ?>    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'type',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>
