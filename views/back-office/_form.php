<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\jui\DatePicker;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\LegalService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="back-office-form">
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <!--input type="hidden" id="backoffice-id" class="form-control" name="BackOffice[id]" value="<?= ( $model->isNewRecord ? 0 : $model->id ) ?>"-->
        <input type="hidden" id="backoffice-proc_id" class="form-control" name="BackOffice[proc_id]" value="<?= Yii::$app->session->get('current_proc_id') ?>">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-4">
                    <?= $form->field($model, 'number')->textInput(['type' => 'number', 'value' => ( $model->number ? $model->number : $model->maxNumber + 1)]) ?>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <?=
            $form->field($model, 'data_time')->textInput()->widget(DatePicker::classname(), [
                'language' => 'ru',
                'options' => ['value' => ( $model->data_time ? date('d.m.Y', strtotime($model->data_time)) : date('d.m.Y') )],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy'
                ]
                /*
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control', 'required' => true,],
                
                'name' => 'check_issue_date',
                'value' => date('d-M-Y', strtotime('+2 days')),
                'options' => ['placeholder' => 'Select issue date ...'],
                'pluginOptions' => [
                    'format' => 'dd-M-yyyy',
                    'todayHighlight' => true
                ]*/
            ])
            ?> 
        </div>
        <div class="col-xs-12">
<?= $form->field($model, 'event')->textarea(['maxlength' => true, 'rows' => 7, 'required' => true]) ?>
        </div>
        <div class="col-xs-12">
<?= $form->field($model, 'result')->textarea(['maxlength' => true, 'rows' => 7, 'required' => true]) ?>
        </div>
        <div class="col-xs-12 form-group">
<?= Html::submitButton($model->isNewRecord ? Yii::t('BackOffice', 'Create') : Yii::t('BackOffice', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

<?php ActiveForm::end(); ?>
    </div>
</div>
