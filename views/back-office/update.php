<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BackOffice */

$this->title = Yii::t('BackOffice', 'Update Record', [
    'modelClass' => 'Back Office',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('BackOffice', 'Back Office'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('BackOffice', 'Update');
?>
<div class="back-office-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
