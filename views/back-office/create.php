<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BackOffice */

$this->title = Yii::t('BackOffice', 'Create record');
$this->params['breadcrumbs'][] = ['label' => Yii::t('BackOffice', 'Back Office'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="back-office-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
