<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('BackOffice', 'Back Office');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="back-office-index shadow-box">
    <div class = "row">
        <div class = "col-xs-12 tables">
            <h2 class="title"><?= Html::encode($this->title) ?></h2>
            <div class="col-xs-12">
                <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
                <br>
                <p><?php 
                    if( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') ): 
                       echo Html::a(Yii::t('BackOffice', 'Create Record'), ['create'], ['class' => 'btn btn-success']);
                    endif; 
                    ?>
                    
                </p>
                <?php Pjax::begin(['id' => 'back-office']); ?>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                      //  'id',
                        [
                            'attribute' => 'number', 
                            'headerOptions' => [ 'width' => '50' ], 
                            'contentOptions' => [ 'style' => 'text-align: center' ]
                        ],
                        [
                            'attribute' => 'data_time',
                            'format' => ['date', 'php:d.m.Y'],
                            'headerOptions' => [ 'width' => '130' ],
                            'contentOptions' => [ 'style' => 'text-align: center' ]
                        ], 
                        //'number',
                        //'data_time',
                        'event',
                        'result',
                        [ 'class' => 'yii\grid\ActionColumn',
                          'template' => ( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') ? '{update} {delete}' : '{view}' ) 
                                
                        ],
                    ],
                    'tableOptions' => [
                        'class' => 'sTables table'
                    ],
                ]);
                ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>