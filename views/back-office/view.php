<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BackOffice */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('BackOffice', 'Back Office'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="back-office-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if( Yii::$app->user->can('manager') || Yii::$app->user->can('backOffice') ): ?>
        <?= Html::a(Yii::t('BackOffice', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('BackOffice', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('BackOffice', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
         //   'id',
            'number',
            'data_time',
            'event',
            'result',
        ],
    ]) ?>

</div>
