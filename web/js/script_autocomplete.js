$(document).ready(function(){

    $('#find-by-title').autocomplete({
        serviceUrl: '/references/findbytitle',
        onSelect: function (suggestion) {
            $('#find-by-title').val(suggestion.value);
            $.post('/references/listing', { "id":suggestion.organizations_id }, function(data) {
                $('#list').html(data);
            }, "html");
        }
    });

    $(document).on('click', '#find-by-title', function(){
        $('#find-by-inn').val('');
        $('#organizations-types-id #create-type-blank').prop('selected', true);
        //$('#list').html('');
    });

    $('#find-by-inn').autocomplete({
        serviceUrl: '/references/findbyinn',
        onSelect: function (suggestion) {
            $('#find-by-inn').val(suggestion.value);
            $.post('/references/listing', { "id":suggestion.organizations_id }, function(data) {
                $('#list').html(data);
            }, "html");
        }
    });

    $(document).on('click', '#find-by-inn', function(){
        $('#find-by-title').val('');
        $('#organizations-types-id #create-type-blank').prop('selected', true);
        //$('#list').html('');
    });

});