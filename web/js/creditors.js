/**
 * Created by shnm on 3/28/17.
 */
$(document).ready(function(){
    $('#find-organization-by-title').autocomplete({
        serviceUrl: '/references/findbytitle',
        minChars: 0,
        params: {
            types: [1, 7]
        },
        onSelect: function (suggestion) {
            $('#find-organization-by-title').val(suggestion.value);
            $('#found-organization').val(suggestion.organizations_id);
        }
    });

    $('#find-creditor-by-title').autocomplete({
        serviceUrl: '/creditors/default/find-by-title',
        minChars: 0,
        onSelect: function (suggestion) {
            $('#find-creditor-by-title').val(suggestion.value);
            $('#found-creditor').val(suggestion.id);
        }
    });
});

$(document).on('change', '#update-initiation', function () {
    var form = $(this).closest('form');
    form.find('#update-container').fadeToggle();

    if (form.find('#find-organization-by-title').length) {
        if (this.checked) {
            $('#find-organization-by-title').removeAttr('readonly');
        } else {
            $('#find-organization-by-title').attr('readonly', true);
        }
    }

    if (form.find('#creditor-select-params').length) {
        $('#creditor-select-params').fadeToggle();
        $('#creditor-current-params').fadeToggle();

        if (this.checked) {
            selectType(queueDropdown.val());
        }
    }
});

$('#update-details-initiation').change(function () {
    $('#update-details-container').fadeToggle();

    if (this.checked) {
        $('.base-attributes input').attr('readonly', true);
    } else {
        $('.base-attributes input').removeAttr('readonly');
    }
});

$('#enable-toggle').change(function () {
    $('#enabled-block').fadeToggle();
    $('#disable-toggle').attr('disabled', !$('#disable-toggle').attr('disabled'));
});

$('#disable-toggle').change(function () {
    $('#disabled-block').fadeToggle();
    $('#enable-toggle').attr('disabled', !$('#enable-toggle').attr('disabled'));
});

var TYPE_GUARANTEE = String(3);
var TYPE_REQUIREMENT = String(5);

var guaranteeTab = $('#guarantee-tab');
var creditorTitleContainer = $('#find-creditor-by-title-container');
var requestDropdownContainer = $('#find-creditor-by-request-container');
var requirementInputs = $('#requirement-fields');
var queueDropdown = $('#queue-dropdown');
var requestTypeInput = $('#request-type');

var selectType = function (type) {
    if (type === TYPE_REQUIREMENT) {
        creditorTitleContainer.hide();
        requestDropdownContainer.show();
        requirementInputs.show();
        requestTypeInput.hide();
    } else {
        if (type === TYPE_GUARANTEE) {
            guaranteeTab.show();
        } else {
            guaranteeTab.hide();
        }

        requestTypeInput.show();
        requestDropdownContainer.hide();
        requirementInputs.hide();
        creditorTitleContainer.show();
    }
};

queueDropdown.on('change', function() {
    selectType(this.value);
});

$('#creditor-request-dropdown').on('change', function() {
    $('#found-creditor').val(this.value);

    if (this.value) {
        var total = $(this).parents('form').find('#total');

        $.post({
            url: '/creditors/request/get-as-json',
            data: {
                'id': this.value
            },
            success: function (response) {
                total.val(response.total);
            }
        })
    }
});

$('#calculate-percents').click(function (e) {
    e.preventDefault();

    var total = $('#total').val();
    var percentage = $('#requirement-percentage').val();
    var dayCount = $('#requirement-day-count').val();
    var res = total / 360 / 100 * dayCount * percentage;

    $('#requirement-total-accrued').val(res.toFixed(2));
});

$('#guarantee-total').on('input', function () {
    var total = $('#total').val();
    var form = $(this).parents('form');
    var ratio = form.find('#guarantee-total-ratio');
    var res = $(this).val() / total * 100;

    ratio.val(res.toFixed(2));
});


$('#amortization-total-payed, #amortization-total').on('input', function () {
    var form = $(this).parents('form');

    var total = form.find('#amortization-total');
    var payed = form.find('#amortization-total-payed');
    var ratio = form.find('#amortization-total-ratio');

    var res = payed.val() / total.val() * 100;

    ratio.val(res.toFixed(2));
});