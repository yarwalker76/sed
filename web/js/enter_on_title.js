$(document).ready(function() {
    $('#find-by-title').keypress(function(e){
        if(e.which == 13) {
            var find = $('#find-by-title').val();
            if(find.length){
                window.location.href= '/references?fname=' + find;
            }else{
                window.location.href= '/references';
            }
        }    
    });
    $('#find-by-inn').keypress(function(e){
        if(e.which == 13) {
            var find = $('#find-by-inn').val();
            if(find.length){
                window.location.href= '/references?finn=' + find;
            }else{
                window.location.href= '/references';
            }
        }    
    });
});
