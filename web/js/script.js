    $(document).ready(function(){
        
        $(document).on('change', '#create-type',function(){
            id = $('#create-type').val();
            if (id != '') {
                $.post('/references/forms', {id: id}, function(form) { 
                    $('#create-data').html(form);
                }, "html");
                $('#create-form-status-panel').attr('class', 'panel-danger');
                $('#create-form-status').hide();
                $('#create-form-status').html('');
            } else
                $('#create-data').html('');
        });
        
        $(document).on('click', '#create-button',function(){
            type = $('#create-type').val();
            if (type != '') {
                var formdata = $('#create-form').serializeArray();
                //console.log(formdata);
                $.post('/references/save', formdata, function(data) { 
                    console.log(data.status);
                    $('#create-form-status-panel').attr('class', 'panel-danger');
                    if (data.status == 1) {
                        $('#create-data').html('');
                        $('#create-type #create-type-blank').prop('selected', true);
                        $('#create-form-status-panel').attr('class', 'panel-success');
                    }
                    $('#create-form-status').html(data.message + '<br>' + data.errors);
                    $('#create-form-status').show();
                }, "json");
            } else
                $('#create-data').html('');
        });

        $(document).on('click', '#add-account-button', function(){
            i = $('#count-accounts').val();
            if (i >= 0){
                $('#count-accounts').val( parseInt(i) + 1 );
                $.post('/references/accountform', {"i":i}, function(data) { 
                    $('#add-accounts').append(data);
                }, "html");
            } else {
                id = $('#organization_id').val(); 
                $.post('/references/accountadd', {"id":id}, function(data) { 
                    $('#add-accounts').append(data);
                }, "html");
            }
        });

        $(document).on('click', '.delete-account-button', function(){
            name = $(this).children('#delete-account-block-id').val();
            id = $(this).children('#delete-account-id').val();
            if (id !== '')
                $.post('/references/accountdelete', {"id":id}, function(data) {
                    //console.log(data);
                    $('#create-form-status-panel').attr('class', 'panel-danger');
                    if (data.status == 1)
                        $('#create-form-status-panel').attr('class', 'panel-success');
                    $('#create-form-status').html(data.message);
                    $('#create-form-status').show();
                    $('#create-form-status').fadeOut(15000);
                }, "json");
            $('#' + name).detach(); 
        });
        
        $(document).on('change', '#organizations-types-id', function(){
            organizations_types_id = $('#organizations-types-id').val();
            $('#find-by-inn').val('');
            $('#find-by-title').val('');
            if (organizations_types_id == ''){
                $.get('/references/listingbyot', { "ot": null }, function(data) {
                    $('#list').html(data);
                }, "html");
            } else {
                $.get('/references/listingbyot', { "ot": organizations_types_id }, function(data) {
                    $('#list').html(data);
                }, "html");
            }
        });
        
        $('.nav-vertical a.dropdown-toggle').on('click', function(ev){
            ev.preventDefault();
            $(this).next('ul.dropdown-menu').slideToggle(350);
        });

        $('#proc-list').on('click', function(ev){
            ev.preventDefault();
            $(this).next('ul.dropdown-menu').slideToggle(350);
        });

        $('#proc-list + ul.dropdown-menu a').on('click', function(ev){
            ev.preventDefault();
            $this = $(this);
            
            /*$('#proc-list h1').html( $this.text() + ' <b class="caret">' );
            $('.status-icon').html($this.data('type'));
            $('#proc-list').next('ul.dropdown-menu').slideToggle(350); */

            $.post('/procedures/default/change-current-procedure', { 'proc_id': $this.attr('href') }, function(data){ 
                if( data.msg === 0 ){ 
                    // все ок
                    $('#proc-list h1').html( $this.data('name') + ' <b class="caret">' );
                    $('.status-icon').html( $this.data('type') );

                    // обновим GridView с записями - документооборот
                    if( $('div[id$=docs-pjax]').length ) {
                        console.log('change 1');
                        $.pjax.reload({container: 'div[id$=docs-pjax]'});
                    }
                    
                    // обновление для свойств процедур
                    if( $('#common-properties').length ) {
                        console.log('change 2');
                        $.pjax.reload({container: '#common-properties'});
                    }
//                    
                    if( $('#additional-properties').length ) {
                        console.log('change 3');
                        setTimeout(function(){
                            $.pjax.reload({container: '#additional-properties'});
                        },200);
                    }
                    
                    // обновление для юр. службы
                    if( $('#legal-service').length ) {
                        console.log('change 4');
                        $.pjax.reload({container: '#legal-service'});
                    }
                    
                    // обновление для бэкофиса
                    if( $('#back-office').length ) {
                        console.log('change 4');
                        $.pjax.reload({container: '#back-office'});
                    }

                    if (window.location.href.indexOf('/creditors')) {
                        location.reload();
                    }

                    if (window.location.href.indexOf('/meetings')) {
                        location.href = '/meetings';
                    }
                } else {
                    $('#modalMessage .modal-body').html( '<p>' + data.msg + '</p>' );
                    $('#modalMessage').modal('show');    
                }

                $('#proc-list').next('ul.dropdown-menu').slideToggle(350);
            }, "json")
            .fail(function(error){
                $('#modalMessage .modal-body').html('<p>' + error.responseText + '</p>');
                $('#modalMessage').modal('show');
            });
        });
        
    });