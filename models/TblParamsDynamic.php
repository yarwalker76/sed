<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\DynamicModel;
use app\models\TblParams;
use app\models\TblParamsTypes;

class TblParamsDynamic extends DynamicModel
{

    public function supervalidate($param, $organizations_types_id)
    {
        $data = self::getParamsByOrganizationsTypesId($organizations_types_id);
        //echo ' TblParamsSearch <pre>'; print_r($data); echo '</pre>';

        $rules = array(); $labels = array();
        foreach ($data as $key => $value) {
            if ( $value['min'] != null )
                array_push($rules, [ 'p' . $value['id'], $value['type'], 'min' => $value['min'], 'max' => $value['max'] ]);
            else
                array_push($rules, [ 'p' . $value['id'], $value['type'], 'max' => $value['max']]);
            if ( $value['required'] == 1 )
                array_push($rules, [ 'p' . $value['id'], 'required' ]);
            $labels['P' . $value['id']] = $value['title'];
        }    
        //echo ' Rules <pre>'; print_r($rules); echo '</pre>';
        //echo ' Labels <pre>'; print_r($labels); echo '</pre>';
        
        $params = array();
        foreach ($param as $key => $value)
            $params[ 'p' . $key ] = $value;
        //echo ' Param <pre>'; var_dump($params);  echo '</pre>';

        $model = DynamicModel::validateData( $params, $rules );
        //echo ' Errors <pre>'; print_r( $model->getErrors() );  echo '</pre>';
        
        $arr1 = $model->getErrors();
        $arr2 = array();
        foreach ($arr1 as $key => $value)
            foreach ($value as $k => $v) {
                preg_match("/(.*)(\«)(.*)(\»)(.*)/i", $v, $matches);
                //print_r($matches);
                $arr2[$key][$k] = str_replace($matches[3], $labels[$matches[3]], $v);
            }
        //print_r($arr2);
        
        return $arr2;
    }
    
    public static function getParamsByOrganizationsTypesId($type)
    {
        $query = (new \yii\db\Query())->
            select(['p.id', 'p.title', 'p.tab', 'pt.type', 'pt.min', 'pt.max', 'pt.required'])->
            from( TblParams::tableName() . ' as p')->
            leftJoin( TblParamsTypes::tableName() . ' as pt', 'pt.id = p.params_types_id')->
            where(['p.organizations_types_id' => $type])->
            orderBy('p.tab ASC, p.top ASC')->
            all();
        return $query;
    }
}
