<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "legal_service".
 *
 * @property integer $id
 * @property integer $number
 * @property string $data_time
 * @property string $event
 * @property string $result
 */
class LegalService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'legal_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'integer'],
            [['data_time'], 'safe'],
            [['data_time', 'event','result'], 'required'],
            [['event', 'result'], 'string', 'max' => 2000],
            ['proc_id', 'integer'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('LegalService', 'ID'),
            'number' => Yii::t('LegalService', 'Number'),
            'data_time' => Yii::t('LegalService', 'Data Time'),
            'event' => Yii::t('LegalService', 'Event'),
            'result' => Yii::t('LegalService', 'Result'),
        ];
    }
    
    /**
    * get maximum serial number
    */
    public function getMaxNumber()
    {
        return $this->find()->select('max(number)')->where(['proc_id' => Yii::$app->session->get('current_proc_id')])->scalar();
    }
    
    public function beforeValidate()
    {
        if ($this->data_time != null) {
            $new_date_format = date('Y-m-d', strtotime($this->data_time));
            $this->data_time = $new_date_format;
        }
        
        return parent::beforeValidate();
    }
}
