<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblOrganization;

/**
 * TblOrganizationSearch represents the model behind the search form about `app\models\TblOrganization`.
 */
class TblOrganizationSearch extends TblOrganization
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inn', 'kpp', 'ogrn'], 'integer'],
            [['name', 'leader'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblOrganization::find();

        // add conditions that should always apply here

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
           // 'inn' => $this->inn,
            //'kpp' => $this->kpp,
            //'ogrn' => $this->ogrn,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'leader', $this->leader]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        return $dataProvider;
    }
}
