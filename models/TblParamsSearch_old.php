<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\TblParams;
use app\models\TblParamsTypes;

class TblParamsSearch extends TblParams
{
    public static function search($type)
    {
        $query = (new \yii\db\Query())->
            select(['p.id', 'p.title', 'p.tab', 'pt.type', 'pt.min', 'pt.max', 'pt.required'])->
            from( TblParams::tableName() . ' as p')->
            leftJoin( TblParamsTypes::tableName() . ' as pt', 'pt.id = p.params_types_id')->
            where(['p.organizations_types_id' => $type])->
            orderBy('p.tab ASC, p.top ASC')->
            all();
        return $query;
    }
}
