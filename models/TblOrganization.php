<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_organization".
 *
 * @property integer $id
 * @property string $name
 * @property string $leader
 * @property integer $inn
 * @property integer $kpp
 * @property integer $ogrn
 */
class TblOrganization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'tbl_organizations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inn', 'kpp', 'ogrn'], 'integer'],
            [['inn', 'kpp', 'ogrn'], 'required'],
            [['name', 'leader'], 'string', 'max' => 255],
            [['name', 'leader'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('handbook', 'ID'),
            'name' => Yii::t('handbook', 'Name:'),
            'leader' => Yii::t('handbook', 'Leader:'),
            'inn' => Yii::t('handbook', 'Inn:'),
            'kpp' => Yii::t('handbook', 'Kpp:'),
            'ogrn' => Yii::t('handbook', 'Ogrn:'),
        ];
    }
}
