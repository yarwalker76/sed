<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use app\models\TblAccounts;

class TblAccountsSearch extends TblAccounts
{

    public function search($id)
    {
        $query = ( new Query() )->
            select( 'a.*, v.value title' )->
            from(TblAccounts::tableName() . ' as a')->
            leftJoin(TblOrganizations::tableName() . ' as o', 'o.id = a.banks_id ')->
            leftJoin(TblParams::tableName() . ' as p', 'o.organizations_types_id = p.organizations_types_id')->
            leftJoin(TblValues::tableName() . ' as v', '(v.params_id = p.id) and (v.organizations_id = o.id)')->
            where( ['a.organizations_id' => $id, 'p.title' => 'Название' ] );
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
}
