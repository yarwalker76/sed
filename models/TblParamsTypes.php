<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_params_types".
 *
 * @property integer $id
 * @property string $type
 * @property string $min
 * @property string $max
 * @property integer $required
 */
class TblParamsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_params_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['min', 'max', 'required'], 'integer'],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('references', 'ID'),
            'type' => Yii::t('references', 'Название типа параметра'),
            'min' => Yii::t('references', 'Ограничение снизу'),
            'max' => Yii::t('references', 'Ограничение сверху'),
            'required' => Yii::t('references', 'Обязательность заполнения'),
        ];
    }
}
