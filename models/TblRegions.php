<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_regions".
 *
 * @property integer $id
 * @property string $title
 */
class TblRegions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('references', 'ID'),
            'title' => Yii::t('references', 'Название региона'),
        ];
    }
}
