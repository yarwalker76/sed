<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\TblOrganization;

class TblOrganizationsSearch extends TblOrganizations
{
    public function rules()
    {
        return [  
            //[['INN'], 'integer'],
            //[['title'], 'string', 'max' => 255],
            //[['organizations_types_id'], 'string'],
            //[['email'], 'email'],
        ]; 
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($id = null, $ot = null, $fi = null, $inn = null)
    {
    
        $filter_id = '';
        if ($id !== null) $filter_id = ' and (o.id = '. (int)$id .') ';
        
        $filter_ot = '';
        if ($ot !== null) $filter_ot = ' and (o.organizations_types_id = '. (int)$ot .') ';
        
        $filter_fi = '';
        if ($fi !== null) 
        {
            $fi = preg_replace('/[^0-9a-zа-я\s\.\-\_]/ui', '', $fi);
            $filter_fi = ' and (p.title IN("Название", "Фамилия") AND v.value LIKE "%'. $fi .'%" ) ';
        }
        
        $filter_inn = '';
        if($inn !== null)
        {
            $inn = preg_replace('/[^0-9]/', '', $inn);
            // $filter_inn = ' and (p.title="ИНН" AND v.value LIKE "%'. $inn .'%" ) ';
            $query = 
                (new \yii\db\Query())->
                    select('v.organizations_id')->
                    from(TblValues::tableName() . ' as v')->
                    leftJoin( TblOrganizations::tableName() . ' as o', 'o.id = v.organizations_id')->
                    leftJoin( TblParams::tableName() . ' as p', 'p.id = v.params_id')->
                    where(['p.title' => "ИНН"])->
                    andFilterWhere(['like', 'v.value', $inn])->
                    orderBy('v.value ASC')->
                    all();
            $arr = array();
            foreach($query as $q) $arr[] = $q['organizations_id'];
            $filter_inn = ' AND o.id IN('.implode(',', $arr).') ';
        }
        
        $sql = 'SELECT * FROM ((SELECT o.id id, o.phone, o.mobile, o.email, ot.type as organizations_type, v.value values_value, "" as values_value2 ' . 
                ' FROM ' . TblOrganizations::tableName() . ' as o ' .
                ' LEFT JOIN ' . TblOrganizationsTypes::tableName() . ' as ot ON (ot.id = o.organizations_types_id) ' .
                ' LEFT JOIN ' . TblParams::tableName() . ' as p ON (o.organizations_types_id = p.organizations_types_id) ' .
                ' LEFT JOIN ' . TblValues::tableName() . ' as v ON (v.params_id = p.id) and (v.organizations_id = o.id) ' .
                ' WHERE 1 AND p.title = "Название" ';
        $sql .= "$filter_id $filter_ot $filter_fi $filter_inn";        
        $sql .=')' .
                ' UNION ' .
                '(SELECT o.id id, o.phone, o.mobile, o.email, ot.type as organizations_type, "" as values_value, v.value values_value2 ' . 
                ' FROM ' . TblOrganizations::tableName() . ' as o ' .
                ' LEFT JOIN ' . TblOrganizationsTypes::tableName() . ' as ot ON (ot.id = o.organizations_types_id) ' .
                ' LEFT JOIN ' . TblParams::tableName() . ' as p ON (o.organizations_types_id = p.organizations_types_id) ' .
                ' LEFT JOIN ' . TblValues::tableName() . ' as v ON (v.params_id = p.id) and (v.organizations_id = o.id) ' .
                ' WHERE 1 AND p.title = "Фамилия" ';
        $sql .= "$filter_id $filter_ot $filter_fi $filter_inn";
        $sql .= ')) AS Z ';

        $sql .= ' GROUP BY id ORDER BY id ';

        $sql_count = 'SELECT  COUNT(*) FROM ' . TblOrganizations::tableName() . ' as o ' .
                ' LEFT JOIN ' . TblOrganizationsTypes::tableName() . ' as ot ON (ot.id = o.organizations_types_id) ' .
                ' LEFT JOIN ' . TblParams::tableName() . ' as p ON (o.organizations_types_id = p.organizations_types_id) ' .
                ' LEFT JOIN ' . TblValues::tableName() . ' as v ON (v.params_id = p.id) and (v.organizations_id = o.id) ' .
                ' WHERE 1 AND p.title IN("Название", "Фамилия") ';
        $sql_count .= "$filter_id $filter_ot $filter_fi $filter_inn";

        $totalCount = Yii::$app->db->createCommand($sql_count)->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => (int) $totalCount,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        /*
        $query1 = (new \yii\db\Query())->
            select('o.id id, o.phone, o.mobile, o.email, ot.type as organizations_type, v.value values_value, v.value as values_value2')->
            from( TblOrganizations::tableName() . ' as o ' )->
            leftJoin( TblOrganizationsTypes::tableName() . ' as ot', 'ot.id = o.organizations_types_id')->
            leftJoin( TblParams::tableName() . ' as p', 'o.organizations_types_id = p.organizations_types_id')->
            leftJoin( TblValues::tableName() . ' as v', '(v.params_id = p.id) and (v.organizations_id = o.id) ')->
            where(['p.title' => "Название"]);
        $query2 = (new \yii\db\Query())->
            select('o.id id, o.phone, o.mobile, o.email, ot.type as organizations_type, v.value as values_value, v.value values_value2')->
            from( TblOrganizations::tableName() . ' as o ' )->
            leftJoin( TblOrganizationsTypes::tableName() . ' as ot', 'ot.id = o.organizations_types_id')->
            leftJoin( TblParams::tableName() . ' as p', 'o.organizations_types_id = p.organizations_types_id')->
            leftJoin( TblValues::tableName() . ' as v', '(v.params_id = p.id) and (v.organizations_id = o.id) ')->
            where(['p.title' => "Фамилия"]);
        $query1->union($query2);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query1,
        ]);
        */
        
        $this->load($id);
        if (!$this->validate()) {
            return $dataProvider;
        }

        //$query1->andFilterWhere(['like', 'title', $this->title]); //->andFilterWhere(['like', 'INN', $this->INN]);
        //$query->all();
        
        return $dataProvider;
    }
    
    static function findBanks()
    {
        $query = (new \yii\db\Query())->
            select(['v.value', 'o.id'])->
            from(TblValues::tableName() . ' as v')->
            leftJoin( TblParams::tableName() . ' as p', 'v.params_id=p.id')->
            leftJoin( TblOrganizations::tableName() . ' as o', 'v.organizations_id=o.id')->
            where(['o.organizations_types_id' => 2, 'p.title' => 'Название'])->
            all();
        return $query;
    }
    
    static function findByTitle()
    {
        $title = \Yii::$app->request->get('query');
        $types = \Yii::$app->request->get('types');
        $query1 =
            (new \yii\db\Query())->
            select(['v.value', 'v.organizations_id'])->
            from(TblValues::tableName() . ' as v')->
            leftJoin( TblOrganizations::tableName() . ' as o', 'o.id = v.organizations_id')->
            leftJoin( TblParams::tableName() . ' as p', 'p.id = v.params_id')->
            where(['p.title' => "Название"])->
            andFilterWhere(['like', 'v.value', $title])->
            andFilterWhere(['o.organizations_types_id' => $types])->
            orderBy('v.value ASC')->
            all();
        $query2 = 
            (new \yii\db\Query())->
            select(['v.value', 'v.organizations_id'])->
            from(TblValues::tableName() . ' as v')->
            leftJoin( TblOrganizations::tableName() . ' as o', 'o.id = v.organizations_id')->
            leftJoin( TblParams::tableName() . ' as p', 'p.id = v.params_id')->
            where(['p.title' => "Фамилия"])->
            andFilterWhere(['like', 'v.value', $title])->
            andFilterWhere(['o.organizations_types_id' => $types])->
            orderBy('v.value ASC')->
            all();
        if (count($query2) > 0) $query1 = array_merge ($query1, $query2);
        return $query1;
    }
    
    static function findByINN()
    {
        $title = \Yii::$app->request->get('query');
        $query = 
            (new \yii\db\Query())->
            select(['v.value', 'v.organizations_id'])->
            from(TblValues::tableName() . ' as v')->
            leftJoin( TblOrganizations::tableName() . ' as o', 'o.id = v.organizations_id')->
            leftJoin( TblParams::tableName() . ' as p', 'p.id = v.params_id')->
            where(['p.title' => "ИНН"])->
            andFilterWhere(['like', 'v.value', $title])->
            orderBy('v.value ASC')->
            all();
        return $query;
    }
    
    static function findByOrganizationsTypes()
    {
        $title = \Yii::$app->request->get('query');
        $query = 
            (new \yii\db\Query())->
            select(['v.value', 'v.organizations_id'])->
            from(TblValues::tableName() . ' as v')->
            leftJoin( TblOrganizations::tableName() . ' as o', 'o.id = v.organizations_id')->
            leftJoin( TblParams::tableName() . ' as p', 'p.id = v.params_id')->
            where(['p.title' => "ИНН"])->
            andFilterWhere(['like', 'v.value', $title])->
            orderBy('v.value ASC')->
            all();
        return $query;
    }
}
