<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/16/17
 * Time: 7:24 PM
 */

namespace app\models;


class AddressHelper
{
    /**
     * @param $attributes
     * @return string
     */
    public static function build($attributes)
    {
        $res = '';

        foreach ($attributes as $key => $value) {
            $comma = ', ';

            if ($key === count($attributes) - 1) {
                $comma = '';
            }

            if ($value) {
                $res .= $value . $comma;
            }
        }

        return $res;
    }
}