<?php
namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\procedures\models\Procedures;
use app\modules\procedures\models\LastSeenProcedures;
use app\models\TblOrganizationsMethods;

class AppHelper {
    public static function getUserProceduresList()
    {
        $dropdowns = [];

        $procedures = Procedures::find()
                        ->select('procedures.*')
                        ->leftJoin('procedure_relations', '`procedure_relations`.`proc_id` = `procedures`.`id`')
                        ->where([ 'procedure_relations.user_id' => Yii::$app->user->identity->id, 'status' => 'in_work' ])
                        //->orderBy(['procedures.id' => SORT_DESC])
                        ->all();

        ArrayHelper::multisort($procedures, function ($value) {
            return $value->getName();
        });

        foreach( $procedures as $proc ):
            $dropdowns[] = [
                        'url' => $proc->id,
                        'label' => $proc->getName() . ' ' . $proc->type->type_abbr,
                        'linkOptions' => [ 'data-type' => $proc->type->type_abbr, 'data-name' => $proc->getName() ]
                        ];
        endforeach;

        return $dropdowns;
    }

    public static function getUserProceduresIDList()
    {
        $procedures = Procedures::find()
                        ->select('procedures.id')
                        ->leftJoin('procedure_relations', '`procedure_relations`.`proc_id` = `procedures`.`id`')
                        ->where([ 'procedure_relations.user_id' => Yii::$app->user->identity->id, 'status' => 'in_work' ])
                        ->asArray()
                        ->all();
        foreach($procedures as &$p):
            $p = $p['id'];
        endforeach;

        return $procedures;
    }

    public static function getLastSeenProcedure()
    {
        $manager_id = Yii::$app->user->identity->id;
        $name = $type = '';

        if( !is_null($manager_id) ):

            $last_procedure = LastSeenProcedures::findOne([ 'user_id' => $manager_id ]);
            if( is_object( $last_procedure ) && !empty( $last_procedure ) ):
                $procedure = Procedures::findOne( $last_procedure->proc_id );
                $name = $procedure->getName();
                $type = $procedure->type->type_abbr;
                Yii::$app->session->set( 'current_proc_id', $procedure->id );
            else:
                $procedure = Procedures::find()
                    ->select('procedures.*')
                    ->leftJoin('procedure_relations', '`procedure_relations`.`proc_id` = `procedures`.`id`')
                    ->where([ 'procedure_relations.user_id' => $manager_id ])
                    ->orderBy(['procedures.id' => SORT_DESC])
                    ->one();

                if( is_object($procedure) && !empty($procedure) ):
                    $last_procedure = new LastSeenProcedures();
                    $last_procedure->user_id = $manager_id;
                    $last_procedure->proc_id = $procedure->id;

                    if ($last_procedure->save() !== false):
                        // update successful
                        $name = $procedure->getName();
                        $type = $procedure->type->type_abbr;
                        // сохраним выбранную процедуру в сессию
                        Yii::$app->session->set( 'current_proc_id', $procedure->id );
                    endif;
                endif;
            endif;
        endif;

        return [ 'name' => $name, 'type' => $type ];
    }

    public static function getLastSeenProcedureID()
    {
        if( !is_null(Yii::$app->session->get( 'current_proc_id')) && is_integer(Yii::$app->session->get( 'current_proc_id')) ):
            return Yii::$app->session->get( 'current_proc_id');
        else:
            $manager_id = Yii::$app->user->identity->id;
            $last_procedure = LastSeenProcedures::findOne([ 'user_id' => $manager_id ]);

            if( is_object( $last_procedure ) && !empty( $last_procedure ) ):
                Yii::$app->session->set( 'current_proc_id', $last_procedure->proc_id );

                return Yii::$app->session->get( 'current_proc_id');
            else:

                $procedure = Procedures::find()
                        ->select('procedures.*')
                        ->leftJoin('procedure_relations', '`procedure_relations`.`proc_id` = `procedures`.`id`')
                        ->where([ 'procedure_relations.user_id' => Yii::$app->user->identity->id ])
                        ->orderBy(['procedures.id' => SORT_DESC])
                        ->one();

                if( is_object($procedure) && !is_null($procedure) ):
                    $last_procedure = new LastSeenProcedures();
                    $last_procedure->user_id = $manager_id;
                    $last_procedure->proc_id = $procedure->id;

                    if ($last_procedure->save() !== false):
                        // update successful
                        $name = $procedure->getName();
                        $type = $procedure->type->type_abbr;
                        // сохраним выбранную процедуру в сессию
                        Yii::$app->session->set( 'current_proc_id', $procedure->id );

                        return Yii::$app->session->get( 'current_proc_id');
                    endif;
                else:
                    return null;
                endif;
            endif;
        endif;
    }

    public static function getContractorTypesList()
    {
        $dropdowns = TblOrganizationsTypes::find()->asArray()->all();
        return ArrayHelper::map($dropdowns, 'id', 'type');
    }
    
    public static function getFIO($id)
    {
        $params = TblOrganizationsMethods::getParams($id);
        foreach( $params as $p ):
            switch ( $p['label'] ) {
                case 'Фамилия':
                    $lastname = trim($p['value']);
                    break;
                case 'Имя':
                    $firstname = trim($p['value']);
                    break;
                case 'Отчество':
                    $patronymic = trim($p['value']);
                    break;
            }
        endforeach;
        
        return $lastname . ' ' . $firstname . ' ' . $patronymic;
    }
    
    public static function makeErrorString( $errors ) {
        $str = '';
        foreach ($errors as $value) {
            foreach( $value as $v ):
                $str .= $v . '<br/>';
            endforeach;
        }

        return $str;
    }

    /**
     * @return null|Procedures
     */
    public static function getCurrentProcedure()
    {
        return Procedures::findOne(Yii::$app->session->get('current_proc_id'));
    }
}
