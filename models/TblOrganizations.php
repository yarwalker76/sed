<?php

namespace app\models;

use Yii;
use app\models\TblOrganizationsTypes;

/**
 * This is the model class for table "tbl_organizations".
 *
 * @property integer $id
 * @property integer $organizations_types_id
 * @property string $title
 * @property integer $INN
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property string $email
 * @property integer $addresses_id_1
 * @property integer $addresses_id_2
 * @property integer $addresses_id_3
 * @property integer $user_id
 */
class TblOrganizations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return 'tbl_organizations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organizations_types_id'], 'required'],
            [['organizations_types_id', 'addresses_id_1', 'addresses_id_2', 'addresses_id_3', 'user_id'], 'integer'],
            [['organizations_types_id'], 'exist', 'targetAttribute' => 'id', 'targetClass' => '\app\models\TblOrganizationsTypes'],   
            [['email'], 'string', 'max' => 50],
            [['phone', 'mobile', 'fax'], 'match', 'pattern' => '/^[\+]{0,1}[\d\-\(\)\ ]{0,49}$/i'],
            [['email'], 'email'],
        ]; 
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('references', 'ID'),
            'organizations_types_id' => Yii::t('references', 'Тип'),
            'phone' => Yii::t('references', 'Телефон'),
            'mobile' => Yii::t('references', 'Мобильный'),
            'fax' => Yii::t('references', 'Факс'),
            'email' => Yii::t('references', 'Эл. почта'),
            'addresses_id_1' => Yii::t('references', 'Addresses Id 1'),
            'addresses_id_2' => Yii::t('references', 'Addresses Id 2'),
            'addresses_id_3' => Yii::t('references', 'Addresses Id 3'),
            'user_id' => Yii::t('references', 'Руководитель'),
        ];
    }   
}
