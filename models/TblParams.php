<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_params".
 *
 * @property integer $id
 * @property string $title
 * @property integer $params_types_id
 * @property integer $organizations_types_id
 * @property integer $tab
 * @property integer $top
 */
class TblParams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['params_types_id', 'organizations_types_id', 'tab', 'top'], 'integer'],
            [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('references', 'ID'),
            'title' => Yii::t('references', 'Название параметра'),
            'params_types_id' => Yii::t('references', 'ID типа параметра'),
            'organizations_types_id' => Yii::t('references', 'ID типа организации'),
            'tab' => Yii::t('references', 'Номер вкладки'),
            'top' => Yii::t('references', 'Позиция'),
        ];
    }
}
