<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\models\TblAccounts;
use app\models\TblAddresses;
use app\models\TblOrganizations;
use app\models\TblOrganizationsTypes;
use app\models\TblParams;
use app\models\TblParamsTypes;
use app\models\TblParamsDynamic;
use app\models\TblRegions;
use app\models\TblValues;
use app\models\TblValuesEdits;

class TblOrganizationsMethods extends TblOrganizations
{
    public static function saveAll($formdata)
    {
        if (isset($formdata['fulladdresses']['0']) and isset($formdata['fulladdresses']['1']) and isset($formdata['fulladdresses']['2'])) {
            $new_organization = new TblOrganizations();
            $address_0 = new TblAddresses();
            $address_1 = new TblAddresses();
            $address_2 = new TblAddresses();
            $params_dynamic = new TblParamsDynamic();
            $new_organization->attributes = $formdata;
            $address_0->attributes = $formdata['fulladdresses']['0'];
            $address_1->attributes = $formdata['fulladdresses']['1'];
            $address_2->attributes = $formdata['fulladdresses']['2'];
            
            $accounts_validate = true;
            $accounts_error = array();
            if (isset($formdata['accounts'])){
                foreach ($formdata['accounts'] as $key => $value) {
                    $accounts{$key} = new TblAccounts();
                    $accounts{$key}->attributes = $value;
                    if (!$accounts{$key}->validate() and $accounts_validate)
                        $accounts_validate = false;
                    $accounts_error[] = $accounts{$key}->getErrors();
                    //echo $accounts{$key}->closing;
                }
            }
            //var_dump($accounts_validate);
 
            $new_organization_validate = $new_organization->validate();
            $address_0_validate = $address_0->validate();
            $address_1_validate = $address_1->validate();
            $address_2_validate = $address_2->validate();
            $params_dynamic_supervalidate = $params_dynamic->supervalidate($formdata['param'], \Yii::$app->request->post('organizations_types_id'));
            
            $errors = array();
            $errors[] = $new_organization->getErrors();
            $errors[] = $address_0->getErrors();
            $errors[] = $address_1->getErrors(); 
            $errors[] = $address_2->getErrors(); 
            $errors[] = $params_dynamic_supervalidate;
            $errors = array_merge($errors, $accounts_error);
            //echo ' Errors <pre>'; print_r($errors); echo '</pre>';

            if ( $new_organization_validate and 
                $address_0_validate and $address_1_validate and $address_2_validate and 
                (count($params_dynamic_supervalidate) == 0) and 
                $accounts_validate) {
                /**/
                //echo 'Good formdata!';
                $address_0->save();
                $address_1->save();
                $address_2->save();
                $new_organization->addresses_id_1 = $address_0->id;
                $new_organization->addresses_id_2 = $address_1->id;
                $new_organization->addresses_id_3 = $address_2->id;
                $new_organization->save();
                $new_values = new TblValuesEdits();
                $new_values->create($formdata['param'], $new_organization->id);
                $accounts_ = true;
                if (isset($formdata['accounts'])){
                    foreach ($formdata['accounts'] as $key => $value) {
                        $accounts{$key}->organizations_id = $new_organization->id;
                        $accounts{$key}->save();
                        if (!$accounts{$key} and $accounts_)
                            $accounts_ = false;
                    }
                }
                /**/
                if ($new_organization and $address_0 and $address_1 and $address_2 and $new_values and $accounts_)
                    $status = 1;
                else
                    $status = 0.5;
            } else
                $status = 0;  

            return [ 'status' => $status, 'errors' => $errors];
        }  else
            return [ 'status' => 0, 'errors' => ['0' => ['0' => ['0' => \Yii::t('references', 'Not a full set of data.')]]]];
    }
    
    public static function updAll($formdata)
    {
        if (isset($formdata['id'])) {
            $formdata['id'] = (int)$formdata['id'];
            if (isset($formdata['fulladdresses']['0']) and isset($formdata['fulladdresses']['1']) and isset($formdata['fulladdresses']['2'])) {
                $new_organization = TblOrganizations::findOne($formdata['id']);;
                $address_0 = TblAddresses::findOne($new_organization->addresses_id_1);
                $address_1 = TblAddresses::findOne($new_organization->addresses_id_2);
                $address_2 = TblAddresses::findOne($new_organization->addresses_id_3);
                $params_dynamic = new TblParamsDynamic();

                $new_organization->attributes = $formdata;
                $address_0->attributes = $formdata['fulladdresses']['0'];
                $address_1->attributes = $formdata['fulladdresses']['1'];
                $address_2->attributes = $formdata['fulladdresses']['2'];

                $accounts_validate = true;
                $accounts_error = array();
                
                if (isset($formdata['accounts'])){
                    foreach ($formdata['accounts'] as $key => $value) {
                        $accounts{$key} = TblAccounts::findOne($key);
                        $accounts{$key}->attributes = $value;
                        if (!$accounts{$key}->validate() and $accounts_validate)
                            $accounts_validate = false;
                        $accounts_error[] = $accounts{$key}->getErrors();
                        //echo $accounts{$key}->closing;
                    }
                }
                //var_dump($accounts_validate);

                $new_organization_validate = $new_organization->validate();
                $address_0_validate = $address_0->validate();
                $address_1_validate = $address_1->validate();
                $address_2_validate = $address_2->validate();
                $params_dynamic_supervalidate = $params_dynamic->supervalidate($formdata['param'], $new_organization->organizations_types_id);

                $errors = array();
                $errors[] = $new_organization->getErrors();
                $errors[] = $address_0->getErrors();
                $errors[] = $address_1->getErrors(); 
                $errors[] = $address_2->getErrors(); 
                $errors[] = $params_dynamic_supervalidate;
                $errors = array_merge($errors, $accounts_error);
                //echo ' Errors <pre>'; print_r($errors); echo '</pre>';

                if ( $new_organization_validate and 
                    $address_0_validate and $address_1_validate and $address_2_validate and 
                    (count($params_dynamic_supervalidate) == 0) and 
                    $accounts_validate) {
                    
                    //echo 'Good formdata!';
                    $new_organization->save();
                    $address_0->save();
                    $address_1->save();
                    $address_2->save();
                    
                    $new_values = new TblValuesEdits();
                    $new_values->upd($formdata['param'], $formdata['id']);

                    $accounts_ = true;
                    if (isset($formdata['accounts'])){
                        foreach ($formdata['accounts'] as $key => $value) {
                            $accounts{$key}->save();
                            if (!$accounts{$key} and $accounts_)
                                $accounts_ = false;
                        }
                    }
                    if ($new_organization and $address_0 and $address_1 and $address_2 and $new_values and $accounts_)
                        $status = 1;
                    else
                        $status = 0.5;
                } else
                    $status = 0;  

                return [ 'status' => $status, 'errors' => $errors];
            }  else
                return [ 'status' => 0, 'errors' => ['0' => ['0' => ['0' => \Yii::t('references', 'Not a full set of data.')]]]];
        }  else
            return [ 'status' => 0, 'errors' => ['0' => ['0' => ['0' => \Yii::t('references', 'Not a full set of data.')]]]];
    }
    
    public static function getOrganization($id)
    {
        return ( new Query() )->
            select( 'o.*, ot.type organizations_types_type, '
                . 'ad0.postcode address_0_postcode, ad0.locality address_0_locality, ad0.address address_0_address, r0.id address_0_id, r0.title address_0_region, '
                . 'ad1.postcode address_1_postcode, ad1.locality address_1_locality, ad1.address address_1_address, r1.id address_1_id, r1.title address_1_region, '
                . 'ad2.postcode address_2_postcode, ad2.locality address_2_locality, ad2.address address_2_address, r2.id address_2_id, r2.title address_2_region ' )->
            from( TblOrganizations::tableName() . ' as o')->
            leftJoin( TblOrganizationsTypes::tableName() . ' as ot', 'ot.id = o.organizations_types_id' )->
            leftJoin( TblAddresses::tableName() . ' as ad0', 'o.addresses_id_1 = ad0.id' )->
            leftJoin( TblRegions::tableName() . ' as r0', 'r0.id = ad0.regions_id' )-> 
            leftJoin( TblAddresses::tableName() . ' as ad1', 'o.addresses_id_2 = ad1.id' )->
            leftJoin( TblRegions::tableName() . ' as r1', 'r1.id = ad1.regions_id' )->
            leftJoin( TblAddresses::tableName() . ' as ad2', 'o.addresses_id_3 = ad2.id' )->  
            leftJoin( TblRegions::tableName() . ' as r2', 'r2.id = ad2.regions_id' )->
            where( ['o.id' => $id] )->
            createCommand()->
            queryOne();
    }
    
    public static function getParams($id)
    {
        //exit($id);
        $params = ( new Query() )->
            select( 'v.params_id, v.value, p.title, p.tab, p.top, pt.type' )->
            from( TblValues::tableName() . ' as v')->
            leftJoin( TblParams::tableName() . ' as p', 'p.id = v.params_id')->
            leftJoin( TblParamsTypes::tableName() . ' as pt', 'p.params_types_id = pt.id')->
            where( ['organizations_id'=>$id] )->
            orderBy( 'p.tab ASC, p.top ASC' )->
            all();
        $params_arr = array();
        for ($i=0; $i<count($params); $i++)
            $params_arr[] = array( 'label' => $params[$i]['title'], 'value' => $params[$i]['value'], 'params_id' => $params[$i]['params_id']);
        return $params_arr;
    }

    /**
     * @param $params array
     * @return null
     */
    public static function extractNameFromParams($params)
    {
        foreach ($params as $param) {
            if ($param['label'] === 'Название' && !empty($param['label'])) {
                return $param['value'];
            }
        }

        $res = [];

        foreach ($params as $param) {
            if ($param['label'] === 'Фамилия' && !empty($param['label'])) {
                $res[0] = $param['value'];
            }

            if ($param['label'] === 'Имя' && !empty($param['label'])) {
                $res[1] = $param['value'];
            }

            if ($param['label'] === 'Отчество' && !empty($param['label'])) {
                $res[2] = $param['value'];
            }
        }

        return implode(' ', $res);
    }
}
