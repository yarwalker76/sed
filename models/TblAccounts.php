<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_accounts".
 *
 * @property integer $id
 * @property integer $accounts_types_id
 * @property integer $organizations_id
 * @property integer $banks_id
 * @property string $number
 * @property string $closing
 * @property integer $closed
 * @property string $comment
 */
class TblAccounts extends \yii\db\ActiveRecord
{
    public $fake = null;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_accounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->fake == null)
            return [
                [['banks_id', 'number'], 'required'],
                [['accounts_types_id', 'organizations_id', 'banks_id', 'closed'], 'integer'],
                [['accounts_types_id'], 'exist', 'targetAttribute' => 'id', 'targetClass' => '\app\models\TblAccountsTypes' ], 
                [['banks_id'], 'exist', 'targetAttribute' => 'id', 'targetClass' => '\app\models\TblOrganizations' ], 
                //[['closing'], 'match', 'pattern' => '/^\d+\d+\-\d+\d+\-\d+\d+\d+\d+$/i'],
                [['closing'], 'date', 'format' => 'dd-mm-yyyy' ],
                [['comment'], 'string'],
                [['number'], 'string', 'max' => 30],
            ];
        else
            return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('references', 'ID'),
            'accounts_types_id' => Yii::t('references', 'Accounts Types ID'),
            'organizations_id' => Yii::t('references', 'Organizations ID'),
            'banks_id' => Yii::t('references', 'Banks ID'),
            'number' => Yii::t('references', 'Номер счета'),
            'closing' => Yii::t('references', 'Дата закрытия счета'),
            'closed' => Yii::t('references', 'Счет закыт (1) или открыт (0)'),
            'comment' => Yii::t('references', 'Примечания'),
        ];
    }
}
