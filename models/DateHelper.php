<?php
/**
 * Created by PhpStorm.
 * User: shnm
 * Date: 4/16/17
 * Time: 11:39 PM
 */

namespace app\models;


class DateHelper
{
    public static function getMonth()
    {
        return [
            null,
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря'
        ];
    }
}