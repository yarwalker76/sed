<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_accounts_types".
 *
 * @property integer $id
 * @property string $type
 */
class TblAccountsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_accounts_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('references', 'ID'),
            'type' => Yii::t('references', 'Тип счета'),
        ];
    }
}
