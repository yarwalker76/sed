<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Command;
use app\models\TblValues;

class TblValuesEdits extends TblValues
{
    public function create($formdata, $new_organization_id)
    {
        //var_dump($new_organization_id);
        $values = [];
        foreach ($formdata as $key => $value)
            $values[] =  [ $key, $new_organization_id, $value ];
        //var_dump($values);
        return Yii::$app->db->createCommand()->batchInsert(
            TblValues::tableName(), 
            ['params_id', 'organizations_id', 'value'], 
            $values
        )->execute();
    }
    
    public function upd($formdata, $new_organization_id)
    {
        foreach ($formdata as $key => $value)
            TblValues::updateAll(['value' => $value], '(params_id=' . $key . ') and (organizations_id=' . $new_organization_id . ')');
    }
}

