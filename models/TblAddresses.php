<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_addresses".
 *
 * @property integer $id
 * @property string $postcode
 * @property integer $regions_id
 * @property string $locality
 * @property string $address
 */
class TblAddresses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_addresses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postcode'], 'string', 'max' => 30],
            [['regions_id'], 'integer'],
            [['locality'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('references', 'ID'),
            'postcode' => Yii::t('references', 'Почтовый индекс'),
            'regions_id' => Yii::t('references', 'Regions ID'),
            'locality' => Yii::t('references', 'Населенный пункт'),
            'address' => Yii::t('references', 'Адрес'),
        ];
    }
}
