<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_values".
 *
 * @property integer $id
 * @property integer $params_id
 * @property integer $organizations_id
 * @property string $value
 */
class TblValues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['params_id', 'value'], 'required'],
            [['params_id', 'organizations_id'], 'integer'],
            [['value'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('references', 'ID'),
            'params_id' => Yii::t('references', 'Params ID'),
            'organizations_id' => Yii::t('references', 'Organizations ID'),
            'value' => Yii::t('references', 'Значеие параметра'),
        ];
    }
}
