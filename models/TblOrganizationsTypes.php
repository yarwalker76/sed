<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_organizations_types".
 *
 * @property integer $id
 * @property string $type
 */
class TblOrganizationsTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_organizations_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'exist'], 
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('references', 'ID'),
            'type' => Yii::t('references', 'Тип'),
        ];
    }
}
