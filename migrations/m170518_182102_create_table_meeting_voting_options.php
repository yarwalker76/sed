<?php

use yii\db\Migration;

class m170518_182102_create_table_meeting_voting_options extends Migration
{
    public $table_name = 'meeting_voting_options';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(),
            'bull_form_id' => $this->integer(),
            'question_id' => $this->integer(),
            'value' => $this->string(512),
        ]);

        $this->insert($this->table_name, ['bull_form_id' => 1, 'question_id' => 0, 'value' => 'За']);
        $this->insert($this->table_name, ['bull_form_id' => 1, 'question_id' => 0, 'value' => 'Против']);
        $this->insert($this->table_name, ['bull_form_id' => 1, 'question_id' => 0, 'value' => 'Воздержался']);

        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => 'Воздержался']);
        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => '3']);
        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => '4']);
        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => '5']);
        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => '6']);
        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => '7']);
        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => '8']);
        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => '9']);
        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => '10']);
        $this->insert($this->table_name, ['bull_form_id' => 3, 'question_id' => 0, 'value' => '11']);

        $this->addForeignKey('fkBullFormsId', $this->table_name, 'bull_form_id',
            'meeting_bulletin_forms', 'id', 'NO ACTION', 'CASCADE');

        $this->createIndex('ixQuestionsId', $this->table_name, 'question_id');
        $this->createIndex('ixQuestionsBullFormsIds', $this->table_name, ['question_id', 'bull_form_id']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('fkBullFormsId',$this->table_name);

        $this->dropIndex('ixQuestionsId', $this->table_name);
        $this->dropIndex('ixQuestionsBullFormsIds', $this->table_name);

        $this->dropTable($this->table_name);
    }

}
