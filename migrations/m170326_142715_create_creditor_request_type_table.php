<?php

use yii\db\Migration;

/**
 * Handles the creation for table `creditor_request_type`.
 */
class m170326_142715_create_creditor_request_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('creditor_request_type', [
            'id' => $this->primaryKey(),
            'title' => $this->string(512),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('creditor_request_type');
    }
}
