<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `creditor_request_reuirement`.
 */
class m170329_213358_drop_creditor_request_requirement_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('creditor_request_requirement');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('creditor_request_requirement', [
            'id' => $this->primaryKey(),
        ]);
    }
}
