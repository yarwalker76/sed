<?php

use yii\db\Migration;

class m170504_140112_create_table_meeting_questions extends Migration
{
    public $table_name = 'meeting_questions';

    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(),
            'meet_id' => $this->integer()->notNull(),
            'question' => $this->string(512)->notNull(),
            'decision' => $this->string(512),
            'bulletin_form_id' => $this->integer(),
            'voice_calc_id' => $this->integer(),
            'mortgage_creditors_voices' => $this->boolean()->defaultValue(false)
        ]);

        $this->addForeignKey('fkMeeting',$this->table_name, 'meet_id',
            'meetings', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fkBulletinForm',$this->table_name, 'bulletin_form_id',
            'meeting_bulletin_forms', 'id', 'NO ACTION', 'CASCADE');
        $this->addForeignKey('fkVoiceCalc',$this->table_name, 'voice_calc_id',
            'meeting_voice_calculations', 'id', 'NO ACTION', 'CASCADE');

        $this->createIndex('ixMeeting', $this->table_name, 'meet_id');
        $this->createIndex('ixBulletinForm', $this->table_name, 'bulletin_form_id');
        $this->createIndex('ixVoiceCalc', $this->table_name, 'voice_calc_id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fkMeeting',$this->table_name);
        $this->dropForeignKey('fkBulletinForm',$this->table_name);
        $this->dropForeignKey('fkVoiceCalc',$this->table_name);

        $this->dropIndex('ixMeeting', $this->table_name);
        $this->dropIndex('ixBulletinForm', $this->table_name);
        $this->dropIndex('ixVoiceCalc', $this->table_name);

        $this->dropTable($this->table_name);
    }

}
