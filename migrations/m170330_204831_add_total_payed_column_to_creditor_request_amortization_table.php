<?php

use yii\db\Migration;

/**
 * Handles adding total_payed to table `creditor_request_amortization`.
 */
class m170330_204831_add_total_payed_column_to_creditor_request_amortization_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor_request_amortization', 'total_payed', $this->float(19,4));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor_request_amortization', 'total_payed');
    }
}
