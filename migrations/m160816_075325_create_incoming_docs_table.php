<?php

use yii\db\Migration;

/**
 * Handles the creation for table `incoming_docs`.
 */
class m160816_075325_create_incoming_docs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('incoming_docs', [
            'id' => $this->primaryKey(),
            'proc_id' => $this->integer(),
            'response_on_id' => $this->integer(),
            'response_type_id' => $this->integer(),
            'serial_number' => $this->integer(),
            'name' => $this->string(),
            'contractor' => $this->string(),
            'send_date' => $this->date(),
            'receive_date' => $this->date(),
            'notes' => $this->text(),
            'files' => $this->text(),
        ]);

        $this->addForeignKey('fk-incoming_docs-response_type_id', 'incoming_docs', 'response_type_id', 'doc_types', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk-incoming_docs-response_on_id', 'incoming_docs', 'response_on_id', 'outgoing_docs', 'id', 'NO ACTION', 'NO ACTION');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-incoming_docs-response_type_id',
            'incoming_docs'
        );

        $this->dropForeignKey(
            'fk-incoming_docs-response_on_id',
            'incoming_docs'
        );

        $this->dropTable('incoming_docs');
    }
}
