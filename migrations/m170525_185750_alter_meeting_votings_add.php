<?php

use yii\db\Migration;

class m170525_185750_alter_meeting_votings_add extends Migration
{
    public function up()
    {
        $this->addColumn('meeting_votings', 'request_sum', $this->double(2));
        $this->addColumn('meeting_votings', 'request_percent', $this->double(2));
    }

    public function down()
    {
        $this->dropColumn('meeting_votings', 'request_sum');
        $this->dropColumn('meeting_votings', 'request_percent');
    }

}
