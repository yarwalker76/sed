<?php

use yii\db\Migration;

/**
 * Handles adding update_time to table `creditor_request`.
 */
class m170412_081949_add_update_time_column_to_creditor_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor_request', 'update_time', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor_request', 'update_time');
    }
}
