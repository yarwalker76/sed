<?php

use yii\db\Migration;

class m161203_115650_alter_outgoing_docs extends Migration
{
    public function safeUp()
    {
        $this->addColumn('outgoing_docs','reg_date','date');
    }

    public function safeDown()
    {
        echo "m161203_115650_alter_outgoing_docs cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
