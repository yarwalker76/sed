<?php

use yii\db\Migration;

class m170531_075232_create_table_meeting_templates extends Migration
{
    public $table_name = 'meeting_templates';

    public function up()
    {
        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'filename' => $this->string(),
        ]);

        $this->insert($this->table_name, ['name' => 'bulletin1', 'filename' => 'Бюллетень1.docx']);
        $this->insert($this->table_name, ['name' => 'bulletin2', 'filename' => 'Бюллетень2.docx']);
        $this->insert($this->table_name, ['name' => 'bulletin3', 'filename' => 'Бюллетень3.docx']);
        $this->insert($this->table_name, ['name' => 'bulletin4', 'filename' => 'Бюллетень4.docx']);
    }

    public function down()
    {
        $this->dropTable($this->table_name);
    }


}
