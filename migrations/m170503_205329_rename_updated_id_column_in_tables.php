<?php

use yii\db\Migration;

class m170503_205329_rename_updated_id_column_in_tables extends Migration
{
    public function up()
    {
        $this->renameColumn('creditor', 'updated_id', 'updated_internal_id');
        $this->renameColumn('creditor_request', 'updated_id', 'updated_internal_id');
        $this->renameColumn('creditor_request_amortization', 'updated_id', 'updated_internal_id');
        $this->renameColumn('creditor_request_guarantee', 'updated_id', 'updated_internal_id');
    }

    public function down()
    {
        echo "m170503_205329_rename_updated_id_column_in_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
