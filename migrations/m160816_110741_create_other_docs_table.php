<?php

use yii\db\Migration;

/**
 * Handles the creation for table `other_docs`.
 */
class m160816_110741_create_other_docs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('other_docs', [
            'id' => $this->primaryKey(),
            'proc_id' => $this->integer(),
            'serial_number' => $this->integer(),
            'name' => $this->string(),
            'date' => $this->date(),
            'files' => $this->text(),
            'notes' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('other_docs');
    }
}
