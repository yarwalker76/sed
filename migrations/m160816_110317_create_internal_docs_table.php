<?php

use yii\db\Migration;

/**
 * Handles the creation for table `internal_docs`.
 */
class m160816_110317_create_internal_docs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('internal_docs', [
            'id' => $this->primaryKey(),
            'proc_id' => $this->integer(),
            'serial_number' => $this->integer(),
            'name' => $this->string(),
            'date' => $this->date(),
            'files' => $this->text(),
            'notes' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('internal_docs');
    }
}
