<?php

use yii\db\Migration;

class m170513_113101_create_table_meeting_participants extends Migration
{
    public $table_name = 'meeting_participants';

    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(),
            'meet_id' => $this->integer()->notNull(),
            'registration' => $this->boolean()->defaultValue(false),
            'name' => $this->string(512)->notNull(),
            'representative_name' => $this->string(512),
            'supporting_docs' => $this->string(512),
            'participant_status_id' => $this->integer(),
            'total_sum' => $this->double(),
        ]);

        $this->addForeignKey('fkParticipantMeetID',$this->table_name, 'meet_id',
            'meetings', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fkParticipantStatus', $this->table_name, 'participant_status_id',
            'meeting_participant_statuses', 'id', 'NO ACTION', 'CASCADE');

        $this->createIndex('ixParticipantMeetID', $this->table_name, 'meet_id');
        $this->createIndex('ixParticipantStatus', $this->table_name, 'participant_status_id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fkParticipantMeetID',$this->table_name);

        $this->dropIndex('ixParticipantMeetID', $this->table_name);

        $this->dropTable($this->table_name);
    }


}
