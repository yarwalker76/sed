<?php

use yii\db\Migration;

/**
 * Handles adding number to table `creditor_organization_bank_account`.
 */
class m170404_203724_add_number_column_to_creditor_organization_bank_account_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor_organization_bank_account', 'number', $this->string(30));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor_organization_bank_account', 'number');
    }
}
