<?php

use yii\db\Migration;

/**
 * Handles the creation for table `creditor_request`.
 */
class m170326_142618_create_creditor_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('creditor_request', [
            'id' => $this->primaryKey(),
            'creditor_id' => $this->integer()->notNull(),
            'registry_number' => $this->integer(),
            'queue_type' => $this->smallInteger(),
            'type_id' => $this->integer(),
            'create_time' => $this->dateTime(),
            'initiation_time' => $this->datetime(),
            'description' => $this->string(512),
            'total' => $this->decimal(19, 4),

            'is_enabled' => $this->boolean(),
            'enable_reason' => $this->string(512),
            'enable_time' => $this->datetime(),
            'disable_reason' => $this->string(512),
            'disable_time' => $this->datetime(),

            'is_updated' => $this->boolean(),
            'update_reason' => $this->string(512),
            'update_initiator' => $this->string(256),
            'updated_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('creditor_request');
    }
}
