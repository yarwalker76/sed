<?php

use yii\db\Migration;

/**
 * Handles adding initiation_time to table `creditor`.
 */
class m170405_203644_add_initiation_time_column_to_creditor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor', 'initiation_time', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor', 'initiation_time');
    }
}
