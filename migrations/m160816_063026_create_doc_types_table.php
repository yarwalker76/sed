<?php

use yii\db\Migration;

/**
 * Handles the creation for table `doc_types`.
 */
class m160816_063026_create_doc_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('doc_types', [
            'id' => $this->primaryKey(),
            'type' => "enum('incoming','outgoing','internal','other') NOT NULL DEFAULT 'incoming'",
            'name' => $this->string(150),
        ]);

        $this->insert('doc_types', [
            'type' => 'outgoing',
            'name' => 'Другое',
        ]);

        $this->insert('doc_types', [
            'type' => 'outgoing',
            'name' => 'Запрос в ФНС',
        ]);

        $this->insert('doc_types', [
            'type' => 'outgoing',
            'name' => 'Запрос в ГИБДД',
        ]);

        $this->insert('doc_types', [
            'type' => 'outgoing',
            'name' => 'Запрос в банк',
        ]);

        $this->insert('doc_types', [
            'type' => 'incoming',
            'name' => 'Ответ на запрос',
        ]);        

        $this->insert('doc_types', [
            'type' => 'incoming',
            'name' => 'Письмо',
        ]);        

        $this->insert('doc_types', [
            'type' => 'incoming',
            'name' => 'Уведомление',
        ]);        

        $this->insert('doc_types', [
            'type' => 'incoming',
            'name' => 'Другое',
        ]);        
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('doc_types');
    }
}
