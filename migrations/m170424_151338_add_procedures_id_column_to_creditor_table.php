<?php

use yii\db\Migration;

/**
 * Handles adding procedures_id to table `creditor`.
 * Has foreign keys to the tables:
 *
 * - `procedures`
 */
class m170424_151338_add_procedures_id_column_to_creditor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->truncateTable('creditor');

        $this->addColumn('creditor', 'procedures_id', $this->integer()->notNull());

        // creates index for column `procedures_id`
        $this->createIndex(
            'idx-creditor-procedures_id',
            'creditor',
            'procedures_id'
        );

        // add foreign key for table `procedures`
        $this->addForeignKey(
            'fk-creditor-procedures_id',
            'creditor',
            'procedures_id',
            'procedures',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `procedures`
        $this->dropForeignKey(
            'fk-creditor-procedures_id',
            'creditor'
        );

        // drops index for column `procedures_id`
        $this->dropIndex(
            'idx-creditor-procedures_id',
            'creditor'
        );

        $this->dropColumn('creditor', 'procedures_id');
    }
}
