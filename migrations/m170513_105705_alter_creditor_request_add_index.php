<?php

use yii\db\Migration;

class m170513_105705_alter_creditor_request_add_index extends Migration
{
    public function up()
    {
        $this->createIndex('ixCreditorID', 'creditor_request', 'creditor_id');
    }

    public function down()
    {
        $this->dropIndex('ixCreditorID', 'creditor_request');
    }

}
