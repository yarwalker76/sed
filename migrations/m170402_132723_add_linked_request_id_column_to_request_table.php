<?php

use yii\db\Migration;

/**
 * Handles adding linked_request_id to table `request`.
 */
class m170402_132723_add_linked_request_id_column_to_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor_request', 'linked_request_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor_request', 'linked_request_id');
    }
}
