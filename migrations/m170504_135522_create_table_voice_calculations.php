<?php

use yii\db\Migration;

class m170504_135522_create_table_voice_calculations extends Migration
{
    public $table_name = 'meeting_voice_calculations';

    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(),
            'name' => $this->string(150),
        ]);

        $this->insert($this->table_name, [
            'name' => 'Из числа присутствующих на собрании',
        ]);
        $this->insert($this->table_name, [
            'name' => 'От общего числа кредиторов',
        ]);

    }

    public function safeDown()
    {
        $this->dropTable($this->table_name);
    }

}
