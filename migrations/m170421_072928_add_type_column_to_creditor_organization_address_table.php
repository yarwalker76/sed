<?php

use yii\db\Migration;

/**
 * Handles adding type to table `creditor_organization_address`.
 */
class m170421_072928_add_type_column_to_creditor_organization_address_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor_organization_address', 'type', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor_organization_address', 'type');
    }
}
