<?php

use yii\db\Migration;

class m170419_061211_add_update_time_columns_to_models extends Migration
{
    public function up()
    {
        $this->addColumn('creditor_request_amortization', 'update_time', $this->datetime());
        $this->addColumn('creditor_request_guarantee', 'update_time', $this->datetime());
    }

    public function down()
    {
        echo "m170419_061211_add_update_time_columns_to_models cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
