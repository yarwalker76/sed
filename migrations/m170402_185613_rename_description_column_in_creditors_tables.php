<?php

use yii\db\Migration;

class m170402_185613_rename_description_column_in_creditors_tables extends Migration
{
    public function up()
    {
        $this->renameColumn('creditor_request', 'description', 'details');
    }

    public function down()
    {
        echo "m170402_185613_rename_description_column_in_creditors_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
