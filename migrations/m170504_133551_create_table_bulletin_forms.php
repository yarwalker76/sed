<?php

use yii\db\Migration;

class m170504_133551_create_table_bulletin_forms extends Migration
{
    public $table_name = 'meeting_bulletin_forms';

    public function safeUp()
    {

        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(),
            'name' => $this->string(150),
        ]);

        $this->insert($this->table_name, [
            'name' => 'Бюллетень простой №1',
        ]);
        $this->insert($this->table_name, [
            'name' => 'Бюллетень с вариантами №2',
        ]);
        $this->insert($this->table_name, [
            'name' => 'Бюллетень о количественном составе комитета кредиторов',
        ]);
        $this->insert($this->table_name, [
            'name' => 'Бюллетень об избрании членов комитета кредиторов',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->table_name);
    }

}
