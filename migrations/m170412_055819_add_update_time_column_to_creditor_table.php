<?php

use yii\db\Migration;

/**
 * Handles adding update_time to table `creditor`.
 */
class m170412_055819_add_update_time_column_to_creditor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor', 'update_time', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor', 'update_time');
    }
}
