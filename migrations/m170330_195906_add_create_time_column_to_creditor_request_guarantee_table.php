<?php

use yii\db\Migration;

/**
 * Handles adding create_time to table `creditor_request_guarantee`.
 */
class m170330_195906_add_create_time_column_to_creditor_request_guarantee_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor_request_guarantee', 'create_time', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor_request_guarantee', 'create_time');
    }
}
