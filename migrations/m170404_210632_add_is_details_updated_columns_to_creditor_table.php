<?php

use yii\db\Migration;

/**
 * Handles adding is_details_updated to table `creditor`.
 */
class m170404_210632_add_is_details_updated_columns_to_creditor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor', 'is_details_updated', $this->boolean());
        $this->addColumn('creditor', 'update_details_description', $this->string(512));
        $this->addColumn('creditor', 'update_details_initiator', $this->string(256));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor', 'is_details_updated');
        $this->dropColumn('creditor', 'update_details_description');
        $this->dropColumn('creditor', 'update_details_initiator');
    }
}
