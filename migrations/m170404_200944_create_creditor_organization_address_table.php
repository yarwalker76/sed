<?php

use yii\db\Migration;

/**
 * Handles the creation for table `creditor_organization_address`.
 */
class m170404_200944_create_creditor_organization_address_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('creditor_organization_address', [
            'id' => $this->primaryKey(),
            'creditor_id' => $this->integer()->notNull(),
            'regions_id' => $this->integer()->notNull(),
            'postcode' => $this->string(30),
            'locality' => $this->string(100),
            'address' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('creditor_organization_address');
    }
}
