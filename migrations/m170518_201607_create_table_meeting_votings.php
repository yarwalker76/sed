<?php

use yii\db\Migration;

class m170518_201607_create_table_meeting_votings extends Migration
{
    public $table_name = 'meeting_votings';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(),
            'question_id' => $this->integer(),
            'participant_id' => $this->integer(),
            'voting_option_id' => $this->integer(),
        ]);

        $this->addForeignKey('fkVotingsQuestionsId', $this->table_name, 'question_id',
            'meeting_questions', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('ixVotingsQuestionsId', $this->table_name, 'question_id');

        $this->addForeignKey('fkVotingsParticipantsId', $this->table_name, 'participant_id',
            'meeting_participants', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('ixVotingsParticipantsId', $this->table_name, 'question_id');

        $this->addForeignKey('fkVotingsOptionsId', $this->table_name, 'voting_option_id',
            'meeting_voting_options', 'id', 'NO ACTION', 'CASCADE');
        $this->createIndex('ixVotingsOptionsId', $this->table_name, 'question_id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fkVotingsQuestionsId', $this->table_name, 'question_id');
        $this->dropIndex('ixVotingsQuestionsId', $this->table_name);

        $this->dropForeignKey('fkVotingsParticipantsId', $this->table_name);
        $this->dropIndex('ixVotingsParticipantsId', $this->table_name);

        $this->dropForeignKey('fkVotingsOptionsId', $this->table_name);
        $this->dropIndex('ixVotingsOptionsId', $this->table_name);

        $this->dropTable($this->table_name);
    }

}
