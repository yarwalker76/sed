<?php

use yii\db\Migration;

class m160818_102313_alter_incoming_docs_table extends Migration
{
    public function safeUp()
    {
         $this->alterColumn('incoming_docs','response_type_id','integer NOT NULL DEFAULT 5');
    }

    public function safeDown()
    {
        echo "m160818_102313_alter_incoming_docs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
