<?php

use yii\db\Migration;

class m170330_204347_amortization_table_fixes extends Migration
{
    public function up()
    {
        $this->renameColumn('creditor_request_amortization', 'total_payed', 'total_ratio');
    }

    public function down()
    {
        $this->renameColumn('creditor_request_amortization', 'total_ratio', 'total_payed');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
