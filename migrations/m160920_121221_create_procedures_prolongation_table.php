<?php

use yii\db\Migration;

/**
 * Handles the creation for table `procedures_prolongation`.
 */
class m160920_121221_create_procedures_prolongation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('procedures_prolongation', [
            'id' => $this->primaryKey(),
            'date_with' => $this->date(),
            'date_till' => $this->date(),
            'date_dssz' => $this->date(),
            'proc_id' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('procedures_prolongation');
    }
}
