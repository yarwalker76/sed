<?php

use yii\db\Migration;

class m170522_134057_alter_add_conclusion_column_meetings extends Migration
{
    public function up()
    {
        $this->addColumn('meetings', 'conclusion', $this->text(2048));
    }

    public function down()
    {
        $this->dropColumn('meetings', 'conclusion');
    }


}
