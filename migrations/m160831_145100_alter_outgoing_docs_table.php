<?php

use yii\db\Migration;

class m160831_145100_alter_outgoing_docs_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn ( 'outgoing_docs', 'receive_date' );
    }

    public function safeDown()
    {
        echo "m160831_145100_alter_outgoing_docs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
