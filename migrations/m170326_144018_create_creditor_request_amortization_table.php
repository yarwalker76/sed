<?php

use yii\db\Migration;

/**
 * Handles the creation for table `creditor_request_amortization`.
 */
class m170326_144018_create_creditor_request_amortization_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('creditor_request_amortization', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->notNull(),
            'total' => $this->decimal(19,4),
            'total_payed' => $this->decimal(19,4),
            'details' => $this->string(512),
            'repeat_details' => $this->string(512),
            'create_time' => $this->dateTime(),
            'initiation_time' => $this->datetime(),
            'is_updated' => $this->boolean(),
            'update_reason' => $this->string(512),
            'update_initiator' => $this->string(256),
            'updated_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('creditor_request_amortization');
    }
}
