<?php

use yii\db\Migration;

/**
 * Handles the creation for table `outgoing_docs`.
 */
class m160816_070316_create_outgoing_docs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('outgoing_docs', [
            'id' => $this->primaryKey(),
            'proc_id' => $this->integer(),
            'serial_number' => $this->integer(),
            'request_type_id' => $this->integer(),
            'name' => $this->string(),
            'contractor' => $this->string(),
            'send_date' => $this->date(),
            'receive_date' => $this->date(),
            'delivery_address' => $this->string(),
            'notes' => $this->text(),
            'files' => $this->text(),
        ]);

        $this->addForeignKey('fk-outgoing_docs-request_type_id', 'outgoing_docs', 'request_type_id', 'doc_types', 'id', 'NO ACTION', 'NO ACTION');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table 
        $this->dropForeignKey(
            'fk-outgoing_docs-request_type_id',
            'outgoing_docs'
        );

        $this->dropTable('outgoing_docs');
    }
}
