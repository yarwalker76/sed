<?php

use yii\db\Migration;

/**
 * Handles the creation for table `creditor_organization_bank_account`.
 */
class m170404_200643_create_creditor_organization_bank_account_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('creditor_organization_bank_account', [
            'id' => $this->primaryKey(),
            'creditor_id' => $this->integer()->notNull(),
            'banks_id' => $this->integer()->notNull(),
            'close_time' => $this->datetime(),
            'is_closed' => $this->boolean(),
            'comment' => $this->string(512),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('creditor_organization_bank_account');
    }
}
