<?php

use yii\db\Migration;

class m170519_182855_add_column_discussion_table_meeting_question extends Migration
{
    public function up()
    {
        $this->addColumn('meeting_questions', 'discussion', $this->text(2048));
    }

    public function down()
    {
        $this->dropColumn('meeting_questions', 'discussion');
    }

}
