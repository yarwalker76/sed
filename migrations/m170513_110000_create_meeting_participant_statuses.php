<?php

use yii\db\Migration;

class m170513_110000_create_meeting_participant_statuses extends Migration
{
    public $table_name = 'meeting_participant_statuses';

    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'short_name' => $this->string(50)
        ]);

        $this->insert($this->table_name, ['name' => 'Конкурсный кредитор', 'short_name' => "к.к."]);
        $this->insert($this->table_name, ['name' => 'Уполномоченный орган', 'short_name' => 'уп.ор.']);
        $this->insert($this->table_name, ['name' => 'Представитель работников должника', 'short_name' => 'п.р.д.']);
        $this->insert($this->table_name, ['name' => 'Представитель учредителей (участников) должника
предприятия', 'short_name' => 'п.уч.д.']);
        $this->insert($this->table_name, ['name' => 'Представитель собственника имущества должника - унитарного', 'short_name' => 'п.с.д.']);
    }

    public function safeDown()
    {
        $this->dropTable($this->table_name);
    }

}
