<?php

use yii\db\Migration;

/**
 * Handles the creation for table `creditor_request_guarantee`.
 */
class m170326_151717_create_creditor_request_guarantee_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('creditor_request_guarantee', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->notNull(),
            'total' => $this->decimal(19,4),
            'details' => $this->string(512),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('creditor_request_guarantee');
    }
}
