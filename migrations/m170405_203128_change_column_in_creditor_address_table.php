<?php

use yii\db\Migration;

class m170405_203128_change_column_in_creditor_address_table extends Migration
{
    public function up()
    {
        $this->alterColumn('creditor_organization_address', 'regions_id', $this->integer());
    }

    public function down()
    {
        echo "m170405_203128_change_column_in_creditor_address_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
