<?php

use yii\db\Migration;

class m160818_101502_alter_outgoing_docs_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('outgoing_docs','request_type_id','integer NOT NULL DEFAULT 1');
    }

    public function safeDown()
    {
        echo "m160818_101502_alter_outgoing_docs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
