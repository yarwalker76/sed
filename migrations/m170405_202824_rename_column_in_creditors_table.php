<?php

use yii\db\Migration;

class m170405_202824_rename_column_in_creditors_table extends Migration
{
    public function up()
    {
        $this->renameColumn('creditor', 'update_details_description', 'update_details_reason');
    }

    public function down()
    {
        echo "m170405_202824_rename_column_in_creditors_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
