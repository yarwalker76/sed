<?php

use yii\db\Migration;

class m170503_201758_add_internal_id_column_to_tables extends Migration
{
    public function up()
    {
        $this->addColumn('creditor', 'internal_id', $this->integer());
        $this->addColumn('creditor_request', 'internal_id', $this->integer());
        $this->addColumn('creditor_request_amortization', 'internal_id', $this->integer());
        $this->addColumn('creditor_request_guarantee', 'internal_id', $this->integer());
    }

    public function down()
    {
        echo "m170503_201758_add_internal_id_column_to_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
