<?php

use yii\db\Migration;

/**
 * Handles the creation for table `doc_files`.
 */
class m160819_095342_create_doc_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('doc_files', [
            'id' => $this->primaryKey(),
            'doc_type' => "enum('incoming','outgoing','internal','other') NOT NULL DEFAULT 'incoming'",
            'name' => $this->string(150),
            'path' => $this->string(255),
            'doc_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('doc_files');
    }
}
