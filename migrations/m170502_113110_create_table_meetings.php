<?php

use yii\db\Migration;

class m170502_113110_create_table_meetings extends Migration
{
    public function safeUp()
    {
        $this->createTable('meetings', [
            'id' => $this->primaryKey(),
            'proc_id' => $this->integer(),
            'start_date' => $this->date(),
            'start_time' => $this->time(),
            'reg_start_time' => $this->time(),
            'reg_end_time' => $this->time(),
            'meet_address' => $this->string(512),
            'acquaintance_start_date' => $this->date(),
            'acquaintance_end_date' => $this->date(),
            'acquaintance_start_time' => $this->time(),
            'acquaintance_end_time' => $this->time(),
            'acquaintance_address' => $this->string(512),
        ]);

        $this->addForeignKey('fkProcId', 'meetings', 'proc_id', 'procedures', 'id', 'CASCADE', 'CASCADE' );
        $this->createIndex('ixProcId', 'meetings', 'proc_id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fkProcId', 'meetings');
        $this->dropIndex('ixProcId', 'meetings');
        $this->dropTable('meetings');
        return false;
    }


}
