<?php

use yii\db\Migration;

/**
 * Handles the creation for table `creditor_organization`.
 */
class m170326_142051_create_creditor_organization_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('creditor_organization_details', [
            'id' => $this->primaryKey(),
            'creditor_id' => $this->integer()->notNull(),
            'params_id' => $this->integer()->notNull(),
            'value' => $this->string(512),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('creditor_organization_details');
    }
}
