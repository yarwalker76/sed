<?php

use yii\db\Migration;

class m170520_151140_alter_add_creditor_id_column_meeting_participants extends Migration
{
    public function up()
    {
        $this->addColumn('meeting_participants', 'creditor_id', $this->integer() . ' DEFAULT 0' );
        $this->createIndex('ixParticipantsCreditorsId', 'meeting_participants', 'creditor_id');
    }

    public function down()
    {
        $this->dropIndex('ixParticipantsCreditorsId', 'meeting_participants');
        $this->dropColumn('meeting_participants', 'creditor_id');
    }

}
