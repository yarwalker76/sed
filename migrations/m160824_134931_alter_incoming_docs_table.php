<?php

use yii\db\Migration;

class m160824_134931_alter_incoming_docs_table extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk-incoming_docs-response_on_id', 'incoming_docs');
    }

    public function safeDown()
    {
        echo "m160824_134931_alter_incoming_docs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
