<?php

use yii\db\Migration;

/**
 * Handles the creation for table `creditor_request_requirement`.
 */
class m170326_143619_create_creditor_request_requirement_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('creditor_request_requirement', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer()->notNull(),
            'total_initial' => $this->decimal(19,4),
            'create_time' => $this->dateTime(),
            'initiation_time' => $this->datetime(),
            'day_count' => $this->integer(),
            'is_updated' => $this->boolean(),
            'update_reason' => $this->string(512),
            'update_initiator' => $this->string(256),
            'updated_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('creditor_request_requirement');
    }
}
