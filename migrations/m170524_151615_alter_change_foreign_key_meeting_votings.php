<?php

use yii\db\Migration;

class m170524_151615_alter_change_foreign_key_meeting_votings extends Migration
{
    public $table_name = 'meeting_votings';

    public function up()
    {
        $this->dropForeignKey('fkVotingsOptionsId', $this->table_name);
        $this->dropIndex('ixVotingsOptionsId', $this->table_name);

        $this->addForeignKey('fkVotingsOptionsId', $this->table_name, 'voting_option_id',
            'meeting_voting_options', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('ixVotingsOptionsId', $this->table_name, 'question_id');
    }

    public function down()
    {
        $this->dropForeignKey('fkVotingsOptionsId', $this->table_name);
        $this->dropIndex('ixVotingsOptionsId', $this->table_name);

        $this->addForeignKey('fkVotingsOptionsId', $this->table_name, 'voting_option_id',
            'meeting_voting_options', 'id', 'NO ACTION', 'CASCADE');
        $this->createIndex('ixVotingsOptionsId', $this->table_name, 'question_id');
    }


}
