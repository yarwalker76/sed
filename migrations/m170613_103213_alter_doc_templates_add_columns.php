<?php

use yii\db\Migration;

class m170613_103213_alter_doc_templates_add_columns extends Migration
{
    public function up()
    {
        $this->addColumn('doc_templates', 'template_type', $this->string(25));
        $this->addColumn('doc_templates', 'proc_type', $this->string(10));
        $this->addColumn('doc_templates', 'question_type', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('doc_templates', 'template_type');
        $this->dropColumn('doc_templates', 'proc_type');
        $this->dropColumn('doc_templates', 'question_type');
    }

}
