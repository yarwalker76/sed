<?php

use yii\db\Migration;

/**
 * Handles adding regsitry_number to table `creditor`.
 */
class m170402_111538_add_regsitry_number_column_to_creditor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('creditor', 'registry_number', $this->integer()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('creditor', 'registry_number');
    }
}
