<?php

use yii\db\Migration;

class m170628_090124_alter_meeting_participants_add_queue_column extends Migration
{
    public function up()
    {
        //$this->addColumn('meeting_participants', 'queue_type', $this->smallInteger(6));
        $this->addColumn('meeting_participants', 'sum_31', $this->double() . ' default 0');
        $this->addColumn('meeting_participants', 'sum_32', $this->double() . ' default 0');
    }

    public function down()
    {
        //$this->dropColumn('meeting_participants', 'queue_type');
        $this->dropColumn('meeting_participants', 'sum_31');
        $this->dropColumn('meeting_participants', 'sum_32');
    }


}
