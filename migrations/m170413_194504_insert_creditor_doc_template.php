<?php

use yii\db\Migration;

class m170413_194504_insert_creditor_doc_template extends Migration
{
    public function up()
    {
        $this->insert('doc_groups', [
            'id' => 7,
            'name' => 'Реестр Кредиторов',
        ]);
    }

    public function down()
    {
        echo "m170413_194504_insert_creditor_doc_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
