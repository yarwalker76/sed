<?php

use yii\db\Migration;

/**
 * Handles the creation for table `procedures_publications`.
 */
class m160922_111700_create_procedures_publications_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('procedures_publications', [
            'id' => $this->primaryKey(),
            'proc_id' => $this->integer(),
            'np_id' => $this->integer(),
            'np_date' => $this->date(),
            'np_number' => $this->string(),
            'np_page_number' => $this->smallInteger(),
            'np_post_number' => $this->string(),
            'efrsb_date' => $this->date(),
            'efrsb_post_number' => $this->string(),
            'reg_credit_close_date' => $this->date(),
            'property_inventory' => $this->string(),
            'property_assessment' => $this->string(),
            'insurance_company_name' => $this->string(),
            'insurance_register_data' => $this->string(),
            'insurance_background' => $this->text(),
            'insurance_reason' => $this->text(),
            'registry_holder_id' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('procedures_publications');
    }
}
