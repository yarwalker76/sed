<?php

use yii\db\Migration;

class m170510_160016_insert_request_types extends Migration
{
    public function up()
    {
        $this->execute("
INSERT INTO 
  creditor_request_type(title)
VALUES
  ('Основной долг'),
  ('Проценты за пользования чужими денежными средствами'),
  ('Штрафные санкции по договору'),
  ('Неустойка'),
  ('Проценты на сумму основного долга'),
  ('Задолженность по заработной плате'),
  ('Моральный вред'),
  ('Неустойка по договору'),
  ('Пени');
");
    }

    public function down()
    {
        echo "m170510_160016_insert_request_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
