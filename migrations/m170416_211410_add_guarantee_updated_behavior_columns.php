<?php

use yii\db\Migration;

class m170416_211410_add_guarantee_updated_behavior_columns extends Migration
{
    public function up()
    {
        $this->addColumn('creditor_request_guarantee', 'is_updated', $this->boolean());
        $this->addColumn('creditor_request_guarantee', 'update_reason', $this->string(512));
        $this->addColumn('creditor_request_guarantee', 'update_initiator', $this->string(256));
        $this->addColumn('creditor_request_guarantee', 'updated_id', $this->integer());
    }

    public function down()
    {
        echo "m170416_211410_add_guarantee_updated_behavior_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
