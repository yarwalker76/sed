<?php

use yii\db\Migration;

class m170522_105341_alter_add_final_decision_column_meeting_questios extends Migration
{
    public function up()
    {
        $this->addColumn('meeting_questions', 'final_decision', $this->text(2048));
    }

    public function down()
    {
        $this->dropColumn('meeting_questions', 'final_decision');
    }

}
