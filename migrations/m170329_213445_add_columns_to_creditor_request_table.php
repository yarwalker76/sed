<?php

use yii\db\Migration;

class m170329_213445_add_columns_to_creditor_request_table extends Migration
{
    public function up()
    {
        $this->addColumn('creditor_request', 'total_accrued', $this->decimal(19,4));
        $this->addColumn('creditor_request', 'percentage', $this->decimal(19,4));
        $this->addColumn('creditor_request', 'day_count', $this->integer());
    }

    public function down()
    {
        echo "m170329_213445_add_columns_to_creditor_request_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
