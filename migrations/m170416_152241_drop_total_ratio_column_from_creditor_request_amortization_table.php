<?php

use yii\db\Migration;

/**
 * Handles dropping total_ratio from table `creditor_request_amortization`.
 */
class m170416_152241_drop_total_ratio_column_from_creditor_request_amortization_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('creditor_request_amortization', 'total_ratio');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        return false;
    }
}
