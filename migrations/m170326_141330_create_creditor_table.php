<?php

use yii\db\Migration;

/**
 * Handles the creation for table `creditor`.
 */
class m170326_141330_create_creditor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('creditor', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'organization_id' => $this->integer()->notNull(),
            'organization_type_id' => $this->integer()->notNull(),
            'create_time' => $this->dateTime(),
            'is_updated' => $this->boolean(),
            'update_reason' => $this->string(512),
            'update_initiator' => $this->string(256),
            'updated_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('creditor');
    }
}
