<?php

use yii\db\Migration;

/**
 * Handles the creation for table `procedure_types`.
 */
class m160905_064629_create_procedure_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('procedure_types', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
        ]);

        $this->insert('procedure_types', [
            'type' => 'Наблюдение',
        ]);

        $this->insert('procedure_types', [
            'type' => 'Внешнее управление',
        ]);

        $this->insert('procedure_types', [
            'type' => 'Конкурсное производство',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('procedure_types');
    }
}
