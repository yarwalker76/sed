<?php

use yii\db\Migration;

class m161211_131308_alter_table_outgoing_docs extends Migration
{
    public function safeUp()
    {
        $this->addColumn('outgoing_docs','response_on_id','integer');
        $this->createIndex( 'outgoing_docs-response_on_id_Idx', 'outgoing_docs', 'response_on_id', $unique = false );
    }

    public function safeDown()
    {
        echo "m161211_131308_alter_table_outgoing_docs cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
