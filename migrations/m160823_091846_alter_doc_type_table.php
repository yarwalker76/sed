<?php

use yii\db\Migration;

class m160823_091846_alter_doc_type_table extends Migration
{
    public function safeUp()
    {
        $this->insert('doc_types', [
            'id' => 1,
            'type' => 'outgoing',
            'name' => null,
        ]);

        $this->insert('doc_types', [
            'id' => 2,
            'type' => 'outgoing',
            'name' => 'Письмо',
        ]);

        $this->insert('doc_types', [
            'id' => 3,
            'type' => 'outgoing',
            'name' => "Запрос",
        ]);

        $this->insert('doc_types', [
            'id' => 4,
            'type' => 'outgoing',
            'name' => "Уведомление",
        ]);

        $this->insert('doc_types', [
            'id' => 5,
            'type' => 'outgoing',
            'name' => "Отзыв",
        ]);

        $this->insert('doc_types', [
            'id' => 6,
            'type' => 'outgoing',
            'name' => "Ходатайство",
        ]);

        $this->insert('doc_types', [
            'id' => 7,
            'type' => 'outgoing',
            'name' => "Другое",
        ]);


        $this->insert('doc_types', [
            'id' => 8,
            'type' => 'incoming',
            'name' => null,
        ]);

        $this->insert('doc_types', [
            'id' => 9,
            'type' => 'incoming',
            'name' => 'Письмо',
        ]);

        $this->insert('doc_types', [
            'id' => 10,
            'type' => 'incoming',
            'name' => "Запрос",
        ]);

        $this->insert('doc_types', [
            'id' => 11,
            'type' => 'incoming',
            'name' => "Уведомление",
        ]);

        $this->insert('doc_types', [
            'id' => 12,
            'type' => 'incoming',
            'name' => "Отзыв",
        ]);

        $this->insert('doc_types', [
            'id' => 13,
            'type' => 'incoming',
            'name' => "Ходатайство",
        ]);

        $this->insert('doc_types', [
            'id' => 14,
            'type' => 'incoming',
            'name' => "Другое",
        ]);
    }

    public function down()
    {
        echo "m160823_091846_alter_doc_type_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
