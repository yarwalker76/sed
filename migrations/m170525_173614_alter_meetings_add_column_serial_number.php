<?php

use yii\db\Migration;

class m170525_173614_alter_meetings_add_column_serial_number extends Migration
{
    public function up()
    {
        $this->addColumn('meetings', 'serial_number', $this->string(20));
    }

    public function down()
    {
        $this->dropColumn('meetings', 'serial_number');
    }


}
