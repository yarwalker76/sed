<?php

use yii\db\Migration;

class m170424_154027_change_decimal_columns_dimensions extends Migration
{
    public function up()
    {
        $this->alterColumn('creditor_request', 'total', $this->decimal(19, 2));
        $this->alterColumn('creditor_request', 'total_accrued', $this->decimal(19, 2));
        $this->alterColumn('creditor_request', 'percentage', $this->decimal(19, 2));
        $this->alterColumn('creditor_request_amortization', 'total', $this->decimal(19, 2));
        $this->alterColumn('creditor_request_guarantee', 'total', $this->decimal(19, 2));
    }

    public function down()
    {
        echo "m170424_154027_change_decimal_columns_dimensions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
