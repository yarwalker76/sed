<?php

use yii\db\Migration;

class m170506_090357_remove_internal_id_column extends Migration
{
    public function up()
    {
        $this->dropColumn('creditor', 'internal_id');
        $this->dropColumn('creditor_request', 'internal_id');
        $this->dropColumn('creditor_request_amortization', 'internal_id');
        $this->dropColumn('creditor_request_guarantee', 'internal_id');

        $this->renameColumn('creditor', 'updated_internal_id', 'updated_id');
        $this->renameColumn('creditor_request', 'updated_internal_id', 'updated_id');
        $this->renameColumn('creditor_request_amortization', 'updated_internal_id', 'updated_id');
        $this->renameColumn('creditor_request_guarantee', 'updated_internal_id', 'updated_id');
    }

    public function down()
    {
        echo "m170506_090357_remove_internal_id_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
