<?php

namespace app\controllers;

use Yii;
use budyaga\users\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use budyaga\users\models\forms\AssignmentForm;
use yii\filters\AccessControl;

use app\modules\procedures\models\ProcedureRelations;

/**
 * AdminController implements the CRUD actions for User model.
 */
class AdminController extends Controller
{
    private $_model = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'view', 'update', 'delete', 'permissions'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['userManage'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['userCreate'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['userView'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can('userUpdate', ['user' => $this->findModel(Yii::$app->request->get('id'))]);
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['userDelete'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['permissions'],
                        'roles' => ['userPermissions'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())){ 
            $model->setPassword($model->password);
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                throw new NotFoundHttpException(var_dump($model->errors));
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $proc = Yii::$app->request->post('ProcedureForm');
        
        if($proc['action'] == 'assign_proc'){
            
            foreach($proc['unassigned_proc'] as $up)
            {
                $ins = new ProcedureRelations();
                $ins->user_id = $id;
                $ins->proc_id = $up;
                $ins->insert();
            }
        }

        if($proc['action'] == 'revoke_proc'){
            
            foreach($proc['assigned_proc'] as $up)
            {
                \Yii::$app
                    ->db
                    ->createCommand()
                    ->delete('procedure_relations', ['user_id' => $id, 'proc_id' => $up])
                    ->execute();

                \Yii::$app
                    ->db
                    ->createCommand()
                    ->delete('last_seen_procedures', ['user_id' => $id, 'proc_id' => $up])
                    ->execute();
            }
        }
                
        $modelForm = new AssignmentForm;
        $modelForm->model = $this->findModel($id);
        
        $model = $this->findModel($id);
        
        $procedures = array();

        $procedures = ProcedureRelations::getProcRelations($id);

        $procedures_user = array();
        $procedures_all = array();
        $procedures_ids = array();

        //exit('<pre>'.print_r($procedures,true).'</pre>');

        foreach( $procedures as $p ):
            if( $id === $p['user_id'] ):
                $procedures_user[] = [ 'id' => $p['procedure_id'], 'name' => $p['name'] ];
                $procedures_ids[] = $p['procedure_id'];
            endif;
        endforeach;

        $arr_ids = [];
        foreach( $procedures as $p ):
            if( !in_array( $p['procedure_id'], $arr_ids ) && !in_array( $p['procedure_id'], $procedures_ids ) ):
                $procedures_all[] = ['id' => $p['procedure_id'], 'name' => $p['name']];
                $arr_ids[] = $p['procedure_id'];
            endif;
        endforeach;

        if ($modelForm->load(Yii::$app->request->post()) && ($modelForm->save())) {
            Yii::$app->session->setFlash('success', Yii::t('users', 'SUCCESS_UPDATE_PERMISSIONS'));
           // return $this->redirect(['view', 'id' => $id]);
        }

        if ($model->load(Yii::$app->request->post())){
            if($model->password !== ""){
                $model->setPassword($model->password);
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->render('update', [
                    'model' => $model,
                    'modelForm' => $modelForm,
                    'procedures_all' => $procedures_all,
                    'procedures_user'   => $procedures_user
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelForm' => $modelForm,
                'procedures_all' => $procedures_all,
                'procedures_user'   => $procedures_user
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionPermissions($id)
    {
        $modelForm = new AssignmentForm;
        $modelForm->model = $this->findModel($id);

        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->save()) {
            Yii::$app->session->setFlash('success', Yii::t('users', 'SUCCESS_UPDATE_PERMISSIONS'));
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('permissions', [
            'modelForm' => $modelForm
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ($this->_model === false) {
            $this->_model = User::findOne($id);
        }

        if ($this->_model !== null) {
            return $this->_model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
