<?php

namespace app\controllers;

use Yii;
use app\models\BackOffice;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * LegalServiceController implements the CRUD actions for BackOffice model.
 */
class BackOfficeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['index', 'error'],
                        'allow' => true,
                        'roles' => ['manager', 'backOffice', 'lawyer'],
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['lawyer'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BackOffice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $totalCount = Yii::$app->db->createCommand('SELECT COUNT(*) FROM ' . BackOffice::tableName() . ' WHERE proc_id = :proc_id', [':proc_id' => Yii::$app->session->get('current_proc_id')])->queryScalar();

        $dataProvider = new ActiveDataProvider([
            'query' => BackOffice::find()->where(['proc_id' => Yii::$app->session->get('current_proc_id')]),
            'totalCount' => (int)$totalCount,
            'pagination' => [
                // количество пунктов на странице
                'pageSize' => 20,
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BackOffice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BackOffice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if( !Yii::$app->user->can('manager') && !Yii::$app->user->can('backOffice') ):
            return $this->redirect(['index']);
        endif;
        
        $model = new BackOffice();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']); //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BackOffice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if( !Yii::$app->user->can('manager') && !Yii::$app->user->can('backOffice') ):
            return $this->redirect(['index']);
        endif;
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']); //return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BackOffice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if( !Yii::$app->user->can('manager') && !Yii::$app->user->can('backOffice') ):
            return $this->redirect(['index']);
        endif;
        
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BackOffice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LegalService the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BackOffice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
