<?php

namespace app\controllers;

use app\modules\procedures\models\Procedures;
use app\modules\procedures\models\ProceduresAdditionalProperties;
use Yii;
use yii\web\HttpException;
use app\models\TblAccounts;
use app\models\TblAccountsSearch;
use app\models\TblAddresses;
use app\models\TblOrganizations;
use app\models\TblOrganizationsSearch;
use app\models\TblOrganizationsMethods;
use app\models\TblOrganizationsTypes;
use app\models\TblParamsDynamic;
use app\models\TblRegions;
use app\models\TblValues;
use yii\filters\AccessControl;

class ReferencesController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //'actions' => ['index', 'error'],
                        'allow' => true,
                        'roles' => ['administrator', 'bookkeeping', 'officeManager', 'manager', 'backOffice', 'lawyer'],
                    ],
                ],
            ]
        ];
    }
    
    public function actionIndex()
    {
        $find_name = \Yii::$app->request->get('fname');
        $find_inn = \Yii::$app->request->get('finn');
        
        $searchModel = new TblOrganizationsSearch();
        $dataProvider = $searchModel->search(null, null, $find_name, $find_inn);
        
        $model = new TblOrganizationsTypes();
        $organizationsTypes = $model->find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'organizationsTypes' => $organizationsTypes,
            'search_word_name'   => $find_name,
            'search_word_inn'   => $find_inn,
        ]);
    }
    
    public function actionListing()
    {
        $searchModel = new TblOrganizationsSearch();
        $dataProvider = $searchModel->search( \Yii::$app->request->post('id') );
        return $this->renderPartial('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionFindbytitle()
    {
        return $this->renderPartial('json', [
            'data_json' => json_encode(TblOrganizationsSearch::findByTitle()),
        ]);
    }
    
    public function actionFindbyinn()
    {
        return $this->renderPartial('json', [
            'data_json' => json_encode(TblOrganizationsSearch::findByINN()),
        ]);
    }
    
    public function actionListingbyot()
    {
        $searchModel = new TblOrganizationsSearch();
        $dataProvider = $searchModel->search( null, (int)\Yii::$app->request->get('ot') );
        return $this->renderPartial('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreate()
    {
        $model = new TblOrganizationsTypes();
        $data = $model->find()->all();
        return $this->render('create', [
            'data' => $data, 
            'model' => $model
        ]);
    }
    
    public function actionForms()
    {
        $model = new TblOrganizationsTypes();
        $model->attributes = \Yii::$app->request->post();
        if ($model->validate()){
            return $this->renderPartial('form', [
                'fields' => TblParamsDynamic::getParamsByOrganizationsTypesId($model->attributes['id']),
                'regions' => TblRegions::find()->all(),
            ]); 
        } else
            throw new HttpException(404, 'Type not found');
    }
    
    public function actionAccountform()
    {
        return $this->renderPartial('account', [
            'i' => (int)\Yii::$app->request->post('i'),
        ]); 
    }
    
    public function actionAccountdelete()
    {
        $id = \Yii::$app->request->post('id');
        if (($model = TblAccounts::findOne($id)) !== null) {
            $model->delete();
            return json_encode( array( 'status' => 1, 'message' => Yii::t('references', 'Account was deleted.') ) ); 
        } else
            return json_encode( array( 'status' => 0, 'message' => Yii::t('references', 'Not found!') ) );
    }
    
    public function actionAccountadd()
    {
        $id = \Yii::$app->request->post('id');
        $organization = $this->findModel($id);
        $model = new TblAccounts();
        $model->organizations_id = $organization->id;
        $model->fake = 1;
        $model->insert();
        return $this->renderPartial('account', [
            'i' => $model->id,
            'data' => array(),
        ]);
    }
    
    public function actionSave()
    {
        $formdata = \Yii::$app->request->post(); //print_r($formdata);
        $array = [ 'status' => 0, 'errors' => []]; 
        if (isset($formdata['id']))
            if ($formdata['id'] != '')
                $array = TblOrganizationsMethods::updAll($formdata);
            else
                $array = TblOrganizationsMethods::saveAll($formdata);
        return $this->renderPartial('popup', $array );
    }
    
    public function actionView()
    {
        $id = \Yii::$app->request->get('id');
        $tab = \Yii::$app->request->get('tab');
        $accountSearchModel = new TblAccountsSearch();
        $accountDataProvider = $accountSearchModel->search($id);
        return $this->render('view', [
            'organization' => TblOrganizationsMethods::getOrganization($id),
            'params' => TblOrganizationsMethods::getParams($id),
            'accountSearchModel' => $accountSearchModel,
            'accountDataProvider' => $accountDataProvider,
        ]);
    }
    
    public function actionUpdate($id)
    {
        $id = (int)\Yii::$app->request->get('id');
        $organization = TblOrganizationsMethods::getOrganization($id);
        $accountSearchModel = new TblAccountsSearch();
        $accountDataProvider = $accountSearchModel->search($id);
        $params = TblOrganizationsMethods::getParams($id);
        $params_arr = array();
        foreach ($params as $key => $value)
            $params_arr[$value['params_id']] = $value['value'];
        if ($organization)
            return $this->render('form', [
                'id' => $id,
                'fields' => TblParamsDynamic::getParamsByOrganizationsTypesId($organization['organizations_types_id']),
                'params' => $params_arr,
                'organization' => $organization,
                'regions' => TblRegions::find()->all(),
                'accountSearchModel' => $accountSearchModel,
                'accountDataProvider' => array_values($accountDataProvider->getModels()),
            ]);
        else
            return $this->redirect(['index']);
    }

    public function actionDelete($id)
    {
        // если удаляем организацию
        $count_pd = Procedures::find()->where(['organization_id' => $id])->count();
        if( $count_pd ):
            \Yii::$app->getSession()->setFlash('error', Yii::t('references', 'Delete error, there is related procedure'));
        endif;

        // если удаляем АУ
        $count_au = Procedures::find()->where(['manager_id' => $id])->count();
        if( $count_au ):
            \Yii::$app->getSession()->setFlash('error', Yii::t('references', 'Delete error, there is related procedure'));
        endif;

        // если удаляем Суд
        $count_court = Procedures::find()->where(['court_id' => $id])->count();
        if( $count_court ):
            \Yii::$app->getSession()->setFlash('error', Yii::t('references', 'Delete error, there is related procedure'));
        endif;

        // если удаляем Судью
        $count_judge = Procedures::find()->where(['judge_id' => $id])->count();
        if( $count_judge ):
            \Yii::$app->getSession()->setFlash('error', Yii::t('references', 'Delete error, there is related procedure'));
        endif;

        // если удаляем Издание
        $count_np = ProceduresAdditionalProperties::find()->where(['np_id' => $id])->count();
        if( $count_np ):
            \Yii::$app->getSession()->setFlash('error', Yii::t('references', 'Delete error, there is related procedure'));
        endif;

        // если удаляем Банк
        $count_bank = TblAccounts::find()->where(['organizations_id' => $id])->orWhere(['banks_id' => $id])->count();
        if( $count_bank ):
            \Yii::$app->getSession()->setFlash('error', Yii::t('references', 'Delete error, there is related procedure'));
        endif;

        if( $count_pd == 0 && $count_au == 0 && $count_court == 0 && $count_judge == 0 && $count_np == 0 && $count_bank == 0 ):
            $organization = $this->findModel($id);
            TblAddresses::findOne( $organization->addresses_id_1 )->delete();
            TblAddresses::findOne( $organization->addresses_id_2 )->delete();
            TblAddresses::findOne( $organization->addresses_id_3 )->delete();
            TblValues::deleteAll( [ 'organizations_id' => $organization->id ] );
            TblAccounts::deleteAll( [ 'organizations_id' => $organization->id ] );
            $organization->delete();
        endif;

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = TblOrganizations::findOne($id)) !== null)
            return $model;
        else
            throw new NotFoundHttpException('The requested page does not exist.');
    }
    
}
